# ca.meet.coop

## Disks

The `ca.meet.coop` server has:

```
mount   | /boot | /     & swap  |               |               |               |
---------------------------------------------------------------------------------
lv      | -     | slash & swap  | NONE          |               | NONE          |
vg      | -     | root          | data          | NONE          | data2         |
luks dev| -     | crypt_dev_md1 | crypt_dev_md2 | crypt_dev_md3 | crypt_dev_md4 |
Raid dev| md0   | md1           | md2           | md3           | md4           |
Size    | 1G    | 35G           | 918G          | 2G            | 3,7T          |
        -------------------------------------------------------------------------
RAID1   | sdc1  | sdc2          | sdc3          | sda1          | sda2          |
        | sdd1  | sdd2          | sdd3          | sdb1          | sdb2          |
Drives  | <------- 2TB -------> | <--- 2TB ---> | <--- 4TB ---> | <--- 4TB ---> |
```

## lvs

```
lvs
  LV    VG   Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  slash root -wi-ao---- 25.00g                                                    
  swap  root -wi-ao----  8.00g 
```

## vgs

```bash
vgs
  VG    #PV #LV #SN Attr   VSize   VFree  
  data    1   0   0 wz--n- 917.74g 917.74g
  data2   1   0   0 wz--n-   3.64t   3.64t
  root    1   2   0 wz--n-  34.96g   1.96g
```

## /dev/sda

Consumer grade 4TB SSD

```sh
fdisk -l /dev/sda
Disk /dev/sda: 3.7 TiB, 4000787030016 bytes, 7814037168 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 277EE38F-2FD7-4D07-860F-D0E17806E4BB

Device       Start        End    Sectors  Size Type
/dev/sda1     4096    4198399    4194304    2G Linux RAID
/dev/sda2  4198400 7814035086 7809836687  3.7T Linux RAID
/dev/sda3     2048       4095       2048    1M BIOS boot

Partition table entries are not in disk order.
```

## /dev/sdb

Consumer grade 4TB SSD

```sh
fdisk -l /dev/sdb
Disk /dev/sdb: 3.7 TiB, 4000787030016 bytes, 7814037168 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 1FF924CD-9C02-4E7A-A9CC-60D17DB4DABB

Device       Start        End    Sectors  Size Type
/dev/sdb1     4096    4198399    4194304    2G Linux RAID
/dev/sdb2  4198400 7814035086 7809836687  3.7T Linux RAID
/dev/sdb3     2048       4095       2048    1M BIOS boot

Partition table entries are not in disk order.
```

# /dev/sdc

Samsung Pro 860 1TB

```sh
fdisk -l /dev/sdc
Disk /dev/sdc: 953.9 GiB, 1024209543168 bytes, 2000409264 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 1093EF1C-9763-48CB-A8CD-0DDEBBA9DC8D

Device        Start        End    Sectors   Size Type
/dev/sdc1      4096    2101247    2097152     1G Linux RAID
/dev/sdc2   2101248   75501567   73400320    35G Linux RAID
/dev/sdc3  75501568 2000407182 1924905615 917.9G Linux RAID
/dev/sdc4      2048       4095       2048     1M BIOS boot

Partition table entries are not in disk order.
```

## /dev/sdd

```sh
fdisk -l /dev/sdd
Disk /dev/sdd: 953.9 GiB, 1024209543168 bytes, 2000409264 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: D7616E12-7DF2-4548-A4B8-A64359850DAF

Device        Start        End    Sectors   Size Type
/dev/sdd1      4096    2101247    2097152     1G Linux RAID
/dev/sdd2   2101248   75501567   73400320    35G Linux RAID
/dev/sdd3  75501568 2000407182 1924905615 917.9G Linux RAID
/dev/sdd4      2048       4095       2048     1M BIOS boot

Partition table entries are not in disk order.
```

## Ansible debug

```yml
---
# Ansible managed

# The following dictionary is generated from getent passwd and the template for
# this file https://git.coop/webarch/debug/-/blob/master/templates/debug.yml.j2
users:
  etherpad:
    password: 995
    uid: 994 
    gid: etherpad user-daemon 
    home: /usr/share/etherpad-lite 
    shell: /bin/false 
  games:
    password: 5
    uid: 60 
    gid: games 
    home: /usr/games 
    shell: /usr/sbin/nologin 
  meteor:
    password: 994
    uid: 993 
    gid: meteor user-daemon 
    home: /usr/share/meteor 
    shell: /bin/bash 
  colord:
    password: 111
    uid: 117 
    gid: colord colour management daemon,,, 
    home: /var/lib/colord 
    shell: /bin/false 
  backup:
    password: 34
    uid: 34 
    gid: backup 
    home: /var/backups 
    shell: /usr/sbin/nologin 
  list:
    password: 38
    uid: 38 
    gid: Mailing List Manager 
    home: /var/list 
    shell: /usr/sbin/nologin 
  Debian-exim:
    password: 108
    uid: 112 
    gid:  
    home: /var/spool/exim4 
    shell: /bin/false 
  koumbit:
    password: 1000
    uid: 1000 
    gid: Koumbit sysadmin!,,, 
    home: /home/koumbit 
    shell: /bin/bash 
  _apt:
    password: 105
    uid: 65534 
    gid:  
    home: /nonexistent 
    shell: /bin/false 
  man:
    password: 6
    uid: 12 
    gid: man 
    home: /var/cache/man 
    shell: /usr/sbin/nologin 
  sync:
    password: 4
    uid: 65534 
    gid: sync 
    home: /bin 
    shell: /bin/sync 
  news:
    password: 9
    uid: 9 
    gid: news 
    home: /var/spool/news 
    shell: /usr/sbin/nologin 
  gnats:
    password: 41
    uid: 41 
    gid: Gnats Bug-Reporting System (admin) 
    home: /var/lib/gnats 
    shell: /usr/sbin/nologin 
  freeswitch:
    password: 997
    uid: 996 
    gid: freeswitch 
    home: /opt/freeswitch 
    shell: /bin/bash 
  chris:
    password: 1001
    uid: 1001 
    gid:  
    home: /home/chris 
    shell: /bin/bash 
  red5:
    password: 999
    uid: 998 
    gid: red5 user-daemon 
    home: /usr/share/red5 
    shell: /bin/false 
  systemd-resolve:
    password: 102
    uid: 104 
    gid: systemd Resolver,,, 
    home: /run/systemd/resolve 
    shell: /bin/false 
  messagebus:
    password: 106
    uid: 110 
    gid:  
    home: /var/run/dbus 
    shell: /bin/false 
  systemd-timesync:
    password: 100
    uid: 102 
    gid: systemd Time Synchronization,,, 
    home: /run/systemd 
    shell: /bin/false 
  daemon:
    password: 1
    uid: 1 
    gid: daemon 
    home: /usr/sbin 
    shell: /usr/sbin/nologin 
  redis:
    password: 112
    uid: 118 
    gid:  
    home: /var/lib/redis 
    shell: /bin/false 
  root:
    password: 0
    uid: 0 
    gid: root 
    home: /root 
    shell: /bin/bash 
  uucp:
    password: 10
    uid: 10 
    gid: uucp 
    home: /var/spool/uucp 
    shell: /usr/sbin/nologin 
  munin:
    password: 110
    uid: 115 
    gid: munin application user,,, 
    home: /var/lib/munin 
    shell: /bin/false 
  bin:
    password: 2
    uid: 2 
    gid: bin 
    home: /bin 
    shell: /usr/sbin/nologin 
  mongodb:
    password: 109
    uid: 65534 
    gid:  
    home: /home/mongodb 
    shell: /bin/false 
  decentral1se:
    password: 1002
    uid: 1002 
    gid:  
    home: /home/decentral1se 
    shell: /bin/bash 
  sshd:
    password: 107
    uid: 65534 
    gid:  
    home: /var/run/sshd 
    shell: /usr/sbin/nologin 
  www-data:
    password: 33
    uid: 33 
    gid: www-data 
    home: /var/www 
    shell: /usr/sbin/nologin 
  lp:
    password: 7
    uid: 7 
    gid: lp 
    home: /var/spool/lpd 
    shell: /usr/sbin/nologin 
  syslog:
    password: 104
    uid: 108 
    gid:  
    home: /home/syslog 
    shell: /bin/false 
  proxy:
    password: 13
    uid: 13 
    gid: proxy 
    home: /bin 
    shell: /usr/sbin/nologin 
  irc:
    password: 39
    uid: 39 
    gid: ircd 
    home: /var/run/ircd 
    shell: /usr/sbin/nologin 
  bigbluebutton:
    password: 998
    uid: 997 
    gid: bigbluebutton 
    home: /home/bigbluebutton 
    shell: /bin/false 
  systemd-network:
    password: 101
    uid: 103 
    gid: systemd Network Management,,, 
    home: /run/systemd/netif 
    shell: /bin/false 
  mail:
    password: 8
    uid: 8 
    gid: mail 
    home: /var/mail 
    shell: /usr/sbin/nologin 
  kurento:
    password: 996
    uid: 995 
    gid:  
    home: /var/lib/kurento 
    shell:  
  nobody:
    password: 65534
    uid: 65534 
    gid: nobody 
    home: /nonexistent 
    shell: /usr/sbin/nologin 
  systemd-bus-proxy:
    password: 103
    uid: 105 
    gid: systemd Bus Proxy,,, 
    home: /run/systemd 
    shell: /bin/false 
  sys:
    password: 3
    uid: 3 
    gid: sys 
    home: /dev 
    shell: /usr/sbin/nologin 

# The following dictionary is generated from getent group and the template for
# this file https://git.coop/webarch/debug/-/blob/master/templates/debug.yml.j2
groups:
  adm:
    password: x
    gid: 4
    group_list:
      - syslog
  syslog:
    password: x
    gid: 108
  etherpad:
    password: x
    gid: 994
  systemd-network:
    password: x
    gid: 103
  decentral1se:
    password: x
    gid: 1002
    group_list:
      - decentral1se
  chris:
    password: x
    gid: 1001
    group_list:
      - chris
  koumbit:
    password: x
    gid: 1000
  netdev:
    password: x
    gid: 109
  mail:
    password: x
    gid: 8
  voice:
    password: x
    gid: 22
  ssh:
    password: x
    gid: 111
  systemd-resolve:
    password: x
    gid: 104
  redis:
    password: x
    gid: 118
  gnats:
    password: x
    gid: 41
  fax:
    password: x
    gid: 21
  backup:
    password: x
    gid: 34
  disk:
    password: x
    gid: 6
  plugdev:
    password: x
    gid: 46
  sudo:
    password: x
    gid: 27
    group_list:
      - koumbit
      - chris
      - decentral1se
  bin:
    password: x
    gid: 2
  messagebus:
    password: x
    gid: 110
  tape:
    password: x
    gid: 26
  cdrom:
    password: x
    gid: 24
  systemd-timesync:
    password: x
    gid: 102
  lp:
    password: x
    gid: 7
  red5:
    password: x
    gid: 998
  ssl-cert:
    password: x
    gid: 114
  systemd-bus-proxy:
    password: x
    gid: 105
  freeswitch:
    password: x
    gid: 996
    group_list:
      - bigbluebutton
  staff:
    password: x
    gid: 50
  games:
    password: x
    gid: 60
  bigbluebutton:
    password: x
    gid: 997
  kurento:
    password: x
    gid: 995
    group_list:
      - bigbluebutton
  nogroup:
    password: x
    gid: 65534
  www-data:
    password: x
    gid: 33
  shadow:
    password: x
    gid: 42
  systemd-journal:
    password: x
    gid: 101
  tty:
    password: x
    gid: 5
  docker:
    password: x
    gid: 999
  src:
    password: x
    gid: 40
  dialout:
    password: x
    gid: 20
  irc:
    password: x
    gid: 39
  video:
    password: x
    gid: 44
  man:
    password: x
    gid: 12
  users:
    password: x
    gid: 100
  sasl:
    password: x
    gid: 45
  list:
    password: x
    gid: 38
  Debian-exim:
    password: x
    gid: 112
  root:
    password: x
    gid: 0
  audio:
    password: x
    gid: 29
  munin:
    password: x
    gid: 115
  input:
    password: x
    gid: 106
  colord:
    password: x
    gid: 117
  kmem:
    password: x
    gid: 15
  sys:
    password: x
    gid: 3
  uucp:
    password: x
    gid: 10
  mongodb:
    password: x
    gid: 113
    group_list:
      - mongodb
  floppy:
    password: x
    gid: 25
  crontab:
    password: x
    gid: 107
  dip:
    password: x
    gid: 30
  proxy:
    password: x
    gid: 13
  meteor:
    password: x
    gid: 993
  daemon:
    password: x
    gid: 1
  news:
    password: x
    gid: 9
  operator:
    password: x
    gid: 37
  utmp:
    password: x
    gid: 43
  scanner:
    password: x
    gid: 116

# getent hosts
127.0.0.1:
- localhost
- ip6-localhost
- ip6-loopback
199.58.80.6:
- faiserver0.koumbit.net


# getent services
acr-nema:
- 104/udp
- dicom
afbackup:
- 2988/udp
afmbackup:
- 2989/udp
afpovertcp:
- 548/udp
afs3-bos:
- 7007/udp
afs3-callback:
- 7001/udp
afs3-errors:
- 7006/udp
afs3-fileserver:
- 7000/udp
- bbs
afs3-kaserver:
- 7004/udp
afs3-prserver:
- 7002/udp
afs3-rmtsys:
- 7009/udp
afs3-update:
- 7008/udp
afs3-vlserver:
- 7003/udp
afs3-volser:
- 7005/udp
amanda:
- 10080/udp
amandaidx:
- 10082/tcp
amidxtape:
- 10083/tcp
amqp:
- 5672/sctp
amqps:
- 5671/tcp
aol:
- 5190/udp
asf-rmcp:
- 623/udp
asp:
- 27374/udp
at-echo:
- 204/udp
at-nbp:
- 202/udp
at-rtmp:
- 201/udp
at-zis:
- 206/udp
auth:
- 113/tcp
- authentication
- tap
- ident
bacula-dir:
- 9101/udp
bacula-fd:
- 9102/udp
bacula-sd:
- 9103/udp
bgp:
- 179/udp
bgpd:
- 2605/tcp
bgpsim:
- 5675/tcp
biff:
- 512/udp
- comsat
binkp:
- 24554/tcp
bootpc:
- 68/udp
bootps:
- 67/udp
bpcd:
- 13782/udp
bpdbm:
- 13721/udp
bpjava-msvc:
- 13722/udp
bprd:
- 13720/udp
canna:
- 5680/tcp
cfengine:
- 5308/udp
cfinger:
- 2003/tcp
chargen:
- 19/udp
- ttytst
- source
cisco-sccp:
- 2000/udp
clc-build-daemon:
- 8990/tcp
clearcase:
- 371/udp
- Clearcase
cmip-agent:
- 164/udp
cmip-man:
- 163/udp
codaauth2:
- 370/udp
codasrv:
- 2432/udp
codasrv-se:
- 2433/udp
conference:
- 531/tcp
- chat
courier:
- 530/tcp
- rpc
csnet-ns:
- 105/udp
- cso-ns
csync2:
- 30865/tcp
customs:
- 1001/udp
cvspserver:
- 2401/udp
daap:
- 3689/udp
datametrics:
- 1645/udp
- old-radius
daytime:
- 13/udp
db-lsp:
- 17500/tcp
dcap:
- 22125/tcp
dhcpv6-client:
- 546/udp
dhcpv6-server:
- 547/udp
dicom:
- 11112/tcp
dict:
- 2628/udp
dircproxy:
- 57000/tcp
discard:
- 9/udp
- sink
- 'null'
distcc:
- 3632/udp
distmp3:
- 4600/tcp
domain:
- 53/udp
echo:
- 4/ddp
eklogin:
- 2105/tcp
enbd-cstatd:
- 5051/tcp
enbd-sstatd:
- 5052/tcp
epmd:
- 4369/udp
exec:
- 512/tcp
f5-globalsite:
- 2792/udp
f5-iquery:
- 4353/udp
fatserv:
- 347/udp
fax:
- 4557/tcp
fido:
- 60179/tcp
finger:
- 79/tcp
font-service:
- 7100/udp
- xfs
freeciv:
- 5556/udp
frox:
- 2121/tcp
fsp:
- 21/udp
- fspd
ftp:
- 21/tcp
ftp-data:
- 20/tcp
ftps:
- 990/tcp
ftps-data:
- 989/tcp
gdomap:
- 538/udp
gds-db:
- 3050/udp
- gds_db
ggz:
- 5688/udp
git:
- 9418/tcp
gnunet:
- 2086/udp
gnutella-rtr:
- 6347/udp
gnutella-svc:
- 6346/udp
gopher:
- 70/udp
gpsd:
- 2947/udp
gris:
- 2135/udp
groupwise:
- 1677/udp
gsidcap:
- 22128/tcp
gsiftp:
- 2811/udp
gsigatekeeper:
- 2119/udp
hkp:
- 11371/udp
hmmp-ind:
- 612/udp
- dqs313_intercell
hostmon:
- 5355/udp
hostnames:
- 101/tcp
- hostname
http:
- 80/udp
http-alt:
- 8080/udp
https:
- 443/udp
hylafax:
- 4559/tcp
iax:
- 4569/udp
icpv2:
- 3130/udp
- icp
idfp:
- 549/udp
imap2:
- 143/udp
- imap
imap3:
- 220/udp
imaps:
- 993/udp
imsp:
- 406/udp
ingreslock:
- 1524/udp
ipp:
- 631/udp
iprop:
- 2121/tcp
ipsec-nat-t:
- 4500/udp
ipx:
- 213/udp
irc:
- 194/udp
ircd:
- 6667/tcp
ircs:
- 994/udp
isakmp:
- 500/udp
iscsi-target:
- 3260/tcp
isdnlog:
- 20011/udp
isisd:
- 2608/tcp
iso-tsap:
- 102/tcp
- tsap
kamanda:
- 10081/udp
kazaa:
- 1214/udp
kerberos:
- 88/udp
- kerberos5
- krb5
- kerberos-sec
kerberos-adm:
- 749/tcp
kerberos-master:
- 751/tcp
kerberos4:
- 750/tcp
- kerberos-iv
- kdc
kermit:
- 1649/udp
klogin:
- 543/tcp
knetd:
- 2053/tcp
kpasswd:
- 464/udp
kpop:
- 1109/tcp
krb-prop:
- 754/tcp
- krb_prop
- krb5_prop
- hprop
krbupdate:
- 760/tcp
- kreg
kshell:
- 544/tcp
- krcmd
kx:
- 2111/tcp
l2f:
- 1701/udp
- l2tp
ldap:
- 389/udp
ldaps:
- 636/udp
link:
- 87/tcp
- ttylink
linuxconf:
- 98/tcp
loc-srv:
- 135/udp
- epmap
log-server:
- 1958/tcp
login:
- 513/tcp
lotusnote:
- 1352/udp
- lotusnotes
mailq:
- 174/udp
mandelspawn:
- 9359/udp
- mandelbrot
mdns:
- 5353/udp
microsoft-ds:
- 445/udp
mmcc:
- 5050/udp
moira-db:
- 775/tcp
- moira_db
moira-update:
- 777/tcp
- moira_update
moira-ureg:
- 779/udp
- moira_ureg
mon:
- 2583/udp
mrtd:
- 5674/tcp
ms-sql-m:
- 1434/udp
ms-sql-s:
- 1433/udp
msnp:
- 1863/udp
msp:
- 18/udp
mtn:
- 4691/udp
mtp:
- 57/tcp
munin:
- 4949/tcp
- lrrd
mysql:
- 3306/udp
mysql-proxy:
- 6446/udp
nameserver:
- 42/tcp
- name
nbd:
- 10809/tcp
nbp:
- 2/ddp
nessus:
- 1241/udp
netbios-dgm:
- 138/udp
netbios-ns:
- 137/udp
netbios-ssn:
- 139/udp
netnews:
- 532/tcp
- readnews
netstat:
- 15/tcp
netwall:
- 533/udp
nextstep:
- 178/udp
- NeXTStep
- NextStep
nfs:
- 2049/udp
ninstall:
- 2150/udp
nntp:
- 119/tcp
- readnews
- untp
nntps:
- 563/udp
- snntp
noclog:
- 5354/udp
npmp-gui:
- 611/udp
- dqs313_execd
npmp-local:
- 610/udp
- dqs313_qmaster
nqs:
- 607/udp
nrpe:
- 5666/tcp
nsca:
- 5667/tcp
ntalk:
- 518/udp
ntp:
- 123/udp
nut:
- 3493/udp
omirr:
- 808/udp
- omirrd
omniorb:
- 8088/udp
openvpn:
- 1194/udp
ospf6d:
- 2606/tcp
ospfapi:
- 2607/tcp
ospfd:
- 2604/tcp
passwd-server:
- 752/udp
- passwd_server
pawserv:
- 345/udp
pcrd:
- 5151/tcp
pipe-server:
- 2010/tcp
- pipe_server
pop2:
- 109/udp
- pop-2
pop3:
- 110/udp
- pop-3
pop3s:
- 995/udp
poppassd:
- 106/udp
postgresql:
- 5432/udp
- postgres
predict:
- 1210/udp
printer:
- 515/tcp
- spooler
proofd:
- 1093/udp
prospero:
- 191/udp
prospero-np:
- 1525/udp
pwdgen:
- 129/udp
qmqp:
- 628/udp
qmtp:
- 209/udp
qotd:
- 17/tcp
- quote
radius:
- 1812/udp
radius-acct:
- 1813/udp
- radacct
radmin-port:
- 4899/udp
re-mail-ck:
- 50/udp
remctl:
- 4373/udp
remotefs:
- 556/tcp
- rfs_server
- rfs
remoteping:
- 1959/tcp
rfe:
- 5002/tcp
ripd:
- 2602/tcp
ripngd:
- 2603/tcp
rje:
- 77/tcp
- netrjs
rlp:
- 39/udp
- resource
rmiregistry:
- 1099/udp
rmtcfg:
- 1236/tcp
rootd:
- 1094/udp
route:
- 520/udp
- router
- routed
rpc2portmap:
- 369/udp
rplay:
- 5555/udp
rsync:
- 873/udp
rtcm-sc104:
- 2101/udp
rtelnet:
- 107/udp
rtmp:
- 1/ddp
rtsp:
- 554/udp
sa-msg-port:
- 1646/udp
- old-radacct
saft:
- 487/udp
sane-port:
- 6566/tcp
- sane
- saned
search:
- 2010/tcp
- ndtp
sftp:
- 115/tcp
sge-execd:
- 6445/udp
- sge_execd
sge-qmaster:
- 6444/udp
- sge_qmaster
sgi-cad:
- 17004/tcp
sgi-cmsd:
- 17001/udp
sgi-crsd:
- 17002/udp
sgi-gcd:
- 17003/udp
shell:
- 514/tcp
- cmd
sieve:
- 4190/tcp
silc:
- 706/udp
sip:
- 5060/udp
sip-tls:
- 5061/udp
skkserv:
- 1178/tcp
smsqp:
- 11201/udp
smtp:
- 25/tcp
- mail
smux:
- 199/udp
snmp:
- 161/udp
snmp-trap:
- 162/udp
- snmptrap
snpp:
- 444/udp
socks:
- 1080/udp
spamd:
- 783/tcp
ssh:
- 22/udp
submission:
- 587/udp
sunrpc:
- 111/udp
- portmapper
supdup:
- 95/tcp
supfiledbg:
- 1127/tcp
supfilesrv:
- 871/tcp
support:
- 1529/tcp
suucp:
- 4031/udp
svn:
- 3690/udp
- subversion
svrloc:
- 427/udp
swat:
- 901/tcp
syslog:
- 514/udp
syslog-tls:
- 6514/tcp
sysrqd:
- 4094/udp
systat:
- 11/tcp
- users
tacacs:
- 49/udp
tacacs-ds:
- 65/udp
talk:
- 517/udp
tcpmux:
- 1/tcp
telnet:
- 23/tcp
telnets:
- 992/udp
tempo:
- 526/tcp
- newdate
tfido:
- 60177/tcp
tftp:
- 69/udp
time:
- 37/udp
- timserver
timed:
- 525/udp
- timeserver
tinc:
- 655/udp
tproxy:
- 8081/tcp
ulistserv:
- 372/udp
unix-status:
- 1957/tcp
urd:
- 465/tcp
- ssmtp
- smtps
uucp:
- 540/tcp
- uucpd
uucp-path:
- 117/tcp
vboxd:
- 20012/udp
venus:
- 2430/udp
venus-se:
- 2431/udp
vnetd:
- 13724/udp
vopied:
- 13783/udp
webmin:
- 10000/tcp
webster:
- 765/udp
who:
- 513/udp
- whod
whois:
- 43/tcp
- nicname
wipld:
- 1300/tcp
wnn6:
- 22273/udp
x11:
- 6000/udp
- x11-0
x11-1:
- 6001/udp
x11-2:
- 6002/udp
x11-3:
- 6003/udp
x11-4:
- 6004/udp
x11-5:
- 6005/udp
x11-6:
- 6006/udp
x11-7:
- 6007/udp
xdmcp:
- 177/udp
xinetd:
- 9098/tcp
xmms2:
- 9667/udp
xmpp-client:
- 5222/udp
- jabber-client
xmpp-server:
- 5269/udp
- jabber-server
xpilot:
- 15345/udp
xtel:
- 1313/tcp
xtell:
- 4224/tcp
xtelw:
- 1314/tcp
z3950:
- 210/udp
- wais
zabbix-agent:
- 10050/udp
zabbix-trapper:
- 10051/udp
zebra:
- 2601/tcp
zebrasrv:
- 2600/tcp
zephyr-clt:
- 2103/udp
zephyr-hm:
- 2104/udp
zephyr-srv:
- 2102/udp
zip:
- 6/ddp
zope:
- 9673/tcp
zope-ftp:
- 8021/tcp
zserv:
- 346/udp


# User Variables
chris:
    users_editor: vim
    users_email: chris@webarchitects.co.uk
    users_groups:
    - sudo
    users_home_mode: '0750'
    users_name: Chris Croome
    users_ssh_public_keys:
    - https://git.coop/chris.keys
    users_state: present
decentral1se:
    users_editor: vim
    users_groups:
    - sudo
    users_home_mode: '0750'
    users_ssh_public_keys:
    - https://git.coop/decentral1se.keys
    users_state: present
root:
    users_editor: vim
    users_email: chris@webarchitects.co.uk
    users_home: /root
    users_home_mode: '0700'
    users_ssh_public_keys:
    - https://git.coop/chris.keys
    - https://git.coop/decentral1se.keys
    users_state: present



# Group Variables
all:
- ca.meet.coop
- demo.meet.coop
- dev.meet.coop
- bbb.webarch.org.uk
- turn.bbb.webarch.org.uk
- turn.fosshost.org
- monitor.webarch.net
bbb22_servers:
- ca.meet.coop
- demo.meet.coop
- dev.meet.coop
- bbb.webarch.org.uk
munin_servers:
- monitor.webarch.net
turn_servers:
- turn.bbb.webarch.org.uk
- turn.fosshost.org
ungrouped: []


# Host Variables
ansible_all_ipv4_addresses:
- 172.18.0.1
- 199.58.82.68
- 172.17.0.1
ansible_all_ipv6_addresses:
- fe80::42:61ff:fe4c:32ea
- fe80::3877:d1ff:fee9:ae33
- fe80::9cdc:d7ff:fedc:c53f
- fe80::3eec:efff:fe40:552a
- fe80::42:c9ff:fe56:3953
ansible_apparmor:
    status: enabled
ansible_architecture: x86_64
ansible_bios_date: 12/02/2019
ansible_bios_version: '3.2'
ansible_br_5391ff83ad91:
    active: true
    device: br-5391ff83ad91
    id: 8000.0242614c32ea
    interfaces:
    - vethc060a5f
    - vetheee40b5
    ipv4:
        address: 172.18.0.1
        broadcast: 172.18.255.255
        netmask: 255.255.0.0
        network: 172.18.0.0
    ipv6:
    -   address: fe80::42:61ff:fe4c:32ea
        prefix: '64'
        scope: link
    macaddress: 02:42:61:4c:32:ea
    mtu: 1500
    promisc: false
    stp: false
    type: bridge
ansible_check_mode: false
ansible_cmdline:
    BOOT_IMAGE: /vmlinuz-4.4.0-179-generic
    console: ttyS0,115200n8
    quiet: true
    ro: true
    root: /dev/mapper/root-slash
ansible_date_time:
    date: '2020-06-05'
    day: '05'
    epoch: '1591362313'
    hour: '13'
    iso8601: '2020-06-05T13:05:13Z'
    iso8601_basic: 20200605T130513532673
    iso8601_basic_short: 20200605T130513
    iso8601_micro: '2020-06-05T13:05:13.532788Z'
    minute: '05'
    month: '06'
    second: '13'
    time: '13:05:13'
    tz: UTC
    tz_offset: '+0000'
    weekday: Friday
    weekday_number: '5'
    weeknumber: '22'
    year: '2020'
ansible_default_ipv4:
    address: 199.58.82.68
    alias: eno1
    broadcast: 199.58.82.127
    gateway: 199.58.82.65
    interface: eno1
    macaddress: 3c:ec:ef:40:55:2a
    mtu: 1500
    netmask: 255.255.255.192
    network: 199.58.82.64
    type: ether
ansible_default_ipv6: {}
ansible_device_links:
    ids:
        dm-0:
        - dm-name-crypt_dev_md1
        - dm-uuid-CRYPT-LUKS1-249cebd00c924fe6ac9696e5c29dc5d9-crypt_dev_md1
        - lvm-pv-uuid-JcNSqL-Z76N-o6dE-vd9r-K5wv-8Ted-Td5Waa
        dm-1:
        - dm-name-root-swap
        - dm-uuid-LVM-5boxsqsWaxZhfSZyx7ovN61IbMpcx2uEXIIVLWoVshVgWmRWlGd1qPFVuLpxAuqw
        dm-2:
        - dm-name-root-slash
        - dm-uuid-LVM-5boxsqsWaxZhfSZyx7ovN61IbMpcx2uEJ8g5n1aTXZ4nTWPR5bScwqwDexMGX4ES
        dm-3:
        - dm-name-crypt_dev_md3
        - dm-uuid-CRYPT-LUKS1-0a54a1b5eac6440aa8e1df237bb6806a-crypt_dev_md3
        dm-4:
        - dm-name-crypt_dev_md4
        - dm-uuid-CRYPT-LUKS1-2c6919ef27d641e29f458cf37c38c01c-crypt_dev_md4
        - lvm-pv-uuid-LU3PKb-WySz-OxGu-fefd-aYVA-pgZq-V1Ln2U
        dm-5:
        - dm-name-crypt_dev_md2
        - dm-uuid-CRYPT-LUKS1-b0ca3c8363fd475385a1d032e6078057-crypt_dev_md2
        - lvm-pv-uuid-jrDppM-7jfz-jtLi-z7Kx-zB0d-D0q1-Byefks
        md0:
        - md-name-meet-coop0:0
        - md-uuid-52bfe1ad:907369cd:db51166e:6f352ee4
        md1:
        - md-name-meet-coop0:1
        - md-uuid-6bd78460:60164f31:4189c715:4254259f
        md2:
        - md-name-meet-coop0:2
        - md-uuid-2ea2b57e:8bc5cb1a:3555d411:d0eb12d8
        md3:
        - md-name-meet-coop0:3
        - md-uuid-5aaae292:3f01b7f3:c6aa287b:f21fb6d1
        md4:
        - md-name-meet-coop0:4
        - md-uuid-bf4e6eb2:d210bbd7:f74e086d:d349f0cd
        sda:
        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z
        - wwn-0x5002538e1012a4bd
        sda1:
        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z-part1
        - wwn-0x5002538e1012a4bd-part1
        sda2:
        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z-part2
        - wwn-0x5002538e1012a4bd-part2
        sda3:
        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z-part3
        - wwn-0x5002538e1012a4bd-part3
        sdb:
        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A
        - wwn-0x5002538e1012a4ce
        sdb1:
        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A-part1
        - wwn-0x5002538e1012a4ce-part1
        sdb2:
        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A-part2
        - wwn-0x5002538e1012a4ce-part2
        sdb3:
        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A-part3
        - wwn-0x5002538e1012a4ce-part3
        sdc:
        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J
        - wwn-0x5002538e09b716c4
        sdc1:
        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part1
        - wwn-0x5002538e09b716c4-part1
        sdc2:
        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part2
        - wwn-0x5002538e09b716c4-part2
        sdc3:
        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part3
        - wwn-0x5002538e09b716c4-part3
        sdc4:
        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part4
        - wwn-0x5002538e09b716c4-part4
        sdd:
        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V
        - wwn-0x5002538e09b716aa
        sdd1:
        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part1
        - wwn-0x5002538e09b716aa-part1
        sdd2:
        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part2
        - wwn-0x5002538e09b716aa-part2
        sdd3:
        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part3
        - wwn-0x5002538e09b716aa-part3
        sdd4:
        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part4
        - wwn-0x5002538e09b716aa-part4
    labels: {}
    masters:
        dm-0:
        - dm-1
        - dm-2
        md1:
        - dm-0
        md2:
        - dm-5
        md3:
        - dm-3
        md4:
        - dm-4
        sda1:
        - md3
        sda2:
        - md4
        sdb1:
        - md3
        sdb2:
        - md4
        sdc1:
        - md0
        sdc2:
        - md1
        sdc3:
        - md2
        sdd1:
        - md0
        sdd2:
        - md1
        sdd3:
        - md2
    uuids:
        dm-1:
        - 67447b4b-2a54-4921-9db8-69f9af331d7e
        dm-2:
        - 6a037d59-4597-46bb-91d5-b28dd907d7a8
        md0:
        - 27484465-4af1-4e52-a67d-6b142b00bde5
        md1:
        - 249cebd0-0c92-4fe6-ac96-96e5c29dc5d9
        md2:
        - b0ca3c83-63fd-4753-85a1-d032e6078057
        md3:
        - 0a54a1b5-eac6-440a-a8e1-df237bb6806a
        md4:
        - 2c6919ef-27d6-41e2-9f45-8cf37c38c01c
ansible_devices:
    dm-0:
        holders:
        - root-swap
        - root-slash
        host: ''
        links:
            ids:
            - dm-name-crypt_dev_md1
            - dm-uuid-CRYPT-LUKS1-249cebd00c924fe6ac9696e5c29dc5d9-crypt_dev_md1
            - lvm-pv-uuid-JcNSqL-Z76N-o6dE-vd9r-K5wv-8Ted-Td5Waa
            labels: []
            masters:
            - dm-1
            - dm-2
            uuids: []
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '73330688'
        sectorsize: '512'
        size: 34.97 GB
        support_discard: '512'
        vendor: null
        virtual: 1
    dm-1:
        holders: []
        host: ''
        links:
            ids:
            - dm-name-root-swap
            - dm-uuid-LVM-5boxsqsWaxZhfSZyx7ovN61IbMpcx2uEXIIVLWoVshVgWmRWlGd1qPFVuLpxAuqw
            labels: []
            masters: []
            uuids:
            - 67447b4b-2a54-4921-9db8-69f9af331d7e
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '16777216'
        sectorsize: '512'
        size: 8.00 GB
        support_discard: '512'
        vendor: null
        virtual: 1
    dm-2:
        holders: []
        host: ''
        links:
            ids:
            - dm-name-root-slash
            - dm-uuid-LVM-5boxsqsWaxZhfSZyx7ovN61IbMpcx2uEJ8g5n1aTXZ4nTWPR5bScwqwDexMGX4ES
            labels: []
            masters: []
            uuids:
            - 6a037d59-4597-46bb-91d5-b28dd907d7a8
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '52428800'
        sectorsize: '512'
        size: 25.00 GB
        support_discard: '512'
        vendor: null
        virtual: 1
    dm-3:
        holders: []
        host: ''
        links:
            ids:
            - dm-name-crypt_dev_md3
            - dm-uuid-CRYPT-LUKS1-0a54a1b5eac6440aa8e1df237bb6806a-crypt_dev_md3
            labels: []
            masters: []
            uuids: []
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '4186112'
        sectorsize: '512'
        size: 2.00 GB
        support_discard: '512'
        vendor: null
        virtual: 1
    dm-4:
        holders: []
        host: ''
        links:
            ids:
            - dm-name-crypt_dev_md4
            - dm-uuid-CRYPT-LUKS1-2c6919ef27d641e29f458cf37c38c01c-crypt_dev_md4
            - lvm-pv-uuid-LU3PKb-WySz-OxGu-fefd-aYVA-pgZq-V1Ln2U
            labels: []
            masters: []
            uuids: []
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '7809570432'
        sectorsize: '512'
        size: 3.64 TB
        support_discard: '512'
        vendor: null
        virtual: 1
    dm-5:
        holders: []
        host: ''
        links:
            ids:
            - dm-name-crypt_dev_md2
            - dm-uuid-CRYPT-LUKS1-b0ca3c8363fd475385a1d032e6078057-crypt_dev_md2
            - lvm-pv-uuid-jrDppM-7jfz-jtLi-z7Kx-zB0d-D0q1-Byefks
            labels: []
            masters: []
            uuids: []
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '1924639360'
        sectorsize: '512'
        size: 917.74 GB
        support_discard: '512'
        vendor: null
        virtual: 1
    loop0:
        holders: []
        host: ''
        links:
            ids: []
            labels: []
            masters: []
            uuids: []
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '0'
        sectorsize: '512'
        size: 0.00 Bytes
        support_discard: '0'
        vendor: null
        virtual: 1
    loop1:
        holders: []
        host: ''
        links:
            ids: []
            labels: []
            masters: []
            uuids: []
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '0'
        sectorsize: '512'
        size: 0.00 Bytes
        support_discard: '0'
        vendor: null
        virtual: 1
    loop2:
        holders: []
        host: ''
        links:
            ids: []
            labels: []
            masters: []
            uuids: []
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '0'
        sectorsize: '512'
        size: 0.00 Bytes
        support_discard: '0'
        vendor: null
        virtual: 1
    loop3:
        holders: []
        host: ''
        links:
            ids: []
            labels: []
            masters: []
            uuids: []
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '0'
        sectorsize: '512'
        size: 0.00 Bytes
        support_discard: '0'
        vendor: null
        virtual: 1
    loop4:
        holders: []
        host: ''
        links:
            ids: []
            labels: []
            masters: []
            uuids: []
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '0'
        sectorsize: '512'
        size: 0.00 Bytes
        support_discard: '0'
        vendor: null
        virtual: 1
    loop5:
        holders: []
        host: ''
        links:
            ids: []
            labels: []
            masters: []
            uuids: []
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '0'
        sectorsize: '512'
        size: 0.00 Bytes
        support_discard: '0'
        vendor: null
        virtual: 1
    loop6:
        holders: []
        host: ''
        links:
            ids: []
            labels: []
            masters: []
            uuids: []
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '0'
        sectorsize: '512'
        size: 0.00 Bytes
        support_discard: '0'
        vendor: null
        virtual: 1
    loop7:
        holders: []
        host: ''
        links:
            ids: []
            labels: []
            masters: []
            uuids: []
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '0'
        sectorsize: '512'
        size: 0.00 Bytes
        support_discard: '0'
        vendor: null
        virtual: 1
    md0:
        holders: []
        host: ''
        links:
            ids:
            - md-name-meet-coop0:0
            - md-uuid-52bfe1ad:907369cd:db51166e:6f352ee4
            labels: []
            masters: []
            uuids:
            - 27484465-4af1-4e52-a67d-6b142b00bde5
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '2095104'
        sectorsize: '512'
        size: 1023.00 MB
        support_discard: '512'
        vendor: null
        virtual: 1
    md1:
        holders:
        - crypt_dev_md1
        host: ''
        links:
            ids:
            - md-name-meet-coop0:1
            - md-uuid-6bd78460:60164f31:4189c715:4254259f
            labels: []
            masters:
            - dm-0
            uuids:
            - 249cebd0-0c92-4fe6-ac96-96e5c29dc5d9
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '73334784'
        sectorsize: '512'
        size: 34.97 GB
        support_discard: '512'
        vendor: null
        virtual: 1
    md2:
        holders:
        - crypt_dev_md2
        host: ''
        links:
            ids:
            - md-name-meet-coop0:2
            - md-uuid-2ea2b57e:8bc5cb1a:3555d411:d0eb12d8
            labels: []
            masters:
            - dm-5
            uuids:
            - b0ca3c83-63fd-4753-85a1-d032e6078057
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '1924643456'
        sectorsize: '512'
        size: 917.74 GB
        support_discard: '512'
        vendor: null
        virtual: 1
    md3:
        holders:
        - crypt_dev_md3
        host: ''
        links:
            ids:
            - md-name-meet-coop0:3
            - md-uuid-5aaae292:3f01b7f3:c6aa287b:f21fb6d1
            labels: []
            masters:
            - dm-3
            uuids:
            - 0a54a1b5-eac6-440a-a8e1-df237bb6806a
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '4190208'
        sectorsize: '512'
        size: 2.00 GB
        support_discard: '512'
        vendor: null
        virtual: 1
    md4:
        holders:
        - crypt_dev_md4
        host: ''
        links:
            ids:
            - md-name-meet-coop0:4
            - md-uuid-bf4e6eb2:d210bbd7:f74e086d:d349f0cd
            labels: []
            masters:
            - dm-4
            uuids:
            - 2c6919ef-27d6-41e2-9f45-8cf37c38c01c
        model: null
        partitions: {}
        removable: '0'
        rotational: '1'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: ''
        sectors: '7809574528'
        sectorsize: '512'
        size: 3.64 TB
        support_discard: '512'
        vendor: null
        virtual: 1
    sda:
        holders: []
        host: 'SATA controller: Intel Corporation C620 Series Chipset Family SATA
            Controller [AHCI mode] (rev 09)'
        links:
            ids:
            - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z
            - wwn-0x5002538e1012a4bd
            labels: []
            masters: []
            uuids: []
        model: Samsung SSD 860
        partitions:
            sda1:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z-part1
                    - wwn-0x5002538e1012a4bd-part1
                    labels: []
                    masters:
                    - md3
                    uuids: []
                sectors: '4194304'
                sectorsize: 512
                size: 2.00 GB
                start: '4096'
                uuid: null
            sda2:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z-part2
                    - wwn-0x5002538e1012a4bd-part2
                    labels: []
                    masters:
                    - md4
                    uuids: []
                sectors: '7809836687'
                sectorsize: 512
                size: 3.64 TB
                start: '4198400'
                uuid: null
            sda3:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z-part3
                    - wwn-0x5002538e1012a4bd-part3
                    labels: []
                    masters: []
                    uuids: []
                sectors: '2048'
                sectorsize: 512
                size: 1.00 MB
                start: '2048'
                uuid: null
        removable: '0'
        rotational: '0'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: deadline
        sectors: '7814037168'
        sectorsize: '512'
        size: 3.64 TB
        support_discard: '512'
        vendor: ATA
        virtual: 1
        wwn: '0x5002538e1012a4bd'
    sdb:
        holders: []
        host: 'SATA controller: Intel Corporation C620 Series Chipset Family SATA
            Controller [AHCI mode] (rev 09)'
        links:
            ids:
            - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A
            - wwn-0x5002538e1012a4ce
            labels: []
            masters: []
            uuids: []
        model: Samsung SSD 860
        partitions:
            sdb1:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A-part1
                    - wwn-0x5002538e1012a4ce-part1
                    labels: []
                    masters:
                    - md3
                    uuids: []
                sectors: '4194304'
                sectorsize: 512
                size: 2.00 GB
                start: '4096'
                uuid: null
            sdb2:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A-part2
                    - wwn-0x5002538e1012a4ce-part2
                    labels: []
                    masters:
                    - md4
                    uuids: []
                sectors: '7809836687'
                sectorsize: 512
                size: 3.64 TB
                start: '4198400'
                uuid: null
            sdb3:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A-part3
                    - wwn-0x5002538e1012a4ce-part3
                    labels: []
                    masters: []
                    uuids: []
                sectors: '2048'
                sectorsize: 512
                size: 1.00 MB
                start: '2048'
                uuid: null
        removable: '0'
        rotational: '0'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: deadline
        sectors: '7814037168'
        sectorsize: '512'
        size: 3.64 TB
        support_discard: '512'
        vendor: ATA
        virtual: 1
        wwn: '0x5002538e1012a4ce'
    sdc:
        holders: []
        host: 'SATA controller: Intel Corporation C620 Series Chipset Family SATA
            Controller [AHCI mode] (rev 09)'
        links:
            ids:
            - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J
            - wwn-0x5002538e09b716c4
            labels: []
            masters: []
            uuids: []
        model: Samsung SSD 860
        partitions:
            sdc1:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part1
                    - wwn-0x5002538e09b716c4-part1
                    labels: []
                    masters:
                    - md0
                    uuids: []
                sectors: '2097152'
                sectorsize: 512
                size: 1.00 GB
                start: '4096'
                uuid: null
            sdc2:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part2
                    - wwn-0x5002538e09b716c4-part2
                    labels: []
                    masters:
                    - md1
                    uuids: []
                sectors: '73400320'
                sectorsize: 512
                size: 35.00 GB
                start: '2101248'
                uuid: null
            sdc3:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part3
                    - wwn-0x5002538e09b716c4-part3
                    labels: []
                    masters:
                    - md2
                    uuids: []
                sectors: '1924905615'
                sectorsize: 512
                size: 917.87 GB
                start: '75501568'
                uuid: null
            sdc4:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part4
                    - wwn-0x5002538e09b716c4-part4
                    labels: []
                    masters: []
                    uuids: []
                sectors: '2048'
                sectorsize: 512
                size: 1.00 MB
                start: '2048'
                uuid: null
        removable: '0'
        rotational: '0'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: deadline
        sectors: '2000409264'
        sectorsize: '512'
        size: 953.87 GB
        support_discard: '512'
        vendor: ATA
        virtual: 1
        wwn: '0x5002538e09b716c4'
    sdd:
        holders: []
        host: 'SATA controller: Intel Corporation C620 Series Chipset Family SATA
            Controller [AHCI mode] (rev 09)'
        links:
            ids:
            - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V
            - wwn-0x5002538e09b716aa
            labels: []
            masters: []
            uuids: []
        model: Samsung SSD 860
        partitions:
            sdd1:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part1
                    - wwn-0x5002538e09b716aa-part1
                    labels: []
                    masters:
                    - md0
                    uuids: []
                sectors: '2097152'
                sectorsize: 512
                size: 1.00 GB
                start: '4096'
                uuid: null
            sdd2:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part2
                    - wwn-0x5002538e09b716aa-part2
                    labels: []
                    masters:
                    - md1
                    uuids: []
                sectors: '73400320'
                sectorsize: 512
                size: 35.00 GB
                start: '2101248'
                uuid: null
            sdd3:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part3
                    - wwn-0x5002538e09b716aa-part3
                    labels: []
                    masters:
                    - md2
                    uuids: []
                sectors: '1924905615'
                sectorsize: 512
                size: 917.87 GB
                start: '75501568'
                uuid: null
            sdd4:
                holders: []
                links:
                    ids:
                    - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part4
                    - wwn-0x5002538e09b716aa-part4
                    labels: []
                    masters: []
                    uuids: []
                sectors: '2048'
                sectorsize: 512
                size: 1.00 MB
                start: '2048'
                uuid: null
        removable: '0'
        rotational: '0'
        sas_address: null
        sas_device_handle: null
        scheduler_mode: deadline
        sectors: '2000409264'
        sectorsize: '512'
        size: 953.87 GB
        support_discard: '512'
        vendor: ATA
        virtual: 1
        wwn: '0x5002538e09b716aa'
ansible_diff_mode: false
ansible_distribution: Ubuntu
ansible_distribution_file_parsed: true
ansible_distribution_file_path: /etc/os-release
ansible_distribution_file_variety: Debian
ansible_distribution_major_version: '16'
ansible_distribution_release: xenial
ansible_distribution_version: '16.04'
ansible_dns:
    nameservers:
    - 199.58.80.70
    - 199.58.80.71
    search:
    - koumbit.net
ansible_docker0:
    active: false
    device: docker0
    id: 8000.0242c9563953
    interfaces: []
    ipv4:
        address: 172.17.0.1
        broadcast: 172.17.255.255
        netmask: 255.255.0.0
        network: 172.17.0.0
    ipv6:
    -   address: fe80::42:c9ff:fe56:3953
        prefix: '64'
        scope: link
    macaddress: 02:42:c9:56:39:53
    mtu: 1500
    promisc: false
    stp: false
    type: bridge
ansible_domain: koumbit.net
ansible_effective_group_id: 0
ansible_effective_user_id: 0
ansible_eno1:
    active: true
    device: eno1
    ipv4:
        address: 199.58.82.68
        broadcast: 199.58.82.127
        netmask: 255.255.255.192
        network: 199.58.82.64
    ipv6:
    -   address: fe80::3eec:efff:fe40:552a
        prefix: '64'
        scope: link
    macaddress: 3c:ec:ef:40:55:2a
    module: i40e
    mtu: 1500
    pciid: 0000:60:00.0
    promisc: false
    speed: 1000
    type: ether
ansible_eno2:
    active: false
    device: eno2
    macaddress: 3c:ec:ef:40:55:2b
    module: i40e
    mtu: 1500
    pciid: 0000:60:00.1
    promisc: false
    type: ether
ansible_env:
    HOME: /root
    LANG: C
    LC_ALL: C
    LC_NUMERIC: C
    LOGNAME: root
    MAIL: /var/mail/root
    PATH: /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games
    PWD: /root
    SHELL: /bin/bash
    SHLVL: '1'
    SSH_CLIENT: 92.8.189.164 39840 22
    SSH_CONNECTION: 92.8.189.164 39840 199.58.82.68 22
    SSH_TTY: /dev/pts/1
    TERM: xterm
    USER: root
    XDG_RUNTIME_DIR: /run/user/0
    XDG_SESSION_ID: '41'
    _: /bin/sh
ansible_facts:
    _ansible_facts_gathered: true
    all_ipv4_addresses:
    - 172.18.0.1
    - 199.58.82.68
    - 172.17.0.1
    all_ipv6_addresses:
    - fe80::42:61ff:fe4c:32ea
    - fe80::3877:d1ff:fee9:ae33
    - fe80::9cdc:d7ff:fedc:c53f
    - fe80::3eec:efff:fe40:552a
    - fe80::42:c9ff:fe56:3953
    ansible_local: {}
    apparmor:
        status: enabled
    architecture: x86_64
    bios_date: 12/02/2019
    bios_version: '3.2'
    br_5391ff83ad91:
        active: true
        device: br-5391ff83ad91
        id: 8000.0242614c32ea
        interfaces:
        - vethc060a5f
        - vetheee40b5
        ipv4:
            address: 172.18.0.1
            broadcast: 172.18.255.255
            netmask: 255.255.0.0
            network: 172.18.0.0
        ipv6:
        -   address: fe80::42:61ff:fe4c:32ea
            prefix: '64'
            scope: link
        macaddress: 02:42:61:4c:32:ea
        mtu: 1500
        promisc: false
        stp: false
        type: bridge
    cmdline:
        BOOT_IMAGE: /vmlinuz-4.4.0-179-generic
        console: ttyS0,115200n8
        quiet: true
        ro: true
        root: /dev/mapper/root-slash
    date_time:
        date: '2020-06-05'
        day: '05'
        epoch: '1591362313'
        hour: '13'
        iso8601: '2020-06-05T13:05:13Z'
        iso8601_basic: 20200605T130513532673
        iso8601_basic_short: 20200605T130513
        iso8601_micro: '2020-06-05T13:05:13.532788Z'
        minute: '05'
        month: '06'
        second: '13'
        time: '13:05:13'
        tz: UTC
        tz_offset: '+0000'
        weekday: Friday
        weekday_number: '5'
        weeknumber: '22'
        year: '2020'
    default_ipv4:
        address: 199.58.82.68
        alias: eno1
        broadcast: 199.58.82.127
        gateway: 199.58.82.65
        interface: eno1
        macaddress: 3c:ec:ef:40:55:2a
        mtu: 1500
        netmask: 255.255.255.192
        network: 199.58.82.64
        type: ether
    default_ipv6: {}
    device_links:
        ids:
            dm-0:
            - dm-name-crypt_dev_md1
            - dm-uuid-CRYPT-LUKS1-249cebd00c924fe6ac9696e5c29dc5d9-crypt_dev_md1
            - lvm-pv-uuid-JcNSqL-Z76N-o6dE-vd9r-K5wv-8Ted-Td5Waa
            dm-1:
            - dm-name-root-swap
            - dm-uuid-LVM-5boxsqsWaxZhfSZyx7ovN61IbMpcx2uEXIIVLWoVshVgWmRWlGd1qPFVuLpxAuqw
            dm-2:
            - dm-name-root-slash
            - dm-uuid-LVM-5boxsqsWaxZhfSZyx7ovN61IbMpcx2uEJ8g5n1aTXZ4nTWPR5bScwqwDexMGX4ES
            dm-3:
            - dm-name-crypt_dev_md3
            - dm-uuid-CRYPT-LUKS1-0a54a1b5eac6440aa8e1df237bb6806a-crypt_dev_md3
            dm-4:
            - dm-name-crypt_dev_md4
            - dm-uuid-CRYPT-LUKS1-2c6919ef27d641e29f458cf37c38c01c-crypt_dev_md4
            - lvm-pv-uuid-LU3PKb-WySz-OxGu-fefd-aYVA-pgZq-V1Ln2U
            dm-5:
            - dm-name-crypt_dev_md2
            - dm-uuid-CRYPT-LUKS1-b0ca3c8363fd475385a1d032e6078057-crypt_dev_md2
            - lvm-pv-uuid-jrDppM-7jfz-jtLi-z7Kx-zB0d-D0q1-Byefks
            md0:
            - md-name-meet-coop0:0
            - md-uuid-52bfe1ad:907369cd:db51166e:6f352ee4
            md1:
            - md-name-meet-coop0:1
            - md-uuid-6bd78460:60164f31:4189c715:4254259f
            md2:
            - md-name-meet-coop0:2
            - md-uuid-2ea2b57e:8bc5cb1a:3555d411:d0eb12d8
            md3:
            - md-name-meet-coop0:3
            - md-uuid-5aaae292:3f01b7f3:c6aa287b:f21fb6d1
            md4:
            - md-name-meet-coop0:4
            - md-uuid-bf4e6eb2:d210bbd7:f74e086d:d349f0cd
            sda:
            - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z
            - wwn-0x5002538e1012a4bd
            sda1:
            - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z-part1
            - wwn-0x5002538e1012a4bd-part1
            sda2:
            - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z-part2
            - wwn-0x5002538e1012a4bd-part2
            sda3:
            - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z-part3
            - wwn-0x5002538e1012a4bd-part3
            sdb:
            - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A
            - wwn-0x5002538e1012a4ce
            sdb1:
            - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A-part1
            - wwn-0x5002538e1012a4ce-part1
            sdb2:
            - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A-part2
            - wwn-0x5002538e1012a4ce-part2
            sdb3:
            - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A-part3
            - wwn-0x5002538e1012a4ce-part3
            sdc:
            - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J
            - wwn-0x5002538e09b716c4
            sdc1:
            - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part1
            - wwn-0x5002538e09b716c4-part1
            sdc2:
            - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part2
            - wwn-0x5002538e09b716c4-part2
            sdc3:
            - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part3
            - wwn-0x5002538e09b716c4-part3
            sdc4:
            - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part4
            - wwn-0x5002538e09b716c4-part4
            sdd:
            - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V
            - wwn-0x5002538e09b716aa
            sdd1:
            - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part1
            - wwn-0x5002538e09b716aa-part1
            sdd2:
            - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part2
            - wwn-0x5002538e09b716aa-part2
            sdd3:
            - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part3
            - wwn-0x5002538e09b716aa-part3
            sdd4:
            - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part4
            - wwn-0x5002538e09b716aa-part4
        labels: {}
        masters:
            dm-0:
            - dm-1
            - dm-2
            md1:
            - dm-0
            md2:
            - dm-5
            md3:
            - dm-3
            md4:
            - dm-4
            sda1:
            - md3
            sda2:
            - md4
            sdb1:
            - md3
            sdb2:
            - md4
            sdc1:
            - md0
            sdc2:
            - md1
            sdc3:
            - md2
            sdd1:
            - md0
            sdd2:
            - md1
            sdd3:
            - md2
        uuids:
            dm-1:
            - 67447b4b-2a54-4921-9db8-69f9af331d7e
            dm-2:
            - 6a037d59-4597-46bb-91d5-b28dd907d7a8
            md0:
            - 27484465-4af1-4e52-a67d-6b142b00bde5
            md1:
            - 249cebd0-0c92-4fe6-ac96-96e5c29dc5d9
            md2:
            - b0ca3c83-63fd-4753-85a1-d032e6078057
            md3:
            - 0a54a1b5-eac6-440a-a8e1-df237bb6806a
            md4:
            - 2c6919ef-27d6-41e2-9f45-8cf37c38c01c
    devices:
        dm-0:
            holders:
            - root-swap
            - root-slash
            host: ''
            links:
                ids:
                - dm-name-crypt_dev_md1
                - dm-uuid-CRYPT-LUKS1-249cebd00c924fe6ac9696e5c29dc5d9-crypt_dev_md1
                - lvm-pv-uuid-JcNSqL-Z76N-o6dE-vd9r-K5wv-8Ted-Td5Waa
                labels: []
                masters:
                - dm-1
                - dm-2
                uuids: []
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '73330688'
            sectorsize: '512'
            size: 34.97 GB
            support_discard: '512'
            vendor: null
            virtual: 1
        dm-1:
            holders: []
            host: ''
            links:
                ids:
                - dm-name-root-swap
                - dm-uuid-LVM-5boxsqsWaxZhfSZyx7ovN61IbMpcx2uEXIIVLWoVshVgWmRWlGd1qPFVuLpxAuqw
                labels: []
                masters: []
                uuids:
                - 67447b4b-2a54-4921-9db8-69f9af331d7e
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '16777216'
            sectorsize: '512'
            size: 8.00 GB
            support_discard: '512'
            vendor: null
            virtual: 1
        dm-2:
            holders: []
            host: ''
            links:
                ids:
                - dm-name-root-slash
                - dm-uuid-LVM-5boxsqsWaxZhfSZyx7ovN61IbMpcx2uEJ8g5n1aTXZ4nTWPR5bScwqwDexMGX4ES
                labels: []
                masters: []
                uuids:
                - 6a037d59-4597-46bb-91d5-b28dd907d7a8
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '52428800'
            sectorsize: '512'
            size: 25.00 GB
            support_discard: '512'
            vendor: null
            virtual: 1
        dm-3:
            holders: []
            host: ''
            links:
                ids:
                - dm-name-crypt_dev_md3
                - dm-uuid-CRYPT-LUKS1-0a54a1b5eac6440aa8e1df237bb6806a-crypt_dev_md3
                labels: []
                masters: []
                uuids: []
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '4186112'
            sectorsize: '512'
            size: 2.00 GB
            support_discard: '512'
            vendor: null
            virtual: 1
        dm-4:
            holders: []
            host: ''
            links:
                ids:
                - dm-name-crypt_dev_md4
                - dm-uuid-CRYPT-LUKS1-2c6919ef27d641e29f458cf37c38c01c-crypt_dev_md4
                - lvm-pv-uuid-LU3PKb-WySz-OxGu-fefd-aYVA-pgZq-V1Ln2U
                labels: []
                masters: []
                uuids: []
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '7809570432'
            sectorsize: '512'
            size: 3.64 TB
            support_discard: '512'
            vendor: null
            virtual: 1
        dm-5:
            holders: []
            host: ''
            links:
                ids:
                - dm-name-crypt_dev_md2
                - dm-uuid-CRYPT-LUKS1-b0ca3c8363fd475385a1d032e6078057-crypt_dev_md2
                - lvm-pv-uuid-jrDppM-7jfz-jtLi-z7Kx-zB0d-D0q1-Byefks
                labels: []
                masters: []
                uuids: []
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '1924639360'
            sectorsize: '512'
            size: 917.74 GB
            support_discard: '512'
            vendor: null
            virtual: 1
        loop0:
            holders: []
            host: ''
            links:
                ids: []
                labels: []
                masters: []
                uuids: []
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '0'
            sectorsize: '512'
            size: 0.00 Bytes
            support_discard: '0'
            vendor: null
            virtual: 1
        loop1:
            holders: []
            host: ''
            links:
                ids: []
                labels: []
                masters: []
                uuids: []
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '0'
            sectorsize: '512'
            size: 0.00 Bytes
            support_discard: '0'
            vendor: null
            virtual: 1
        loop2:
            holders: []
            host: ''
            links:
                ids: []
                labels: []
                masters: []
                uuids: []
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '0'
            sectorsize: '512'
            size: 0.00 Bytes
            support_discard: '0'
            vendor: null
            virtual: 1
        loop3:
            holders: []
            host: ''
            links:
                ids: []
                labels: []
                masters: []
                uuids: []
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '0'
            sectorsize: '512'
            size: 0.00 Bytes
            support_discard: '0'
            vendor: null
            virtual: 1
        loop4:
            holders: []
            host: ''
            links:
                ids: []
                labels: []
                masters: []
                uuids: []
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '0'
            sectorsize: '512'
            size: 0.00 Bytes
            support_discard: '0'
            vendor: null
            virtual: 1
        loop5:
            holders: []
            host: ''
            links:
                ids: []
                labels: []
                masters: []
                uuids: []
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '0'
            sectorsize: '512'
            size: 0.00 Bytes
            support_discard: '0'
            vendor: null
            virtual: 1
        loop6:
            holders: []
            host: ''
            links:
                ids: []
                labels: []
                masters: []
                uuids: []
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '0'
            sectorsize: '512'
            size: 0.00 Bytes
            support_discard: '0'
            vendor: null
            virtual: 1
        loop7:
            holders: []
            host: ''
            links:
                ids: []
                labels: []
                masters: []
                uuids: []
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '0'
            sectorsize: '512'
            size: 0.00 Bytes
            support_discard: '0'
            vendor: null
            virtual: 1
        md0:
            holders: []
            host: ''
            links:
                ids:
                - md-name-meet-coop0:0
                - md-uuid-52bfe1ad:907369cd:db51166e:6f352ee4
                labels: []
                masters: []
                uuids:
                - 27484465-4af1-4e52-a67d-6b142b00bde5
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '2095104'
            sectorsize: '512'
            size: 1023.00 MB
            support_discard: '512'
            vendor: null
            virtual: 1
        md1:
            holders:
            - crypt_dev_md1
            host: ''
            links:
                ids:
                - md-name-meet-coop0:1
                - md-uuid-6bd78460:60164f31:4189c715:4254259f
                labels: []
                masters:
                - dm-0
                uuids:
                - 249cebd0-0c92-4fe6-ac96-96e5c29dc5d9
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '73334784'
            sectorsize: '512'
            size: 34.97 GB
            support_discard: '512'
            vendor: null
            virtual: 1
        md2:
            holders:
            - crypt_dev_md2
            host: ''
            links:
                ids:
                - md-name-meet-coop0:2
                - md-uuid-2ea2b57e:8bc5cb1a:3555d411:d0eb12d8
                labels: []
                masters:
                - dm-5
                uuids:
                - b0ca3c83-63fd-4753-85a1-d032e6078057
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '1924643456'
            sectorsize: '512'
            size: 917.74 GB
            support_discard: '512'
            vendor: null
            virtual: 1
        md3:
            holders:
            - crypt_dev_md3
            host: ''
            links:
                ids:
                - md-name-meet-coop0:3
                - md-uuid-5aaae292:3f01b7f3:c6aa287b:f21fb6d1
                labels: []
                masters:
                - dm-3
                uuids:
                - 0a54a1b5-eac6-440a-a8e1-df237bb6806a
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '4190208'
            sectorsize: '512'
            size: 2.00 GB
            support_discard: '512'
            vendor: null
            virtual: 1
        md4:
            holders:
            - crypt_dev_md4
            host: ''
            links:
                ids:
                - md-name-meet-coop0:4
                - md-uuid-bf4e6eb2:d210bbd7:f74e086d:d349f0cd
                labels: []
                masters:
                - dm-4
                uuids:
                - 2c6919ef-27d6-41e2-9f45-8cf37c38c01c
            model: null
            partitions: {}
            removable: '0'
            rotational: '1'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: ''
            sectors: '7809574528'
            sectorsize: '512'
            size: 3.64 TB
            support_discard: '512'
            vendor: null
            virtual: 1
        sda:
            holders: []
            host: 'SATA controller: Intel Corporation C620 Series Chipset Family SATA
                Controller [AHCI mode] (rev 09)'
            links:
                ids:
                - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z
                - wwn-0x5002538e1012a4bd
                labels: []
                masters: []
                uuids: []
            model: Samsung SSD 860
            partitions:
                sda1:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z-part1
                        - wwn-0x5002538e1012a4bd-part1
                        labels: []
                        masters:
                        - md3
                        uuids: []
                    sectors: '4194304'
                    sectorsize: 512
                    size: 2.00 GB
                    start: '4096'
                    uuid: null
                sda2:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z-part2
                        - wwn-0x5002538e1012a4bd-part2
                        labels: []
                        masters:
                        - md4
                        uuids: []
                    sectors: '7809836687'
                    sectorsize: 512
                    size: 3.64 TB
                    start: '4198400'
                    uuid: null
                sda3:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102381Z-part3
                        - wwn-0x5002538e1012a4bd-part3
                        labels: []
                        masters: []
                        uuids: []
                    sectors: '2048'
                    sectorsize: 512
                    size: 1.00 MB
                    start: '2048'
                    uuid: null
            removable: '0'
            rotational: '0'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: deadline
            sectors: '7814037168'
            sectorsize: '512'
            size: 3.64 TB
            support_discard: '512'
            vendor: ATA
            virtual: 1
            wwn: '0x5002538e1012a4bd'
        sdb:
            holders: []
            host: 'SATA controller: Intel Corporation C620 Series Chipset Family SATA
                Controller [AHCI mode] (rev 09)'
            links:
                ids:
                - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A
                - wwn-0x5002538e1012a4ce
                labels: []
                masters: []
                uuids: []
            model: Samsung SSD 860
            partitions:
                sdb1:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A-part1
                        - wwn-0x5002538e1012a4ce-part1
                        labels: []
                        masters:
                        - md3
                        uuids: []
                    sectors: '4194304'
                    sectorsize: 512
                    size: 2.00 GB
                    start: '4096'
                    uuid: null
                sdb2:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A-part2
                        - wwn-0x5002538e1012a4ce-part2
                        labels: []
                        masters:
                        - md4
                        uuids: []
                    sectors: '7809836687'
                    sectorsize: 512
                    size: 3.64 TB
                    start: '4198400'
                    uuid: null
                sdb3:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_EVO_4TB_S596NE0N102388A-part3
                        - wwn-0x5002538e1012a4ce-part3
                        labels: []
                        masters: []
                        uuids: []
                    sectors: '2048'
                    sectorsize: 512
                    size: 1.00 MB
                    start: '2048'
                    uuid: null
            removable: '0'
            rotational: '0'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: deadline
            sectors: '7814037168'
            sectorsize: '512'
            size: 3.64 TB
            support_discard: '512'
            vendor: ATA
            virtual: 1
            wwn: '0x5002538e1012a4ce'
        sdc:
            holders: []
            host: 'SATA controller: Intel Corporation C620 Series Chipset Family SATA
                Controller [AHCI mode] (rev 09)'
            links:
                ids:
                - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J
                - wwn-0x5002538e09b716c4
                labels: []
                masters: []
                uuids: []
            model: Samsung SSD 860
            partitions:
                sdc1:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part1
                        - wwn-0x5002538e09b716c4-part1
                        labels: []
                        masters:
                        - md0
                        uuids: []
                    sectors: '2097152'
                    sectorsize: 512
                    size: 1.00 GB
                    start: '4096'
                    uuid: null
                sdc2:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part2
                        - wwn-0x5002538e09b716c4-part2
                        labels: []
                        masters:
                        - md1
                        uuids: []
                    sectors: '73400320'
                    sectorsize: 512
                    size: 35.00 GB
                    start: '2101248'
                    uuid: null
                sdc3:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part3
                        - wwn-0x5002538e09b716c4-part3
                        labels: []
                        masters:
                        - md2
                        uuids: []
                    sectors: '1924905615'
                    sectorsize: 512
                    size: 917.87 GB
                    start: '75501568'
                    uuid: null
                sdc4:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03193J-part4
                        - wwn-0x5002538e09b716c4-part4
                        labels: []
                        masters: []
                        uuids: []
                    sectors: '2048'
                    sectorsize: 512
                    size: 1.00 MB
                    start: '2048'
                    uuid: null
            removable: '0'
            rotational: '0'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: deadline
            sectors: '2000409264'
            sectorsize: '512'
            size: 953.87 GB
            support_discard: '512'
            vendor: ATA
            virtual: 1
            wwn: '0x5002538e09b716c4'
        sdd:
            holders: []
            host: 'SATA controller: Intel Corporation C620 Series Chipset Family SATA
                Controller [AHCI mode] (rev 09)'
            links:
                ids:
                - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V
                - wwn-0x5002538e09b716aa
                labels: []
                masters: []
                uuids: []
            model: Samsung SSD 860
            partitions:
                sdd1:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part1
                        - wwn-0x5002538e09b716aa-part1
                        labels: []
                        masters:
                        - md0
                        uuids: []
                    sectors: '2097152'
                    sectorsize: 512
                    size: 1.00 GB
                    start: '4096'
                    uuid: null
                sdd2:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part2
                        - wwn-0x5002538e09b716aa-part2
                        labels: []
                        masters:
                        - md1
                        uuids: []
                    sectors: '73400320'
                    sectorsize: 512
                    size: 35.00 GB
                    start: '2101248'
                    uuid: null
                sdd3:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part3
                        - wwn-0x5002538e09b716aa-part3
                        labels: []
                        masters:
                        - md2
                        uuids: []
                    sectors: '1924905615'
                    sectorsize: 512
                    size: 917.87 GB
                    start: '75501568'
                    uuid: null
                sdd4:
                    holders: []
                    links:
                        ids:
                        - ata-Samsung_SSD_860_PRO_1TB_S5G8NE0MB03187V-part4
                        - wwn-0x5002538e09b716aa-part4
                        labels: []
                        masters: []
                        uuids: []
                    sectors: '2048'
                    sectorsize: 512
                    size: 1.00 MB
                    start: '2048'
                    uuid: null
            removable: '0'
            rotational: '0'
            sas_address: null
            sas_device_handle: null
            scheduler_mode: deadline
            sectors: '2000409264'
            sectorsize: '512'
            size: 953.87 GB
            support_discard: '512'
            vendor: ATA
            virtual: 1
            wwn: '0x5002538e09b716aa'
    distribution: Ubuntu
    distribution_file_parsed: true
    distribution_file_path: /etc/os-release
    distribution_file_variety: Debian
    distribution_major_version: '16'
    distribution_release: xenial
    distribution_version: '16.04'
    dns:
        nameservers:
        - 199.58.80.70
        - 199.58.80.71
        search:
        - koumbit.net
    docker0:
        active: false
        device: docker0
        id: 8000.0242c9563953
        interfaces: []
        ipv4:
            address: 172.17.0.1
            broadcast: 172.17.255.255
            netmask: 255.255.0.0
            network: 172.17.0.0
        ipv6:
        -   address: fe80::42:c9ff:fe56:3953
            prefix: '64'
            scope: link
        macaddress: 02:42:c9:56:39:53
        mtu: 1500
        promisc: false
        stp: false
        type: bridge
    domain: koumbit.net
    effective_group_id: 0
    effective_user_id: 0
    eno1:
        active: true
        device: eno1
        ipv4:
            address: 199.58.82.68
            broadcast: 199.58.82.127
            netmask: 255.255.255.192
            network: 199.58.82.64
        ipv6:
        -   address: fe80::3eec:efff:fe40:552a
            prefix: '64'
            scope: link
        macaddress: 3c:ec:ef:40:55:2a
        module: i40e
        mtu: 1500
        pciid: 0000:60:00.0
        promisc: false
        speed: 1000
        type: ether
    eno2:
        active: false
        device: eno2
        macaddress: 3c:ec:ef:40:55:2b
        module: i40e
        mtu: 1500
        pciid: 0000:60:00.1
        promisc: false
        type: ether
    env:
        HOME: /root
        LANG: C
        LC_ALL: C
        LC_NUMERIC: C
        LOGNAME: root
        MAIL: /var/mail/root
        PATH: /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games
        PWD: /root
        SHELL: /bin/bash
        SHLVL: '1'
        SSH_CLIENT: 92.8.189.164 39840 22
        SSH_CONNECTION: 92.8.189.164 39840 199.58.82.68 22
        SSH_TTY: /dev/pts/1
        TERM: xterm
        USER: root
        XDG_RUNTIME_DIR: /run/user/0
        XDG_SESSION_ID: '41'
        _: /bin/sh
    fibre_channel_wwn: []
    fips: false
    form_factor: Main Server Chassis
    fqdn: meet-coop0.koumbit.net
    gather_subset:
    - all
    getent_group:
        Debian-exim:
        - x
        - '112'
        - ''
        adm:
        - x
        - '4'
        - syslog
        audio:
        - x
        - '29'
        - ''
        backup:
        - x
        - '34'
        - ''
        bigbluebutton:
        - x
        - '997'
        - ''
        bin:
        - x
        - '2'
        - ''
        cdrom:
        - x
        - '24'
        - ''
        chris:
        - x
        - '1001'
        - chris
        colord:
        - x
        - '117'
        - ''
        crontab:
        - x
        - '107'
        - ''
        daemon:
        - x
        - '1'
        - ''
        decentral1se:
        - x
        - '1002'
        - decentral1se
        dialout:
        - x
        - '20'
        - ''
        dip:
        - x
        - '30'
        - ''
        disk:
        - x
        - '6'
        - ''
        docker:
        - x
        - '999'
        - ''
        etherpad:
        - x
        - '994'
        - ''
        fax:
        - x
        - '21'
        - ''
        floppy:
        - x
        - '25'
        - ''
        freeswitch:
        - x
        - '996'
        - bigbluebutton
        games:
        - x
        - '60'
        - ''
        gnats:
        - x
        - '41'
        - ''
        input:
        - x
        - '106'
        - ''
        irc:
        - x
        - '39'
        - ''
        kmem:
        - x
        - '15'
        - ''
        koumbit:
        - x
        - '1000'
        - ''
        kurento:
        - x
        - '995'
        - bigbluebutton
        list:
        - x
        - '38'
        - ''
        lp:
        - x
        - '7'
        - ''
        mail:
        - x
        - '8'
        - ''
        man:
        - x
        - '12'
        - ''
        messagebus:
        - x
        - '110'
        - ''
        meteor:
        - x
        - '993'
        - ''
        mongodb:
        - x
        - '113'
        - mongodb
        munin:
        - x
        - '115'
        - ''
        netdev:
        - x
        - '109'
        - ''
        news:
        - x
        - '9'
        - ''
        nogroup:
        - x
        - '65534'
        - ''
        operator:
        - x
        - '37'
        - ''
        plugdev:
        - x
        - '46'
        - ''
        proxy:
        - x
        - '13'
        - ''
        red5:
        - x
        - '998'
        - ''
        redis:
        - x
        - '118'
        - ''
        root:
        - x
        - '0'
        - ''
        sasl:
        - x
        - '45'
        - ''
        scanner:
        - x
        - '116'
        - ''
        shadow:
        - x
        - '42'
        - ''
        src:
        - x
        - '40'
        - ''
        ssh:
        - x
        - '111'
        - ''
        ssl-cert:
        - x
        - '114'
        - ''
        staff:
        - x
        - '50'
        - ''
        sudo:
        - x
        - '27'
        - koumbit,chris,decentral1se
        sys:
        - x
        - '3'
        - ''
        syslog:
        - x
        - '108'
        - ''
        systemd-bus-proxy:
        - x
        - '105'
        - ''
        systemd-journal:
        - x
        - '101'
        - ''
        systemd-network:
        - x
        - '103'
        - ''
        systemd-resolve:
        - x
        - '104'
        - ''
        systemd-timesync:
        - x
        - '102'
        - ''
        tape:
        - x
        - '26'
        - ''
        tty:
        - x
        - '5'
        - ''
        users:
        - x
        - '100'
        - ''
        utmp:
        - x
        - '43'
        - ''
        uucp:
        - x
        - '10'
        - ''
        video:
        - x
        - '44'
        - ''
        voice:
        - x
        - '22'
        - ''
        www-data:
        - x
        - '33'
        - ''
    getent_hosts:
        127.0.0.1:
        - localhost
        - ip6-localhost
        - ip6-loopback
        199.58.80.6:
        - faiserver0.koumbit.net
    getent_passwd:
        Debian-exim:
        - x
        - '108'
        - '112'
        - ''
        - /var/spool/exim4
        - /bin/false
        _apt:
        - x
        - '105'
        - '65534'
        - ''
        - /nonexistent
        - /bin/false
        backup:
        - x
        - '34'
        - '34'
        - backup
        - /var/backups
        - /usr/sbin/nologin
        bigbluebutton:
        - x
        - '998'
        - '997'
        - bigbluebutton
        - /home/bigbluebutton
        - /bin/false
        bin:
        - x
        - '2'
        - '2'
        - bin
        - /bin
        - /usr/sbin/nologin
        chris:
        - x
        - '1001'
        - '1001'
        - ''
        - /home/chris
        - /bin/bash
        colord:
        - x
        - '111'
        - '117'
        - colord colour management daemon,,,
        - /var/lib/colord
        - /bin/false
        daemon:
        - x
        - '1'
        - '1'
        - daemon
        - /usr/sbin
        - /usr/sbin/nologin
        decentral1se:
        - x
        - '1002'
        - '1002'
        - ''
        - /home/decentral1se
        - /bin/bash
        etherpad:
        - x
        - '995'
        - '994'
        - etherpad user-daemon
        - /usr/share/etherpad-lite
        - /bin/false
        freeswitch:
        - x
        - '997'
        - '996'
        - freeswitch
        - /opt/freeswitch
        - /bin/bash
        games:
        - x
        - '5'
        - '60'
        - games
        - /usr/games
        - /usr/sbin/nologin
        gnats:
        - x
        - '41'
        - '41'
        - Gnats Bug-Reporting System (admin)
        - /var/lib/gnats
        - /usr/sbin/nologin
        irc:
        - x
        - '39'
        - '39'
        - ircd
        - /var/run/ircd
        - /usr/sbin/nologin
        koumbit:
        - x
        - '1000'
        - '1000'
        - Koumbit sysadmin!,,,
        - /home/koumbit
        - /bin/bash
        kurento:
        - x
        - '996'
        - '995'
        - ''
        - /var/lib/kurento
        - ''
        list:
        - x
        - '38'
        - '38'
        - Mailing List Manager
        - /var/list
        - /usr/sbin/nologin
        lp:
        - x
        - '7'
        - '7'
        - lp
        - /var/spool/lpd
        - /usr/sbin/nologin
        mail:
        - x
        - '8'
        - '8'
        - mail
        - /var/mail
        - /usr/sbin/nologin
        man:
        - x
        - '6'
        - '12'
        - man
        - /var/cache/man
        - /usr/sbin/nologin
        messagebus:
        - x
        - '106'
        - '110'
        - ''
        - /var/run/dbus
        - /bin/false
        meteor:
        - x
        - '994'
        - '993'
        - meteor user-daemon
        - /usr/share/meteor
        - /bin/bash
        mongodb:
        - x
        - '109'
        - '65534'
        - ''
        - /home/mongodb
        - /bin/false
        munin:
        - x
        - '110'
        - '115'
        - munin application user,,,
        - /var/lib/munin
        - /bin/false
        news:
        - x
        - '9'
        - '9'
        - news
        - /var/spool/news
        - /usr/sbin/nologin
        nobody:
        - x
        - '65534'
        - '65534'
        - nobody
        - /nonexistent
        - /usr/sbin/nologin
        proxy:
        - x
        - '13'
        - '13'
        - proxy
        - /bin
        - /usr/sbin/nologin
        red5:
        - x
        - '999'
        - '998'
        - red5 user-daemon
        - /usr/share/red5
        - /bin/false
        redis:
        - x
        - '112'
        - '118'
        - ''
        - /var/lib/redis
        - /bin/false
        root:
        - x
        - '0'
        - '0'
        - root
        - /root
        - /bin/bash
        sshd:
        - x
        - '107'
        - '65534'
        - ''
        - /var/run/sshd
        - /usr/sbin/nologin
        sync:
        - x
        - '4'
        - '65534'
        - sync
        - /bin
        - /bin/sync
        sys:
        - x
        - '3'
        - '3'
        - sys
        - /dev
        - /usr/sbin/nologin
        syslog:
        - x
        - '104'
        - '108'
        - ''
        - /home/syslog
        - /bin/false
        systemd-bus-proxy:
        - x
        - '103'
        - '105'
        - systemd Bus Proxy,,,
        - /run/systemd
        - /bin/false
        systemd-network:
        - x
        - '101'
        - '103'
        - systemd Network Management,,,
        - /run/systemd/netif
        - /bin/false
        systemd-resolve:
        - x
        - '102'
        - '104'
        - systemd Resolver,,,
        - /run/systemd/resolve
        - /bin/false
        systemd-timesync:
        - x
        - '100'
        - '102'
        - systemd Time Synchronization,,,
        - /run/systemd
        - /bin/false
        uucp:
        - x
        - '10'
        - '10'
        - uucp
        - /var/spool/uucp
        - /usr/sbin/nologin
        www-data:
        - x
        - '33'
        - '33'
        - www-data
        - /var/www
        - /usr/sbin/nologin
    getent_services:
        acr-nema:
        - 104/udp
        - dicom
        afbackup:
        - 2988/udp
        afmbackup:
        - 2989/udp
        afpovertcp:
        - 548/udp
        afs3-bos:
        - 7007/udp
        afs3-callback:
        - 7001/udp
        afs3-errors:
        - 7006/udp
        afs3-fileserver:
        - 7000/udp
        - bbs
        afs3-kaserver:
        - 7004/udp
        afs3-prserver:
        - 7002/udp
        afs3-rmtsys:
        - 7009/udp
        afs3-update:
        - 7008/udp
        afs3-vlserver:
        - 7003/udp
        afs3-volser:
        - 7005/udp
        amanda:
        - 10080/udp
        amandaidx:
        - 10082/tcp
        amidxtape:
        - 10083/tcp
        amqp:
        - 5672/sctp
        amqps:
        - 5671/tcp
        aol:
        - 5190/udp
        asf-rmcp:
        - 623/udp
        asp:
        - 27374/udp
        at-echo:
        - 204/udp
        at-nbp:
        - 202/udp
        at-rtmp:
        - 201/udp
        at-zis:
        - 206/udp
        auth:
        - 113/tcp
        - authentication
        - tap
        - ident
        bacula-dir:
        - 9101/udp
        bacula-fd:
        - 9102/udp
        bacula-sd:
        - 9103/udp
        bgp:
        - 179/udp
        bgpd:
        - 2605/tcp
        bgpsim:
        - 5675/tcp
        biff:
        - 512/udp
        - comsat
        binkp:
        - 24554/tcp
        bootpc:
        - 68/udp
        bootps:
        - 67/udp
        bpcd:
        - 13782/udp
        bpdbm:
        - 13721/udp
        bpjava-msvc:
        - 13722/udp
        bprd:
        - 13720/udp
        canna:
        - 5680/tcp
        cfengine:
        - 5308/udp
        cfinger:
        - 2003/tcp
        chargen:
        - 19/udp
        - ttytst
        - source
        cisco-sccp:
        - 2000/udp
        clc-build-daemon:
        - 8990/tcp
        clearcase:
        - 371/udp
        - Clearcase
        cmip-agent:
        - 164/udp
        cmip-man:
        - 163/udp
        codaauth2:
        - 370/udp
        codasrv:
        - 2432/udp
        codasrv-se:
        - 2433/udp
        conference:
        - 531/tcp
        - chat
        courier:
        - 530/tcp
        - rpc
        csnet-ns:
        - 105/udp
        - cso-ns
        csync2:
        - 30865/tcp
        customs:
        - 1001/udp
        cvspserver:
        - 2401/udp
        daap:
        - 3689/udp
        datametrics:
        - 1645/udp
        - old-radius
        daytime:
        - 13/udp
        db-lsp:
        - 17500/tcp
        dcap:
        - 22125/tcp
        dhcpv6-client:
        - 546/udp
        dhcpv6-server:
        - 547/udp
        dicom:
        - 11112/tcp
        dict:
        - 2628/udp
        dircproxy:
        - 57000/tcp
        discard:
        - 9/udp
        - sink
        - 'null'
        distcc:
        - 3632/udp
        distmp3:
        - 4600/tcp
        domain:
        - 53/udp
        echo:
        - 4/ddp
        eklogin:
        - 2105/tcp
        enbd-cstatd:
        - 5051/tcp
        enbd-sstatd:
        - 5052/tcp
        epmd:
        - 4369/udp
        exec:
        - 512/tcp
        f5-globalsite:
        - 2792/udp
        f5-iquery:
        - 4353/udp
        fatserv:
        - 347/udp
        fax:
        - 4557/tcp
        fido:
        - 60179/tcp
        finger:
        - 79/tcp
        font-service:
        - 7100/udp
        - xfs
        freeciv:
        - 5556/udp
        frox:
        - 2121/tcp
        fsp:
        - 21/udp
        - fspd
        ftp:
        - 21/tcp
        ftp-data:
        - 20/tcp
        ftps:
        - 990/tcp
        ftps-data:
        - 989/tcp
        gdomap:
        - 538/udp
        gds-db:
        - 3050/udp
        - gds_db
        ggz:
        - 5688/udp
        git:
        - 9418/tcp
        gnunet:
        - 2086/udp
        gnutella-rtr:
        - 6347/udp
        gnutella-svc:
        - 6346/udp
        gopher:
        - 70/udp
        gpsd:
        - 2947/udp
        gris:
        - 2135/udp
        groupwise:
        - 1677/udp
        gsidcap:
        - 22128/tcp
        gsiftp:
        - 2811/udp
        gsigatekeeper:
        - 2119/udp
        hkp:
        - 11371/udp
        hmmp-ind:
        - 612/udp
        - dqs313_intercell
        hostmon:
        - 5355/udp
        hostnames:
        - 101/tcp
        - hostname
        http:
        - 80/udp
        http-alt:
        - 8080/udp
        https:
        - 443/udp
        hylafax:
        - 4559/tcp
        iax:
        - 4569/udp
        icpv2:
        - 3130/udp
        - icp
        idfp:
        - 549/udp
        imap2:
        - 143/udp
        - imap
        imap3:
        - 220/udp
        imaps:
        - 993/udp
        imsp:
        - 406/udp
        ingreslock:
        - 1524/udp
        ipp:
        - 631/udp
        iprop:
        - 2121/tcp
        ipsec-nat-t:
        - 4500/udp
        ipx:
        - 213/udp
        irc:
        - 194/udp
        ircd:
        - 6667/tcp
        ircs:
        - 994/udp
        isakmp:
        - 500/udp
        iscsi-target:
        - 3260/tcp
        isdnlog:
        - 20011/udp
        isisd:
        - 2608/tcp
        iso-tsap:
        - 102/tcp
        - tsap
        kamanda:
        - 10081/udp
        kazaa:
        - 1214/udp
        kerberos:
        - 88/udp
        - kerberos5
        - krb5
        - kerberos-sec
        kerberos-adm:
        - 749/tcp
        kerberos-master:
        - 751/tcp
        kerberos4:
        - 750/tcp
        - kerberos-iv
        - kdc
        kermit:
        - 1649/udp
        klogin:
        - 543/tcp
        knetd:
        - 2053/tcp
        kpasswd:
        - 464/udp
        kpop:
        - 1109/tcp
        krb-prop:
        - 754/tcp
        - krb_prop
        - krb5_prop
        - hprop
        krbupdate:
        - 760/tcp
        - kreg
        kshell:
        - 544/tcp
        - krcmd
        kx:
        - 2111/tcp
        l2f:
        - 1701/udp
        - l2tp
        ldap:
        - 389/udp
        ldaps:
        - 636/udp
        link:
        - 87/tcp
        - ttylink
        linuxconf:
        - 98/tcp
        loc-srv:
        - 135/udp
        - epmap
        log-server:
        - 1958/tcp
        login:
        - 513/tcp
        lotusnote:
        - 1352/udp
        - lotusnotes
        mailq:
        - 174/udp
        mandelspawn:
        - 9359/udp
        - mandelbrot
        mdns:
        - 5353/udp
        microsoft-ds:
        - 445/udp
        mmcc:
        - 5050/udp
        moira-db:
        - 775/tcp
        - moira_db
        moira-update:
        - 777/tcp
        - moira_update
        moira-ureg:
        - 779/udp
        - moira_ureg
        mon:
        - 2583/udp
        mrtd:
        - 5674/tcp
        ms-sql-m:
        - 1434/udp
        ms-sql-s:
        - 1433/udp
        msnp:
        - 1863/udp
        msp:
        - 18/udp
        mtn:
        - 4691/udp
        mtp:
        - 57/tcp
        munin:
        - 4949/tcp
        - lrrd
        mysql:
        - 3306/udp
        mysql-proxy:
        - 6446/udp
        nameserver:
        - 42/tcp
        - name
        nbd:
        - 10809/tcp
        nbp:
        - 2/ddp
        nessus:
        - 1241/udp
        netbios-dgm:
        - 138/udp
        netbios-ns:
        - 137/udp
        netbios-ssn:
        - 139/udp
        netnews:
        - 532/tcp
        - readnews
        netstat:
        - 15/tcp
        netwall:
        - 533/udp
        nextstep:
        - 178/udp
        - NeXTStep
        - NextStep
        nfs:
        - 2049/udp
        ninstall:
        - 2150/udp
        nntp:
        - 119/tcp
        - readnews
        - untp
        nntps:
        - 563/udp
        - snntp
        noclog:
        - 5354/udp
        npmp-gui:
        - 611/udp
        - dqs313_execd
        npmp-local:
        - 610/udp
        - dqs313_qmaster
        nqs:
        - 607/udp
        nrpe:
        - 5666/tcp
        nsca:
        - 5667/tcp
        ntalk:
        - 518/udp
        ntp:
        - 123/udp
        nut:
        - 3493/udp
        omirr:
        - 808/udp
        - omirrd
        omniorb:
        - 8088/udp
        openvpn:
        - 1194/udp
        ospf6d:
        - 2606/tcp
        ospfapi:
        - 2607/tcp
        ospfd:
        - 2604/tcp
        passwd-server:
        - 752/udp
        - passwd_server
        pawserv:
        - 345/udp
        pcrd:
        - 5151/tcp
        pipe-server:
        - 2010/tcp
        - pipe_server
        pop2:
        - 109/udp
        - pop-2
        pop3:
        - 110/udp
        - pop-3
        pop3s:
        - 995/udp
        poppassd:
        - 106/udp
        postgresql:
        - 5432/udp
        - postgres
        predict:
        - 1210/udp
        printer:
        - 515/tcp
        - spooler
        proofd:
        - 1093/udp
        prospero:
        - 191/udp
        prospero-np:
        - 1525/udp
        pwdgen:
        - 129/udp
        qmqp:
        - 628/udp
        qmtp:
        - 209/udp
        qotd:
        - 17/tcp
        - quote
        radius:
        - 1812/udp
        radius-acct:
        - 1813/udp
        - radacct
        radmin-port:
        - 4899/udp
        re-mail-ck:
        - 50/udp
        remctl:
        - 4373/udp
        remotefs:
        - 556/tcp
        - rfs_server
        - rfs
        remoteping:
        - 1959/tcp
        rfe:
        - 5002/tcp
        ripd:
        - 2602/tcp
        ripngd:
        - 2603/tcp
        rje:
        - 77/tcp
        - netrjs
        rlp:
        - 39/udp
        - resource
        rmiregistry:
        - 1099/udp
        rmtcfg:
        - 1236/tcp
        rootd:
        - 1094/udp
        route:
        - 520/udp
        - router
        - routed
        rpc2portmap:
        - 369/udp
        rplay:
        - 5555/udp
        rsync:
        - 873/udp
        rtcm-sc104:
        - 2101/udp
        rtelnet:
        - 107/udp
        rtmp:
        - 1/ddp
        rtsp:
        - 554/udp
        sa-msg-port:
        - 1646/udp
        - old-radacct
        saft:
        - 487/udp
        sane-port:
        - 6566/tcp
        - sane
        - saned
        search:
        - 2010/tcp
        - ndtp
        sftp:
        - 115/tcp
        sge-execd:
        - 6445/udp
        - sge_execd
        sge-qmaster:
        - 6444/udp
        - sge_qmaster
        sgi-cad:
        - 17004/tcp
        sgi-cmsd:
        - 17001/udp
        sgi-crsd:
        - 17002/udp
        sgi-gcd:
        - 17003/udp
        shell:
        - 514/tcp
        - cmd
        sieve:
        - 4190/tcp
        silc:
        - 706/udp
        sip:
        - 5060/udp
        sip-tls:
        - 5061/udp
        skkserv:
        - 1178/tcp
        smsqp:
        - 11201/udp
        smtp:
        - 25/tcp
        - mail
        smux:
        - 199/udp
        snmp:
        - 161/udp
        snmp-trap:
        - 162/udp
        - snmptrap
        snpp:
        - 444/udp
        socks:
        - 1080/udp
        spamd:
        - 783/tcp
        ssh:
        - 22/udp
        submission:
        - 587/udp
        sunrpc:
        - 111/udp
        - portmapper
        supdup:
        - 95/tcp
        supfiledbg:
        - 1127/tcp
        supfilesrv:
        - 871/tcp
        support:
        - 1529/tcp
        suucp:
        - 4031/udp
        svn:
        - 3690/udp
        - subversion
        svrloc:
        - 427/udp
        swat:
        - 901/tcp
        syslog:
        - 514/udp
        syslog-tls:
        - 6514/tcp
        sysrqd:
        - 4094/udp
        systat:
        - 11/tcp
        - users
        tacacs:
        - 49/udp
        tacacs-ds:
        - 65/udp
        talk:
        - 517/udp
        tcpmux:
        - 1/tcp
        telnet:
        - 23/tcp
        telnets:
        - 992/udp
        tempo:
        - 526/tcp
        - newdate
        tfido:
        - 60177/tcp
        tftp:
        - 69/udp
        time:
        - 37/udp
        - timserver
        timed:
        - 525/udp
        - timeserver
        tinc:
        - 655/udp
        tproxy:
        - 8081/tcp
        ulistserv:
        - 372/udp
        unix-status:
        - 1957/tcp
        urd:
        - 465/tcp
        - ssmtp
        - smtps
        uucp:
        - 540/tcp
        - uucpd
        uucp-path:
        - 117/tcp
        vboxd:
        - 20012/udp
        venus:
        - 2430/udp
        venus-se:
        - 2431/udp
        vnetd:
        - 13724/udp
        vopied:
        - 13783/udp
        webmin:
        - 10000/tcp
        webster:
        - 765/udp
        who:
        - 513/udp
        - whod
        whois:
        - 43/tcp
        - nicname
        wipld:
        - 1300/tcp
        wnn6:
        - 22273/udp
        x11:
        - 6000/udp
        - x11-0
        x11-1:
        - 6001/udp
        x11-2:
        - 6002/udp
        x11-3:
        - 6003/udp
        x11-4:
        - 6004/udp
        x11-5:
        - 6005/udp
        x11-6:
        - 6006/udp
        x11-7:
        - 6007/udp
        xdmcp:
        - 177/udp
        xinetd:
        - 9098/tcp
        xmms2:
        - 9667/udp
        xmpp-client:
        - 5222/udp
        - jabber-client
        xmpp-server:
        - 5269/udp
        - jabber-server
        xpilot:
        - 15345/udp
        xtel:
        - 1313/tcp
        xtell:
        - 4224/tcp
        xtelw:
        - 1314/tcp
        z3950:
        - 210/udp
        - wais
        zabbix-agent:
        - 10050/udp
        zabbix-trapper:
        - 10051/udp
        zebra:
        - 2601/tcp
        zebrasrv:
        - 2600/tcp
        zephyr-clt:
        - 2103/udp
        zephyr-hm:
        - 2104/udp
        zephyr-srv:
        - 2102/udp
        zip:
        - 6/ddp
        zope:
        - 9673/tcp
        zope-ftp:
        - 8021/tcp
        zserv:
        - 346/udp
    hostname: meet-coop0
    hostnqn: ''
    interfaces:
    - eno1
    - lo
    - br-5391ff83ad91
    - docker0
    - vetheee40b5
    - eno2
    - vethc060a5f
    is_chroot: false
    iscsi_iqn: ''
    kernel: 4.4.0-179-generic
    kernel_version: '#209-Ubuntu SMP Fri Apr 24 17:48:44 UTC 2020'
    lo:
        active: true
        device: lo
        ipv4:
            address: 127.0.0.1
            broadcast: host
            netmask: 255.0.0.0
            network: 127.0.0.0
        ipv6:
        -   address: ::1
            prefix: '128'
            scope: host
        mtu: 65536
        promisc: false
        type: loopback
    lsb:
        codename: xenial
        description: Ubuntu 16.04.6 LTS
        id: Ubuntu
        major_release: '16'
        release: '16.04'
    lvm:
        lvs:
            slash:
                size_g: '25.00'
                vg: root
            swap:
                size_g: '8.00'
                vg: root
        pvs:
            /dev/mapper/crypt_dev_md1:
                free_g: '1.96'
                size_g: '34.96'
                vg: root
            /dev/mapper/crypt_dev_md2:
                free_g: '917.74'
                size_g: '917.74'
                vg: data
            /dev/mapper/crypt_dev_md4:
                free_g: '3723.89'
                size_g: '3723.89'
                vg: data2
        vgs:
            data:
                free_g: '917.74'
                num_lvs: '0'
                num_pvs: '1'
                size_g: '917.74'
            data2:
                free_g: '3723.89'
                num_lvs: '0'
                num_pvs: '1'
                size_g: '3723.89'
            root:
                free_g: '1.96'
                num_lvs: '2'
                num_pvs: '1'
                size_g: '34.96'
    machine: x86_64
    machine_id: d6a7fa74a5714d20990abf3bb1fba43f
    memfree_mb: 85871
    memory_mb:
        nocache:
            free: 92295
            used: 3991
        real:
            free: 85871
            total: 96286
            used: 10415
        swap:
            cached: 0
            free: 8191
            total: 8191
            used: 0
    memtotal_mb: 96286
    module_setup: true
    mounts:
    -   block_available: 229873
        block_size: 4096
        block_total: 257766
        block_used: 27893
        device: /dev/md0
        fstype: ext2
        inode_available: 65238
        inode_total: 65536
        inode_used: 298
        mount: /boot
        options: rw,noatime
        size_available: 941559808
        size_total: 1055809536
        uuid: 27484465-4af1-4e52-a67d-6b142b00bde5
    -   block_available: 4285283
        block_size: 4096
        block_total: 6417977
        block_used: 2132694
        device: /dev/mapper/root-slash
        fstype: ext4
        inode_available: 1451079
        inode_total: 1638400
        inode_used: 187321
        mount: /
        options: rw,noatime,errors=remount-ro,data=ordered
        size_available: 17552519168
        size_total: 26288033792
        uuid: 6a037d59-4597-46bb-91d5-b28dd907d7a8
    nodename: meet-coop0
    os_family: Debian
    packages:
        acl:
        -   arch: amd64
            category: utils
            name: acl
            origin: Ubuntu
            source: apt
            version: 2.2.52-3
        adduser:
        -   arch: all
            category: admin
            name: adduser
            origin: Ubuntu
            source: apt
            version: 3.113+nmu3ubuntu4
        adwaita-icon-theme:
        -   arch: all
            category: gnome
            name: adwaita-icon-theme
            origin: Ubuntu
            source: apt
            version: 3.18.0-2ubuntu3.1
        aglfn:
        -   arch: all
            category: universe/fonts
            name: aglfn
            origin: Ubuntu
            source: apt
            version: 1.7-3
        amd64-microcode:
        -   arch: amd64
            category: admin
            name: amd64-microcode
            origin: Ubuntu
            source: apt
            version: 3.20191021.1+really3.20180524.1~ubuntu0.16.04.2
        apparmor:
        -   arch: amd64
            category: admin
            name: apparmor
            origin: Ubuntu
            source: apt
            version: 2.10.95-0ubuntu2.11
        apt:
        -   arch: amd64
            category: admin
            name: apt
            origin: Ubuntu
            source: apt
            version: 1.2.32ubuntu0.1
        apt-listchanges:
        -   arch: all
            category: utils
            name: apt-listchanges
            origin: Ubuntu
            source: apt
            version: 2.85.14ubuntu1
        apt-show-versions:
        -   arch: all
            category: universe/admin
            name: apt-show-versions
            origin: Ubuntu
            source: apt
            version: 0.22.7
        apt-transport-https:
        -   arch: amd64
            category: admin
            name: apt-transport-https
            origin: Ubuntu
            source: apt
            version: 1.2.32ubuntu0.1
        apt-utils:
        -   arch: amd64
            category: admin
            name: apt-utils
            origin: Ubuntu
            source: apt
            version: 1.2.32ubuntu0.1
        apticron:
        -   arch: all
            category: universe/admin
            name: apticron
            origin: Ubuntu
            source: apt
            version: 1.1.58ubuntu1
        aptitude:
        -   arch: amd64
            category: admin
            name: aptitude
            origin: Ubuntu
            source: apt
            version: 0.7.4-2ubuntu2
        aptitude-common:
        -   arch: all
            category: admin
            name: aptitude-common
            origin: Ubuntu
            source: apt
            version: 0.7.4-2ubuntu2
        at-spi2-core:
        -   arch: amd64
            category: misc
            name: at-spi2-core
            origin: Ubuntu
            source: apt
            version: 2.18.3-4ubuntu1
        aufs-tools:
        -   arch: amd64
            category: universe/kernel
            name: aufs-tools
            origin: Ubuntu
            source: apt
            version: 1:3.2+20130722-1.1ubuntu1
        base-files:
        -   arch: amd64
            category: admin
            name: base-files
            origin: Ubuntu
            source: apt
            version: 9.4ubuntu4.11
        base-passwd:
        -   arch: amd64
            category: admin
            name: base-passwd
            origin: Ubuntu
            source: apt
            version: 3.5.39
        bash:
        -   arch: amd64
            category: shells
            name: bash
            origin: Ubuntu
            source: apt
            version: 4.3-14ubuntu1.4
        bbb-apps:
        -   arch: amd64
            category: default
            name: bbb-apps
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-45
        bbb-apps-akka:
        -   arch: all
            category: java
            name: bbb-apps-akka
            origin: BigBlueButton
            source: apt
            version: 2.2.0-87
        bbb-apps-screenshare:
        -   arch: amd64
            category: default
            name: bbb-apps-screenshare
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-45
        bbb-apps-sip:
        -   arch: amd64
            category: default
            name: bbb-apps-sip
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-11
        bbb-apps-video:
        -   arch: amd64
            category: default
            name: bbb-apps-video
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-42
        bbb-apps-video-broadcast:
        -   arch: amd64
            category: default
            name: bbb-apps-video-broadcast
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-8
        bbb-client:
        -   arch: amd64
            category: default
            name: bbb-client
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-40
        bbb-config:
        -   arch: amd64
            category: default
            name: bbb-config
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-189
        bbb-etherpad:
        -   arch: amd64
            category: default
            name: bbb-etherpad
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-51
        bbb-freeswitch-core:
        -   arch: amd64
            category: default
            name: bbb-freeswitch-core
            origin: BigBlueButton
            source: apt
            version: 2:2.2.0-112
        bbb-freeswitch-sounds:
        -   arch: amd64
            category: default
            name: bbb-freeswitch-sounds
            origin: BigBlueButton
            source: apt
            version: 1:1.6.7-6
        bbb-fsesl-akka:
        -   arch: all
            category: java
            name: bbb-fsesl-akka
            origin: BigBlueButton
            source: apt
            version: 2.2.0-65
        bbb-html5:
        -   arch: amd64
            category: default
            name: bbb-html5
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-934
        bbb-mkclean:
        -   arch: amd64
            category: default
            name: bbb-mkclean
            origin: BigBlueButton
            source: apt
            version: 1:0.8.7-3
        bbb-playback-presentation:
        -   arch: amd64
            category: default
            name: bbb-playback-presentation
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-30
        bbb-record-core:
        -   arch: amd64
            category: default
            name: bbb-record-core
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-66
        bbb-red5:
        -   arch: amd64
            category: default
            name: bbb-red5
            origin: BigBlueButton
            source: apt
            version: 1:1.0.10-16
        bbb-transcode-akka:
        -   arch: all
            category: java
            name: bbb-transcode-akka
            origin: BigBlueButton
            source: apt
            version: 2.2.0-8
        bbb-web:
        -   arch: amd64
            category: default
            name: bbb-web
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-130
        bbb-webrtc-sfu:
        -   arch: amd64
            category: default
            name: bbb-webrtc-sfu
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-99
        bc:
        -   arch: amd64
            category: math
            name: bc
            origin: Ubuntu
            source: apt
            version: 1.06.95-9build1
        bigbluebutton:
        -   arch: amd64
            category: default
            name: bigbluebutton
            origin: BigBlueButton
            source: apt
            version: 1:2.2.0-5
        bind9-host:
        -   arch: amd64
            category: net
            name: bind9-host
            origin: Ubuntu
            source: apt
            version: 1:9.10.3.dfsg.P4-8ubuntu1.16
        binutils:
        -   arch: amd64
            category: devel
            name: binutils
            origin: Ubuntu
            source: apt
            version: 2.26.1-1ubuntu1~16.04.8
        bsdutils:
        -   arch: amd64
            category: utils
            name: bsdutils
            origin: Ubuntu
            source: apt
            version: 1:2.27.1-6ubuntu3.10
        build-essential:
        -   arch: amd64
            category: devel
            name: build-essential
            origin: Ubuntu
            source: apt
            version: 12.1ubuntu2
        busybox-initramfs:
        -   arch: amd64
            category: shells
            name: busybox-initramfs
            origin: Ubuntu
            source: apt
            version: 1:1.22.0-15ubuntu1.4
        bzip2:
        -   arch: amd64
            category: utils
            name: bzip2
            origin: Ubuntu
            source: apt
            version: 1.0.6-8ubuntu0.2
        ca-certificates:
        -   arch: all
            category: misc
            name: ca-certificates
            origin: Ubuntu
            source: apt
            version: 20190110~16.04.1
        ca-certificates-java:
        -   arch: all
            category: misc
            name: ca-certificates-java
            origin: Ubuntu
            source: apt
            version: 20160321ubuntu1
        cabextract:
        -   arch: amd64
            category: universe/utils
            name: cabextract
            origin: Ubuntu
            source: apt
            version: 1.6-1
        cgroupfs-mount:
        -   arch: all
            category: universe/admin
            name: cgroupfs-mount
            origin: Ubuntu
            source: apt
            version: '1.2'
        colord:
        -   arch: amd64
            category: graphics
            name: colord
            origin: Ubuntu
            source: apt
            version: 1.2.12-1ubuntu1
        colord-data:
        -   arch: all
            category: graphics
            name: colord-data
            origin: Ubuntu
            source: apt
            version: 1.2.12-1ubuntu1
        console-setup:
        -   arch: all
            category: utils
            name: console-setup
            origin: Ubuntu
            source: apt
            version: 1.108ubuntu15.5
        console-setup-linux:
        -   arch: all
            category: utils
            name: console-setup-linux
            origin: Ubuntu
            source: apt
            version: 1.108ubuntu15.5
        containerd.io:
        -   arch: amd64
            category: devel
            name: containerd.io
            origin: Docker
            source: apt
            version: 1.2.13-2
        coreutils:
        -   arch: amd64
            category: utils
            name: coreutils
            origin: Ubuntu
            source: apt
            version: 8.25-2ubuntu3~16.04
        cpio:
        -   arch: amd64
            category: utils
            name: cpio
            origin: Ubuntu
            source: apt
            version: 2.11+dfsg-5ubuntu1.1
        cpp:
        -   arch: amd64
            category: interpreters
            name: cpp
            origin: Ubuntu
            source: apt
            version: 4:5.3.1-1ubuntu1
        cpp-5:
        -   arch: amd64
            category: interpreters
            name: cpp-5
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        crda:
        -   arch: amd64
            category: net
            name: crda
            origin: Ubuntu
            source: apt
            version: 3.13-1
        cron:
        -   arch: amd64
            category: admin
            name: cron
            origin: Ubuntu
            source: apt
            version: 3.0pl1-128ubuntu2
        cryptsetup:
        -   arch: amd64
            category: admin
            name: cryptsetup
            origin: Ubuntu
            source: apt
            version: 2:1.6.6-5ubuntu2.1
        cryptsetup-bin:
        -   arch: amd64
            category: admin
            name: cryptsetup-bin
            origin: Ubuntu
            source: apt
            version: 2:1.6.6-5ubuntu2.1
        curl:
        -   arch: amd64
            category: web
            name: curl
            origin: Ubuntu
            source: apt
            version: 7.47.0-1ubuntu2.14
        dash:
        -   arch: amd64
            category: shells
            name: dash
            origin: Ubuntu
            source: apt
            version: 0.5.8-2.1ubuntu2
        dbus:
        -   arch: amd64
            category: devel
            name: dbus
            origin: Ubuntu
            source: apt
            version: 1.10.6-1ubuntu3.5
        dconf-gsettings-backend:
        -   arch: amd64
            category: libs
            name: dconf-gsettings-backend
            origin: Ubuntu
            source: apt
            version: 0.24.0-2
        dconf-service:
        -   arch: amd64
            category: libs
            name: dconf-service
            origin: Ubuntu
            source: apt
            version: 0.24.0-2
        debconf:
        -   arch: all
            category: admin
            name: debconf
            origin: Ubuntu
            source: apt
            version: 1.5.58ubuntu2
        debconf-i18n:
        -   arch: all
            category: admin
            name: debconf-i18n
            origin: Ubuntu
            source: apt
            version: 1.5.58ubuntu2
        debconf-utils:
        -   arch: all
            category: universe/devel
            name: debconf-utils
            origin: Ubuntu
            source: apt
            version: 1.5.58ubuntu2
        debianutils:
        -   arch: amd64
            category: utils
            name: debianutils
            origin: Ubuntu
            source: apt
            version: '4.7'
        default-jre:
        -   arch: amd64
            category: interpreters
            name: default-jre
            origin: Ubuntu
            source: apt
            version: 2:1.8-56ubuntu2
        default-jre-headless:
        -   arch: amd64
            category: interpreters
            name: default-jre-headless
            origin: Ubuntu
            source: apt
            version: 2:1.8-56ubuntu2
        dh-python:
        -   arch: all
            category: python
            name: dh-python
            origin: Ubuntu
            source: apt
            version: 2.20151103ubuntu1.2
        dictionaries-common:
        -   arch: all
            category: text
            name: dictionaries-common
            origin: Ubuntu
            source: apt
            version: 1.26.3
        diffutils:
        -   arch: amd64
            category: utils
            name: diffutils
            origin: Ubuntu
            source: apt
            version: 1:3.3-3
        dirmngr:
        -   arch: amd64
            category: utils
            name: dirmngr
            origin: Ubuntu
            source: apt
            version: 2.1.11-6ubuntu2.1
        distro-info-data:
        -   arch: all
            category: devel
            name: distro-info-data
            origin: Ubuntu
            source: apt
            version: 0.28ubuntu0.14
        dmeventd:
        -   arch: amd64
            category: admin
            name: dmeventd
            origin: Ubuntu
            source: apt
            version: 2:1.02.110-1ubuntu10
        dmsetup:
        -   arch: amd64
            category: admin
            name: dmsetup
            origin: Ubuntu
            source: apt
            version: 2:1.02.110-1ubuntu10
        dnsutils:
        -   arch: amd64
            category: net
            name: dnsutils
            origin: Ubuntu
            source: apt
            version: 1:9.10.3.dfsg.P4-8ubuntu1.16
        docker-ce:
        -   arch: amd64
            category: admin
            name: docker-ce
            origin: Docker
            source: apt
            version: 5:19.03.11~3-0~ubuntu-xenial
        docker-ce-cli:
        -   arch: amd64
            category: admin
            name: docker-ce-cli
            origin: Docker
            source: apt
            version: 5:19.03.11~3-0~ubuntu-xenial
        dpkg:
        -   arch: amd64
            category: admin
            name: dpkg
            origin: Ubuntu
            source: apt
            version: 1.18.4ubuntu1.6
        dpkg-dev:
        -   arch: all
            category: utils
            name: dpkg-dev
            origin: Ubuntu
            source: apt
            version: 1.18.4ubuntu1.6
        dstat:
        -   arch: all
            category: universe/admin
            name: dstat
            origin: Ubuntu
            source: apt
            version: 0.7.2-4
        e2fslibs:
        -   arch: amd64
            category: libs
            name: e2fslibs
            origin: Ubuntu
            source: apt
            version: 1.42.13-1ubuntu1.2
        e2fsprogs:
        -   arch: amd64
            category: admin
            name: e2fsprogs
            origin: Ubuntu
            source: apt
            version: 1.42.13-1ubuntu1.2
        eject:
        -   arch: amd64
            category: utils
            name: eject
            origin: Ubuntu
            source: apt
            version: 2.1.5+deb1+cvs20081104-13.1ubuntu0.16.04.1
        emacsen-common:
        -   arch: all
            category: editors
            name: emacsen-common
            origin: Ubuntu
            source: apt
            version: 2.0.8
        esound-common:
        -   arch: all
            category: universe/sound
            name: esound-common
            origin: Ubuntu
            source: apt
            version: 0.2.41-11
        etckeeper:
        -   arch: all
            category: admin
            name: etckeeper
            origin: Ubuntu
            source: apt
            version: 1.18.2-1ubuntu1
        exim4-base:
        -   arch: amd64
            category: mail
            name: exim4-base
            origin: Ubuntu
            source: apt
            version: 4.86.2-2ubuntu2.6
        exim4-config:
        -   arch: all
            category: mail
            name: exim4-config
            origin: Ubuntu
            source: apt
            version: 4.86.2-2ubuntu2.6
        exim4-daemon-light:
        -   arch: amd64
            category: mail
            name: exim4-daemon-light
            origin: Ubuntu
            source: apt
            version: 4.86.2-2ubuntu2.6
        fakeroot:
        -   arch: amd64
            category: utils
            name: fakeroot
            origin: Ubuntu
            source: apt
            version: 1.20.2-1ubuntu1
        ffmpeg:
        -   arch: amd64
            category: video
            name: ffmpeg
            origin: LP-PPA-bigbluebutton-support
            source: apt
            version: 7:4.2.2-1bbb1~ubuntu16.04
        file:
        -   arch: amd64
            category: utils
            name: file
            origin: Ubuntu
            source: apt
            version: 1:5.25-2ubuntu1.4
        findutils:
        -   arch: amd64
            category: utils
            name: findutils
            origin: Ubuntu
            source: apt
            version: 4.6.0+git+20160126-2
        fontconfig:
        -   arch: amd64
            category: utils
            name: fontconfig
            origin: Ubuntu
            source: apt
            version: 2.11.94-0ubuntu1.1
        fontconfig-config:
        -   arch: all
            category: libs
            name: fontconfig-config
            origin: Ubuntu
            source: apt
            version: 2.11.94-0ubuntu1.1
        fonts-crosextra-caladea:
        -   arch: all
            category: universe/fonts
            name: fonts-crosextra-caladea
            origin: Ubuntu
            source: apt
            version: 20130214-1
        fonts-crosextra-carlito:
        -   arch: all
            category: universe/fonts
            name: fonts-crosextra-carlito
            origin: Ubuntu
            source: apt
            version: 20130920-1
        fonts-dejavu:
        -   arch: all
            category: universe/fonts
            name: fonts-dejavu
            origin: Ubuntu
            source: apt
            version: 2.35-1
        fonts-dejavu-core:
        -   arch: all
            category: fonts
            name: fonts-dejavu-core
            origin: Ubuntu
            source: apt
            version: 2.35-1
        fonts-dejavu-extra:
        -   arch: all
            category: fonts
            name: fonts-dejavu-extra
            origin: Ubuntu
            source: apt
            version: 2.35-1
        fonts-lato:
        -   arch: all
            category: fonts
            name: fonts-lato
            origin: Ubuntu
            source: apt
            version: 2.0-1
        fonts-liberation:
        -   arch: all
            category: fonts
            name: fonts-liberation
            origin: Ubuntu
            source: apt
            version: 1.07.4-1
        fonts-noto:
        -   arch: all
            category: universe/fonts
            name: fonts-noto
            origin: Ubuntu
            source: apt
            version: 20160116-1
        fonts-noto-cjk:
        -   arch: all
            category: fonts
            name: fonts-noto-cjk
            origin: Ubuntu
            source: apt
            version: 1:1.004+repack2-1~ubuntu1
        fonts-noto-hinted:
        -   arch: all
            category: universe/fonts
            name: fonts-noto-hinted
            origin: Ubuntu
            source: apt
            version: 20160116-1
        fonts-noto-mono:
        -   arch: all
            category: universe/fonts
            name: fonts-noto-mono
            origin: Ubuntu
            source: apt
            version: 20160116-1
        fonts-noto-unhinted:
        -   arch: all
            category: universe/fonts
            name: fonts-noto-unhinted
            origin: Ubuntu
            source: apt
            version: 20160116-1
        fonts-opensymbol:
        -   arch: all
            category: fonts
            name: fonts-opensymbol
            origin: Ubuntu
            source: apt
            version: 2:102.7+LibO5.1.6~rc2-0ubuntu1~xenial10
        fonts-sil-gentium:
        -   arch: all
            category: universe/fonts
            name: fonts-sil-gentium
            origin: Ubuntu
            source: apt
            version: 20081126:1.03-1
        fonts-sil-gentium-basic:
        -   arch: all
            category: universe/fonts
            name: fonts-sil-gentium-basic
            origin: Ubuntu
            source: apt
            version: 1.1-7
        fonts-stix:
        -   arch: all
            category: fonts
            name: fonts-stix
            origin: Ubuntu
            source: apt
            version: 1.1.1-4
        freepats:
        -   arch: all
            category: universe/sound
            name: freepats
            origin: Ubuntu
            source: apt
            version: 20060219-1
        g++:
        -   arch: amd64
            category: devel
            name: g++
            origin: Ubuntu
            source: apt
            version: 4:5.3.1-1ubuntu1
        g++-5:
        -   arch: amd64
            category: devel
            name: g++-5
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        gawk:
        -   arch: amd64
            category: interpreters
            name: gawk
            origin: Ubuntu
            source: apt
            version: 1:4.1.3+dfsg-0.1
        gcc:
        -   arch: amd64
            category: devel
            name: gcc
            origin: Ubuntu
            source: apt
            version: 4:5.3.1-1ubuntu1
        gcc-5:
        -   arch: amd64
            category: devel
            name: gcc-5
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        gcc-5-base:
        -   arch: amd64
            category: libs
            name: gcc-5-base
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        gcc-6-base:
        -   arch: amd64
            category: libs
            name: gcc-6-base
            origin: Ubuntu
            source: apt
            version: 6.0.1-0ubuntu1
        geoip-database:
        -   arch: all
            category: net
            name: geoip-database
            origin: Ubuntu
            source: apt
            version: 20160408-1
        gettext-base:
        -   arch: amd64
            category: utils
            name: gettext-base
            origin: Ubuntu
            source: apt
            version: 0.19.7-2ubuntu3.1
        ghostscript:
        -   arch: amd64
            category: text
            name: ghostscript
            origin: Ubuntu
            source: apt
            version: 9.26~dfsg+0-0ubuntu0.16.04.12
        gir1.2-glib-2.0:
        -   arch: amd64
            category: libs
            name: gir1.2-glib-2.0
            origin: Ubuntu
            source: apt
            version: 1.46.0-3ubuntu1
        git:
        -   arch: amd64
            category: vcs
            name: git
            origin: Ubuntu
            source: apt
            version: 1:2.7.4-0ubuntu1.9
        git-man:
        -   arch: all
            category: vcs
            name: git-man
            origin: Ubuntu
            source: apt
            version: 1:2.7.4-0ubuntu1.9
        glib-networking:
        -   arch: amd64
            category: libs
            name: glib-networking
            origin: Ubuntu
            source: apt
            version: 2.48.2-1~ubuntu16.04.1
        glib-networking-common:
        -   arch: all
            category: libs
            name: glib-networking-common
            origin: Ubuntu
            source: apt
            version: 2.48.2-1~ubuntu16.04.1
        glib-networking-services:
        -   arch: amd64
            category: libs
            name: glib-networking-services
            origin: Ubuntu
            source: apt
            version: 2.48.2-1~ubuntu16.04.1
        gnupg:
        -   arch: amd64
            category: utils
            name: gnupg
            origin: Ubuntu
            source: apt
            version: 1.4.20-1ubuntu3.3
        gnupg-agent:
        -   arch: amd64
            category: utils
            name: gnupg-agent
            origin: Ubuntu
            source: apt
            version: 2.1.11-6ubuntu2.1
        gnupg2:
        -   arch: amd64
            category: utils
            name: gnupg2
            origin: Ubuntu
            source: apt
            version: 2.1.11-6ubuntu2.1
        gnuplot:
        -   arch: all
            category: universe/math
            name: gnuplot
            origin: Ubuntu
            source: apt
            version: 4.6.6-3
        gnuplot-tex:
        -   arch: all
            category: universe/doc
            name: gnuplot-tex
            origin: Ubuntu
            source: apt
            version: 4.6.6-3
        gnuplot5-data:
        -   arch: all
            category: universe/doc
            name: gnuplot5-data
            origin: Ubuntu
            source: apt
            version: 5.0.3+dfsg2-1
        gnuplot5-qt:
        -   arch: amd64
            category: universe/math
            name: gnuplot5-qt
            origin: Ubuntu
            source: apt
            version: 5.0.3+dfsg2-1
        gpgv:
        -   arch: amd64
            category: utils
            name: gpgv
            origin: Ubuntu
            source: apt
            version: 1.4.20-1ubuntu3.3
        grep:
        -   arch: amd64
            category: utils
            name: grep
            origin: Ubuntu
            source: apt
            version: 2.25-1~16.04.1
        grub-common:
        -   arch: amd64
            category: admin
            name: grub-common
            origin: Ubuntu
            source: apt
            version: 2.02~beta2-36ubuntu3.23
        grub-gfxpayload-lists:
        -   arch: amd64
            category: admin
            name: grub-gfxpayload-lists
            origin: Ubuntu
            source: apt
            version: '0.7'
        grub-pc:
        -   arch: amd64
            category: admin
            name: grub-pc
            origin: Ubuntu
            source: apt
            version: 2.02~beta2-36ubuntu3.23
        grub-pc-bin:
        -   arch: amd64
            category: admin
            name: grub-pc-bin
            origin: Ubuntu
            source: apt
            version: 2.02~beta2-36ubuntu3.23
        grub2-common:
        -   arch: amd64
            category: admin
            name: grub2-common
            origin: Ubuntu
            source: apt
            version: 2.02~beta2-36ubuntu3.23
        gsettings-desktop-schemas:
        -   arch: all
            category: libs
            name: gsettings-desktop-schemas
            origin: Ubuntu
            source: apt
            version: 3.18.1-1ubuntu1
        gsfonts:
        -   arch: all
            category: text
            name: gsfonts
            origin: Ubuntu
            source: apt
            version: 1:8.11+urwcyr1.0.7~pre44-4.2ubuntu1
        gstreamer1.0-plugins-base:
        -   arch: amd64
            category: libs
            name: gstreamer1.0-plugins-base
            origin: Ubuntu
            source: apt
            version: 1.8.3-1ubuntu0.3
        gstreamer1.5-libav:
        -   arch: amd64
            category: libs
            name: gstreamer1.5-libav
            origin: BigBlueButton
            source: apt
            version: 1.8.1-1kurento1.16.04
        gstreamer1.5-nice:
        -   arch: amd64
            category: net
            name: gstreamer1.5-nice
            origin: BigBlueButton
            source: apt
            version: 0.1.15-1kurento3.16.04
        gstreamer1.5-plugins-bad:
        -   arch: amd64
            category: libs
            name: gstreamer1.5-plugins-bad
            origin: BigBlueButton
            source: apt
            version: 1.8.1-1kurento4.16.04
        gstreamer1.5-plugins-base:
        -   arch: amd64
            category: libs
            name: gstreamer1.5-plugins-base
            origin: BigBlueButton
            source: apt
            version: 1.8.1-1kurento2.16.04
        gstreamer1.5-plugins-good:
        -   arch: amd64
            category: libs
            name: gstreamer1.5-plugins-good
            origin: BigBlueButton
            source: apt
            version: 1.8.1-1kurento3.16.04
        gstreamer1.5-plugins-ugly:
        -   arch: amd64
            category: libs
            name: gstreamer1.5-plugins-ugly
            origin: BigBlueButton
            source: apt
            version: 1.8.1-1kurento1.16.04
        gstreamer1.5-pulseaudio:
        -   arch: amd64
            category: sound
            name: gstreamer1.5-pulseaudio
            origin: BigBlueButton
            source: apt
            version: 1.8.1-1kurento3.16.04
        gstreamer1.5-x:
        -   arch: amd64
            category: libs
            name: gstreamer1.5-x
            origin: BigBlueButton
            source: apt
            version: 1.8.1-1kurento2.16.04
        gzip:
        -   arch: amd64
            category: utils
            name: gzip
            origin: Ubuntu
            source: apt
            version: 1.6-4ubuntu1
        haveged:
        -   arch: amd64
            category: universe/misc
            name: haveged
            origin: Ubuntu
            source: apt
            version: 1.9.1-3
        hicolor-icon-theme:
        -   arch: all
            category: misc
            name: hicolor-icon-theme
            origin: Ubuntu
            source: apt
            version: 0.15-0ubuntu1.1
        hostname:
        -   arch: amd64
            category: admin
            name: hostname
            origin: Ubuntu
            source: apt
            version: 3.16ubuntu2
        htop:
        -   arch: amd64
            category: universe/utils
            name: htop
            origin: Ubuntu
            source: apt
            version: 2.0.1-1ubuntu1
        humanity-icon-theme:
        -   arch: all
            category: gnome
            name: humanity-icon-theme
            origin: Ubuntu
            source: apt
            version: 0.6.10.1
        hunspell-en-us:
        -   arch: all
            category: text
            name: hunspell-en-us
            origin: Ubuntu
            source: apt
            version: 20070829-6ubuntu3
        i965-va-driver:
        -   arch: amd64
            category: universe/oldlibs
            name: i965-va-driver
            origin: Ubuntu
            source: apt
            version: 1.7.0-1
        icu-devtools:
        -   arch: amd64
            category: libdevel
            name: icu-devtools
            origin: Ubuntu
            source: apt
            version: 55.1-7ubuntu0.5
        ifupdown:
        -   arch: amd64
            category: admin
            name: ifupdown
            origin: Ubuntu
            source: apt
            version: 0.8.10ubuntu1.4
        imagemagick:
        -   arch: amd64
            category: graphics
            name: imagemagick
            origin: Ubuntu
            source: apt
            version: 8:6.8.9.9-7ubuntu5.15
        imagemagick-6.q16:
        -   arch: amd64
            category: graphics
            name: imagemagick-6.q16
            origin: Ubuntu
            source: apt
            version: 8:6.8.9.9-7ubuntu5.15
        imagemagick-common:
        -   arch: all
            category: graphics
            name: imagemagick-common
            origin: Ubuntu
            source: apt
            version: 8:6.8.9.9-7ubuntu5.15
        init:
        -   arch: amd64
            category: metapackages
            name: init
            origin: Ubuntu
            source: apt
            version: 1.29ubuntu4
        init-system-helpers:
        -   arch: all
            category: admin
            name: init-system-helpers
            origin: Ubuntu
            source: apt
            version: 1.29ubuntu4
        initramfs-tools:
        -   arch: all
            category: utils
            name: initramfs-tools
            origin: Ubuntu
            source: apt
            version: 0.122ubuntu8.16
        initramfs-tools-bin:
        -   arch: amd64
            category: utils
            name: initramfs-tools-bin
            origin: Ubuntu
            source: apt
            version: 0.122ubuntu8.16
        initramfs-tools-core:
        -   arch: all
            category: utils
            name: initramfs-tools-core
            origin: Ubuntu
            source: apt
            version: 0.122ubuntu8.16
        initscripts:
        -   arch: amd64
            category: admin
            name: initscripts
            origin: Ubuntu
            source: apt
            version: 2.88dsf-59.3ubuntu2
        insserv:
        -   arch: amd64
            category: misc
            name: insserv
            origin: Ubuntu
            source: apt
            version: 1.14.0-5ubuntu3
        intel-microcode:
        -   arch: amd64
            category: admin
            name: intel-microcode
            origin: Ubuntu
            source: apt
            version: 3.20191115.1ubuntu0.16.04.2
        iotop:
        -   arch: amd64
            category: universe/admin
            name: iotop
            origin: Ubuntu
            source: apt
            version: 0.6-1
        iproute2:
        -   arch: amd64
            category: net
            name: iproute2
            origin: Ubuntu
            source: apt
            version: 4.3.0-1ubuntu3.16.04.5
        iptables:
        -   arch: amd64
            category: net
            name: iptables
            origin: Ubuntu
            source: apt
            version: 1.6.0-2ubuntu3
        iputils-ping:
        -   arch: amd64
            category: net
            name: iputils-ping
            origin: Ubuntu
            source: apt
            version: 3:20121221-5ubuntu2
        isc-dhcp-client:
        -   arch: amd64
            category: net
            name: isc-dhcp-client
            origin: Ubuntu
            source: apt
            version: 4.3.3-5ubuntu12.10
        isc-dhcp-common:
        -   arch: amd64
            category: net
            name: isc-dhcp-common
            origin: Ubuntu
            source: apt
            version: 4.3.3-5ubuntu12.10
        iso-codes:
        -   arch: all
            category: libs
            name: iso-codes
            origin: Ubuntu
            source: apt
            version: 3.65-1
        iucode-tool:
        -   arch: amd64
            category: utils
            name: iucode-tool
            origin: Ubuntu
            source: apt
            version: 1.5.1-1ubuntu0.1
        iw:
        -   arch: amd64
            category: net
            name: iw
            origin: Ubuntu
            source: apt
            version: 3.17-1
        java-common:
        -   arch: all
            category: misc
            name: java-common
            origin: Ubuntu
            source: apt
            version: 0.56ubuntu2
        javascript-common:
        -   arch: all
            category: web
            name: javascript-common
            origin: Ubuntu
            source: apt
            version: '11'
        jsvc:
        -   arch: amd64
            category: universe/net
            name: jsvc
            origin: Ubuntu
            source: apt
            version: 1.0.15-6
        kbd:
        -   arch: amd64
            category: utils
            name: kbd
            origin: Ubuntu
            source: apt
            version: 1.15.5-1ubuntu5
        keyboard-configuration:
        -   arch: all
            category: utils
            name: keyboard-configuration
            origin: Ubuntu
            source: apt
            version: 1.108ubuntu15.5
        klibc-utils:
        -   arch: amd64
            category: libs
            name: klibc-utils
            origin: Ubuntu
            source: apt
            version: 2.0.4-8ubuntu1.16.04.4
        kmod:
        -   arch: amd64
            category: admin
            name: kmod
            origin: Ubuntu
            source: apt
            version: 22-1ubuntu5.2
        kms-core:
        -   arch: amd64
            category: libs
            name: kms-core
            origin: BigBlueButton
            source: apt
            version: 6.13.0.xenial~20200506234430.57.5a9fdb5
        kms-elements:
        -   arch: amd64
            category: libs
            name: kms-elements
            origin: BigBlueButton
            source: apt
            version: 6.13.0.xenial~20200506235224.48.cc7e5c4
        kms-filters:
        -   arch: amd64
            category: libs
            name: kms-filters
            origin: BigBlueButton
            source: apt
            version: 6.13.0.xenial.20200506235848.48.72a06d5
        kms-jsonrpc:
        -   arch: amd64
            category: libs
            name: kms-jsonrpc
            origin: BigBlueButton
            source: apt
            version: 6.13.0-0kurento1.16.04
        kmsjsoncpp:
        -   arch: amd64
            category: utils
            name: kmsjsoncpp
            origin: BigBlueButton
            source: apt
            version: 1.6.3-1kurento1.16.04
        kurento-media-server:
        -   arch: amd64
            category: video
            name: kurento-media-server
            origin: BigBlueButton
            source: apt
            version: 6.13.0-0kurento1.16.04
        language-pack-en:
        -   arch: all
            category: translations
            name: language-pack-en
            origin: Ubuntu
            source: apt
            version: 1:16.04+20161009
        language-pack-en-base:
        -   arch: all
            category: translations
            name: language-pack-en-base
            origin: Ubuntu
            source: apt
            version: 1:16.04+20160627
        less:
        -   arch: amd64
            category: text
            name: less
            origin: Ubuntu
            source: apt
            version: 481-2.1ubuntu0.2
        liba52-0.7.4:
        -   arch: amd64
            category: universe/libs
            name: liba52-0.7.4
            origin: Ubuntu
            source: apt
            version: 0.7.4-18
        libaa1:
        -   arch: amd64
            category: libs
            name: libaa1
            origin: Ubuntu
            source: apt
            version: 1.4p5-44build1
        libaacs0:
        -   arch: amd64
            category: universe/video
            name: libaacs0
            origin: Ubuntu
            source: apt
            version: 0.8.1-1
        libabw-0.1-1v5:
        -   arch: amd64
            category: libs
            name: libabw-0.1-1v5
            origin: Ubuntu
            source: apt
            version: 0.1.1-2ubuntu2
        libacl1:
        -   arch: amd64
            category: libs
            name: libacl1
            origin: Ubuntu
            source: apt
            version: 2.2.52-3
        libalgorithm-diff-perl:
        -   arch: all
            category: perl
            name: libalgorithm-diff-perl
            origin: Ubuntu
            source: apt
            version: 1.19.03-1
        libalgorithm-diff-xs-perl:
        -   arch: amd64
            category: perl
            name: libalgorithm-diff-xs-perl
            origin: Ubuntu
            source: apt
            version: 0.04-4build1
        libalgorithm-merge-perl:
        -   arch: all
            category: perl
            name: libalgorithm-merge-perl
            origin: Ubuntu
            source: apt
            version: 0.08-3
        libao-common:
        -   arch: all
            category: libs
            name: libao-common
            origin: Ubuntu
            source: apt
            version: 1.1.0-3ubuntu1
        libao4:
        -   arch: amd64
            category: libs
            name: libao4
            origin: Ubuntu
            source: apt
            version: 1.1.0-3ubuntu1
        libapparmor-perl:
        -   arch: amd64
            category: perl
            name: libapparmor-perl
            origin: Ubuntu
            source: apt
            version: 2.10.95-0ubuntu2.11
        libapparmor1:
        -   arch: amd64
            category: libs
            name: libapparmor1
            origin: Ubuntu
            source: apt
            version: 2.10.95-0ubuntu2.11
        libapt-inst2.0:
        -   arch: amd64
            category: libs
            name: libapt-inst2.0
            origin: Ubuntu
            source: apt
            version: 1.2.32ubuntu0.1
        libapt-pkg-perl:
        -   arch: amd64
            category: perl
            name: libapt-pkg-perl
            origin: Ubuntu
            source: apt
            version: 0.1.29build7
        libapt-pkg5.0:
        -   arch: amd64
            category: libs
            name: libapt-pkg5.0
            origin: Ubuntu
            source: apt
            version: 1.2.32ubuntu0.1
        libasan2:
        -   arch: amd64
            category: libs
            name: libasan2
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libasn1-8-heimdal:
        -   arch: amd64
            category: libs
            name: libasn1-8-heimdal
            origin: Ubuntu
            source: apt
            version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
        libasound2:
        -   arch: amd64
            category: libs
            name: libasound2
            origin: Ubuntu
            source: apt
            version: 1.1.0-0ubuntu1
        libasound2-data:
        -   arch: all
            category: libs
            name: libasound2-data
            origin: Ubuntu
            source: apt
            version: 1.1.0-0ubuntu1
        libasprintf0v5:
        -   arch: amd64
            category: libs
            name: libasprintf0v5
            origin: Ubuntu
            source: apt
            version: 0.19.7-2ubuntu3.1
        libass5:
        -   arch: amd64
            category: universe/libs
            name: libass5
            origin: Ubuntu
            source: apt
            version: 0.13.1-1
        libassuan0:
        -   arch: amd64
            category: libs
            name: libassuan0
            origin: Ubuntu
            source: apt
            version: 2.4.2-2
        libasyncns0:
        -   arch: amd64
            category: libs
            name: libasyncns0
            origin: Ubuntu
            source: apt
            version: 0.8-5build1
        libatk-bridge2.0-0:
        -   arch: amd64
            category: libs
            name: libatk-bridge2.0-0
            origin: Ubuntu
            source: apt
            version: 2.18.1-2ubuntu1
        libatk1.0-0:
        -   arch: amd64
            category: libs
            name: libatk1.0-0
            origin: Ubuntu
            source: apt
            version: 2.18.0-1
        libatk1.0-data:
        -   arch: all
            category: misc
            name: libatk1.0-data
            origin: Ubuntu
            source: apt
            version: 2.18.0-1
        libatm1:
        -   arch: amd64
            category: libs
            name: libatm1
            origin: Ubuntu
            source: apt
            version: 1:2.5.1-1.5
        libatomic1:
        -   arch: amd64
            category: libs
            name: libatomic1
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libatspi2.0-0:
        -   arch: amd64
            category: misc
            name: libatspi2.0-0
            origin: Ubuntu
            source: apt
            version: 2.18.3-4ubuntu1
        libattr1:
        -   arch: amd64
            category: libs
            name: libattr1
            origin: Ubuntu
            source: apt
            version: 1:2.4.47-2
        libaudio2:
        -   arch: amd64
            category: libs
            name: libaudio2
            origin: Ubuntu
            source: apt
            version: 1.9.4-4
        libaudiofile1:
        -   arch: amd64
            category: universe/libs
            name: libaudiofile1
            origin: Ubuntu
            source: apt
            version: 0.3.6-2ubuntu0.16.04.1
        libaudit-common:
        -   arch: all
            category: libs
            name: libaudit-common
            origin: Ubuntu
            source: apt
            version: 1:2.4.5-1ubuntu2.1
        libaudit1:
        -   arch: amd64
            category: libs
            name: libaudit1
            origin: Ubuntu
            source: apt
            version: 1:2.4.5-1ubuntu2.1
        libauthen-sasl-perl:
        -   arch: all
            category: perl
            name: libauthen-sasl-perl
            origin: Ubuntu
            source: apt
            version: 2.1600-1
        libav-tools:
        -   arch: all
            category: universe/oldlibs
            name: libav-tools
            origin: Ubuntu
            source: apt
            version: 7:2.8.15-0ubuntu0.16.04.1
        libavahi-client3:
        -   arch: amd64
            category: libs
            name: libavahi-client3
            origin: Ubuntu
            source: apt
            version: 0.6.32~rc+dfsg-1ubuntu2.3
        libavahi-common-data:
        -   arch: amd64
            category: libs
            name: libavahi-common-data
            origin: Ubuntu
            source: apt
            version: 0.6.32~rc+dfsg-1ubuntu2.3
        libavahi-common3:
        -   arch: amd64
            category: libs
            name: libavahi-common3
            origin: Ubuntu
            source: apt
            version: 0.6.32~rc+dfsg-1ubuntu2.3
        libavc1394-0:
        -   arch: amd64
            category: libs
            name: libavc1394-0
            origin: Ubuntu
            source: apt
            version: 0.5.4-4
        libavcodec-ffmpeg56:
        -   arch: amd64
            category: universe/libs
            name: libavcodec-ffmpeg56
            origin: Ubuntu
            source: apt
            version: 7:2.8.15-0ubuntu0.16.04.1
        libavcodec58:
        -   arch: amd64
            category: libs
            name: libavcodec58
            origin: LP-PPA-bigbluebutton-support
            source: apt
            version: 7:4.2.2-1bbb1~ubuntu16.04
        libavdevice58:
        -   arch: amd64
            category: libs
            name: libavdevice58
            origin: LP-PPA-bigbluebutton-support
            source: apt
            version: 7:4.2.2-1bbb1~ubuntu16.04
        libavfilter7:
        -   arch: amd64
            category: libs
            name: libavfilter7
            origin: LP-PPA-bigbluebutton-support
            source: apt
            version: 7:4.2.2-1bbb1~ubuntu16.04
        libavformat-ffmpeg56:
        -   arch: amd64
            category: universe/libs
            name: libavformat-ffmpeg56
            origin: Ubuntu
            source: apt
            version: 7:2.8.15-0ubuntu0.16.04.1
        libavformat58:
        -   arch: amd64
            category: libs
            name: libavformat58
            origin: LP-PPA-bigbluebutton-support
            source: apt
            version: 7:4.2.2-1bbb1~ubuntu16.04
        libavresample4:
        -   arch: amd64
            category: libs
            name: libavresample4
            origin: LP-PPA-bigbluebutton-support
            source: apt
            version: 7:4.2.2-1bbb1~ubuntu16.04
        libavutil-ffmpeg54:
        -   arch: amd64
            category: universe/libs
            name: libavutil-ffmpeg54
            origin: Ubuntu
            source: apt
            version: 7:2.8.15-0ubuntu0.16.04.1
        libavutil56:
        -   arch: amd64
            category: libs
            name: libavutil56
            origin: LP-PPA-bigbluebutton-support
            source: apt
            version: 7:4.2.2-1bbb1~ubuntu16.04
        libbdplus0:
        -   arch: amd64
            category: universe/libs
            name: libbdplus0
            origin: Ubuntu
            source: apt
            version: 0.1.2-1
        libbind9-140:
        -   arch: amd64
            category: libs
            name: libbind9-140
            origin: Ubuntu
            source: apt
            version: 1:9.10.3.dfsg.P4-8ubuntu1.16
        libblkid1:
        -   arch: amd64
            category: libs
            name: libblkid1
            origin: Ubuntu
            source: apt
            version: 2.27.1-6ubuntu3.10
        libbluray1:
        -   arch: amd64
            category: universe/libs
            name: libbluray1
            origin: Ubuntu
            source: apt
            version: 1:0.9.2-2
        libboost-date-time1.58.0:
        -   arch: amd64
            category: libs
            name: libboost-date-time1.58.0
            origin: Ubuntu
            source: apt
            version: 1.58.0+dfsg-5ubuntu3.1
        libboost-filesystem1.58.0:
        -   arch: amd64
            category: libs
            name: libboost-filesystem1.58.0
            origin: Ubuntu
            source: apt
            version: 1.58.0+dfsg-5ubuntu3.1
        libboost-iostreams1.58.0:
        -   arch: amd64
            category: libs
            name: libboost-iostreams1.58.0
            origin: Ubuntu
            source: apt
            version: 1.58.0+dfsg-5ubuntu3.1
        libboost-log1.58.0:
        -   arch: amd64
            category: universe/libs
            name: libboost-log1.58.0
            origin: Ubuntu
            source: apt
            version: 1.58.0+dfsg-5ubuntu3.1
        libboost-program-options1.58.0:
        -   arch: amd64
            category: libs
            name: libboost-program-options1.58.0
            origin: Ubuntu
            source: apt
            version: 1.58.0+dfsg-5ubuntu3.1
        libboost-regex1.58.0:
        -   arch: amd64
            category: libs
            name: libboost-regex1.58.0
            origin: Ubuntu
            source: apt
            version: 1.58.0+dfsg-5ubuntu3.1
        libboost-system1.58.0:
        -   arch: amd64
            category: libs
            name: libboost-system1.58.0
            origin: Ubuntu
            source: apt
            version: 1.58.0+dfsg-5ubuntu3.1
        libboost-thread1.58.0:
        -   arch: amd64
            category: libs
            name: libboost-thread1.58.0
            origin: Ubuntu
            source: apt
            version: 1.58.0+dfsg-5ubuntu3.1
        libbs2b0:
        -   arch: amd64
            category: universe/libs
            name: libbs2b0
            origin: Ubuntu
            source: apt
            version: 3.1.0+dfsg-2.2
        libbsd0:
        -   arch: amd64
            category: libs
            name: libbsd0
            origin: Ubuntu
            source: apt
            version: 0.8.2-1ubuntu0.1
        libbz2-1.0:
        -   arch: amd64
            category: libs
            name: libbz2-1.0
            origin: Ubuntu
            source: apt
            version: 1.0.6-8ubuntu0.2
        libc-bin:
        -   arch: amd64
            category: libs
            name: libc-bin
            origin: Ubuntu
            source: apt
            version: 2.23-0ubuntu11
        libc-dev-bin:
        -   arch: amd64
            category: libdevel
            name: libc-dev-bin
            origin: Ubuntu
            source: apt
            version: 2.23-0ubuntu11
        libc6:
        -   arch: amd64
            category: libs
            name: libc6
            origin: Ubuntu
            source: apt
            version: 2.23-0ubuntu11
        libc6-dev:
        -   arch: amd64
            category: libdevel
            name: libc6-dev
            origin: Ubuntu
            source: apt
            version: 2.23-0ubuntu11
        libcaca0:
        -   arch: amd64
            category: libs
            name: libcaca0
            origin: Ubuntu
            source: apt
            version: 0.99.beta19-2ubuntu0.16.04.1
        libcache-cache-perl:
        -   arch: all
            category: universe/perl
            name: libcache-cache-perl
            origin: Ubuntu
            source: apt
            version: 1.08-2
        libcache-memcached-perl:
        -   arch: all
            category: universe/perl
            name: libcache-memcached-perl
            origin: Ubuntu
            source: apt
            version: 1.30-1
        libcairo-gobject2:
        -   arch: amd64
            category: libs
            name: libcairo-gobject2
            origin: Ubuntu
            source: apt
            version: 1.14.6-1
        libcairo2:
        -   arch: amd64
            category: libs
            name: libcairo2
            origin: Ubuntu
            source: apt
            version: 1.14.6-1
        libcap-ng0:
        -   arch: amd64
            category: libs
            name: libcap-ng0
            origin: Ubuntu
            source: apt
            version: 0.7.7-1
        libcap2:
        -   arch: amd64
            category: libs
            name: libcap2
            origin: Ubuntu
            source: apt
            version: 1:2.24-12
        libcap2-bin:
        -   arch: amd64
            category: utils
            name: libcap2-bin
            origin: Ubuntu
            source: apt
            version: 1:2.24-12
        libcapnp-0.5.3:
        -   arch: amd64
            category: libs
            name: libcapnp-0.5.3
            origin: Ubuntu
            source: apt
            version: 0.5.3-2ubuntu1.1
        libcc1-0:
        -   arch: amd64
            category: libs
            name: libcc1-0
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libcdio-cdda1:
        -   arch: amd64
            category: libs
            name: libcdio-cdda1
            origin: Ubuntu
            source: apt
            version: 0.83-4.2ubuntu1
        libcdio-paranoia1:
        -   arch: amd64
            category: libs
            name: libcdio-paranoia1
            origin: Ubuntu
            source: apt
            version: 0.83-4.2ubuntu1
        libcdio13:
        -   arch: amd64
            category: libs
            name: libcdio13
            origin: Ubuntu
            source: apt
            version: 0.83-4.2ubuntu1
        libcdparanoia0:
        -   arch: amd64
            category: sound
            name: libcdparanoia0
            origin: Ubuntu
            source: apt
            version: 3.10.2+debian-11
        libcdr-0.1-1:
        -   arch: amd64
            category: libs
            name: libcdr-0.1-1
            origin: Ubuntu
            source: apt
            version: 0.1.2-2ubuntu2
        libcgi-fast-perl:
        -   arch: all
            category: perl
            name: libcgi-fast-perl
            origin: Ubuntu
            source: apt
            version: 1:2.10-1
        libcgi-pm-perl:
        -   arch: all
            category: perl
            name: libcgi-pm-perl
            origin: Ubuntu
            source: apt
            version: 4.26-1
        libchromaprint0:
        -   arch: amd64
            category: universe/libs
            name: libchromaprint0
            origin: Ubuntu
            source: apt
            version: 1.3-1
        libcilkrts5:
        -   arch: amd64
            category: libs
            name: libcilkrts5
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libclass-accessor-perl:
        -   arch: all
            category: perl
            name: libclass-accessor-perl
            origin: Ubuntu
            source: apt
            version: 0.34-1
        libclucene-contribs1v5:
        -   arch: amd64
            category: libs
            name: libclucene-contribs1v5
            origin: Ubuntu
            source: apt
            version: 2.3.3.4-4.1
        libclucene-core1v5:
        -   arch: amd64
            category: libs
            name: libclucene-core1v5
            origin: Ubuntu
            source: apt
            version: 2.3.3.4-4.1
        libcmis-0.5-5v5:
        -   arch: amd64
            category: libs
            name: libcmis-0.5-5v5
            origin: Ubuntu
            source: apt
            version: 0.5.1-2ubuntu2
        libcolamd2.9.1:
        -   arch: amd64
            category: libs
            name: libcolamd2.9.1
            origin: Ubuntu
            source: apt
            version: 1:4.4.6-1
        libcolord2:
        -   arch: amd64
            category: libs
            name: libcolord2
            origin: Ubuntu
            source: apt
            version: 1.2.12-1ubuntu1
        libcolorhug2:
        -   arch: amd64
            category: libs
            name: libcolorhug2
            origin: Ubuntu
            source: apt
            version: 1.2.12-1ubuntu1
        libcomerr2:
        -   arch: amd64
            category: libs
            name: libcomerr2
            origin: Ubuntu
            source: apt
            version: 1.42.13-1ubuntu1.2
        libcommons-daemon-java:
        -   arch: all
            category: universe/libs
            name: libcommons-daemon-java
            origin: Ubuntu
            source: apt
            version: 1.0.15-6
        libcroco3:
        -   arch: amd64
            category: libs
            name: libcroco3
            origin: Ubuntu
            source: apt
            version: 0.6.11-1
        libcryptsetup4:
        -   arch: amd64
            category: libs
            name: libcryptsetup4
            origin: Ubuntu
            source: apt
            version: 2:1.6.6-5ubuntu2.1
        libcrystalhd3:
        -   arch: amd64
            category: universe/libs
            name: libcrystalhd3
            origin: Ubuntu
            source: apt
            version: 1:0.0~git20110715.fdd2f19-11build1
        libcups2:
        -   arch: amd64
            category: libs
            name: libcups2
            origin: Ubuntu
            source: apt
            version: 2.1.3-4ubuntu0.11
        libcupsfilters1:
        -   arch: amd64
            category: libs
            name: libcupsfilters1
            origin: Ubuntu
            source: apt
            version: 1.8.3-2ubuntu3.5
        libcupsimage2:
        -   arch: amd64
            category: libs
            name: libcupsimage2
            origin: Ubuntu
            source: apt
            version: 2.1.3-4ubuntu0.11
        libcurl3:
        -   arch: amd64
            category: libs
            name: libcurl3
            origin: Ubuntu
            source: apt
            version: 7.47.0-1ubuntu2.14
        libcurl3-gnutls:
        -   arch: amd64
            category: libs
            name: libcurl3-gnutls
            origin: Ubuntu
            source: apt
            version: 7.47.0-1ubuntu2.14
        libcurl4-openssl-dev:
        -   arch: amd64
            category: libdevel
            name: libcurl4-openssl-dev
            origin: Ubuntu
            source: apt
            version: 7.47.0-1ubuntu2.14
        libcwidget3v5:
        -   arch: amd64
            category: libs
            name: libcwidget3v5
            origin: Ubuntu
            source: apt
            version: 0.5.17-4ubuntu2
        libdatrie1:
        -   arch: amd64
            category: libs
            name: libdatrie1
            origin: Ubuntu
            source: apt
            version: 0.2.10-2
        libdb5.3:
        -   arch: amd64
            category: libs
            name: libdb5.3
            origin: Ubuntu
            source: apt
            version: 5.3.28-11ubuntu0.2
        libdbus-1-3:
        -   arch: amd64
            category: libs
            name: libdbus-1-3
            origin: Ubuntu
            source: apt
            version: 1.10.6-1ubuntu3.5
        libdbus-glib-1-2:
        -   arch: amd64
            category: libs
            name: libdbus-glib-1-2
            origin: Ubuntu
            source: apt
            version: 0.106-1
        libdc1394-22:
        -   arch: amd64
            category: universe/libs
            name: libdc1394-22
            origin: Ubuntu
            source: apt
            version: 2.2.4-1
        libdca0:
        -   arch: amd64
            category: universe/libs
            name: libdca0
            origin: Ubuntu
            source: apt
            version: 0.0.5-7build1
        libdconf1:
        -   arch: amd64
            category: libs
            name: libdconf1
            origin: Ubuntu
            source: apt
            version: 0.24.0-2
        libde265-0:
        -   arch: amd64
            category: universe/libs
            name: libde265-0
            origin: Ubuntu
            source: apt
            version: 1.0.2-2
        libdebconfclient0:
        -   arch: amd64
            category: libs
            name: libdebconfclient0
            origin: Ubuntu
            source: apt
            version: 0.198ubuntu1
        libdevmapper-event1.02.1:
        -   arch: amd64
            category: libs
            name: libdevmapper-event1.02.1
            origin: Ubuntu
            source: apt
            version: 2:1.02.110-1ubuntu10
        libdevmapper1.02.1:
        -   arch: amd64
            category: libs
            name: libdevmapper1.02.1
            origin: Ubuntu
            source: apt
            version: 2:1.02.110-1ubuntu10
        libdirectfb-1.2-9:
        -   arch: amd64
            category: universe/libs
            name: libdirectfb-1.2-9
            origin: Ubuntu
            source: apt
            version: 1.2.10.0-5.1
        libdjvulibre-text:
        -   arch: all
            category: libs
            name: libdjvulibre-text
            origin: Ubuntu
            source: apt
            version: 3.5.27.1-5ubuntu0.1
        libdjvulibre21:
        -   arch: amd64
            category: libs
            name: libdjvulibre21
            origin: Ubuntu
            source: apt
            version: 3.5.27.1-5ubuntu0.1
        libdns-export162:
        -   arch: amd64
            category: libs
            name: libdns-export162
            origin: Ubuntu
            source: apt
            version: 1:9.10.3.dfsg.P4-8ubuntu1.16
        libdns162:
        -   arch: amd64
            category: libs
            name: libdns162
            origin: Ubuntu
            source: apt
            version: 1:9.10.3.dfsg.P4-8ubuntu1.16
        libdpkg-perl:
        -   arch: all
            category: perl
            name: libdpkg-perl
            origin: Ubuntu
            source: apt
            version: 1.18.4ubuntu1.6
        libdrm-amdgpu1:
        -   arch: amd64
            category: libs
            name: libdrm-amdgpu1
            origin: Ubuntu
            source: apt
            version: 2.4.91-2~16.04.1
        libdrm-common:
        -   arch: all
            category: libs
            name: libdrm-common
            origin: Ubuntu
            source: apt
            version: 2.4.91-2~16.04.1
        libdrm-intel1:
        -   arch: amd64
            category: libs
            name: libdrm-intel1
            origin: Ubuntu
            source: apt
            version: 2.4.91-2~16.04.1
        libdrm-nouveau2:
        -   arch: amd64
            category: libs
            name: libdrm-nouveau2
            origin: Ubuntu
            source: apt
            version: 2.4.91-2~16.04.1
        libdrm-radeon1:
        -   arch: amd64
            category: libs
            name: libdrm-radeon1
            origin: Ubuntu
            source: apt
            version: 2.4.91-2~16.04.1
        libdrm2:
        -   arch: amd64
            category: libs
            name: libdrm2
            origin: Ubuntu
            source: apt
            version: 2.4.91-2~16.04.1
        libdv4:
        -   arch: amd64
            category: libs
            name: libdv4
            origin: Ubuntu
            source: apt
            version: 1.0.0-7
        libdvdnav4:
        -   arch: amd64
            category: universe/libs
            name: libdvdnav4
            origin: Ubuntu
            source: apt
            version: 5.0.3-1
        libdvdread4:
        -   arch: amd64
            category: universe/libs
            name: libdvdread4
            origin: Ubuntu
            source: apt
            version: 5.0.3-1
        libe-book-0.1-1:
        -   arch: amd64
            category: libs
            name: libe-book-0.1-1
            origin: Ubuntu
            source: apt
            version: 0.1.2-2ubuntu1
        libedit2:
        -   arch: amd64
            category: libs
            name: libedit2
            origin: Ubuntu
            source: apt
            version: 3.1-20150325-1ubuntu2
        libegl1-mesa:
        -   arch: amd64
            category: libs
            name: libegl1-mesa
            origin: Ubuntu
            source: apt
            version: 18.0.5-0ubuntu0~16.04.1
        libelf1:
        -   arch: amd64
            category: libs
            name: libelf1
            origin: Ubuntu
            source: apt
            version: 0.165-3ubuntu1.2
        libenca0:
        -   arch: amd64
            category: universe/libs
            name: libenca0
            origin: Ubuntu
            source: apt
            version: 1.18-1
        libencode-locale-perl:
        -   arch: all
            category: perl
            name: libencode-locale-perl
            origin: Ubuntu
            source: apt
            version: 1.05-1
        libeot0:
        -   arch: amd64
            category: libs
            name: libeot0
            origin: Ubuntu
            source: apt
            version: 0.01-3ubuntu1
        libepoxy0:
        -   arch: amd64
            category: libs
            name: libepoxy0
            origin: Ubuntu
            source: apt
            version: 1.3.1-1ubuntu0.16.04.2
        liberror-perl:
        -   arch: all
            category: perl
            name: liberror-perl
            origin: Ubuntu
            source: apt
            version: 0.17-1.2
        libesd0:
        -   arch: amd64
            category: universe/libs
            name: libesd0
            origin: Ubuntu
            source: apt
            version: 0.2.41-11
        libestr0:
        -   arch: amd64
            category: libs
            name: libestr0
            origin: Ubuntu
            source: apt
            version: 0.1.10-1
        libetonyek-0.1-1:
        -   arch: amd64
            category: libs
            name: libetonyek-0.1-1
            origin: Ubuntu
            source: apt
            version: 0.1.6-1ubuntu1
        libevdev2:
        -   arch: amd64
            category: libs
            name: libevdev2
            origin: Ubuntu
            source: apt
            version: 1.4.6+dfsg-1
        libevent-2.0-5:
        -   arch: amd64
            category: libs
            name: libevent-2.0-5
            origin: Ubuntu
            source: apt
            version: 2.0.21-stable-2ubuntu0.16.04.1
        libexif12:
        -   arch: amd64
            category: libs
            name: libexif12
            origin: Ubuntu
            source: apt
            version: 0.6.21-2ubuntu0.2
        libexpat1:
        -   arch: amd64
            category: libs
            name: libexpat1
            origin: Ubuntu
            source: apt
            version: 2.1.0-7ubuntu0.16.04.5
        libexporter-tiny-perl:
        -   arch: all
            category: perl
            name: libexporter-tiny-perl
            origin: Ubuntu
            source: apt
            version: 0.042-1
        libexttextcat-2.0-0:
        -   arch: amd64
            category: libs
            name: libexttextcat-2.0-0
            origin: Ubuntu
            source: apt
            version: 3.4.4-1ubuntu3
        libexttextcat-data:
        -   arch: all
            category: text
            name: libexttextcat-data
            origin: Ubuntu
            source: apt
            version: 3.4.4-1ubuntu3
        libfaad2:
        -   arch: amd64
            category: universe/libs
            name: libfaad2
            origin: Ubuntu
            source: apt
            version: 2.8.0~cvs20150510-1ubuntu0.1
        libfakeroot:
        -   arch: amd64
            category: utils
            name: libfakeroot
            origin: Ubuntu
            source: apt
            version: 1.20.2-1ubuntu1
        libfcgi-perl:
        -   arch: amd64
            category: perl
            name: libfcgi-perl
            origin: Ubuntu
            source: apt
            version: 0.77-1build1
        libfdisk1:
        -   arch: amd64
            category: libs
            name: libfdisk1
            origin: Ubuntu
            source: apt
            version: 2.27.1-6ubuntu3.10
        libffi6:
        -   arch: amd64
            category: libs
            name: libffi6
            origin: Ubuntu
            source: apt
            version: 3.2.1-4
        libfftw3-double3:
        -   arch: amd64
            category: libs
            name: libfftw3-double3
            origin: Ubuntu
            source: apt
            version: 3.3.4-2ubuntu1
        libfile-fcntllock-perl:
        -   arch: amd64
            category: perl
            name: libfile-fcntllock-perl
            origin: Ubuntu
            source: apt
            version: 0.22-3
        libfile-listing-perl:
        -   arch: all
            category: perl
            name: libfile-listing-perl
            origin: Ubuntu
            source: apt
            version: 6.04-1
        libflac8:
        -   arch: amd64
            category: libs
            name: libflac8
            origin: Ubuntu
            source: apt
            version: 1.3.1-4
        libflite1:
        -   arch: amd64
            category: universe/libs
            name: libflite1
            origin: Ubuntu
            source: apt
            version: 2.0.0-release-1
        libfluidsynth1:
        -   arch: amd64
            category: universe/libs
            name: libfluidsynth1
            origin: Ubuntu
            source: apt
            version: 1.1.6-3
        libfont-afm-perl:
        -   arch: all
            category: perl
            name: libfont-afm-perl
            origin: Ubuntu
            source: apt
            version: 1.20-1
        libfontconfig1:
        -   arch: amd64
            category: libs
            name: libfontconfig1
            origin: Ubuntu
            source: apt
            version: 2.11.94-0ubuntu1.1
        libfontenc1:
        -   arch: amd64
            category: x11
            name: libfontenc1
            origin: Ubuntu
            source: apt
            version: 1:1.1.3-1
        libfreehand-0.1-1:
        -   arch: amd64
            category: libs
            name: libfreehand-0.1-1
            origin: Ubuntu
            source: apt
            version: 0.1.1-1ubuntu1
        libfreetype6:
        -   arch: amd64
            category: libs
            name: libfreetype6
            origin: Ubuntu
            source: apt
            version: 2.6.1-0.1ubuntu2.4
        libfribidi0:
        -   arch: amd64
            category: libs
            name: libfribidi0
            origin: Ubuntu
            source: apt
            version: 0.19.7-1
        libfuse2:
        -   arch: amd64
            category: libs
            name: libfuse2
            origin: Ubuntu
            source: apt
            version: 2.9.4-1ubuntu3.1
        libgbm1:
        -   arch: amd64
            category: libs
            name: libgbm1
            origin: Ubuntu
            source: apt
            version: 18.0.5-0ubuntu0~16.04.1
        libgcc-5-dev:
        -   arch: amd64
            category: libdevel
            name: libgcc-5-dev
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libgcc1:
        -   arch: amd64
            category: libs
            name: libgcc1
            origin: Ubuntu
            source: apt
            version: 1:6.0.1-0ubuntu1
        libgcrypt20:
        -   arch: amd64
            category: libs
            name: libgcrypt20
            origin: Ubuntu
            source: apt
            version: 1.6.5-2ubuntu0.6
        libgd3:
        -   arch: amd64
            category: libs
            name: libgd3
            origin: Ubuntu
            source: apt
            version: 2.1.1-4ubuntu0.16.04.12
        libgdbm3:
        -   arch: amd64
            category: libs
            name: libgdbm3
            origin: Ubuntu
            source: apt
            version: 1.8.3-13.1
        libgdk-pixbuf2.0-0:
        -   arch: amd64
            category: libs
            name: libgdk-pixbuf2.0-0
            origin: Ubuntu
            source: apt
            version: 2.32.2-1ubuntu1.6
        libgdk-pixbuf2.0-common:
        -   arch: all
            category: libs
            name: libgdk-pixbuf2.0-common
            origin: Ubuntu
            source: apt
            version: 2.32.2-1ubuntu1.6
        libgeoip1:
        -   arch: amd64
            category: libs
            name: libgeoip1
            origin: Ubuntu
            source: apt
            version: 1.6.9-1
        libgif7:
        -   arch: amd64
            category: libs
            name: libgif7
            origin: Ubuntu
            source: apt
            version: 5.1.4-0.3~16.04.1
        libgirepository-1.0-1:
        -   arch: amd64
            category: libs
            name: libgirepository-1.0-1
            origin: Ubuntu
            source: apt
            version: 1.46.0-3ubuntu1
        libgl1-mesa-dri:
        -   arch: amd64
            category: libs
            name: libgl1-mesa-dri
            origin: Ubuntu
            source: apt
            version: 18.0.5-0ubuntu0~16.04.1
        libgl1-mesa-glx:
        -   arch: amd64
            category: libs
            name: libgl1-mesa-glx
            origin: Ubuntu
            source: apt
            version: 18.0.5-0ubuntu0~16.04.1
        libglapi-mesa:
        -   arch: amd64
            category: libs
            name: libglapi-mesa
            origin: Ubuntu
            source: apt
            version: 18.0.5-0ubuntu0~16.04.1
        libglew1.13:
        -   arch: amd64
            category: libs
            name: libglew1.13
            origin: Ubuntu
            source: apt
            version: 1.13.0-2
        libglib2.0-0:
        -   arch: amd64
            category: libs
            name: libglib2.0-0
            origin: Ubuntu
            source: apt
            version: 2.48.2-0ubuntu4.6
        libglib2.0-data:
        -   arch: all
            category: misc
            name: libglib2.0-data
            origin: Ubuntu
            source: apt
            version: 2.48.2-0ubuntu4.6
        libglibmm-2.4-1v5:
        -   arch: amd64
            category: libs
            name: libglibmm-2.4-1v5
            origin: Ubuntu
            source: apt
            version: 2.46.3-1
        libglu1-mesa:
        -   arch: amd64
            category: libs
            name: libglu1-mesa
            origin: Ubuntu
            source: apt
            version: 9.0.0-2.1
        libgme0:
        -   arch: amd64
            category: universe/libs
            name: libgme0
            origin: Ubuntu
            source: apt
            version: 0.6.0-3ubuntu0.16.04.1
        libgmp-dev:
        -   arch: amd64
            category: libdevel
            name: libgmp-dev
            origin: Ubuntu
            source: apt
            version: 2:6.1.0+dfsg-2
        libgmp10:
        -   arch: amd64
            category: libs
            name: libgmp10
            origin: Ubuntu
            source: apt
            version: 2:6.1.0+dfsg-2
        libgmpxx4ldbl:
        -   arch: amd64
            category: libs
            name: libgmpxx4ldbl
            origin: Ubuntu
            source: apt
            version: 2:6.1.0+dfsg-2
        libgnutls-openssl27:
        -   arch: amd64
            category: libs
            name: libgnutls-openssl27
            origin: Ubuntu
            source: apt
            version: 3.4.10-4ubuntu1.7
        libgnutls30:
        -   arch: amd64
            category: libs
            name: libgnutls30
            origin: Ubuntu
            source: apt
            version: 3.4.10-4ubuntu1.7
        libgomp1:
        -   arch: amd64
            category: libs
            name: libgomp1
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libgpg-error0:
        -   arch: amd64
            category: libs
            name: libgpg-error0
            origin: Ubuntu
            source: apt
            version: 1.21-2ubuntu1
        libgpgme11:
        -   arch: amd64
            category: libs
            name: libgpgme11
            origin: Ubuntu
            source: apt
            version: 1.6.0-1
        libgphoto2-6:
        -   arch: amd64
            category: libs
            name: libgphoto2-6
            origin: Ubuntu
            source: apt
            version: 2.5.9-3
        libgphoto2-l10n:
        -   arch: all
            category: localization
            name: libgphoto2-l10n
            origin: Ubuntu
            source: apt
            version: 2.5.9-3
        libgphoto2-port12:
        -   arch: amd64
            category: libs
            name: libgphoto2-port12
            origin: Ubuntu
            source: apt
            version: 2.5.9-3
        libgpm2:
        -   arch: amd64
            category: libs
            name: libgpm2
            origin: Ubuntu
            source: apt
            version: 1.20.4-6.1
        libgraphite2-3:
        -   arch: amd64
            category: libs
            name: libgraphite2-3
            origin: Ubuntu
            source: apt
            version: 1.3.10-0ubuntu0.16.04.1
        libgs9:
        -   arch: amd64
            category: libs
            name: libgs9
            origin: Ubuntu
            source: apt
            version: 9.26~dfsg+0-0ubuntu0.16.04.12
        libgs9-common:
        -   arch: all
            category: libs
            name: libgs9-common
            origin: Ubuntu
            source: apt
            version: 9.26~dfsg+0-0ubuntu0.16.04.12
        libgsm1:
        -   arch: amd64
            category: universe/libs
            name: libgsm1
            origin: Ubuntu
            source: apt
            version: 1.0.13-4
        libgssapi-krb5-2:
        -   arch: amd64
            category: libs
            name: libgssapi-krb5-2
            origin: Ubuntu
            source: apt
            version: 1.13.2+dfsg-5ubuntu2.1
        libgssapi3-heimdal:
        -   arch: amd64
            category: libs
            name: libgssapi3-heimdal
            origin: Ubuntu
            source: apt
            version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
        libgssdp-1.0-3:
        -   arch: amd64
            category: universe/libs
            name: libgssdp-1.0-3
            origin: Ubuntu
            source: apt
            version: 0.14.14-1ubuntu1
        libgstreamer-plugins-bad1.5-0:
        -   arch: amd64
            category: libs
            name: libgstreamer-plugins-bad1.5-0
            origin: BigBlueButton
            source: apt
            version: 1.8.1-1kurento4.16.04
        libgstreamer-plugins-base1.0-0:
        -   arch: amd64
            category: libs
            name: libgstreamer-plugins-base1.0-0
            origin: Ubuntu
            source: apt
            version: 1.8.3-1ubuntu0.3
        libgstreamer-plugins-base1.5-0:
        -   arch: amd64
            category: libs
            name: libgstreamer-plugins-base1.5-0
            origin: BigBlueButton
            source: apt
            version: 1.8.1-1kurento2.16.04
        libgstreamer1.0-0:
        -   arch: amd64
            category: libs
            name: libgstreamer1.0-0
            origin: Ubuntu
            source: apt
            version: 1.8.3-1~ubuntu0.1
        libgstreamer1.5-0:
        -   arch: amd64
            category: libs
            name: libgstreamer1.5-0
            origin: BigBlueButton
            source: apt
            version: 1.8.1-1kurento2.16.04
        libgtk-3-0:
        -   arch: amd64
            category: libs
            name: libgtk-3-0
            origin: Ubuntu
            source: apt
            version: 3.18.9-1ubuntu3.3
        libgtk-3-bin:
        -   arch: amd64
            category: misc
            name: libgtk-3-bin
            origin: Ubuntu
            source: apt
            version: 3.18.9-1ubuntu3.3
        libgtk-3-common:
        -   arch: all
            category: misc
            name: libgtk-3-common
            origin: Ubuntu
            source: apt
            version: 3.18.9-1ubuntu3.3
        libgtk2.0-0:
        -   arch: amd64
            category: libs
            name: libgtk2.0-0
            origin: Ubuntu
            source: apt
            version: 2.24.30-1ubuntu1.16.04.2
        libgtk2.0-bin:
        -   arch: amd64
            category: misc
            name: libgtk2.0-bin
            origin: Ubuntu
            source: apt
            version: 2.24.30-1ubuntu1.16.04.2
        libgtk2.0-common:
        -   arch: all
            category: misc
            name: libgtk2.0-common
            origin: Ubuntu
            source: apt
            version: 2.24.30-1ubuntu1.16.04.2
        libgtkglext1:
        -   arch: amd64
            category: universe/libs
            name: libgtkglext1
            origin: Ubuntu
            source: apt
            version: 1.2.0-3.2fakesync1ubuntu1
        libgudev-1.0-0:
        -   arch: amd64
            category: libs
            name: libgudev-1.0-0
            origin: Ubuntu
            source: apt
            version: 1:230-2
        libgupnp-1.0-4:
        -   arch: amd64
            category: universe/libs
            name: libgupnp-1.0-4
            origin: Ubuntu
            source: apt
            version: 0.20.16-1
        libgupnp-igd-1.0-4:
        -   arch: amd64
            category: universe/libs
            name: libgupnp-igd-1.0-4
            origin: Ubuntu
            source: apt
            version: 0.2.4-1
        libgusb2:
        -   arch: amd64
            category: libs
            name: libgusb2
            origin: Ubuntu
            source: apt
            version: 0.2.9-0ubuntu1
        libharfbuzz-icu0:
        -   arch: amd64
            category: libs
            name: libharfbuzz-icu0
            origin: Ubuntu
            source: apt
            version: 1.0.1-1ubuntu0.1
        libharfbuzz0b:
        -   arch: amd64
            category: libs
            name: libharfbuzz0b
            origin: Ubuntu
            source: apt
            version: 1.0.1-1ubuntu0.1
        libhavege1:
        -   arch: amd64
            category: universe/libs
            name: libhavege1
            origin: Ubuntu
            source: apt
            version: 1.9.1-3
        libhcrypto4-heimdal:
        -   arch: amd64
            category: libs
            name: libhcrypto4-heimdal
            origin: Ubuntu
            source: apt
            version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
        libheimbase1-heimdal:
        -   arch: amd64
            category: libs
            name: libheimbase1-heimdal
            origin: Ubuntu
            source: apt
            version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
        libheimntlm0-heimdal:
        -   arch: amd64
            category: libs
            name: libheimntlm0-heimdal
            origin: Ubuntu
            source: apt
            version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
        libhogweed4:
        -   arch: amd64
            category: libs
            name: libhogweed4
            origin: Ubuntu
            source: apt
            version: 3.2-1ubuntu0.16.04.1
        libhsqldb1.8.0-java:
        -   arch: all
            category: java
            name: libhsqldb1.8.0-java
            origin: Ubuntu
            source: apt
            version: 1.8.0.10+dfsg-6
        libhtml-form-perl:
        -   arch: all
            category: perl
            name: libhtml-form-perl
            origin: Ubuntu
            source: apt
            version: 6.03-1
        libhtml-format-perl:
        -   arch: all
            category: perl
            name: libhtml-format-perl
            origin: Ubuntu
            source: apt
            version: 2.11-2
        libhtml-parser-perl:
        -   arch: amd64
            category: perl
            name: libhtml-parser-perl
            origin: Ubuntu
            source: apt
            version: 3.72-1
        libhtml-tagset-perl:
        -   arch: all
            category: perl
            name: libhtml-tagset-perl
            origin: Ubuntu
            source: apt
            version: 3.20-2
        libhtml-tree-perl:
        -   arch: all
            category: perl
            name: libhtml-tree-perl
            origin: Ubuntu
            source: apt
            version: 5.03-2
        libhttp-cookies-perl:
        -   arch: all
            category: perl
            name: libhttp-cookies-perl
            origin: Ubuntu
            source: apt
            version: 6.01-1
        libhttp-daemon-perl:
        -   arch: all
            category: perl
            name: libhttp-daemon-perl
            origin: Ubuntu
            source: apt
            version: 6.01-1
        libhttp-date-perl:
        -   arch: all
            category: perl
            name: libhttp-date-perl
            origin: Ubuntu
            source: apt
            version: 6.02-1
        libhttp-message-perl:
        -   arch: all
            category: perl
            name: libhttp-message-perl
            origin: Ubuntu
            source: apt
            version: 6.11-1
        libhttp-negotiate-perl:
        -   arch: all
            category: perl
            name: libhttp-negotiate-perl
            origin: Ubuntu
            source: apt
            version: 6.00-2
        libhunspell-1.3-0:
        -   arch: amd64
            category: libs
            name: libhunspell-1.3-0
            origin: Ubuntu
            source: apt
            version: 1.3.3-4ubuntu1
        libhx509-5-heimdal:
        -   arch: amd64
            category: libs
            name: libhx509-5-heimdal
            origin: Ubuntu
            source: apt
            version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
        libhyphen0:
        -   arch: amd64
            category: libs
            name: libhyphen0
            origin: Ubuntu
            source: apt
            version: 2.8.8-2ubuntu1
        libice6:
        -   arch: amd64
            category: libs
            name: libice6
            origin: Ubuntu
            source: apt
            version: 2:1.0.9-1
        libicu-dev:
        -   arch: amd64
            category: libdevel
            name: libicu-dev
            origin: Ubuntu
            source: apt
            version: 55.1-7ubuntu0.5
        libicu55:
        -   arch: amd64
            category: libs
            name: libicu55
            origin: Ubuntu
            source: apt
            version: 55.1-7ubuntu0.5
        libidn11:
        -   arch: amd64
            category: libs
            name: libidn11
            origin: Ubuntu
            source: apt
            version: 1.32-3ubuntu1.2
        libiec61883-0:
        -   arch: amd64
            category: libs
            name: libiec61883-0
            origin: Ubuntu
            source: apt
            version: 1.2.0-0.2
        libieee1284-3:
        -   arch: amd64
            category: libs
            name: libieee1284-3
            origin: Ubuntu
            source: apt
            version: 0.2.11-12
        libijs-0.35:
        -   arch: amd64
            category: libs
            name: libijs-0.35
            origin: Ubuntu
            source: apt
            version: 0.35-12
        libilmbase12:
        -   arch: amd64
            category: libs
            name: libilmbase12
            origin: Ubuntu
            source: apt
            version: 2.2.0-11ubuntu2
        libinput-bin:
        -   arch: amd64
            category: libs
            name: libinput-bin
            origin: Ubuntu
            source: apt
            version: 1.6.3-1ubuntu1~16.04.1
        libinput10:
        -   arch: amd64
            category: libs
            name: libinput10
            origin: Ubuntu
            source: apt
            version: 1.6.3-1ubuntu1~16.04.1
        libio-html-perl:
        -   arch: all
            category: perl
            name: libio-html-perl
            origin: Ubuntu
            source: apt
            version: 1.001-1
        libio-multiplex-perl:
        -   arch: all
            category: perl
            name: libio-multiplex-perl
            origin: Ubuntu
            source: apt
            version: 1.16-1
        libio-socket-inet6-perl:
        -   arch: all
            category: perl
            name: libio-socket-inet6-perl
            origin: Ubuntu
            source: apt
            version: 2.72-2
        libio-socket-ssl-perl:
        -   arch: all
            category: perl
            name: libio-socket-ssl-perl
            origin: Ubuntu
            source: apt
            version: 2.024-1
        libio-string-perl:
        -   arch: all
            category: perl
            name: libio-string-perl
            origin: Ubuntu
            source: apt
            version: 1.08-3
        libipc-sharelite-perl:
        -   arch: amd64
            category: universe/perl
            name: libipc-sharelite-perl
            origin: Ubuntu
            source: apt
            version: 0.17-3build3
        libisc-export160:
        -   arch: amd64
            category: libs
            name: libisc-export160
            origin: Ubuntu
            source: apt
            version: 1:9.10.3.dfsg.P4-8ubuntu1.16
        libisc160:
        -   arch: amd64
            category: libs
            name: libisc160
            origin: Ubuntu
            source: apt
            version: 1:9.10.3.dfsg.P4-8ubuntu1.16
        libisccc140:
        -   arch: amd64
            category: libs
            name: libisccc140
            origin: Ubuntu
            source: apt
            version: 1:9.10.3.dfsg.P4-8ubuntu1.16
        libisccfg140:
        -   arch: amd64
            category: libs
            name: libisccfg140
            origin: Ubuntu
            source: apt
            version: 1:9.10.3.dfsg.P4-8ubuntu1.16
        libisl15:
        -   arch: amd64
            category: libs
            name: libisl15
            origin: Ubuntu
            source: apt
            version: 0.16.1-1
        libitm1:
        -   arch: amd64
            category: libs
            name: libitm1
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libjack-jackd2-0:
        -   arch: amd64
            category: libs
            name: libjack-jackd2-0
            origin: Ubuntu
            source: apt
            version: 1.9.10+20150825git1ed50c92~dfsg-1ubuntu1
        libjasper1:
        -   arch: amd64
            category: libs
            name: libjasper1
            origin: Ubuntu
            source: apt
            version: 1.900.1-debian1-2.4ubuntu1.2
        libjbig0:
        -   arch: amd64
            category: libs
            name: libjbig0
            origin: Ubuntu
            source: apt
            version: 2.1-3.1
        libjbig2dec0:
        -   arch: amd64
            category: libs
            name: libjbig2dec0
            origin: Ubuntu
            source: apt
            version: 0.12+20150918-1ubuntu0.1
        libjemalloc1:
        -   arch: amd64
            category: universe/libs
            name: libjemalloc1
            origin: Ubuntu
            source: apt
            version: 3.6.0-9ubuntu1
        libjpeg-turbo8:
        -   arch: amd64
            category: libs
            name: libjpeg-turbo8
            origin: Ubuntu
            source: apt
            version: 1.4.2-0ubuntu3.3
        libjpeg8:
        -   arch: amd64
            category: libs
            name: libjpeg8
            origin: Ubuntu
            source: apt
            version: 8c-2ubuntu8
        libjs-jquery:
        -   arch: all
            category: web
            name: libjs-jquery
            origin: Ubuntu
            source: apt
            version: 1.11.3+dfsg-4
        libjson-c2:
        -   arch: amd64
            category: libs
            name: libjson-c2
            origin: Ubuntu
            source: apt
            version: 0.11-4ubuntu2.6
        libjson-glib-1.0-0:
        -   arch: amd64
            category: libs
            name: libjson-glib-1.0-0
            origin: Ubuntu
            source: apt
            version: 1.1.2-0ubuntu1
        libjson-glib-1.0-common:
        -   arch: all
            category: libs
            name: libjson-glib-1.0-common
            origin: Ubuntu
            source: apt
            version: 1.1.2-0ubuntu1
        libk5crypto3:
        -   arch: amd64
            category: libs
            name: libk5crypto3
            origin: Ubuntu
            source: apt
            version: 1.13.2+dfsg-5ubuntu2.1
        libkate1:
        -   arch: amd64
            category: universe/libs
            name: libkate1
            origin: Ubuntu
            source: apt
            version: 0.4.1-7
        libkeyutils1:
        -   arch: amd64
            category: misc
            name: libkeyutils1
            origin: Ubuntu
            source: apt
            version: 1.5.9-8ubuntu1
        libklibc:
        -   arch: amd64
            category: libs
            name: libklibc
            origin: Ubuntu
            source: apt
            version: 2.0.4-8ubuntu1.16.04.4
        libkmod2:
        -   arch: amd64
            category: libs
            name: libkmod2
            origin: Ubuntu
            source: apt
            version: 22-1ubuntu5.2
        libkrb5-26-heimdal:
        -   arch: amd64
            category: libs
            name: libkrb5-26-heimdal
            origin: Ubuntu
            source: apt
            version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
        libkrb5-3:
        -   arch: amd64
            category: libs
            name: libkrb5-3
            origin: Ubuntu
            source: apt
            version: 1.13.2+dfsg-5ubuntu2.1
        libkrb5support0:
        -   arch: amd64
            category: libs
            name: libkrb5support0
            origin: Ubuntu
            source: apt
            version: 1.13.2+dfsg-5ubuntu2.1
        libksba8:
        -   arch: amd64
            category: libs
            name: libksba8
            origin: Ubuntu
            source: apt
            version: 1.3.3-1ubuntu0.16.04.1
        liblangtag-common:
        -   arch: all
            category: libs
            name: liblangtag-common
            origin: Ubuntu
            source: apt
            version: 0.5.7-2ubuntu1
        liblangtag1:
        -   arch: amd64
            category: libs
            name: liblangtag1
            origin: Ubuntu
            source: apt
            version: 0.5.7-2ubuntu1
        liblcms2-2:
        -   arch: amd64
            category: libs
            name: liblcms2-2
            origin: Ubuntu
            source: apt
            version: 2.6-3ubuntu2.1
        libldap-2.4-2:
        -   arch: amd64
            category: libs
            name: libldap-2.4-2
            origin: Ubuntu
            source: apt
            version: 2.4.42+dfsg-2ubuntu3.8
        libldb1:
        -   arch: amd64
            category: libs
            name: libldb1
            origin: Ubuntu
            source: apt
            version: 2:1.1.24-1ubuntu3.1
        libldns1:
        -   arch: amd64
            category: libs
            name: libldns1
            origin: Ubuntu
            source: apt
            version: 1.6.17-8ubuntu0.1
        liblircclient0:
        -   arch: amd64
            category: libs
            name: liblircclient0
            origin: Ubuntu
            source: apt
            version: 0.9.0-0ubuntu6
        liblist-moreutils-perl:
        -   arch: amd64
            category: perl
            name: liblist-moreutils-perl
            origin: Ubuntu
            source: apt
            version: 0.413-1build1
        libllvm6.0:
        -   arch: amd64
            category: libs
            name: libllvm6.0
            origin: Ubuntu
            source: apt
            version: 1:6.0-1ubuntu2~16.04.1
        liblocale-gettext-perl:
        -   arch: amd64
            category: perl
            name: liblocale-gettext-perl
            origin: Ubuntu
            source: apt
            version: 1.07-1build1
        liblqr-1-0:
        -   arch: amd64
            category: libs
            name: liblqr-1-0
            origin: Ubuntu
            source: apt
            version: 0.4.2-2
        liblsan0:
        -   arch: amd64
            category: libs
            name: liblsan0
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libltdl7:
        -   arch: amd64
            category: libs
            name: libltdl7
            origin: Ubuntu
            source: apt
            version: 2.4.6-0.1
        liblua5.1-0:
        -   arch: amd64
            category: libs
            name: liblua5.1-0
            origin: Ubuntu
            source: apt
            version: 5.1.5-8ubuntu1
        liblua5.2-0:
        -   arch: amd64
            category: libs
            name: liblua5.2-0
            origin: Ubuntu
            source: apt
            version: 5.2.4-1ubuntu1
        libluajit-5.1-2:
        -   arch: amd64
            category: universe/interpreters
            name: libluajit-5.1-2
            origin: Ubuntu
            source: apt
            version: 2.0.4+dfsg-1
        libluajit-5.1-common:
        -   arch: all
            category: universe/interpreters
            name: libluajit-5.1-common
            origin: Ubuntu
            source: apt
            version: 2.0.4+dfsg-1
        liblvm2app2.2:
        -   arch: amd64
            category: libs
            name: liblvm2app2.2
            origin: Ubuntu
            source: apt
            version: 2.02.133-1ubuntu10
        liblvm2cmd2.02:
        -   arch: amd64
            category: libs
            name: liblvm2cmd2.02
            origin: Ubuntu
            source: apt
            version: 2.02.133-1ubuntu10
        liblwp-mediatypes-perl:
        -   arch: all
            category: perl
            name: liblwp-mediatypes-perl
            origin: Ubuntu
            source: apt
            version: 6.02-1
        liblwp-protocol-https-perl:
        -   arch: all
            category: perl
            name: liblwp-protocol-https-perl
            origin: Ubuntu
            source: apt
            version: 6.06-2
        liblwres141:
        -   arch: amd64
            category: libs
            name: liblwres141
            origin: Ubuntu
            source: apt
            version: 1:9.10.3.dfsg.P4-8ubuntu1.16
        liblz4-1:
        -   arch: amd64
            category: libs
            name: liblz4-1
            origin: Ubuntu
            source: apt
            version: 0.0~r131-2ubuntu2
        liblzma5:
        -   arch: amd64
            category: libs
            name: liblzma5
            origin: Ubuntu
            source: apt
            version: 5.1.1alpha+20120614-2ubuntu2
        liblzo2-2:
        -   arch: amd64
            category: libs
            name: liblzo2-2
            origin: Ubuntu
            source: apt
            version: 2.08-1.2
        libmad0:
        -   arch: amd64
            category: universe/libs
            name: libmad0
            origin: Ubuntu
            source: apt
            version: 0.15.1b-9ubuntu16.04.1
        libmagic1:
        -   arch: amd64
            category: libs
            name: libmagic1
            origin: Ubuntu
            source: apt
            version: 1:5.25-2ubuntu1.4
        libmagickcore-6.q16-2:
        -   arch: amd64
            category: libs
            name: libmagickcore-6.q16-2
            origin: Ubuntu
            source: apt
            version: 8:6.8.9.9-7ubuntu5.15
        libmagickcore-6.q16-2-extra:
        -   arch: amd64
            category: libs
            name: libmagickcore-6.q16-2-extra
            origin: Ubuntu
            source: apt
            version: 8:6.8.9.9-7ubuntu5.15
        libmagickwand-6.q16-2:
        -   arch: amd64
            category: libs
            name: libmagickwand-6.q16-2
            origin: Ubuntu
            source: apt
            version: 8:6.8.9.9-7ubuntu5.15
        libmailtools-perl:
        -   arch: all
            category: perl
            name: libmailtools-perl
            origin: Ubuntu
            source: apt
            version: 2.13-1
        libmhash2:
        -   arch: amd64
            category: libs
            name: libmhash2
            origin: Ubuntu
            source: apt
            version: 0.9.9.9-7
        libmimic0:
        -   arch: amd64
            category: universe/libs
            name: libmimic0
            origin: Ubuntu
            source: apt
            version: 1.0.4-2.3
        libmirclient9:
        -   arch: amd64
            category: libs
            name: libmirclient9
            origin: Ubuntu
            source: apt
            version: 0.26.3+16.04.20170605-0ubuntu1.1
        libmircommon7:
        -   arch: amd64
            category: libs
            name: libmircommon7
            origin: Ubuntu
            source: apt
            version: 0.26.3+16.04.20170605-0ubuntu1.1
        libmircore1:
        -   arch: amd64
            category: libs
            name: libmircore1
            origin: Ubuntu
            source: apt
            version: 0.26.3+16.04.20170605-0ubuntu1.1
        libmirprotobuf3:
        -   arch: amd64
            category: libs
            name: libmirprotobuf3
            origin: Ubuntu
            source: apt
            version: 0.26.3+16.04.20170605-0ubuntu1.1
        libmjpegutils-2.1-0:
        -   arch: amd64
            category: universe/libs
            name: libmjpegutils-2.1-0
            origin: Ubuntu
            source: apt
            version: 1:2.1.0+debian-4
        libmms0:
        -   arch: amd64
            category: universe/libs
            name: libmms0
            origin: Ubuntu
            source: apt
            version: 0.6.4-1
        libmnl0:
        -   arch: amd64
            category: libs
            name: libmnl0
            origin: Ubuntu
            source: apt
            version: 1.0.3-5
        libmodplug1:
        -   arch: amd64
            category: universe/libs
            name: libmodplug1
            origin: Ubuntu
            source: apt
            version: 1:0.8.8.5-2
        libmount1:
        -   arch: amd64
            category: libs
            name: libmount1
            origin: Ubuntu
            source: apt
            version: 2.27.1-6ubuntu3.10
        libmp3lame0:
        -   arch: amd64
            category: universe/libs
            name: libmp3lame0
            origin: Ubuntu
            source: apt
            version: 3.99.5+repack1-9build1
        libmpc3:
        -   arch: amd64
            category: libs
            name: libmpc3
            origin: Ubuntu
            source: apt
            version: 1.0.3-1
        libmpdec2:
        -   arch: amd64
            category: libs
            name: libmpdec2
            origin: Ubuntu
            source: apt
            version: 2.4.2-1
        libmpeg2-4:
        -   arch: amd64
            category: universe/libs
            name: libmpeg2-4
            origin: Ubuntu
            source: apt
            version: 0.5.1-7
        libmpeg2encpp-2.1-0:
        -   arch: amd64
            category: universe/libs
            name: libmpeg2encpp-2.1-0
            origin: Ubuntu
            source: apt
            version: 1:2.1.0+debian-4
        libmpfr4:
        -   arch: amd64
            category: libs
            name: libmpfr4
            origin: Ubuntu
            source: apt
            version: 3.1.4-1
        libmpg123-0:
        -   arch: amd64
            category: universe/libs
            name: libmpg123-0
            origin: Ubuntu
            source: apt
            version: 1.22.4-1ubuntu0.1
        libmplex2-2.1-0:
        -   arch: amd64
            category: universe/libs
            name: libmplex2-2.1-0
            origin: Ubuntu
            source: apt
            version: 1:2.1.0+debian-4
        libmpx0:
        -   arch: amd64
            category: libs
            name: libmpx0
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libmspack0:
        -   arch: amd64
            category: libs
            name: libmspack0
            origin: Ubuntu
            source: apt
            version: 0.5-1ubuntu0.16.04.4
        libmspub-0.1-1:
        -   arch: amd64
            category: libs
            name: libmspub-0.1-1
            origin: Ubuntu
            source: apt
            version: 0.1.2-2ubuntu1
        libmtdev1:
        -   arch: amd64
            category: libs
            name: libmtdev1
            origin: Ubuntu
            source: apt
            version: 1.1.5-1ubuntu2
        libmwaw-0.3-3:
        -   arch: amd64
            category: libs
            name: libmwaw-0.3-3
            origin: Ubuntu
            source: apt
            version: 0.3.7-1ubuntu2.1
        libmythes-1.2-0:
        -   arch: amd64
            category: libs
            name: libmythes-1.2-0
            origin: Ubuntu
            source: apt
            version: 2:1.2.4-1ubuntu3
        libncurses5:
        -   arch: amd64
            category: libs
            name: libncurses5
            origin: Ubuntu
            source: apt
            version: 6.0+20160213-1ubuntu1
        libncurses5-dev:
        -   arch: amd64
            category: libdevel
            name: libncurses5-dev
            origin: Ubuntu
            source: apt
            version: 6.0+20160213-1ubuntu1
        libncursesw5:
        -   arch: amd64
            category: libs
            name: libncursesw5
            origin: Ubuntu
            source: apt
            version: 6.0+20160213-1ubuntu1
        libneon27-gnutls:
        -   arch: amd64
            category: libs
            name: libneon27-gnutls
            origin: Ubuntu
            source: apt
            version: 0.30.1-3build1
        libnet-cidr-perl:
        -   arch: all
            category: perl
            name: libnet-cidr-perl
            origin: Ubuntu
            source: apt
            version: 0.17-1
        libnet-http-perl:
        -   arch: all
            category: perl
            name: libnet-http-perl
            origin: Ubuntu
            source: apt
            version: 6.09-1
        libnet-server-perl:
        -   arch: all
            category: perl
            name: libnet-server-perl
            origin: Ubuntu
            source: apt
            version: 2.008-2
        libnet-smtp-ssl-perl:
        -   arch: all
            category: perl
            name: libnet-smtp-ssl-perl
            origin: Ubuntu
            source: apt
            version: 1.03-1
        libnet-snmp-perl:
        -   arch: all
            category: perl
            name: libnet-snmp-perl
            origin: Ubuntu
            source: apt
            version: 6.0.1-2
        libnet-ssleay-perl:
        -   arch: amd64
            category: perl
            name: libnet-ssleay-perl
            origin: Ubuntu
            source: apt
            version: 1.72-1build1
        libnetpbm10:
        -   arch: amd64
            category: libs
            name: libnetpbm10
            origin: Ubuntu
            source: apt
            version: 2:10.0-15.3
        libnettle6:
        -   arch: amd64
            category: libs
            name: libnettle6
            origin: Ubuntu
            source: apt
            version: 3.2-1ubuntu0.16.04.1
        libnewt0.52:
        -   arch: amd64
            category: libs
            name: libnewt0.52
            origin: Ubuntu
            source: apt
            version: 0.52.18-1ubuntu2
        libnfnetlink0:
        -   arch: amd64
            category: libs
            name: libnfnetlink0
            origin: Ubuntu
            source: apt
            version: 1.0.1-3
        libnice10:
        -   arch: amd64
            category: libs
            name: libnice10
            origin: BigBlueButton
            source: apt
            version: 0.1.15-1kurento3.16.04
        libnih1:
        -   arch: amd64
            category: libs
            name: libnih1
            origin: Ubuntu
            source: apt
            version: 1.0.3-4.3ubuntu1
        libnl-3-200:
        -   arch: amd64
            category: libs
            name: libnl-3-200
            origin: Ubuntu
            source: apt
            version: 3.2.27-1ubuntu0.16.04.1
        libnl-genl-3-200:
        -   arch: amd64
            category: libs
            name: libnl-genl-3-200
            origin: Ubuntu
            source: apt
            version: 3.2.27-1ubuntu0.16.04.1
        libnotify4:
        -   arch: amd64
            category: libs
            name: libnotify4
            origin: Ubuntu
            source: apt
            version: 0.7.6-2svn1
        libnpth0:
        -   arch: amd64
            category: libs
            name: libnpth0
            origin: Ubuntu
            source: apt
            version: 1.2-3
        libnspr4:
        -   arch: amd64
            category: libs
            name: libnspr4
            origin: Ubuntu
            source: apt
            version: 2:4.13.1-0ubuntu0.16.04.1
        libnss3:
        -   arch: amd64
            category: libs
            name: libnss3
            origin: Ubuntu
            source: apt
            version: 2:3.28.4-0ubuntu0.16.04.10
        libnss3-nssdb:
        -   arch: all
            category: admin
            name: libnss3-nssdb
            origin: Ubuntu
            source: apt
            version: 2:3.28.4-0ubuntu0.16.04.10
        libnuma1:
        -   arch: amd64
            category: libs
            name: libnuma1
            origin: Ubuntu
            source: apt
            version: 2.0.11-1ubuntu1.1
        libodfgen-0.1-1:
        -   arch: amd64
            category: libs
            name: libodfgen-0.1-1
            origin: Ubuntu
            source: apt
            version: 0.1.6-1ubuntu2
        libofa0:
        -   arch: amd64
            category: universe/libs
            name: libofa0
            origin: Ubuntu
            source: apt
            version: 0.9.3-10ubuntu1
        libogg0:
        -   arch: amd64
            category: libs
            name: libogg0
            origin: Ubuntu
            source: apt
            version: 1.3.2-1
        libopenal-data:
        -   arch: all
            category: universe/libs
            name: libopenal-data
            origin: Ubuntu
            source: apt
            version: 1:1.16.0-3
        libopenal1:
        -   arch: amd64
            category: universe/libs
            name: libopenal1
            origin: Ubuntu
            source: apt
            version: 1:1.16.0-3
        libopencore-amrnb0:
        -   arch: amd64
            category: universe/libs
            name: libopencore-amrnb0
            origin: Ubuntu
            source: apt
            version: 0.1.3-2.1
        libopencore-amrwb0:
        -   arch: amd64
            category: universe/libs
            name: libopencore-amrwb0
            origin: Ubuntu
            source: apt
            version: 0.1.3-2.1
        libopencv-core2.4v5:
        -   arch: amd64
            category: universe/libs
            name: libopencv-core2.4v5
            origin: Ubuntu
            source: apt
            version: 2.4.9.1+dfsg-1.5ubuntu1.1
        libopencv-highgui2.4v5:
        -   arch: amd64
            category: universe/libs
            name: libopencv-highgui2.4v5
            origin: Ubuntu
            source: apt
            version: 2.4.9.1+dfsg-1.5ubuntu1.1
        libopencv-imgproc2.4v5:
        -   arch: amd64
            category: universe/libs
            name: libopencv-imgproc2.4v5
            origin: Ubuntu
            source: apt
            version: 2.4.9.1+dfsg-1.5ubuntu1.1
        libopencv-objdetect2.4v5:
        -   arch: amd64
            category: universe/libs
            name: libopencv-objdetect2.4v5
            origin: Ubuntu
            source: apt
            version: 2.4.9.1+dfsg-1.5ubuntu1.1
        libopenexr22:
        -   arch: amd64
            category: libs
            name: libopenexr22
            origin: Ubuntu
            source: apt
            version: 2.2.0-10ubuntu2.2
        libopenjpeg5:
        -   arch: amd64
            category: universe/libs
            name: libopenjpeg5
            origin: Ubuntu
            source: apt
            version: 1:1.5.2-3.1
        libopus0:
        -   arch: amd64
            category: libs
            name: libopus0
            origin: Ubuntu
            source: apt
            version: 1.1.2-1ubuntu1
        libopusenc0:
        -   arch: amd64
            category: sound
            name: libopusenc0
            origin: LP-PPA-bigbluebutton-support
            source: apt
            version: 0.2.1-1bbb1
        libopusfile0:
        -   arch: amd64
            category: universe/libs
            name: libopusfile0
            origin: Ubuntu
            source: apt
            version: 0.7-1
        liborc-0.4-0:
        -   arch: amd64
            category: libs
            name: liborc-0.4-0
            origin: Ubuntu
            source: apt
            version: 1:0.4.25-1
        liborcus-0.10-0v5:
        -   arch: amd64
            category: libs
            name: liborcus-0.10-0v5
            origin: Ubuntu
            source: apt
            version: 0.9.2-4ubuntu2
        libp11-kit0:
        -   arch: amd64
            category: libs
            name: libp11-kit0
            origin: Ubuntu
            source: apt
            version: 0.23.2-5~ubuntu16.04.1
        libpagemaker-0.0-0:
        -   arch: amd64
            category: libs
            name: libpagemaker-0.0-0
            origin: Ubuntu
            source: apt
            version: 0.0.3-1ubuntu1
        libpam-modules:
        -   arch: amd64
            category: admin
            name: libpam-modules
            origin: Ubuntu
            source: apt
            version: 1.1.8-3.2ubuntu2.1
        libpam-modules-bin:
        -   arch: amd64
            category: admin
            name: libpam-modules-bin
            origin: Ubuntu
            source: apt
            version: 1.1.8-3.2ubuntu2.1
        libpam-runtime:
        -   arch: all
            category: admin
            name: libpam-runtime
            origin: Ubuntu
            source: apt
            version: 1.1.8-3.2ubuntu2.1
        libpam-systemd:
        -   arch: amd64
            category: admin
            name: libpam-systemd
            origin: Ubuntu
            source: apt
            version: 229-4ubuntu21.28
        libpam0g:
        -   arch: amd64
            category: libs
            name: libpam0g
            origin: Ubuntu
            source: apt
            version: 1.1.8-3.2ubuntu2.1
        libpango-1.0-0:
        -   arch: amd64
            category: libs
            name: libpango-1.0-0
            origin: Ubuntu
            source: apt
            version: 1.38.1-1
        libpangocairo-1.0-0:
        -   arch: amd64
            category: libs
            name: libpangocairo-1.0-0
            origin: Ubuntu
            source: apt
            version: 1.38.1-1
        libpangoft2-1.0-0:
        -   arch: amd64
            category: libs
            name: libpangoft2-1.0-0
            origin: Ubuntu
            source: apt
            version: 1.38.1-1
        libpangox-1.0-0:
        -   arch: amd64
            category: oldlibs
            name: libpangox-1.0-0
            origin: Ubuntu
            source: apt
            version: 0.0.2-5
        libpaper-utils:
        -   arch: amd64
            category: utils
            name: libpaper-utils
            origin: Ubuntu
            source: apt
            version: 1.1.24+nmu4ubuntu1
        libpaper1:
        -   arch: amd64
            category: libs
            name: libpaper1
            origin: Ubuntu
            source: apt
            version: 1.1.24+nmu4ubuntu1
        libparse-debianchangelog-perl:
        -   arch: all
            category: perl
            name: libparse-debianchangelog-perl
            origin: Ubuntu
            source: apt
            version: 1.2.0-8
        libpci3:
        -   arch: amd64
            category: libs
            name: libpci3
            origin: Ubuntu
            source: apt
            version: 1:3.3.1-1.1ubuntu1.3
        libpciaccess0:
        -   arch: amd64
            category: libs
            name: libpciaccess0
            origin: Ubuntu
            source: apt
            version: 0.13.4-1
        libpcre16-3:
        -   arch: amd64
            category: libs
            name: libpcre16-3
            origin: Ubuntu
            source: apt
            version: 2:8.38-3.1
        libpcre3:
        -   arch: amd64
            category: libs
            name: libpcre3
            origin: Ubuntu
            source: apt
            version: 2:8.38-3.1
        libpcsclite1:
        -   arch: amd64
            category: libs
            name: libpcsclite1
            origin: Ubuntu
            source: apt
            version: 1.8.14-1ubuntu1.16.04.1
        libperl5.22:
        -   arch: amd64
            category: libs
            name: libperl5.22
            origin: Ubuntu
            source: apt
            version: 5.22.1-9ubuntu0.6
        libpixman-1-0:
        -   arch: amd64
            category: libs
            name: libpixman-1-0
            origin: Ubuntu
            source: apt
            version: 0.33.6-1
        libplymouth4:
        -   arch: amd64
            category: libs
            name: libplymouth4
            origin: Ubuntu
            source: apt
            version: 0.9.2-3ubuntu13.5
        libpng12-0:
        -   arch: amd64
            category: libs
            name: libpng12-0
            origin: Ubuntu
            source: apt
            version: 1.2.54-1ubuntu1.1
        libpolkit-agent-1-0:
        -   arch: amd64
            category: libs
            name: libpolkit-agent-1-0
            origin: Ubuntu
            source: apt
            version: 0.105-14.1ubuntu0.5
        libpolkit-backend-1-0:
        -   arch: amd64
            category: libs
            name: libpolkit-backend-1-0
            origin: Ubuntu
            source: apt
            version: 0.105-14.1ubuntu0.5
        libpolkit-gobject-1-0:
        -   arch: amd64
            category: libs
            name: libpolkit-gobject-1-0
            origin: Ubuntu
            source: apt
            version: 0.105-14.1ubuntu0.5
        libpoppler58:
        -   arch: amd64
            category: libs
            name: libpoppler58
            origin: Ubuntu
            source: apt
            version: 0.41.0-0ubuntu1.14
        libpopt0:
        -   arch: amd64
            category: libs
            name: libpopt0
            origin: Ubuntu
            source: apt
            version: 1.16-10
        libpostproc-ffmpeg53:
        -   arch: amd64
            category: universe/libs
            name: libpostproc-ffmpeg53
            origin: Ubuntu
            source: apt
            version: 7:2.8.15-0ubuntu0.16.04.1
        libpostproc55:
        -   arch: amd64
            category: libs
            name: libpostproc55
            origin: LP-PPA-bigbluebutton-support
            source: apt
            version: 7:4.2.2-1bbb1~ubuntu16.04
        libprocps4:
        -   arch: amd64
            category: libs
            name: libprocps4
            origin: Ubuntu
            source: apt
            version: 2:3.3.10-4ubuntu2.5
        libprotobuf-lite9v5:
        -   arch: amd64
            category: libs
            name: libprotobuf-lite9v5
            origin: Ubuntu
            source: apt
            version: 2.6.1-1.3
        libproxy1v5:
        -   arch: amd64
            category: libs
            name: libproxy1v5
            origin: Ubuntu
            source: apt
            version: 0.4.11-5ubuntu1
        libpulse0:
        -   arch: amd64
            category: libs
            name: libpulse0
            origin: Ubuntu
            source: apt
            version: 1:8.0-0ubuntu3.12
        libpython-stdlib:
        -   arch: amd64
            category: python
            name: libpython-stdlib
            origin: Ubuntu
            source: apt
            version: 2.7.12-1~16.04
        libpython2.7:
        -   arch: amd64
            category: libs
            name: libpython2.7
            origin: Ubuntu
            source: apt
            version: 2.7.12-1ubuntu0~16.04.11
        libpython2.7-minimal:
        -   arch: amd64
            category: python
            name: libpython2.7-minimal
            origin: Ubuntu
            source: apt
            version: 2.7.12-1ubuntu0~16.04.11
        libpython2.7-stdlib:
        -   arch: amd64
            category: python
            name: libpython2.7-stdlib
            origin: Ubuntu
            source: apt
            version: 2.7.12-1ubuntu0~16.04.11
        libpython3-stdlib:
        -   arch: amd64
            category: python
            name: libpython3-stdlib
            origin: Ubuntu
            source: apt
            version: 3.5.1-3
        libpython3.5:
        -   arch: amd64
            category: libs
            name: libpython3.5
            origin: Ubuntu
            source: apt
            version: 3.5.2-2ubuntu0~16.04.10
        libpython3.5-minimal:
        -   arch: amd64
            category: python
            name: libpython3.5-minimal
            origin: Ubuntu
            source: apt
            version: 3.5.2-2ubuntu0~16.04.10
        libpython3.5-stdlib:
        -   arch: amd64
            category: python
            name: libpython3.5-stdlib
            origin: Ubuntu
            source: apt
            version: 3.5.2-2ubuntu0~16.04.10
        libqt5core5a:
        -   arch: amd64
            category: libs
            name: libqt5core5a
            origin: Ubuntu
            source: apt
            version: 5.5.1+dfsg-16ubuntu7.7
        libqt5dbus5:
        -   arch: amd64
            category: libs
            name: libqt5dbus5
            origin: Ubuntu
            source: apt
            version: 5.5.1+dfsg-16ubuntu7.7
        libqt5gui5:
        -   arch: amd64
            category: libs
            name: libqt5gui5
            origin: Ubuntu
            source: apt
            version: 5.5.1+dfsg-16ubuntu7.7
        libqt5network5:
        -   arch: amd64
            category: libs
            name: libqt5network5
            origin: Ubuntu
            source: apt
            version: 5.5.1+dfsg-16ubuntu7.7
        libqt5printsupport5:
        -   arch: amd64
            category: libs
            name: libqt5printsupport5
            origin: Ubuntu
            source: apt
            version: 5.5.1+dfsg-16ubuntu7.7
        libqt5svg5:
        -   arch: amd64
            category: libs
            name: libqt5svg5
            origin: Ubuntu
            source: apt
            version: 5.5.1-2build1
        libqt5widgets5:
        -   arch: amd64
            category: libs
            name: libqt5widgets5
            origin: Ubuntu
            source: apt
            version: 5.5.1+dfsg-16ubuntu7.7
        libquadmath0:
        -   arch: amd64
            category: libs
            name: libquadmath0
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libraptor2-0:
        -   arch: amd64
            category: libs
            name: libraptor2-0
            origin: Ubuntu
            source: apt
            version: 2.0.14-1
        librasqal3:
        -   arch: amd64
            category: libs
            name: librasqal3
            origin: Ubuntu
            source: apt
            version: 0.9.32-1
        libraw1394-11:
        -   arch: amd64
            category: libs
            name: libraw1394-11
            origin: Ubuntu
            source: apt
            version: 2.1.1-2
        librdf0:
        -   arch: amd64
            category: libs
            name: librdf0
            origin: Ubuntu
            source: apt
            version: 1.0.17-1build1
        libreadline5:
        -   arch: amd64
            category: libs
            name: libreadline5
            origin: Ubuntu
            source: apt
            version: 5.2+dfsg-3build1
        libreadline6:
        -   arch: amd64
            category: libs
            name: libreadline6
            origin: Ubuntu
            source: apt
            version: 6.3-8ubuntu2
        libreoffice:
        -   arch: amd64
            category: universe/editors
            name: libreoffice
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-avmedia-backend-gstreamer:
        -   arch: amd64
            category: misc
            name: libreoffice-avmedia-backend-gstreamer
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-base:
        -   arch: amd64
            category: database
            name: libreoffice-base
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-base-core:
        -   arch: amd64
            category: editors
            name: libreoffice-base-core
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-base-drivers:
        -   arch: amd64
            category: database
            name: libreoffice-base-drivers
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-calc:
        -   arch: amd64
            category: editors
            name: libreoffice-calc
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-common:
        -   arch: all
            category: editors
            name: libreoffice-common
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-core:
        -   arch: amd64
            category: editors
            name: libreoffice-core
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-draw:
        -   arch: amd64
            category: editors
            name: libreoffice-draw
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-gnome:
        -   arch: amd64
            category: gnome
            name: libreoffice-gnome
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-gtk:
        -   arch: amd64
            category: gnome
            name: libreoffice-gtk
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-impress:
        -   arch: amd64
            category: editors
            name: libreoffice-impress
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-java-common:
        -   arch: all
            category: editors
            name: libreoffice-java-common
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-math:
        -   arch: amd64
            category: editors
            name: libreoffice-math
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-pdfimport:
        -   arch: amd64
            category: misc
            name: libreoffice-pdfimport
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-report-builder-bin:
        -   arch: amd64
            category: universe/misc
            name: libreoffice-report-builder-bin
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-sdbc-firebird:
        -   arch: amd64
            category: database
            name: libreoffice-sdbc-firebird
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-sdbc-hsqldb:
        -   arch: amd64
            category: database
            name: libreoffice-sdbc-hsqldb
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-style-elementary:
        -   arch: all
            category: universe/x11
            name: libreoffice-style-elementary
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-style-galaxy:
        -   arch: all
            category: editors
            name: libreoffice-style-galaxy
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        libreoffice-writer:
        -   arch: amd64
            category: editors
            name: libreoffice-writer
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        librest-0.7-0:
        -   arch: amd64
            category: libs
            name: librest-0.7-0
            origin: Ubuntu
            source: apt
            version: 0.7.93-1
        librevenge-0.0-0:
        -   arch: amd64
            category: libs
            name: librevenge-0.0-0
            origin: Ubuntu
            source: apt
            version: 0.0.4-4ubuntu1
        libroken18-heimdal:
        -   arch: amd64
            category: libs
            name: libroken18-heimdal
            origin: Ubuntu
            source: apt
            version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
        librsvg2-2:
        -   arch: amd64
            category: libs
            name: librsvg2-2
            origin: Ubuntu
            source: apt
            version: 2.40.13-3
        librsvg2-common:
        -   arch: amd64
            category: libs
            name: librsvg2-common
            origin: Ubuntu
            source: apt
            version: 2.40.13-3
        librtmp1:
        -   arch: amd64
            category: libs
            name: librtmp1
            origin: Ubuntu
            source: apt
            version: 2.4+20151223.gitfa8646d-1ubuntu0.1
        librubberband2v5:
        -   arch: amd64
            category: universe/libs
            name: librubberband2v5
            origin: Ubuntu
            source: apt
            version: 1.8.1-6ubuntu2
        libruby2.3:
        -   arch: amd64
            category: libs
            name: libruby2.3
            origin: Ubuntu
            source: apt
            version: 2.3.1-2~ubuntu16.04.14
        libsamplerate0:
        -   arch: amd64
            category: libs
            name: libsamplerate0
            origin: Ubuntu
            source: apt
            version: 0.1.8-8
        libsane:
        -   arch: amd64
            category: libs
            name: libsane
            origin: Ubuntu
            source: apt
            version: 1.0.25+git20150528-1ubuntu2.16.04.1
        libsane-common:
        -   arch: all
            category: libs
            name: libsane-common
            origin: Ubuntu
            source: apt
            version: 1.0.25+git20150528-1ubuntu2.16.04.1
        libsasl2-2:
        -   arch: amd64
            category: libs
            name: libsasl2-2
            origin: Ubuntu
            source: apt
            version: 2.1.26.dfsg1-14ubuntu0.2
        libsasl2-modules:
        -   arch: amd64
            category: devel
            name: libsasl2-modules
            origin: Ubuntu
            source: apt
            version: 2.1.26.dfsg1-14ubuntu0.2
        libsasl2-modules-db:
        -   arch: amd64
            category: libs
            name: libsasl2-modules-db
            origin: Ubuntu
            source: apt
            version: 2.1.26.dfsg1-14ubuntu0.2
        libsbc1:
        -   arch: amd64
            category: libs
            name: libsbc1
            origin: Ubuntu
            source: apt
            version: 1.3-1
        libschroedinger-1.0-0:
        -   arch: amd64
            category: universe/libs
            name: libschroedinger-1.0-0
            origin: Ubuntu
            source: apt
            version: 1.0.11-2.1build1
        libsdl1.2debian:
        -   arch: amd64
            category: libs
            name: libsdl1.2debian
            origin: Ubuntu
            source: apt
            version: 1.2.15+dfsg1-3ubuntu0.1
        libsdl2-2.0-0:
        -   arch: amd64
            category: universe/libs
            name: libsdl2-2.0-0
            origin: Ubuntu
            source: apt
            version: 2.0.4+dfsg1-2ubuntu2.16.04.2
        libseccomp2:
        -   arch: amd64
            category: libs
            name: libseccomp2
            origin: Ubuntu
            source: apt
            version: 2.4.1-0ubuntu0.16.04.2
        libselinux1:
        -   arch: amd64
            category: libs
            name: libselinux1
            origin: Ubuntu
            source: apt
            version: 2.4-3build2
        libsemanage-common:
        -   arch: all
            category: libs
            name: libsemanage-common
            origin: Ubuntu
            source: apt
            version: 2.3-1build3
        libsemanage1:
        -   arch: amd64
            category: libs
            name: libsemanage1
            origin: Ubuntu
            source: apt
            version: 2.3-1build3
        libsensors4:
        -   arch: amd64
            category: libs
            name: libsensors4
            origin: Ubuntu
            source: apt
            version: 1:3.4.0-2
        libsepol1:
        -   arch: amd64
            category: libs
            name: libsepol1
            origin: Ubuntu
            source: apt
            version: 2.4-2
        libservlet3.1-java:
        -   arch: all
            category: java
            name: libservlet3.1-java
            origin: Ubuntu
            source: apt
            version: 8.0.32-1ubuntu1.11
        libshine3:
        -   arch: amd64
            category: universe/libs
            name: libshine3
            origin: Ubuntu
            source: apt
            version: 3.1.0-4
        libshout3:
        -   arch: amd64
            category: libs
            name: libshout3
            origin: Ubuntu
            source: apt
            version: 2.3.1-3
        libsidplay1v5:
        -   arch: amd64
            category: universe/libs
            name: libsidplay1v5
            origin: Ubuntu
            source: apt
            version: 1.36.59-8
        libsigc++-2.0-0v5:
        -   arch: amd64
            category: libs
            name: libsigc++-2.0-0v5
            origin: Ubuntu
            source: apt
            version: 2.6.2-1
        libsigsegv2:
        -   arch: amd64
            category: libs
            name: libsigsegv2
            origin: Ubuntu
            source: apt
            version: 2.10-4
        libslang2:
        -   arch: amd64
            category: libs
            name: libslang2
            origin: Ubuntu
            source: apt
            version: 2.3.0-2ubuntu1.1
        libsm6:
        -   arch: amd64
            category: libs
            name: libsm6
            origin: Ubuntu
            source: apt
            version: 2:1.2.2-1
        libsmartcols1:
        -   arch: amd64
            category: libs
            name: libsmartcols1
            origin: Ubuntu
            source: apt
            version: 2.27.1-6ubuntu3.10
        libsmbclient:
        -   arch: amd64
            category: libs
            name: libsmbclient
            origin: Ubuntu
            source: apt
            version: 2:4.3.11+dfsg-0ubuntu0.16.04.27
        libsnappy1v5:
        -   arch: amd64
            category: libs
            name: libsnappy1v5
            origin: Ubuntu
            source: apt
            version: 1.1.3-2
        libsndfile1:
        -   arch: amd64
            category: libs
            name: libsndfile1
            origin: Ubuntu
            source: apt
            version: 1.0.25-10ubuntu0.16.04.2
        libsndio6.1:
        -   arch: amd64
            category: universe/libs
            name: libsndio6.1
            origin: Ubuntu
            source: apt
            version: 1.1.0-2
        libsocket6-perl:
        -   arch: amd64
            category: perl
            name: libsocket6-perl
            origin: Ubuntu
            source: apt
            version: 0.25-1build2
        libsoundtouch1:
        -   arch: amd64
            category: universe/libs
            name: libsoundtouch1
            origin: Ubuntu
            source: apt
            version: 1.9.2-2+deb9u1build0.16.04.1
        libsoup-gnome2.4-1:
        -   arch: amd64
            category: libs
            name: libsoup-gnome2.4-1
            origin: Ubuntu
            source: apt
            version: 2.52.2-1ubuntu0.3
        libsoup2.4-1:
        -   arch: amd64
            category: libs
            name: libsoup2.4-1
            origin: Ubuntu
            source: apt
            version: 2.52.2-1ubuntu0.3
        libsox-fmt-alsa:
        -   arch: amd64
            category: universe/sound
            name: libsox-fmt-alsa
            origin: Ubuntu
            source: apt
            version: 14.4.1-5+deb8u4ubuntu0.1
        libsox-fmt-base:
        -   arch: amd64
            category: universe/sound
            name: libsox-fmt-base
            origin: Ubuntu
            source: apt
            version: 14.4.1-5+deb8u4ubuntu0.1
        libsox2:
        -   arch: amd64
            category: universe/sound
            name: libsox2
            origin: Ubuntu
            source: apt
            version: 14.4.1-5+deb8u4ubuntu0.1
        libsoxr0:
        -   arch: amd64
            category: universe/libs
            name: libsoxr0
            origin: Ubuntu
            source: apt
            version: 0.1.2-1
        libspandsp2:
        -   arch: amd64
            category: universe/libs
            name: libspandsp2
            origin: Ubuntu
            source: apt
            version: 0.0.6-2.1
        libspeex1:
        -   arch: amd64
            category: libs
            name: libspeex1
            origin: Ubuntu
            source: apt
            version: 1.2~rc1.2-1ubuntu1
        libspeexdsp1:
        -   arch: amd64
            category: libs
            name: libspeexdsp1
            origin: Ubuntu
            source: apt
            version: 1.2~rc1.2-1ubuntu1
        libsqlite3-0:
        -   arch: amd64
            category: libs
            name: libsqlite3-0
            origin: Ubuntu
            source: apt
            version: 3.11.0-1ubuntu1.4
        libsrtp0:
        -   arch: amd64
            category: libs
            name: libsrtp0
            origin: BigBlueButton
            source: apt
            version: 1.6.0-0kurento1.16.04
        libss2:
        -   arch: amd64
            category: libs
            name: libss2
            origin: Ubuntu
            source: apt
            version: 1.42.13-1ubuntu1.2
        libssh-gcrypt-4:
        -   arch: amd64
            category: libs
            name: libssh-gcrypt-4
            origin: Ubuntu
            source: apt
            version: 0.6.3-4.3ubuntu0.5
        libssl1.0.0:
        -   arch: amd64
            category: libs
            name: libssl1.0.0
            origin: Ubuntu
            source: apt
            version: 1.0.2g-1ubuntu4.16
        libstdc++-5-dev:
        -   arch: amd64
            category: libdevel
            name: libstdc++-5-dev
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libstdc++6:
        -   arch: amd64
            category: libs
            name: libstdc++6
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libstring-crc32-perl:
        -   arch: amd64
            category: universe/perl
            name: libstring-crc32-perl
            origin: Ubuntu
            source: apt
            version: 1.5-1build2
        libsub-name-perl:
        -   arch: amd64
            category: perl
            name: libsub-name-perl
            origin: Ubuntu
            source: apt
            version: 0.14-1build1
        libsuitesparseconfig4.4.6:
        -   arch: amd64
            category: libs
            name: libsuitesparseconfig4.4.6
            origin: Ubuntu
            source: apt
            version: 1:4.4.6-1
        libswresample-ffmpeg1:
        -   arch: amd64
            category: universe/libs
            name: libswresample-ffmpeg1
            origin: Ubuntu
            source: apt
            version: 7:2.8.15-0ubuntu0.16.04.1
        libswresample3:
        -   arch: amd64
            category: libs
            name: libswresample3
            origin: LP-PPA-bigbluebutton-support
            source: apt
            version: 7:4.2.2-1bbb1~ubuntu16.04
        libswscale-ffmpeg3:
        -   arch: amd64
            category: universe/libs
            name: libswscale-ffmpeg3
            origin: Ubuntu
            source: apt
            version: 7:2.8.15-0ubuntu0.16.04.1
        libswscale5:
        -   arch: amd64
            category: libs
            name: libswscale5
            origin: LP-PPA-bigbluebutton-support
            source: apt
            version: 7:4.2.2-1bbb1~ubuntu16.04
        libsystemd-dev:
        -   arch: amd64
            category: libdevel
            name: libsystemd-dev
            origin: Ubuntu
            source: apt
            version: 229-4ubuntu21.28
        libsystemd0:
        -   arch: amd64
            category: libs
            name: libsystemd0
            origin: Ubuntu
            source: apt
            version: 229-4ubuntu21.28
        libtag1v5:
        -   arch: amd64
            category: libs
            name: libtag1v5
            origin: Ubuntu
            source: apt
            version: 1.9.1-2.4ubuntu1
        libtag1v5-vanilla:
        -   arch: amd64
            category: libs
            name: libtag1v5-vanilla
            origin: Ubuntu
            source: apt
            version: 1.9.1-2.4ubuntu1
        libtalloc2:
        -   arch: amd64
            category: libs
            name: libtalloc2
            origin: Ubuntu
            source: apt
            version: 2.1.5-2
        libtasn1-6:
        -   arch: amd64
            category: libs
            name: libtasn1-6
            origin: Ubuntu
            source: apt
            version: 4.7-3ubuntu0.16.04.3
        libtbb2:
        -   arch: amd64
            category: universe/libs
            name: libtbb2
            origin: Ubuntu
            source: apt
            version: 4.4~20151115-0ubuntu3
        libtdb1:
        -   arch: amd64
            category: libs
            name: libtdb1
            origin: Ubuntu
            source: apt
            version: 1.3.8-2
        libtevent0:
        -   arch: amd64
            category: libs
            name: libtevent0
            origin: Ubuntu
            source: apt
            version: 0.9.28-0ubuntu0.16.04.1
        libtext-charwidth-perl:
        -   arch: amd64
            category: perl
            name: libtext-charwidth-perl
            origin: Ubuntu
            source: apt
            version: 0.04-7build5
        libtext-iconv-perl:
        -   arch: amd64
            category: perl
            name: libtext-iconv-perl
            origin: Ubuntu
            source: apt
            version: 1.7-5build4
        libtext-wrapi18n-perl:
        -   arch: all
            category: perl
            name: libtext-wrapi18n-perl
            origin: Ubuntu
            source: apt
            version: 0.06-7.1
        libthai-data:
        -   arch: all
            category: libs
            name: libthai-data
            origin: Ubuntu
            source: apt
            version: 0.1.24-2
        libthai0:
        -   arch: amd64
            category: libs
            name: libthai0
            origin: Ubuntu
            source: apt
            version: 0.1.24-2
        libtheora0:
        -   arch: amd64
            category: libs
            name: libtheora0
            origin: Ubuntu
            source: apt
            version: 1.1.1+dfsg.1-8
        libtidy-0.99-0:
        -   arch: amd64
            category: libs
            name: libtidy-0.99-0
            origin: Ubuntu
            source: apt
            version: 20091223cvs-1.5
        libtiff5:
        -   arch: amd64
            category: libs
            name: libtiff5
            origin: Ubuntu
            source: apt
            version: 4.0.6-1ubuntu0.7
        libtimedate-perl:
        -   arch: all
            category: perl
            name: libtimedate-perl
            origin: Ubuntu
            source: apt
            version: 2.3000-2
        libtinfo-dev:
        -   arch: amd64
            category: libdevel
            name: libtinfo-dev
            origin: Ubuntu
            source: apt
            version: 6.0+20160213-1ubuntu1
        libtinfo5:
        -   arch: amd64
            category: libs
            name: libtinfo5
            origin: Ubuntu
            source: apt
            version: 6.0+20160213-1ubuntu1
        libtokyocabinet9:
        -   arch: amd64
            category: libs
            name: libtokyocabinet9
            origin: Ubuntu
            source: apt
            version: 1.4.48-10
        libtsan0:
        -   arch: amd64
            category: libs
            name: libtsan0
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libtwolame0:
        -   arch: amd64
            category: universe/libs
            name: libtwolame0
            origin: Ubuntu
            source: apt
            version: 0.3.13-1.2
        libtxc-dxtn-s2tc0:
        -   arch: amd64
            category: libs
            name: libtxc-dxtn-s2tc0
            origin: Ubuntu
            source: apt
            version: 0~git20131104-1.1
        libubsan0:
        -   arch: amd64
            category: libs
            name: libubsan0
            origin: Ubuntu
            source: apt
            version: 5.4.0-6ubuntu1~16.04.12
        libudev1:
        -   arch: amd64
            category: libs
            name: libudev1
            origin: Ubuntu
            source: apt
            version: 229-4ubuntu21.28
        liburi-perl:
        -   arch: all
            category: perl
            name: liburi-perl
            origin: Ubuntu
            source: apt
            version: 1.71-1
        libusb-0.1-4:
        -   arch: amd64
            category: libs
            name: libusb-0.1-4
            origin: Ubuntu
            source: apt
            version: 2:0.1.12-28
        libusb-1.0-0:
        -   arch: amd64
            category: libs
            name: libusb-1.0-0
            origin: Ubuntu
            source: apt
            version: 2:1.0.20-1
        libusrsctp:
        -   arch: amd64
            category: utils
            name: libusrsctp
            origin: BigBlueButton
            source: apt
            version: 0.9.2-1kurento1.16.04
        libustr-1.0-1:
        -   arch: amd64
            category: libs
            name: libustr-1.0-1
            origin: Ubuntu
            source: apt
            version: 1.0.4-5
        libutempter0:
        -   arch: amd64
            category: libs
            name: libutempter0
            origin: Ubuntu
            source: apt
            version: 1.1.6-3
        libuuid1:
        -   arch: amd64
            category: libs
            name: libuuid1
            origin: Ubuntu
            source: apt
            version: 2.27.1-6ubuntu3.10
        libv4l-0:
        -   arch: amd64
            category: libs
            name: libv4l-0
            origin: Ubuntu
            source: apt
            version: 1.10.0-1
        libv4lconvert0:
        -   arch: amd64
            category: libs
            name: libv4lconvert0
            origin: Ubuntu
            source: apt
            version: 1.10.0-1
        libva-drm1:
        -   arch: amd64
            category: universe/libs
            name: libva-drm1
            origin: Ubuntu
            source: apt
            version: 1.7.0-1ubuntu0.1
        libva-x11-1:
        -   arch: amd64
            category: universe/libs
            name: libva-x11-1
            origin: Ubuntu
            source: apt
            version: 1.7.0-1ubuntu0.1
        libva1:
        -   arch: amd64
            category: universe/libs
            name: libva1
            origin: Ubuntu
            source: apt
            version: 1.7.0-1ubuntu0.1
        libvdpau1:
        -   arch: amd64
            category: libs
            name: libvdpau1
            origin: Ubuntu
            source: apt
            version: 1.1.1-3ubuntu1
        libvisio-0.1-1:
        -   arch: amd64
            category: libs
            name: libvisio-0.1-1
            origin: Ubuntu
            source: apt
            version: 0.1.5-1ubuntu1
        libvisual-0.4-0:
        -   arch: amd64
            category: libs
            name: libvisual-0.4-0
            origin: Ubuntu
            source: apt
            version: 0.4.0-8
        libvo-aacenc0:
        -   arch: amd64
            category: universe/libs
            name: libvo-aacenc0
            origin: Ubuntu
            source: apt
            version: 0.1.3-1
        libvo-amrwbenc0:
        -   arch: amd64
            category: universe/libs
            name: libvo-amrwbenc0
            origin: Ubuntu
            source: apt
            version: 0.1.3-1
        libvorbis0a:
        -   arch: amd64
            category: libs
            name: libvorbis0a
            origin: Ubuntu
            source: apt
            version: 1.3.5-3ubuntu0.2
        libvorbisenc2:
        -   arch: amd64
            category: libs
            name: libvorbisenc2
            origin: Ubuntu
            source: apt
            version: 1.3.5-3ubuntu0.2
        libvorbisfile3:
        -   arch: amd64
            category: libs
            name: libvorbisfile3
            origin: Ubuntu
            source: apt
            version: 1.3.5-3ubuntu0.2
        libvorbisidec1:
        -   arch: amd64
            category: universe/libs
            name: libvorbisidec1
            origin: Ubuntu
            source: apt
            version: 1.0.2+svn18153-0.2+deb7u1build0.16.04.1
        libvpx3:
        -   arch: amd64
            category: libs
            name: libvpx3
            origin: Ubuntu
            source: apt
            version: 1.5.0-2ubuntu1.1
        libwacom-bin:
        -   arch: amd64
            category: libs
            name: libwacom-bin
            origin: Ubuntu
            source: apt
            version: 0.22-1~ubuntu16.04.1
        libwacom-common:
        -   arch: all
            category: libs
            name: libwacom-common
            origin: Ubuntu
            source: apt
            version: 0.22-1~ubuntu16.04.1
        libwacom2:
        -   arch: amd64
            category: libs
            name: libwacom2
            origin: Ubuntu
            source: apt
            version: 0.22-1~ubuntu16.04.1
        libwavpack1:
        -   arch: amd64
            category: libs
            name: libwavpack1
            origin: Ubuntu
            source: apt
            version: 4.75.2-2ubuntu0.2
        libwayland-client0:
        -   arch: amd64
            category: libs
            name: libwayland-client0
            origin: Ubuntu
            source: apt
            version: 1.12.0-1~ubuntu16.04.3
        libwayland-cursor0:
        -   arch: amd64
            category: libs
            name: libwayland-cursor0
            origin: Ubuntu
            source: apt
            version: 1.12.0-1~ubuntu16.04.3
        libwayland-egl1-mesa:
        -   arch: amd64
            category: libs
            name: libwayland-egl1-mesa
            origin: Ubuntu
            source: apt
            version: 18.0.5-0ubuntu0~16.04.1
        libwayland-server0:
        -   arch: amd64
            category: libs
            name: libwayland-server0
            origin: Ubuntu
            source: apt
            version: 1.12.0-1~ubuntu16.04.3
        libwbclient0:
        -   arch: amd64
            category: libs
            name: libwbclient0
            origin: Ubuntu
            source: apt
            version: 2:4.3.11+dfsg-0ubuntu0.16.04.27
        libwebp5:
        -   arch: amd64
            category: libs
            name: libwebp5
            origin: Ubuntu
            source: apt
            version: 0.4.4-1
        libwildmidi-config:
        -   arch: all
            category: universe/misc
            name: libwildmidi-config
            origin: Ubuntu
            source: apt
            version: 0.3.8-2
        libwildmidi1:
        -   arch: amd64
            category: universe/libs
            name: libwildmidi1
            origin: Ubuntu
            source: apt
            version: 0.3.8-2
        libwind0-heimdal:
        -   arch: amd64
            category: libs
            name: libwind0-heimdal
            origin: Ubuntu
            source: apt
            version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
        libwmf0.2-7:
        -   arch: amd64
            category: libs
            name: libwmf0.2-7
            origin: Ubuntu
            source: apt
            version: 0.2.8.4-10.5ubuntu1
        libwpd-0.10-10:
        -   arch: amd64
            category: libs
            name: libwpd-0.10-10
            origin: Ubuntu
            source: apt
            version: 0.10.1-1ubuntu1
        libwpg-0.3-3:
        -   arch: amd64
            category: libs
            name: libwpg-0.3-3
            origin: Ubuntu
            source: apt
            version: 0.3.1-1ubuntu1
        libwps-0.4-4:
        -   arch: amd64
            category: libs
            name: libwps-0.4-4
            origin: Ubuntu
            source: apt
            version: 0.4.3-1ubuntu1
        libwrap0:
        -   arch: amd64
            category: libs
            name: libwrap0
            origin: Ubuntu
            source: apt
            version: 7.6.q-25
        libwww-perl:
        -   arch: all
            category: perl
            name: libwww-perl
            origin: Ubuntu
            source: apt
            version: 6.15-1
        libwww-robotrules-perl:
        -   arch: all
            category: perl
            name: libwww-robotrules-perl
            origin: Ubuntu
            source: apt
            version: 6.01-1
        libwxbase3.0-0v5:
        -   arch: amd64
            category: universe/libs
            name: libwxbase3.0-0v5
            origin: Ubuntu
            source: apt
            version: 3.0.2+dfsg-1.3ubuntu0.1
        libwxgtk3.0-0v5:
        -   arch: amd64
            category: universe/libs
            name: libwxgtk3.0-0v5
            origin: Ubuntu
            source: apt
            version: 3.0.2+dfsg-1.3ubuntu0.1
        libx11-6:
        -   arch: amd64
            category: libs
            name: libx11-6
            origin: Ubuntu
            source: apt
            version: 2:1.6.3-1ubuntu2.1
        libx11-data:
        -   arch: all
            category: x11
            name: libx11-data
            origin: Ubuntu
            source: apt
            version: 2:1.6.3-1ubuntu2.1
        libx11-xcb1:
        -   arch: amd64
            category: libs
            name: libx11-xcb1
            origin: Ubuntu
            source: apt
            version: 2:1.6.3-1ubuntu2.1
        libx264-148:
        -   arch: amd64
            category: universe/libs
            name: libx264-148
            origin: Ubuntu
            source: apt
            version: 2:0.148.2643+git5c65704-1
        libx265-79:
        -   arch: amd64
            category: universe/libs
            name: libx265-79
            origin: Ubuntu
            source: apt
            version: 1.9-3
        libxapian22v5:
        -   arch: amd64
            category: libs
            name: libxapian22v5
            origin: Ubuntu
            source: apt
            version: 1.2.22-2
        libxau6:
        -   arch: amd64
            category: libs
            name: libxau6
            origin: Ubuntu
            source: apt
            version: 1:1.0.8-1
        libxcb-dri2-0:
        -   arch: amd64
            category: libs
            name: libxcb-dri2-0
            origin: Ubuntu
            source: apt
            version: 1.11.1-1ubuntu1
        libxcb-dri3-0:
        -   arch: amd64
            category: libs
            name: libxcb-dri3-0
            origin: Ubuntu
            source: apt
            version: 1.11.1-1ubuntu1
        libxcb-glx0:
        -   arch: amd64
            category: libs
            name: libxcb-glx0
            origin: Ubuntu
            source: apt
            version: 1.11.1-1ubuntu1
        libxcb-icccm4:
        -   arch: amd64
            category: libs
            name: libxcb-icccm4
            origin: Ubuntu
            source: apt
            version: 0.4.1-1ubuntu1
        libxcb-image0:
        -   arch: amd64
            category: libdevel
            name: libxcb-image0
            origin: Ubuntu
            source: apt
            version: 0.4.0-1build1
        libxcb-keysyms1:
        -   arch: amd64
            category: libs
            name: libxcb-keysyms1
            origin: Ubuntu
            source: apt
            version: 0.4.0-1
        libxcb-present0:
        -   arch: amd64
            category: libs
            name: libxcb-present0
            origin: Ubuntu
            source: apt
            version: 1.11.1-1ubuntu1
        libxcb-randr0:
        -   arch: amd64
            category: libs
            name: libxcb-randr0
            origin: Ubuntu
            source: apt
            version: 1.11.1-1ubuntu1
        libxcb-render-util0:
        -   arch: amd64
            category: libs
            name: libxcb-render-util0
            origin: Ubuntu
            source: apt
            version: 0.3.9-1
        libxcb-render0:
        -   arch: amd64
            category: libs
            name: libxcb-render0
            origin: Ubuntu
            source: apt
            version: 1.11.1-1ubuntu1
        libxcb-shape0:
        -   arch: amd64
            category: libs
            name: libxcb-shape0
            origin: Ubuntu
            source: apt
            version: 1.11.1-1ubuntu1
        libxcb-shm0:
        -   arch: amd64
            category: libs
            name: libxcb-shm0
            origin: Ubuntu
            source: apt
            version: 1.11.1-1ubuntu1
        libxcb-sync1:
        -   arch: amd64
            category: libs
            name: libxcb-sync1
            origin: Ubuntu
            source: apt
            version: 1.11.1-1ubuntu1
        libxcb-util1:
        -   arch: amd64
            category: libs
            name: libxcb-util1
            origin: Ubuntu
            source: apt
            version: 0.4.0-0ubuntu3
        libxcb-xfixes0:
        -   arch: amd64
            category: libs
            name: libxcb-xfixes0
            origin: Ubuntu
            source: apt
            version: 1.11.1-1ubuntu1
        libxcb-xkb1:
        -   arch: amd64
            category: libs
            name: libxcb-xkb1
            origin: Ubuntu
            source: apt
            version: 1.11.1-1ubuntu1
        libxcb1:
        -   arch: amd64
            category: libs
            name: libxcb1
            origin: Ubuntu
            source: apt
            version: 1.11.1-1ubuntu1
        libxcomposite1:
        -   arch: amd64
            category: libs
            name: libxcomposite1
            origin: Ubuntu
            source: apt
            version: 1:0.4.4-1
        libxcursor1:
        -   arch: amd64
            category: libs
            name: libxcursor1
            origin: Ubuntu
            source: apt
            version: 1:1.1.14-1ubuntu0.16.04.2
        libxdamage1:
        -   arch: amd64
            category: libs
            name: libxdamage1
            origin: Ubuntu
            source: apt
            version: 1:1.1.4-2
        libxdmcp6:
        -   arch: amd64
            category: libs
            name: libxdmcp6
            origin: Ubuntu
            source: apt
            version: 1:1.1.2-1.1
        libxext6:
        -   arch: amd64
            category: libs
            name: libxext6
            origin: Ubuntu
            source: apt
            version: 2:1.3.3-1
        libxfixes3:
        -   arch: amd64
            category: libs
            name: libxfixes3
            origin: Ubuntu
            source: apt
            version: 1:5.0.1-2
        libxfont1:
        -   arch: amd64
            category: libs
            name: libxfont1
            origin: Ubuntu
            source: apt
            version: 1:1.5.1-1ubuntu0.16.04.4
        libxi6:
        -   arch: amd64
            category: libs
            name: libxi6
            origin: Ubuntu
            source: apt
            version: 2:1.7.6-1
        libxinerama1:
        -   arch: amd64
            category: libs
            name: libxinerama1
            origin: Ubuntu
            source: apt
            version: 2:1.1.3-1
        libxkbcommon-x11-0:
        -   arch: amd64
            category: libs
            name: libxkbcommon-x11-0
            origin: Ubuntu
            source: apt
            version: 0.5.0-1ubuntu2.1
        libxkbcommon0:
        -   arch: amd64
            category: libs
            name: libxkbcommon0
            origin: Ubuntu
            source: apt
            version: 0.5.0-1ubuntu2.1
        libxml2:
        -   arch: amd64
            category: libs
            name: libxml2
            origin: Ubuntu
            source: apt
            version: 2.9.3+dfsg1-1ubuntu0.7
        libxml2-dev:
        -   arch: amd64
            category: libdevel
            name: libxml2-dev
            origin: Ubuntu
            source: apt
            version: 2.9.3+dfsg1-1ubuntu0.7
        libxmu6:
        -   arch: amd64
            category: libs
            name: libxmu6
            origin: Ubuntu
            source: apt
            version: 2:1.1.2-2
        libxpm4:
        -   arch: amd64
            category: libs
            name: libxpm4
            origin: Ubuntu
            source: apt
            version: 1:3.5.11-1ubuntu0.16.04.1
        libxrandr2:
        -   arch: amd64
            category: libs
            name: libxrandr2
            origin: Ubuntu
            source: apt
            version: 2:1.5.0-1
        libxrender1:
        -   arch: amd64
            category: libs
            name: libxrender1
            origin: Ubuntu
            source: apt
            version: 1:0.9.9-0ubuntu1
        libxshmfence1:
        -   arch: amd64
            category: libs
            name: libxshmfence1
            origin: Ubuntu
            source: apt
            version: 1.2-1
        libxslt1-dev:
        -   arch: amd64
            category: libdevel
            name: libxslt1-dev
            origin: Ubuntu
            source: apt
            version: 1.1.28-2.1ubuntu0.3
        libxslt1.1:
        -   arch: amd64
            category: libs
            name: libxslt1.1
            origin: Ubuntu
            source: apt
            version: 1.1.28-2.1ubuntu0.3
        libxss1:
        -   arch: amd64
            category: libs
            name: libxss1
            origin: Ubuntu
            source: apt
            version: 1:1.2.2-1
        libxt6:
        -   arch: amd64
            category: libs
            name: libxt6
            origin: Ubuntu
            source: apt
            version: 1:1.1.5-0ubuntu1
        libxtables11:
        -   arch: amd64
            category: net
            name: libxtables11
            origin: Ubuntu
            source: apt
            version: 1.6.0-2ubuntu3
        libxtst6:
        -   arch: amd64
            category: libs
            name: libxtst6
            origin: Ubuntu
            source: apt
            version: 2:1.2.2-1
        libxv1:
        -   arch: amd64
            category: libs
            name: libxv1
            origin: Ubuntu
            source: apt
            version: 2:1.0.10-1
        libxvidcore4:
        -   arch: amd64
            category: universe/libs
            name: libxvidcore4
            origin: Ubuntu
            source: apt
            version: 2:1.3.4-1
        libxvmc1:
        -   arch: amd64
            category: libs
            name: libxvmc1
            origin: Ubuntu
            source: apt
            version: 2:1.0.9-1ubuntu1
        libxxf86dga1:
        -   arch: amd64
            category: libs
            name: libxxf86dga1
            origin: Ubuntu
            source: apt
            version: 2:1.1.4-1
        libxxf86vm1:
        -   arch: amd64
            category: libs
            name: libxxf86vm1
            origin: Ubuntu
            source: apt
            version: 1:1.1.4-1
        libyajl2:
        -   arch: amd64
            category: libs
            name: libyajl2
            origin: Ubuntu
            source: apt
            version: 2.1.0-2
        libyaml-0-2:
        -   arch: amd64
            category: libs
            name: libyaml-0-2
            origin: Ubuntu
            source: apt
            version: 0.1.6-3
        libzbar0:
        -   arch: amd64
            category: universe/libs
            name: libzbar0
            origin: Ubuntu
            source: apt
            version: 0.10+doc-10ubuntu1
        libzvbi-common:
        -   arch: all
            category: universe/devel
            name: libzvbi-common
            origin: Ubuntu
            source: apt
            version: 0.2.35-10
        libzvbi0:
        -   arch: amd64
            category: universe/libs
            name: libzvbi0
            origin: Ubuntu
            source: apt
            version: 0.2.35-10
        linux-base:
        -   arch: all
            category: kernel
            name: linux-base
            origin: Ubuntu
            source: apt
            version: 4.5ubuntu1.1~16.04.1
        linux-firmware:
        -   arch: all
            category: misc
            name: linux-firmware
            origin: Ubuntu
            source: apt
            version: 1.157.23
        linux-image-4.4.0-179-generic:
        -   arch: amd64
            category: kernel
            name: linux-image-4.4.0-179-generic
            origin: Ubuntu
            source: apt
            version: 4.4.0-179.209
        linux-image-generic:
        -   arch: amd64
            category: kernel
            name: linux-image-generic
            origin: Ubuntu
            source: apt
            version: 4.4.0.179.187
        linux-libc-dev:
        -   arch: amd64
            category: devel
            name: linux-libc-dev
            origin: Ubuntu
            source: apt
            version: 4.4.0-179.209
        linux-modules-4.4.0-179-generic:
        -   arch: amd64
            category: kernel
            name: linux-modules-4.4.0-179-generic
            origin: Ubuntu
            source: apt
            version: 4.4.0-179.209
        linux-modules-extra-4.4.0-179-generic:
        -   arch: amd64
            category: kernel
            name: linux-modules-extra-4.4.0-179-generic
            origin: Ubuntu
            source: apt
            version: 4.4.0-179.209
        linuxlogo:
        -   arch: amd64
            category: universe/misc
            name: linuxlogo
            origin: Ubuntu
            source: apt
            version: 5.11-8
        locales:
        -   arch: all
            category: libs
            name: locales
            origin: Ubuntu
            source: apt
            version: 2.23-0ubuntu11
        login:
        -   arch: amd64
            category: admin
            name: login
            origin: Ubuntu
            source: apt
            version: 1:4.2-3.1ubuntu5.4
        logrotate:
        -   arch: amd64
            category: admin
            name: logrotate
            origin: Ubuntu
            source: apt
            version: 3.8.7-2ubuntu2.16.04.2
        lp-solve:
        -   arch: amd64
            category: math
            name: lp-solve
            origin: Ubuntu
            source: apt
            version: 5.5.0.13-7build2
        lsb-base:
        -   arch: all
            category: misc
            name: lsb-base
            origin: Ubuntu
            source: apt
            version: 9.20160110ubuntu0.2
        lsb-release:
        -   arch: all
            category: misc
            name: lsb-release
            origin: Ubuntu
            source: apt
            version: 9.20160110ubuntu0.2
        lvm2:
        -   arch: amd64
            category: admin
            name: lvm2
            origin: Ubuntu
            source: apt
            version: 2.02.133-1ubuntu10
        make:
        -   arch: amd64
            category: devel
            name: make
            origin: Ubuntu
            source: apt
            version: 4.1-6
        makedev:
        -   arch: all
            category: admin
            name: makedev
            origin: Ubuntu
            source: apt
            version: 2.3.1-93ubuntu2~ubuntu16.04.1
        manpages:
        -   arch: all
            category: doc
            name: manpages
            origin: Ubuntu
            source: apt
            version: 4.04-2
        manpages-dev:
        -   arch: all
            category: doc
            name: manpages-dev
            origin: Ubuntu
            source: apt
            version: 4.04-2
        mawk:
        -   arch: amd64
            category: utils
            name: mawk
            origin: Ubuntu
            source: apt
            version: 1.3.3-17ubuntu2
        mdadm:
        -   arch: amd64
            category: admin
            name: mdadm
            origin: Ubuntu
            source: apt
            version: 3.3-2ubuntu7.6
        memtest86+:
        -   arch: amd64
            category: misc
            name: memtest86+
            origin: Ubuntu
            source: apt
            version: 5.01-3ubuntu2
        mencoder:
        -   arch: amd64
            category: universe/video
            name: mencoder
            origin: Ubuntu
            source: apt
            version: 2:1.2.1-1ubuntu1.1
        mesa-va-drivers:
        -   arch: amd64
            category: universe/libs
            name: mesa-va-drivers
            origin: Ubuntu
            source: apt
            version: 18.0.5-0ubuntu0~16.04.1
        mesa-vdpau-drivers:
        -   arch: amd64
            category: libs
            name: mesa-vdpau-drivers
            origin: Ubuntu
            source: apt
            version: 18.0.5-0ubuntu0~16.04.1
        mime-support:
        -   arch: all
            category: net
            name: mime-support
            origin: Ubuntu
            source: apt
            version: 3.59ubuntu1
        mongodb-org:
        -   arch: amd64
            category: database
            name: mongodb-org
            origin: mongodb
            source: apt
            version: 3.4.24
        mongodb-org-mongos:
        -   arch: amd64
            category: database
            name: mongodb-org-mongos
            origin: mongodb
            source: apt
            version: 3.4.24
        mongodb-org-server:
        -   arch: amd64
            category: database
            name: mongodb-org-server
            origin: mongodb
            source: apt
            version: 3.4.24
        mongodb-org-shell:
        -   arch: amd64
            category: database
            name: mongodb-org-shell
            origin: mongodb
            source: apt
            version: 3.4.24
        mongodb-org-tools:
        -   arch: amd64
            category: database
            name: mongodb-org-tools
            origin: mongodb
            source: apt
            version: 3.4.24
        mount:
        -   arch: amd64
            category: admin
            name: mount
            origin: Ubuntu
            source: apt
            version: 2.27.1-6ubuntu3.10
        mplayer:
        -   arch: amd64
            category: universe/video
            name: mplayer
            origin: Ubuntu
            source: apt
            version: 2:1.2.1-1ubuntu1.1
        multiarch-support:
        -   arch: amd64
            category: libs
            name: multiarch-support
            origin: Ubuntu
            source: apt
            version: 2.23-0ubuntu11
        munin-common:
        -   arch: all
            category: universe/net
            name: munin-common
            origin: Ubuntu
            source: apt
            version: 2.0.25-2ubuntu0.16.04.3
        munin-node:
        -   arch: all
            category: universe/net
            name: munin-node
            origin: Ubuntu
            source: apt
            version: 2.0.25-2ubuntu0.16.04.3
        munin-plugins-c:
        -   arch: amd64
            category: universe/net
            name: munin-plugins-c
            origin: Ubuntu
            source: apt
            version: 0.0.9-1
        munin-plugins-core:
        -   arch: all
            category: universe/net
            name: munin-plugins-core
            origin: Ubuntu
            source: apt
            version: 2.0.25-2ubuntu0.16.04.3
        munin-plugins-extra:
        -   arch: all
            category: universe/net
            name: munin-plugins-extra
            origin: Ubuntu
            source: apt
            version: 2.0.25-2ubuntu0.16.04.3
        mutt:
        -   arch: amd64
            category: mail
            name: mutt
            origin: Ubuntu
            source: apt
            version: 1.5.24-1ubuntu0.2
        ncurses-base:
        -   arch: all
            category: utils
            name: ncurses-base
            origin: Ubuntu
            source: apt
            version: 6.0+20160213-1ubuntu1
        ncurses-bin:
        -   arch: amd64
            category: utils
            name: ncurses-bin
            origin: Ubuntu
            source: apt
            version: 6.0+20160213-1ubuntu1
        net-tools:
        -   arch: amd64
            category: net
            name: net-tools
            origin: Ubuntu
            source: apt
            version: 1.60-26ubuntu1
        netbase:
        -   arch: all
            category: admin
            name: netbase
            origin: Ubuntu
            source: apt
            version: '5.3'
        netcat-openbsd:
        -   arch: amd64
            category: net
            name: netcat-openbsd
            origin: Ubuntu
            source: apt
            version: 1.105-7ubuntu1
        netpbm:
        -   arch: amd64
            category: graphics
            name: netpbm
            origin: Ubuntu
            source: apt
            version: 2:10.0-15.3
        nginx-common:
        -   arch: all
            category: httpd
            name: nginx-common
            origin: Ubuntu
            source: apt
            version: 1.10.3-0ubuntu0.16.04.5
        nginx-extras:
        -   arch: amd64
            category: universe/httpd
            name: nginx-extras
            origin: Ubuntu
            source: apt
            version: 1.10.3-0ubuntu0.16.04.5
        nmon:
        -   arch: amd64
            category: universe/utils
            name: nmon
            origin: Ubuntu
            source: apt
            version: 14g+debian-1build1
        nodejs:
        -   arch: amd64
            category: web
            name: nodejs
            origin: Node Source
            source: apt
            version: 8.17.0-1nodesource1
        notification-daemon:
        -   arch: amd64
            category: x11
            name: notification-daemon
            origin: Ubuntu
            source: apt
            version: 3.18.2-1
        opencv-data:
        -   arch: all
            category: universe/libdevel
            name: opencv-data
            origin: Ubuntu
            source: apt
            version: 2.4.9.1+dfsg-1.5ubuntu1.1
        openh264:
        -   arch: amd64
            category: utils
            name: openh264
            origin: BigBlueButton
            source: apt
            version: 1.4.0-1kurento1.16.04
        openh264-gst-plugins-bad-1.5:
        -   arch: amd64
            category: libs
            name: openh264-gst-plugins-bad-1.5
            origin: BigBlueButton
            source: apt
            version: 1.8.1-1kurento4.16.04
        openjdk-8-jre:
        -   arch: amd64
            category: java
            name: openjdk-8-jre
            origin: Ubuntu
            source: apt
            version: 8u252-b09-1~16.04
        openjdk-8-jre-headless:
        -   arch: amd64
            category: java
            name: openjdk-8-jre-headless
            origin: Ubuntu
            source: apt
            version: 8u252-b09-1~16.04
        openssh-client:
        -   arch: amd64
            category: net
            name: openssh-client
            origin: Ubuntu
            source: apt
            version: 1:7.2p2-4ubuntu2.8
        openssh-server:
        -   arch: amd64
            category: net
            name: openssh-server
            origin: Ubuntu
            source: apt
            version: 1:7.2p2-4ubuntu2.8
        openssh-sftp-server:
        -   arch: amd64
            category: net
            name: openssh-sftp-server
            origin: Ubuntu
            source: apt
            version: 1:7.2p2-4ubuntu2.8
        openssl:
        -   arch: amd64
            category: utils
            name: openssl
            origin: Ubuntu
            source: apt
            version: 1.0.2g-1ubuntu4.16
        openwebrtc-gst-plugins:
        -   arch: amd64
            category: utils
            name: openwebrtc-gst-plugins
            origin: BigBlueButton
            source: apt
            version: 0.10.0-1kurento1.16.04
        os-prober:
        -   arch: amd64
            category: utils
            name: os-prober
            origin: Ubuntu
            source: apt
            version: 1.70ubuntu3.3
        passwd:
        -   arch: amd64
            category: admin
            name: passwd
            origin: Ubuntu
            source: apt
            version: 1:4.2-3.1ubuntu5.4
        patch:
        -   arch: amd64
            category: utils
            name: patch
            origin: Ubuntu
            source: apt
            version: 2.7.5-1ubuntu0.16.04.2
        pciutils:
        -   arch: amd64
            category: admin
            name: pciutils
            origin: Ubuntu
            source: apt
            version: 1:3.3.1-1.1ubuntu1.3
        perl:
        -   arch: amd64
            category: perl
            name: perl
            origin: Ubuntu
            source: apt
            version: 5.22.1-9ubuntu0.6
        perl-base:
        -   arch: amd64
            category: perl
            name: perl-base
            origin: Ubuntu
            source: apt
            version: 5.22.1-9ubuntu0.6
        perl-modules-5.22:
        -   arch: all
            category: perl
            name: perl-modules-5.22
            origin: Ubuntu
            source: apt
            version: 5.22.1-9ubuntu0.6
        pigz:
        -   arch: amd64
            category: universe/utils
            name: pigz
            origin: Ubuntu
            source: apt
            version: 2.3.1-2
        pinentry-curses:
        -   arch: amd64
            category: utils
            name: pinentry-curses
            origin: Ubuntu
            source: apt
            version: 0.9.7-3
        plymouth:
        -   arch: amd64
            category: x11
            name: plymouth
            origin: Ubuntu
            source: apt
            version: 0.9.2-3ubuntu13.5
        policykit-1:
        -   arch: amd64
            category: admin
            name: policykit-1
            origin: Ubuntu
            source: apt
            version: 0.105-14.1ubuntu0.5
        poppler-data:
        -   arch: all
            category: misc
            name: poppler-data
            origin: Ubuntu
            source: apt
            version: 0.4.7-7
        poppler-utils:
        -   arch: amd64
            category: utils
            name: poppler-utils
            origin: Ubuntu
            source: apt
            version: 0.41.0-0ubuntu1.14
        powermgmt-base:
        -   arch: all
            category: utils
            name: powermgmt-base
            origin: Ubuntu
            source: apt
            version: 1.31+nmu1
        procinfo:
        -   arch: amd64
            category: universe/utils
            name: procinfo
            origin: Ubuntu
            source: apt
            version: 1:2.0.304-1ubuntu2
        procps:
        -   arch: amd64
            category: admin
            name: procps
            origin: Ubuntu
            source: apt
            version: 2:3.3.10-4ubuntu2.5
        psmisc:
        -   arch: amd64
            category: admin
            name: psmisc
            origin: Ubuntu
            source: apt
            version: 22.21-2.1ubuntu0.1
        pwgen:
        -   arch: amd64
            category: universe/admin
            name: pwgen
            origin: Ubuntu
            source: apt
            version: 2.07-1.1ubuntu1
        python:
        -   arch: amd64
            category: python
            name: python
            origin: Ubuntu
            source: apt
            version: 2.7.12-1~16.04
        python-apt:
        -   arch: amd64
            category: python
            name: python-apt
            origin: Ubuntu
            source: apt
            version: 1.1.0~beta1ubuntu0.16.04.9
        python-apt-common:
        -   arch: all
            category: python
            name: python-apt-common
            origin: Ubuntu
            source: apt
            version: 1.1.0~beta1ubuntu0.16.04.9
        python-backports.ssl-match-hostname:
        -   arch: all
            category: universe/python
            name: python-backports.ssl-match-hostname
            origin: Ubuntu
            source: apt
            version: 3.4.0.2-1
        python-cffi-backend:
        -   arch: amd64
            category: python
            name: python-cffi-backend
            origin: Ubuntu
            source: apt
            version: 1.5.2-1ubuntu1
        python-chardet:
        -   arch: all
            category: python
            name: python-chardet
            origin: Ubuntu
            source: apt
            version: 2.3.0-2
        python-cryptography:
        -   arch: amd64
            category: python
            name: python-cryptography
            origin: Ubuntu
            source: apt
            version: 1.2.3-1ubuntu0.2
        python-dns:
        -   arch: all
            category: universe/python
            name: python-dns
            origin: Ubuntu
            source: apt
            version: 2.3.6-3
        python-dnspython:
        -   arch: all
            category: python
            name: python-dnspython
            origin: Ubuntu
            source: apt
            version: 1.12.0-1
        python-docker:
        -   arch: all
            category: universe/python
            name: python-docker
            origin: Ubuntu
            source: apt
            version: 1.9.0-1~16.04.1
        python-enum34:
        -   arch: all
            category: python
            name: python-enum34
            origin: Ubuntu
            source: apt
            version: 1.1.2-1
        python-idna:
        -   arch: all
            category: python
            name: python-idna
            origin: Ubuntu
            source: apt
            version: 2.0-3
        python-ipaddress:
        -   arch: all
            category: python
            name: python-ipaddress
            origin: Ubuntu
            source: apt
            version: 1.0.16-1
        python-minimal:
        -   arch: amd64
            category: python
            name: python-minimal
            origin: Ubuntu
            source: apt
            version: 2.7.12-1~16.04
        python-ndg-httpsclient:
        -   arch: all
            category: python
            name: python-ndg-httpsclient
            origin: Ubuntu
            source: apt
            version: 0.4.0-3
        python-openssl:
        -   arch: all
            category: python
            name: python-openssl
            origin: Ubuntu
            source: apt
            version: 0.15.1-2ubuntu0.2
        python-passlib:
        -   arch: all
            category: python
            name: python-passlib
            origin: Ubuntu
            source: apt
            version: 1.6.5-4
        python-pkg-resources:
        -   arch: all
            category: python
            name: python-pkg-resources
            origin: Ubuntu
            source: apt
            version: 20.7.0-1
        python-pyasn1:
        -   arch: all
            category: python
            name: python-pyasn1
            origin: Ubuntu
            source: apt
            version: 0.1.9-1
        python-requests:
        -   arch: all
            category: python
            name: python-requests
            origin: Ubuntu
            source: apt
            version: 2.9.1-3ubuntu0.1
        python-six:
        -   arch: all
            category: python
            name: python-six
            origin: Ubuntu
            source: apt
            version: 1.10.0-3
        python-talloc:
        -   arch: amd64
            category: python
            name: python-talloc
            origin: Ubuntu
            source: apt
            version: 2.1.5-2
        python-urllib3:
        -   arch: all
            category: python
            name: python-urllib3
            origin: Ubuntu
            source: apt
            version: 1.13.1-2ubuntu0.16.04.3
        python-websocket:
        -   arch: all
            category: universe/python
            name: python-websocket
            origin: Ubuntu
            source: apt
            version: 0.18.0-2
        python2.7:
        -   arch: amd64
            category: python
            name: python2.7
            origin: Ubuntu
            source: apt
            version: 2.7.12-1ubuntu0~16.04.11
        python2.7-minimal:
        -   arch: amd64
            category: python
            name: python2.7-minimal
            origin: Ubuntu
            source: apt
            version: 2.7.12-1ubuntu0~16.04.11
        python3:
        -   arch: amd64
            category: python
            name: python3
            origin: Ubuntu
            source: apt
            version: 3.5.1-3
        python3-apt:
        -   arch: amd64
            category: python
            name: python3-apt
            origin: Ubuntu
            source: apt
            version: 1.1.0~beta1ubuntu0.16.04.9
        python3-bs4:
        -   arch: all
            category: python
            name: python3-bs4
            origin: Ubuntu
            source: apt
            version: 4.4.1-1
        python3-chardet:
        -   arch: all
            category: python
            name: python3-chardet
            origin: Ubuntu
            source: apt
            version: 2.3.0-2
        python3-dbus:
        -   arch: amd64
            category: python
            name: python3-dbus
            origin: Ubuntu
            source: apt
            version: 1.2.0-3
        python3-debian:
        -   arch: all
            category: python
            name: python3-debian
            origin: Ubuntu
            source: apt
            version: 0.1.27ubuntu2
        python3-distupgrade:
        -   arch: all
            category: python
            name: python3-distupgrade
            origin: Ubuntu
            source: apt
            version: 1:16.04.30
        python3-dnspython:
        -   arch: all
            category: python
            name: python3-dnspython
            origin: Ubuntu
            source: apt
            version: 1.12.0-0ubuntu3
        python3-docker:
        -   arch: all
            category: universe/python
            name: python3-docker
            origin: Ubuntu
            source: apt
            version: 1.9.0-1~16.04.1
        python3-gi:
        -   arch: amd64
            category: python
            name: python3-gi
            origin: Ubuntu
            source: apt
            version: 3.20.0-0ubuntu1
        python3-html5lib:
        -   arch: all
            category: python
            name: python3-html5lib
            origin: Ubuntu
            source: apt
            version: 0.999-4
        python3-icu:
        -   arch: amd64
            category: python
            name: python3-icu
            origin: Ubuntu
            source: apt
            version: 1.9.2-2build1
        python3-lxml:
        -   arch: amd64
            category: python
            name: python3-lxml
            origin: Ubuntu
            source: apt
            version: 3.5.0-1ubuntu0.1
        python3-minimal:
        -   arch: amd64
            category: python
            name: python3-minimal
            origin: Ubuntu
            source: apt
            version: 3.5.1-3
        python3-pkg-resources:
        -   arch: all
            category: python
            name: python3-pkg-resources
            origin: Ubuntu
            source: apt
            version: 20.7.0-1
        python3-pycurl:
        -   arch: amd64
            category: python
            name: python3-pycurl
            origin: Ubuntu
            source: apt
            version: 7.43.0-1ubuntu1
        python3-requests:
        -   arch: all
            category: python
            name: python3-requests
            origin: Ubuntu
            source: apt
            version: 2.9.1-3ubuntu0.1
        python3-six:
        -   arch: all
            category: python
            name: python3-six
            origin: Ubuntu
            source: apt
            version: 1.10.0-3
        python3-software-properties:
        -   arch: all
            category: python
            name: python3-software-properties
            origin: Ubuntu
            source: apt
            version: 0.96.20.9
        python3-uno:
        -   arch: amd64
            category: python
            name: python3-uno
            origin: Ubuntu
            source: apt
            version: 1:5.1.6~rc2-0ubuntu1~xenial10
        python3-update-manager:
        -   arch: all
            category: python
            name: python3-update-manager
            origin: Ubuntu
            source: apt
            version: 1:16.04.17
        python3-urllib3:
        -   arch: all
            category: python
            name: python3-urllib3
            origin: Ubuntu
            source: apt
            version: 1.13.1-2ubuntu0.16.04.3
        python3-websocket:
        -   arch: all
            category: universe/python
            name: python3-websocket
            origin: Ubuntu
            source: apt
            version: 0.18.0-2
        python3.5:
        -   arch: amd64
            category: python
            name: python3.5
            origin: Ubuntu
            source: apt
            version: 3.5.2-2ubuntu0~16.04.10
        python3.5-minimal:
        -   arch: amd64
            category: python
            name: python3.5-minimal
            origin: Ubuntu
            source: apt
            version: 3.5.2-2ubuntu0~16.04.10
        qttranslations5-l10n:
        -   arch: all
            category: localization
            name: qttranslations5-l10n
            origin: Ubuntu
            source: apt
            version: 5.5.1-2build1
        rake:
        -   arch: all
            category: devel
            name: rake
            origin: Ubuntu
            source: apt
            version: 10.5.0-2ubuntu0.1
        readline-common:
        -   arch: all
            category: utils
            name: readline-common
            origin: Ubuntu
            source: apt
            version: 6.3-8ubuntu2
        redis-server:
        -   arch: amd64
            category: universe/misc
            name: redis-server
            origin: Ubuntu
            source: apt
            version: 2:3.0.6-1ubuntu0.4
        redis-tools:
        -   arch: amd64
            category: universe/database
            name: redis-tools
            origin: Ubuntu
            source: apt
            version: 2:3.0.6-1ubuntu0.4
        rename:
        -   arch: all
            category: perl
            name: rename
            origin: Ubuntu
            source: apt
            version: 0.20-4
        resolvconf:
        -   arch: all
            category: net
            name: resolvconf
            origin: Ubuntu
            source: apt
            version: 1.78ubuntu7
        rsync:
        -   arch: amd64
            category: net
            name: rsync
            origin: Ubuntu
            source: apt
            version: 3.1.1-3ubuntu1.3
        rsyslog:
        -   arch: amd64
            category: admin
            name: rsyslog
            origin: Ubuntu
            source: apt
            version: 8.16.0-1ubuntu3.1
        ruby:
        -   arch: all
            category: interpreters
            name: ruby
            origin: Ubuntu
            source: apt
            version: 1:2.3.0+1
        ruby-dev:
        -   arch: amd64
            category: devel
            name: ruby-dev
            origin: Ubuntu
            source: apt
            version: 1:2.3.0+1
        ruby-did-you-mean:
        -   arch: all
            category: ruby
            name: ruby-did-you-mean
            origin: Ubuntu
            source: apt
            version: 1.0.0-2
        ruby-minitest:
        -   arch: all
            category: ruby
            name: ruby-minitest
            origin: Ubuntu
            source: apt
            version: 5.8.4-2
        ruby-net-telnet:
        -   arch: all
            category: ruby
            name: ruby-net-telnet
            origin: Ubuntu
            source: apt
            version: 0.1.1-2
        ruby-power-assert:
        -   arch: all
            category: ruby
            name: ruby-power-assert
            origin: Ubuntu
            source: apt
            version: 0.2.7-1
        ruby-test-unit:
        -   arch: all
            category: ruby
            name: ruby-test-unit
            origin: Ubuntu
            source: apt
            version: 3.1.7-2
        ruby2.3:
        -   arch: amd64
            category: ruby
            name: ruby2.3
            origin: Ubuntu
            source: apt
            version: 2.3.1-2~ubuntu16.04.14
        ruby2.3-dev:
        -   arch: amd64
            category: ruby
            name: ruby2.3-dev
            origin: Ubuntu
            source: apt
            version: 2.3.1-2~ubuntu16.04.14
        rubygems-integration:
        -   arch: all
            category: ruby
            name: rubygems-integration
            origin: Ubuntu
            source: apt
            version: '1.10'
        s-nail:
        -   arch: amd64
            category: universe/mail
            name: s-nail
            origin: Ubuntu
            source: apt
            version: 14.8.6-1
        samba-libs:
        -   arch: amd64
            category: libs
            name: samba-libs
            origin: Ubuntu
            source: apt
            version: 2:4.3.11+dfsg-0ubuntu0.16.04.27
        sed:
        -   arch: amd64
            category: utils
            name: sed
            origin: Ubuntu
            source: apt
            version: 4.2.2-7
        sensible-utils:
        -   arch: all
            category: utils
            name: sensible-utils
            origin: Ubuntu
            source: apt
            version: 0.0.9ubuntu0.16.04.1
        sgml-base:
        -   arch: all
            category: text
            name: sgml-base
            origin: Ubuntu
            source: apt
            version: 1.26+nmu4ubuntu1
        shared-mime-info:
        -   arch: amd64
            category: misc
            name: shared-mime-info
            origin: Ubuntu
            source: apt
            version: 1.5-2ubuntu0.2
        slay:
        -   arch: all
            category: universe/admin
            name: slay
            origin: Ubuntu
            source: apt
            version: 2.7.0
        software-properties-common:
        -   arch: all
            category: admin
            name: software-properties-common
            origin: Ubuntu
            source: apt
            version: 0.96.20.9
        sox:
        -   arch: amd64
            category: universe/sound
            name: sox
            origin: Ubuntu
            source: apt
            version: 14.4.1-5+deb8u4ubuntu0.1
        ssh-import-id:
        -   arch: all
            category: misc
            name: ssh-import-id
            origin: Ubuntu
            source: apt
            version: 5.5-0ubuntu1
        ssl-cert:
        -   arch: all
            category: utils
            name: ssl-cert
            origin: Ubuntu
            source: apt
            version: 1.0.37
        ssl-cert-check:
        -   arch: all
            category: universe/net
            name: ssl-cert-check
            origin: Ubuntu
            source: apt
            version: 3.27-2
        sudo:
        -   arch: amd64
            category: admin
            name: sudo
            origin: Ubuntu
            source: apt
            version: 1.8.16-0ubuntu1.9
        systemd:
        -   arch: amd64
            category: admin
            name: systemd
            origin: Ubuntu
            source: apt
            version: 229-4ubuntu21.28
        systemd-sysv:
        -   arch: amd64
            category: admin
            name: systemd-sysv
            origin: Ubuntu
            source: apt
            version: 229-4ubuntu21.28
        sysv-rc:
        -   arch: all
            category: admin
            name: sysv-rc
            origin: Ubuntu
            source: apt
            version: 2.88dsf-59.3ubuntu2
        sysvinit-utils:
        -   arch: amd64
            category: admin
            name: sysvinit-utils
            origin: Ubuntu
            source: apt
            version: 2.88dsf-59.3ubuntu2
        tar:
        -   arch: amd64
            category: utils
            name: tar
            origin: Ubuntu
            source: apt
            version: 1.28-2.1ubuntu0.1
        thermald:
        -   arch: amd64
            category: admin
            name: thermald
            origin: Ubuntu
            source: apt
            version: 1.5-2ubuntu4
        tidy:
        -   arch: amd64
            category: universe/web
            name: tidy
            origin: Ubuntu
            source: apt
            version: 20091223cvs-1.5
        time:
        -   arch: amd64
            category: utils
            name: time
            origin: Ubuntu
            source: apt
            version: 1.7-25.1
        tmux:
        -   arch: amd64
            category: admin
            name: tmux
            origin: Ubuntu
            source: apt
            version: 2.1-3build1
        ttf-mscorefonts-installer:
        -   arch: all
            category: multiverse/x11
            name: ttf-mscorefonts-installer
            origin: Ubuntu
            source: apt
            version: 3.4+nmu1ubuntu2
        tzdata:
        -   arch: all
            category: libs
            name: tzdata
            origin: Ubuntu
            source: apt
            version: 2020a-0ubuntu0.16.04
        ubuntu-advantage-tools:
        -   arch: all
            category: misc
            name: ubuntu-advantage-tools
            origin: Ubuntu
            source: apt
            version: 10ubuntu0.16.04.1
        ubuntu-keyring:
        -   arch: all
            category: misc
            name: ubuntu-keyring
            origin: Ubuntu
            source: apt
            version: 2012.05.19
        ubuntu-minimal:
        -   arch: amd64
            category: metapackages
            name: ubuntu-minimal
            origin: Ubuntu
            source: apt
            version: 1.361.4
        ubuntu-mono:
        -   arch: all
            category: gnome
            name: ubuntu-mono
            origin: Ubuntu
            source: apt
            version: 14.04+16.04.20180326-0ubuntu1
        ubuntu-release-upgrader-core:
        -   arch: all
            category: admin
            name: ubuntu-release-upgrader-core
            origin: Ubuntu
            source: apt
            version: 1:16.04.30
        ucf:
        -   arch: all
            category: utils
            name: ucf
            origin: Ubuntu
            source: apt
            version: '3.0036'
        udev:
        -   arch: amd64
            category: admin
            name: udev
            origin: Ubuntu
            source: apt
            version: 229-4ubuntu21.28
        ufw:
        -   arch: all
            category: admin
            name: ufw
            origin: Ubuntu
            source: apt
            version: 0.35-0ubuntu2
        unattended-upgrades:
        -   arch: all
            category: admin
            name: unattended-upgrades
            origin: Ubuntu
            source: apt
            version: 1.1ubuntu1.18.04.7~16.04.6
        uno-libs3:
        -   arch: amd64
            category: libs
            name: uno-libs3
            origin: Ubuntu
            source: apt
            version: 5.1.6~rc2-0ubuntu1~xenial10
        unzip:
        -   arch: amd64
            category: utils
            name: unzip
            origin: Ubuntu
            source: apt
            version: 6.0-20ubuntu1
        update-manager-core:
        -   arch: all
            category: admin
            name: update-manager-core
            origin: Ubuntu
            source: apt
            version: 1:16.04.17
        update-notifier-common:
        -   arch: all
            category: gnome
            name: update-notifier-common
            origin: Ubuntu
            source: apt
            version: 3.168.10
        ure:
        -   arch: amd64
            category: libs
            name: ure
            origin: Ubuntu
            source: apt
            version: 5.1.6~rc2-0ubuntu1~xenial10
        ureadahead:
        -   arch: amd64
            category: admin
            name: ureadahead
            origin: Ubuntu
            source: apt
            version: 0.100.0-19.1
        usbutils:
        -   arch: amd64
            category: utils
            name: usbutils
            origin: Ubuntu
            source: apt
            version: 1:007-4
        util-linux:
        -   arch: amd64
            category: utils
            name: util-linux
            origin: Ubuntu
            source: apt
            version: 2.27.1-6ubuntu3.10
        va-driver-all:
        -   arch: amd64
            category: universe/video
            name: va-driver-all
            origin: Ubuntu
            source: apt
            version: 1.7.0-1ubuntu0.1
        vdpau-driver-all:
        -   arch: amd64
            category: video
            name: vdpau-driver-all
            origin: Ubuntu
            source: apt
            version: 1.1.1-3ubuntu1
        vim:
        -   arch: amd64
            category: editors
            name: vim
            origin: Ubuntu
            source: apt
            version: 2:7.4.1689-3ubuntu1.4
        vim-common:
        -   arch: amd64
            category: editors
            name: vim-common
            origin: Ubuntu
            source: apt
            version: 2:7.4.1689-3ubuntu1.4
        vim-runtime:
        -   arch: all
            category: editors
            name: vim-runtime
            origin: Ubuntu
            source: apt
            version: 2:7.4.1689-3ubuntu1.4
        vim-tiny:
        -   arch: amd64
            category: editors
            name: vim-tiny
            origin: Ubuntu
            source: apt
            version: 2:7.4.1689-3ubuntu1.4
        vorbis-tools:
        -   arch: amd64
            category: universe/sound
            name: vorbis-tools
            origin: Ubuntu
            source: apt
            version: 1.4.0-7ubuntu1
        wget:
        -   arch: amd64
            category: web
            name: wget
            origin: Ubuntu
            source: apt
            version: 1.17.1-1ubuntu1.5
        whiptail:
        -   arch: amd64
            category: utils
            name: whiptail
            origin: Ubuntu
            source: apt
            version: 0.52.18-1ubuntu2
        whois:
        -   arch: amd64
            category: net
            name: whois
            origin: Ubuntu
            source: apt
            version: 5.2.11
        wireless-regdb:
        -   arch: all
            category: net
            name: wireless-regdb
            origin: Ubuntu
            source: apt
            version: 2018.05.09-0ubuntu1~16.04.1
        x11-common:
        -   arch: all
            category: x11
            name: x11-common
            origin: Ubuntu
            source: apt
            version: 1:7.7+13ubuntu3.1
        xdg-user-dirs:
        -   arch: amd64
            category: utils
            name: xdg-user-dirs
            origin: Ubuntu
            source: apt
            version: 0.15-2ubuntu6.16.04.1
        xfonts-encodings:
        -   arch: all
            category: x11
            name: xfonts-encodings
            origin: Ubuntu
            source: apt
            version: 1:1.0.4-2
        xfonts-utils:
        -   arch: amd64
            category: x11
            name: xfonts-utils
            origin: Ubuntu
            source: apt
            version: 1:7.7+3ubuntu0.16.04.2
        xkb-data:
        -   arch: all
            category: x11
            name: xkb-data
            origin: Ubuntu
            source: apt
            version: 2.16-1ubuntu1
        xml-core:
        -   arch: all
            category: text
            name: xml-core
            origin: Ubuntu
            source: apt
            version: 0.13+nmu2
        xmlstarlet:
        -   arch: amd64
            category: universe/text
            name: xmlstarlet
            origin: Ubuntu
            source: apt
            version: 1.6.1-1ubuntu1
        xz-utils:
        -   arch: amd64
            category: utils
            name: xz-utils
            origin: Ubuntu
            source: apt
            version: 5.1.1alpha+20120614-2ubuntu2
        yq:
        -   arch: amd64
            category: devel
            name: yq
            origin: LP-PPA-rmescandon-yq
            source: apt
            version: 3.1-2
        zip:
        -   arch: amd64
            category: utils
            name: zip
            origin: Ubuntu
            source: apt
            version: 3.0-11
        zlib1g:
        -   arch: amd64
            category: libs
            name: zlib1g
            origin: Ubuntu
            source: apt
            version: 1:1.2.8.dfsg-2ubuntu4.3
        zlib1g-dev:
        -   arch: amd64
            category: libdevel
            name: zlib1g-dev
            origin: Ubuntu
            source: apt
            version: 1:1.2.8.dfsg-2ubuntu4.3
    pkg_mgr: apt
    proc_cmdline:
        BOOT_IMAGE: /vmlinuz-4.4.0-179-generic
        console:
        - tty0
        - ttyS0,115200n8
        quiet: true
        ro: true
        root: /dev/mapper/root-slash
    processor:
    - '0'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '1'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '2'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '3'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '4'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '5'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '6'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '7'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '8'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '9'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '10'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '11'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '12'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '13'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '14'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '15'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '16'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '17'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '18'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '19'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '20'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '21'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '22'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '23'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '24'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '25'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '26'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '27'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '28'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '29'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '30'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '31'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '32'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '33'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '34'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '35'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '36'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '37'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '38'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '39'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '40'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '41'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '42'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '43'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '44'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '45'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '46'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    - '47'
    - GenuineIntel
    - Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
    processor_cores: 12
    processor_count: 2
    processor_threads_per_core: 2
    processor_vcpus: 48
    product_name: Super Server
    product_serial: 0123456789
    product_uuid: E7764400-EBE0-11E9-8000-3CECEF40552A
    product_version: 0123456789
    python:
        executable: /usr/bin/python3
        has_sslcontext: true
        type: cpython
        version:
            major: 3
            micro: 2
            minor: 5
            releaselevel: final
            serial: 0
        version_info:
        - 3
        - 5
        - 2
        - final
        - 0
    python_version: 3.5.2
    real_group_id: 0
    real_user_id: 0
    selinux:
        status: Missing selinux Python library
    selinux_python_present: false
    service_mgr: systemd
    ssh_host_key_dsa_public: AAAAB3NzaC1kc3MAAACBAIa2T5plrB1kBjbb9CnombKJ7DjLuUUB3PIHwyXMKREbUZmsLTnwSrLZSboSogu9Bpu8z97exURdYbwF/ZwRDgGkU12DsKjjHirCpOzNBcDuqK49eObGgzHcDxnBuHihp1kl9tkKd1OfREtJJOX1tehsg5aLigWOJEj7A8uOAJTrAAAAFQDIwo3OSnL+drZ/Je/8aopOmdlRpQAAAIBcqoOg+MKf9I6OBnzGG1jYZxhCKLRyD8CT7srvj3+1zidg0+xwdxdWfgIfVAe3I6rYt+p24rNdjORsWqW9xY54QcN+Vo/wObFdlA7LLotkrpcF+0B+7wno9OSkW4muf6bRzjnAvje09E67Du/+E5yl9JsIRT7fwSlJJ5PY8hjLKQAAAIAyaDaEdii6+5H12TFs+/dmjTQVcO15NM8G0wXPOaufbzGR4J8NWR1ufGmNokoU4VTwR9GdBamIdGZjSFQoxhriZaFjGWf0vaNcenaQJRyhVZckFNRrcKRoddibUinAOboxIyUrP5Gn5j39oLEAztnKOlYgic5BJNkgca7Xu6EB1Q==
    ssh_host_key_ecdsa_public: AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBC0NaiSrCZZLcdWL7U/UDtJ/d+ozeF3YhgMhsnvG1UuKxUZeTg1TUWD3YeBZ/oLjMO2imaQGNio/9dz/sXqdM/c=
    ssh_host_key_ed25519_public: AAAAC3NzaC1lZDI1NTE5AAAAIFy29chtuWkzNpr7b3mFJGjPitmPcEuzq4GpBi7tn2//
    ssh_host_key_rsa_public: AAAAB3NzaC1yc2EAAAADAQABAAABAQCzBYoddKiEaaUoNOuZbIxeQU1yZw9Fq7x80SJ6dEzFYsOpB9hTec5kmhuzriCwWLp53qK6TX/AiWJ6kpQuC8gROxZ+bIbb7V7ExEuvtMPxvsSvklaiHuV4vEKoRHkAxBTCENW2cStglhDPISSffOIynhbA0p4ZFQHhpojOO/yHn3FveqesSCtc5q+ezWWp+fllM+JrvXGp+avMLK0d08X42ytNaxrWmbxbCjsp0jcwfKOWK4G/rBaHiIi/6to7Ety2WNNj/c6nDxAIl2E0Scponi4nT6tQl1P4zFtfFY+9rE1lph8Exb6QGp/fGsmK+iHeSq9zYijJdg2z4FDqM17r
    swapfree_mb: 8191
    swaptotal_mb: 8191
    system: Linux
    system_capabilities:
    - cap_chown
    - cap_dac_override
    - cap_dac_read_search
    - cap_fowner
    - cap_fsetid
    - cap_kill
    - cap_setgid
    - cap_setuid
    - cap_setpcap
    - cap_linux_immutable
    - cap_net_bind_service
    - cap_net_broadcast
    - cap_net_admin
    - cap_net_raw
    - cap_ipc_lock
    - cap_ipc_owner
    - cap_sys_module
    - cap_sys_rawio
    - cap_sys_chroot
    - cap_sys_ptrace
    - cap_sys_pacct
    - cap_sys_admin
    - cap_sys_boot
    - cap_sys_nice
    - cap_sys_resource
    - cap_sys_time
    - cap_sys_tty_config
    - cap_mknod
    - cap_lease
    - cap_audit_write
    - cap_audit_control
    - cap_setfcap
    - cap_mac_override
    - cap_mac_admin
    - cap_syslog
    - cap_wake_alarm
    - cap_block_suspend
    - 37+ep
    system_capabilities_enforced: 'True'
    system_vendor: Supermicro
    uptime_seconds: 36751
    user_dir: /root
    user_gecos: root
    user_gid: 0
    user_id: root
    user_shell: /bin/bash
    user_uid: 0
    userspace_architecture: x86_64
    userspace_bits: '64'
    vethc060a5f:
        active: true
        device: vethc060a5f
        ipv6:
        -   address: fe80::3877:d1ff:fee9:ae33
            prefix: '64'
            scope: link
        macaddress: 3a:77:d1:e9:ae:33
        mtu: 1500
        promisc: true
        speed: 10000
        type: ether
    vetheee40b5:
        active: true
        device: vetheee40b5
        ipv6:
        -   address: fe80::9cdc:d7ff:fedc:c53f
            prefix: '64'
            scope: link
        macaddress: 9e:dc:d7:dc:c5:3f
        mtu: 1500
        promisc: true
        speed: 10000
        type: ether
    virtualization_role: host
    virtualization_type: kvm
ansible_fibre_channel_wwn: []
ansible_fips: false
ansible_forks: 5
ansible_form_factor: Main Server Chassis
ansible_fqdn: meet-coop0.koumbit.net
ansible_hostname: meet-coop0
ansible_hostnqn: ''
ansible_interfaces:
- eno1
- lo
- br-5391ff83ad91
- docker0
- vetheee40b5
- eno2
- vethc060a5f
ansible_inventory_sources:
- /home/chris/meet/servers/hosts.yml
ansible_is_chroot: false
ansible_iscsi_iqn: ''
ansible_kernel: 4.4.0-179-generic
ansible_kernel_version: '#209-Ubuntu SMP Fri Apr 24 17:48:44 UTC 2020'
ansible_lo:
    active: true
    device: lo
    ipv4:
        address: 127.0.0.1
        broadcast: host
        netmask: 255.0.0.0
        network: 127.0.0.0
    ipv6:
    -   address: ::1
        prefix: '128'
        scope: host
    mtu: 65536
    promisc: false
    type: loopback
ansible_local: {}
ansible_lsb:
    codename: xenial
    description: Ubuntu 16.04.6 LTS
    id: Ubuntu
    major_release: '16'
    release: '16.04'
ansible_lvm:
    lvs:
        slash:
            size_g: '25.00'
            vg: root
        swap:
            size_g: '8.00'
            vg: root
    pvs:
        /dev/mapper/crypt_dev_md1:
            free_g: '1.96'
            size_g: '34.96'
            vg: root
        /dev/mapper/crypt_dev_md2:
            free_g: '917.74'
            size_g: '917.74'
            vg: data
        /dev/mapper/crypt_dev_md4:
            free_g: '3723.89'
            size_g: '3723.89'
            vg: data2
    vgs:
        data:
            free_g: '917.74'
            num_lvs: '0'
            num_pvs: '1'
            size_g: '917.74'
        data2:
            free_g: '3723.89'
            num_lvs: '0'
            num_pvs: '1'
            size_g: '3723.89'
        root:
            free_g: '1.96'
            num_lvs: '2'
            num_pvs: '1'
            size_g: '34.96'
ansible_machine: x86_64
ansible_machine_id: d6a7fa74a5714d20990abf3bb1fba43f
ansible_memfree_mb: 85871
ansible_memory_mb:
    nocache:
        free: 92295
        used: 3991
    real:
        free: 85871
        total: 96286
        used: 10415
    swap:
        cached: 0
        free: 8191
        total: 8191
        used: 0
ansible_memtotal_mb: 96286
ansible_mounts:
-   block_available: 229873
    block_size: 4096
    block_total: 257766
    block_used: 27893
    device: /dev/md0
    fstype: ext2
    inode_available: 65238
    inode_total: 65536
    inode_used: 298
    mount: /boot
    options: rw,noatime
    size_available: 941559808
    size_total: 1055809536
    uuid: 27484465-4af1-4e52-a67d-6b142b00bde5
-   block_available: 4285283
    block_size: 4096
    block_total: 6417977
    block_used: 2132694
    device: /dev/mapper/root-slash
    fstype: ext4
    inode_available: 1451079
    inode_total: 1638400
    inode_used: 187321
    mount: /
    options: rw,noatime,errors=remount-ro,data=ordered
    size_available: 17552519168
    size_total: 26288033792
    uuid: 6a037d59-4597-46bb-91d5-b28dd907d7a8
ansible_nodename: meet-coop0
ansible_os_family: Debian
ansible_pkg_mgr: apt
ansible_playbook_python: /usr/bin/python3
ansible_proc_cmdline:
    BOOT_IMAGE: /vmlinuz-4.4.0-179-generic
    console:
    - tty0
    - ttyS0,115200n8
    quiet: true
    ro: true
    root: /dev/mapper/root-slash
ansible_processor:
- '0'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '1'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '2'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '3'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '4'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '5'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '6'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '7'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '8'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '9'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '10'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '11'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '12'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '13'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '14'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '15'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '16'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '17'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '18'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '19'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '20'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '21'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '22'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '23'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '24'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '25'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '26'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '27'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '28'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '29'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '30'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '31'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '32'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '33'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '34'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '35'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '36'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '37'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '38'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '39'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '40'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '41'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '42'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '43'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '44'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '45'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '46'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
- '47'
- GenuineIntel
- Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
ansible_processor_cores: 12
ansible_processor_count: 2
ansible_processor_threads_per_core: 2
ansible_processor_vcpus: 48
ansible_product_name: Super Server
ansible_product_serial: 0123456789
ansible_product_uuid: E7764400-EBE0-11E9-8000-3CECEF40552A
ansible_product_version: 0123456789
ansible_python:
    executable: /usr/bin/python3
    has_sslcontext: true
    type: cpython
    version:
        major: 3
        micro: 2
        minor: 5
        releaselevel: final
        serial: 0
    version_info:
    - 3
    - 5
    - 2
    - final
    - 0
ansible_python_interpreter: /usr/bin/python3
ansible_python_version: 3.5.2
ansible_real_group_id: 0
ansible_real_user_id: 0
ansible_run_tags:
- all
ansible_selinux:
    status: Missing selinux Python library
ansible_selinux_python_present: false
ansible_service_mgr: systemd
ansible_skip_tags: []
ansible_ssh_host_key_dsa_public: AAAAB3NzaC1kc3MAAACBAIa2T5plrB1kBjbb9CnombKJ7DjLuUUB3PIHwyXMKREbUZmsLTnwSrLZSboSogu9Bpu8z97exURdYbwF/ZwRDgGkU12DsKjjHirCpOzNBcDuqK49eObGgzHcDxnBuHihp1kl9tkKd1OfREtJJOX1tehsg5aLigWOJEj7A8uOAJTrAAAAFQDIwo3OSnL+drZ/Je/8aopOmdlRpQAAAIBcqoOg+MKf9I6OBnzGG1jYZxhCKLRyD8CT7srvj3+1zidg0+xwdxdWfgIfVAe3I6rYt+p24rNdjORsWqW9xY54QcN+Vo/wObFdlA7LLotkrpcF+0B+7wno9OSkW4muf6bRzjnAvje09E67Du/+E5yl9JsIRT7fwSlJJ5PY8hjLKQAAAIAyaDaEdii6+5H12TFs+/dmjTQVcO15NM8G0wXPOaufbzGR4J8NWR1ufGmNokoU4VTwR9GdBamIdGZjSFQoxhriZaFjGWf0vaNcenaQJRyhVZckFNRrcKRoddibUinAOboxIyUrP5Gn5j39oLEAztnKOlYgic5BJNkgca7Xu6EB1Q==
ansible_ssh_host_key_ecdsa_public: AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBC0NaiSrCZZLcdWL7U/UDtJ/d+ozeF3YhgMhsnvG1UuKxUZeTg1TUWD3YeBZ/oLjMO2imaQGNio/9dz/sXqdM/c=
ansible_ssh_host_key_ed25519_public: AAAAC3NzaC1lZDI1NTE5AAAAIFy29chtuWkzNpr7b3mFJGjPitmPcEuzq4GpBi7tn2//
ansible_ssh_host_key_rsa_public: AAAAB3NzaC1yc2EAAAADAQABAAABAQCzBYoddKiEaaUoNOuZbIxeQU1yZw9Fq7x80SJ6dEzFYsOpB9hTec5kmhuzriCwWLp53qK6TX/AiWJ6kpQuC8gROxZ+bIbb7V7ExEuvtMPxvsSvklaiHuV4vEKoRHkAxBTCENW2cStglhDPISSffOIynhbA0p4ZFQHhpojOO/yHn3FveqesSCtc5q+ezWWp+fllM+JrvXGp+avMLK0d08X42ytNaxrWmbxbCjsp0jcwfKOWK4G/rBaHiIi/6to7Ety2WNNj/c6nDxAIl2E0Scponi4nT6tQl1P4zFtfFY+9rE1lph8Exb6QGp/fGsmK+iHeSq9zYijJdg2z4FDqM17r
ansible_swapfree_mb: 8191
ansible_swaptotal_mb: 8191
ansible_system: Linux
ansible_system_capabilities:
- cap_chown
- cap_dac_override
- cap_dac_read_search
- cap_fowner
- cap_fsetid
- cap_kill
- cap_setgid
- cap_setuid
- cap_setpcap
- cap_linux_immutable
- cap_net_bind_service
- cap_net_broadcast
- cap_net_admin
- cap_net_raw
- cap_ipc_lock
- cap_ipc_owner
- cap_sys_module
- cap_sys_rawio
- cap_sys_chroot
- cap_sys_ptrace
- cap_sys_pacct
- cap_sys_admin
- cap_sys_boot
- cap_sys_nice
- cap_sys_resource
- cap_sys_time
- cap_sys_tty_config
- cap_mknod
- cap_lease
- cap_audit_write
- cap_audit_control
- cap_setfcap
- cap_mac_override
- cap_mac_admin
- cap_syslog
- cap_wake_alarm
- cap_block_suspend
- 37+ep
ansible_system_capabilities_enforced: 'True'
ansible_system_vendor: Supermicro
ansible_uptime_seconds: 36751
ansible_user: root
ansible_user_dir: /root
ansible_user_gecos: root
ansible_user_gid: 0
ansible_user_id: root
ansible_user_shell: /bin/bash
ansible_user_uid: 0
ansible_userspace_architecture: x86_64
ansible_userspace_bits: '64'
ansible_verbosity: 1
ansible_version:
    full: 2.9.9
    major: 2
    minor: 9
    revision: 9
    string: 2.9.9
ansible_vethc060a5f:
    active: true
    device: vethc060a5f
    ipv6:
    -   address: fe80::3877:d1ff:fee9:ae33
        prefix: '64'
        scope: link
    macaddress: 3a:77:d1:e9:ae:33
    mtu: 1500
    promisc: true
    speed: 10000
    type: ether
ansible_vetheee40b5:
    active: true
    device: vetheee40b5
    ipv6:
    -   address: fe80::9cdc:d7ff:fedc:c53f
        prefix: '64'
        scope: link
    macaddress: 9e:dc:d7:dc:c5:3f
    mtu: 1500
    promisc: true
    speed: 10000
    type: ether
ansible_virtualization_role: host
ansible_virtualization_type: kvm
bbb_admins:
    chris:
        email: chris@webarchitects.co.uk
    decentral1se:
        email: lukewm@riseup.net
    petter:
        email: petter@meet.coop
    wouter:
        email: wouter@meet.coop
bbb_demo: false
bbb_greenlight_docker_image: registry.git.coop/meet/greenlight/images/greenlight:www.meet.coop
bbb_greenlight_registration: invite
bbb_greenlight_smtp_domain: meet.coop
bbb_greenlight_smtp_port: 587
bbb_greenlight_smtp_sender: ca@meet.coop
bbb_greenlight_smtp_server: mail.webarch.email
bbb_greenlight_smtp_username: ca@meet.coop
bbb_turn_server: turn.fosshost.org
bbb_ufw: true
exim_dc_local_interfaces: 172.17.0.1 ; 127.0.0.1 ; ::1
gather_subset:
- all
getent_group:
    Debian-exim:
    - x
    - '112'
    - ''
    adm:
    - x
    - '4'
    - syslog
    audio:
    - x
    - '29'
    - ''
    backup:
    - x
    - '34'
    - ''
    bigbluebutton:
    - x
    - '997'
    - ''
    bin:
    - x
    - '2'
    - ''
    cdrom:
    - x
    - '24'
    - ''
    chris:
    - x
    - '1001'
    - chris
    colord:
    - x
    - '117'
    - ''
    crontab:
    - x
    - '107'
    - ''
    daemon:
    - x
    - '1'
    - ''
    decentral1se:
    - x
    - '1002'
    - decentral1se
    dialout:
    - x
    - '20'
    - ''
    dip:
    - x
    - '30'
    - ''
    disk:
    - x
    - '6'
    - ''
    docker:
    - x
    - '999'
    - ''
    etherpad:
    - x
    - '994'
    - ''
    fax:
    - x
    - '21'
    - ''
    floppy:
    - x
    - '25'
    - ''
    freeswitch:
    - x
    - '996'
    - bigbluebutton
    games:
    - x
    - '60'
    - ''
    gnats:
    - x
    - '41'
    - ''
    input:
    - x
    - '106'
    - ''
    irc:
    - x
    - '39'
    - ''
    kmem:
    - x
    - '15'
    - ''
    koumbit:
    - x
    - '1000'
    - ''
    kurento:
    - x
    - '995'
    - bigbluebutton
    list:
    - x
    - '38'
    - ''
    lp:
    - x
    - '7'
    - ''
    mail:
    - x
    - '8'
    - ''
    man:
    - x
    - '12'
    - ''
    messagebus:
    - x
    - '110'
    - ''
    meteor:
    - x
    - '993'
    - ''
    mongodb:
    - x
    - '113'
    - mongodb
    munin:
    - x
    - '115'
    - ''
    netdev:
    - x
    - '109'
    - ''
    news:
    - x
    - '9'
    - ''
    nogroup:
    - x
    - '65534'
    - ''
    operator:
    - x
    - '37'
    - ''
    plugdev:
    - x
    - '46'
    - ''
    proxy:
    - x
    - '13'
    - ''
    red5:
    - x
    - '998'
    - ''
    redis:
    - x
    - '118'
    - ''
    root:
    - x
    - '0'
    - ''
    sasl:
    - x
    - '45'
    - ''
    scanner:
    - x
    - '116'
    - ''
    shadow:
    - x
    - '42'
    - ''
    src:
    - x
    - '40'
    - ''
    ssh:
    - x
    - '111'
    - ''
    ssl-cert:
    - x
    - '114'
    - ''
    staff:
    - x
    - '50'
    - ''
    sudo:
    - x
    - '27'
    - koumbit,chris,decentral1se
    sys:
    - x
    - '3'
    - ''
    syslog:
    - x
    - '108'
    - ''
    systemd-bus-proxy:
    - x
    - '105'
    - ''
    systemd-journal:
    - x
    - '101'
    - ''
    systemd-network:
    - x
    - '103'
    - ''
    systemd-resolve:
    - x
    - '104'
    - ''
    systemd-timesync:
    - x
    - '102'
    - ''
    tape:
    - x
    - '26'
    - ''
    tty:
    - x
    - '5'
    - ''
    users:
    - x
    - '100'
    - ''
    utmp:
    - x
    - '43'
    - ''
    uucp:
    - x
    - '10'
    - ''
    video:
    - x
    - '44'
    - ''
    voice:
    - x
    - '22'
    - ''
    www-data:
    - x
    - '33'
    - ''
getent_hosts:
    127.0.0.1:
    - localhost
    - ip6-localhost
    - ip6-loopback
    199.58.80.6:
    - faiserver0.koumbit.net
getent_passwd:
    Debian-exim:
    - x
    - '108'
    - '112'
    - ''
    - /var/spool/exim4
    - /bin/false
    _apt:
    - x
    - '105'
    - '65534'
    - ''
    - /nonexistent
    - /bin/false
    backup:
    - x
    - '34'
    - '34'
    - backup
    - /var/backups
    - /usr/sbin/nologin
    bigbluebutton:
    - x
    - '998'
    - '997'
    - bigbluebutton
    - /home/bigbluebutton
    - /bin/false
    bin:
    - x
    - '2'
    - '2'
    - bin
    - /bin
    - /usr/sbin/nologin
    chris:
    - x
    - '1001'
    - '1001'
    - ''
    - /home/chris
    - /bin/bash
    colord:
    - x
    - '111'
    - '117'
    - colord colour management daemon,,,
    - /var/lib/colord
    - /bin/false
    daemon:
    - x
    - '1'
    - '1'
    - daemon
    - /usr/sbin
    - /usr/sbin/nologin
    decentral1se:
    - x
    - '1002'
    - '1002'
    - ''
    - /home/decentral1se
    - /bin/bash
    etherpad:
    - x
    - '995'
    - '994'
    - etherpad user-daemon
    - /usr/share/etherpad-lite
    - /bin/false
    freeswitch:
    - x
    - '997'
    - '996'
    - freeswitch
    - /opt/freeswitch
    - /bin/bash
    games:
    - x
    - '5'
    - '60'
    - games
    - /usr/games
    - /usr/sbin/nologin
    gnats:
    - x
    - '41'
    - '41'
    - Gnats Bug-Reporting System (admin)
    - /var/lib/gnats
    - /usr/sbin/nologin
    irc:
    - x
    - '39'
    - '39'
    - ircd
    - /var/run/ircd
    - /usr/sbin/nologin
    koumbit:
    - x
    - '1000'
    - '1000'
    - Koumbit sysadmin!,,,
    - /home/koumbit
    - /bin/bash
    kurento:
    - x
    - '996'
    - '995'
    - ''
    - /var/lib/kurento
    - ''
    list:
    - x
    - '38'
    - '38'
    - Mailing List Manager
    - /var/list
    - /usr/sbin/nologin
    lp:
    - x
    - '7'
    - '7'
    - lp
    - /var/spool/lpd
    - /usr/sbin/nologin
    mail:
    - x
    - '8'
    - '8'
    - mail
    - /var/mail
    - /usr/sbin/nologin
    man:
    - x
    - '6'
    - '12'
    - man
    - /var/cache/man
    - /usr/sbin/nologin
    messagebus:
    - x
    - '106'
    - '110'
    - ''
    - /var/run/dbus
    - /bin/false
    meteor:
    - x
    - '994'
    - '993'
    - meteor user-daemon
    - /usr/share/meteor
    - /bin/bash
    mongodb:
    - x
    - '109'
    - '65534'
    - ''
    - /home/mongodb
    - /bin/false
    munin:
    - x
    - '110'
    - '115'
    - munin application user,,,
    - /var/lib/munin
    - /bin/false
    news:
    - x
    - '9'
    - '9'
    - news
    - /var/spool/news
    - /usr/sbin/nologin
    nobody:
    - x
    - '65534'
    - '65534'
    - nobody
    - /nonexistent
    - /usr/sbin/nologin
    proxy:
    - x
    - '13'
    - '13'
    - proxy
    - /bin
    - /usr/sbin/nologin
    red5:
    - x
    - '999'
    - '998'
    - red5 user-daemon
    - /usr/share/red5
    - /bin/false
    redis:
    - x
    - '112'
    - '118'
    - ''
    - /var/lib/redis
    - /bin/false
    root:
    - x
    - '0'
    - '0'
    - root
    - /root
    - /bin/bash
    sshd:
    - x
    - '107'
    - '65534'
    - ''
    - /var/run/sshd
    - /usr/sbin/nologin
    sync:
    - x
    - '4'
    - '65534'
    - sync
    - /bin
    - /bin/sync
    sys:
    - x
    - '3'
    - '3'
    - sys
    - /dev
    - /usr/sbin/nologin
    syslog:
    - x
    - '104'
    - '108'
    - ''
    - /home/syslog
    - /bin/false
    systemd-bus-proxy:
    - x
    - '103'
    - '105'
    - systemd Bus Proxy,,,
    - /run/systemd
    - /bin/false
    systemd-network:
    - x
    - '101'
    - '103'
    - systemd Network Management,,,
    - /run/systemd/netif
    - /bin/false
    systemd-resolve:
    - x
    - '102'
    - '104'
    - systemd Resolver,,,
    - /run/systemd/resolve
    - /bin/false
    systemd-timesync:
    - x
    - '100'
    - '102'
    - systemd Time Synchronization,,,
    - /run/systemd
    - /bin/false
    uucp:
    - x
    - '10'
    - '10'
    - uucp
    - /var/spool/uucp
    - /usr/sbin/nologin
    www-data:
    - x
    - '33'
    - '33'
    - www-data
    - /var/www
    - /usr/sbin/nologin
getent_services:
    acr-nema:
    - 104/udp
    - dicom
    afbackup:
    - 2988/udp
    afmbackup:
    - 2989/udp
    afpovertcp:
    - 548/udp
    afs3-bos:
    - 7007/udp
    afs3-callback:
    - 7001/udp
    afs3-errors:
    - 7006/udp
    afs3-fileserver:
    - 7000/udp
    - bbs
    afs3-kaserver:
    - 7004/udp
    afs3-prserver:
    - 7002/udp
    afs3-rmtsys:
    - 7009/udp
    afs3-update:
    - 7008/udp
    afs3-vlserver:
    - 7003/udp
    afs3-volser:
    - 7005/udp
    amanda:
    - 10080/udp
    amandaidx:
    - 10082/tcp
    amidxtape:
    - 10083/tcp
    amqp:
    - 5672/sctp
    amqps:
    - 5671/tcp
    aol:
    - 5190/udp
    asf-rmcp:
    - 623/udp
    asp:
    - 27374/udp
    at-echo:
    - 204/udp
    at-nbp:
    - 202/udp
    at-rtmp:
    - 201/udp
    at-zis:
    - 206/udp
    auth:
    - 113/tcp
    - authentication
    - tap
    - ident
    bacula-dir:
    - 9101/udp
    bacula-fd:
    - 9102/udp
    bacula-sd:
    - 9103/udp
    bgp:
    - 179/udp
    bgpd:
    - 2605/tcp
    bgpsim:
    - 5675/tcp
    biff:
    - 512/udp
    - comsat
    binkp:
    - 24554/tcp
    bootpc:
    - 68/udp
    bootps:
    - 67/udp
    bpcd:
    - 13782/udp
    bpdbm:
    - 13721/udp
    bpjava-msvc:
    - 13722/udp
    bprd:
    - 13720/udp
    canna:
    - 5680/tcp
    cfengine:
    - 5308/udp
    cfinger:
    - 2003/tcp
    chargen:
    - 19/udp
    - ttytst
    - source
    cisco-sccp:
    - 2000/udp
    clc-build-daemon:
    - 8990/tcp
    clearcase:
    - 371/udp
    - Clearcase
    cmip-agent:
    - 164/udp
    cmip-man:
    - 163/udp
    codaauth2:
    - 370/udp
    codasrv:
    - 2432/udp
    codasrv-se:
    - 2433/udp
    conference:
    - 531/tcp
    - chat
    courier:
    - 530/tcp
    - rpc
    csnet-ns:
    - 105/udp
    - cso-ns
    csync2:
    - 30865/tcp
    customs:
    - 1001/udp
    cvspserver:
    - 2401/udp
    daap:
    - 3689/udp
    datametrics:
    - 1645/udp
    - old-radius
    daytime:
    - 13/udp
    db-lsp:
    - 17500/tcp
    dcap:
    - 22125/tcp
    dhcpv6-client:
    - 546/udp
    dhcpv6-server:
    - 547/udp
    dicom:
    - 11112/tcp
    dict:
    - 2628/udp
    dircproxy:
    - 57000/tcp
    discard:
    - 9/udp
    - sink
    - 'null'
    distcc:
    - 3632/udp
    distmp3:
    - 4600/tcp
    domain:
    - 53/udp
    echo:
    - 4/ddp
    eklogin:
    - 2105/tcp
    enbd-cstatd:
    - 5051/tcp
    enbd-sstatd:
    - 5052/tcp
    epmd:
    - 4369/udp
    exec:
    - 512/tcp
    f5-globalsite:
    - 2792/udp
    f5-iquery:
    - 4353/udp
    fatserv:
    - 347/udp
    fax:
    - 4557/tcp
    fido:
    - 60179/tcp
    finger:
    - 79/tcp
    font-service:
    - 7100/udp
    - xfs
    freeciv:
    - 5556/udp
    frox:
    - 2121/tcp
    fsp:
    - 21/udp
    - fspd
    ftp:
    - 21/tcp
    ftp-data:
    - 20/tcp
    ftps:
    - 990/tcp
    ftps-data:
    - 989/tcp
    gdomap:
    - 538/udp
    gds-db:
    - 3050/udp
    - gds_db
    ggz:
    - 5688/udp
    git:
    - 9418/tcp
    gnunet:
    - 2086/udp
    gnutella-rtr:
    - 6347/udp
    gnutella-svc:
    - 6346/udp
    gopher:
    - 70/udp
    gpsd:
    - 2947/udp
    gris:
    - 2135/udp
    groupwise:
    - 1677/udp
    gsidcap:
    - 22128/tcp
    gsiftp:
    - 2811/udp
    gsigatekeeper:
    - 2119/udp
    hkp:
    - 11371/udp
    hmmp-ind:
    - 612/udp
    - dqs313_intercell
    hostmon:
    - 5355/udp
    hostnames:
    - 101/tcp
    - hostname
    http:
    - 80/udp
    http-alt:
    - 8080/udp
    https:
    - 443/udp
    hylafax:
    - 4559/tcp
    iax:
    - 4569/udp
    icpv2:
    - 3130/udp
    - icp
    idfp:
    - 549/udp
    imap2:
    - 143/udp
    - imap
    imap3:
    - 220/udp
    imaps:
    - 993/udp
    imsp:
    - 406/udp
    ingreslock:
    - 1524/udp
    ipp:
    - 631/udp
    iprop:
    - 2121/tcp
    ipsec-nat-t:
    - 4500/udp
    ipx:
    - 213/udp
    irc:
    - 194/udp
    ircd:
    - 6667/tcp
    ircs:
    - 994/udp
    isakmp:
    - 500/udp
    iscsi-target:
    - 3260/tcp
    isdnlog:
    - 20011/udp
    isisd:
    - 2608/tcp
    iso-tsap:
    - 102/tcp
    - tsap
    kamanda:
    - 10081/udp
    kazaa:
    - 1214/udp
    kerberos:
    - 88/udp
    - kerberos5
    - krb5
    - kerberos-sec
    kerberos-adm:
    - 749/tcp
    kerberos-master:
    - 751/tcp
    kerberos4:
    - 750/tcp
    - kerberos-iv
    - kdc
    kermit:
    - 1649/udp
    klogin:
    - 543/tcp
    knetd:
    - 2053/tcp
    kpasswd:
    - 464/udp
    kpop:
    - 1109/tcp
    krb-prop:
    - 754/tcp
    - krb_prop
    - krb5_prop
    - hprop
    krbupdate:
    - 760/tcp
    - kreg
    kshell:
    - 544/tcp
    - krcmd
    kx:
    - 2111/tcp
    l2f:
    - 1701/udp
    - l2tp
    ldap:
    - 389/udp
    ldaps:
    - 636/udp
    link:
    - 87/tcp
    - ttylink
    linuxconf:
    - 98/tcp
    loc-srv:
    - 135/udp
    - epmap
    log-server:
    - 1958/tcp
    login:
    - 513/tcp
    lotusnote:
    - 1352/udp
    - lotusnotes
    mailq:
    - 174/udp
    mandelspawn:
    - 9359/udp
    - mandelbrot
    mdns:
    - 5353/udp
    microsoft-ds:
    - 445/udp
    mmcc:
    - 5050/udp
    moira-db:
    - 775/tcp
    - moira_db
    moira-update:
    - 777/tcp
    - moira_update
    moira-ureg:
    - 779/udp
    - moira_ureg
    mon:
    - 2583/udp
    mrtd:
    - 5674/tcp
    ms-sql-m:
    - 1434/udp
    ms-sql-s:
    - 1433/udp
    msnp:
    - 1863/udp
    msp:
    - 18/udp
    mtn:
    - 4691/udp
    mtp:
    - 57/tcp
    munin:
    - 4949/tcp
    - lrrd
    mysql:
    - 3306/udp
    mysql-proxy:
    - 6446/udp
    nameserver:
    - 42/tcp
    - name
    nbd:
    - 10809/tcp
    nbp:
    - 2/ddp
    nessus:
    - 1241/udp
    netbios-dgm:
    - 138/udp
    netbios-ns:
    - 137/udp
    netbios-ssn:
    - 139/udp
    netnews:
    - 532/tcp
    - readnews
    netstat:
    - 15/tcp
    netwall:
    - 533/udp
    nextstep:
    - 178/udp
    - NeXTStep
    - NextStep
    nfs:
    - 2049/udp
    ninstall:
    - 2150/udp
    nntp:
    - 119/tcp
    - readnews
    - untp
    nntps:
    - 563/udp
    - snntp
    noclog:
    - 5354/udp
    npmp-gui:
    - 611/udp
    - dqs313_execd
    npmp-local:
    - 610/udp
    - dqs313_qmaster
    nqs:
    - 607/udp
    nrpe:
    - 5666/tcp
    nsca:
    - 5667/tcp
    ntalk:
    - 518/udp
    ntp:
    - 123/udp
    nut:
    - 3493/udp
    omirr:
    - 808/udp
    - omirrd
    omniorb:
    - 8088/udp
    openvpn:
    - 1194/udp
    ospf6d:
    - 2606/tcp
    ospfapi:
    - 2607/tcp
    ospfd:
    - 2604/tcp
    passwd-server:
    - 752/udp
    - passwd_server
    pawserv:
    - 345/udp
    pcrd:
    - 5151/tcp
    pipe-server:
    - 2010/tcp
    - pipe_server
    pop2:
    - 109/udp
    - pop-2
    pop3:
    - 110/udp
    - pop-3
    pop3s:
    - 995/udp
    poppassd:
    - 106/udp
    postgresql:
    - 5432/udp
    - postgres
    predict:
    - 1210/udp
    printer:
    - 515/tcp
    - spooler
    proofd:
    - 1093/udp
    prospero:
    - 191/udp
    prospero-np:
    - 1525/udp
    pwdgen:
    - 129/udp
    qmqp:
    - 628/udp
    qmtp:
    - 209/udp
    qotd:
    - 17/tcp
    - quote
    radius:
    - 1812/udp
    radius-acct:
    - 1813/udp
    - radacct
    radmin-port:
    - 4899/udp
    re-mail-ck:
    - 50/udp
    remctl:
    - 4373/udp
    remotefs:
    - 556/tcp
    - rfs_server
    - rfs
    remoteping:
    - 1959/tcp
    rfe:
    - 5002/tcp
    ripd:
    - 2602/tcp
    ripngd:
    - 2603/tcp
    rje:
    - 77/tcp
    - netrjs
    rlp:
    - 39/udp
    - resource
    rmiregistry:
    - 1099/udp
    rmtcfg:
    - 1236/tcp
    rootd:
    - 1094/udp
    route:
    - 520/udp
    - router
    - routed
    rpc2portmap:
    - 369/udp
    rplay:
    - 5555/udp
    rsync:
    - 873/udp
    rtcm-sc104:
    - 2101/udp
    rtelnet:
    - 107/udp
    rtmp:
    - 1/ddp
    rtsp:
    - 554/udp
    sa-msg-port:
    - 1646/udp
    - old-radacct
    saft:
    - 487/udp
    sane-port:
    - 6566/tcp
    - sane
    - saned
    search:
    - 2010/tcp
    - ndtp
    sftp:
    - 115/tcp
    sge-execd:
    - 6445/udp
    - sge_execd
    sge-qmaster:
    - 6444/udp
    - sge_qmaster
    sgi-cad:
    - 17004/tcp
    sgi-cmsd:
    - 17001/udp
    sgi-crsd:
    - 17002/udp
    sgi-gcd:
    - 17003/udp
    shell:
    - 514/tcp
    - cmd
    sieve:
    - 4190/tcp
    silc:
    - 706/udp
    sip:
    - 5060/udp
    sip-tls:
    - 5061/udp
    skkserv:
    - 1178/tcp
    smsqp:
    - 11201/udp
    smtp:
    - 25/tcp
    - mail
    smux:
    - 199/udp
    snmp:
    - 161/udp
    snmp-trap:
    - 162/udp
    - snmptrap
    snpp:
    - 444/udp
    socks:
    - 1080/udp
    spamd:
    - 783/tcp
    ssh:
    - 22/udp
    submission:
    - 587/udp
    sunrpc:
    - 111/udp
    - portmapper
    supdup:
    - 95/tcp
    supfiledbg:
    - 1127/tcp
    supfilesrv:
    - 871/tcp
    support:
    - 1529/tcp
    suucp:
    - 4031/udp
    svn:
    - 3690/udp
    - subversion
    svrloc:
    - 427/udp
    swat:
    - 901/tcp
    syslog:
    - 514/udp
    syslog-tls:
    - 6514/tcp
    sysrqd:
    - 4094/udp
    systat:
    - 11/tcp
    - users
    tacacs:
    - 49/udp
    tacacs-ds:
    - 65/udp
    talk:
    - 517/udp
    tcpmux:
    - 1/tcp
    telnet:
    - 23/tcp
    telnets:
    - 992/udp
    tempo:
    - 526/tcp
    - newdate
    tfido:
    - 60177/tcp
    tftp:
    - 69/udp
    time:
    - 37/udp
    - timserver
    timed:
    - 525/udp
    - timeserver
    tinc:
    - 655/udp
    tproxy:
    - 8081/tcp
    ulistserv:
    - 372/udp
    unix-status:
    - 1957/tcp
    urd:
    - 465/tcp
    - ssmtp
    - smtps
    uucp:
    - 540/tcp
    - uucpd
    uucp-path:
    - 117/tcp
    vboxd:
    - 20012/udp
    venus:
    - 2430/udp
    venus-se:
    - 2431/udp
    vnetd:
    - 13724/udp
    vopied:
    - 13783/udp
    webmin:
    - 10000/tcp
    webster:
    - 765/udp
    who:
    - 513/udp
    - whod
    whois:
    - 43/tcp
    - nicname
    wipld:
    - 1300/tcp
    wnn6:
    - 22273/udp
    x11:
    - 6000/udp
    - x11-0
    x11-1:
    - 6001/udp
    x11-2:
    - 6002/udp
    x11-3:
    - 6003/udp
    x11-4:
    - 6004/udp
    x11-5:
    - 6005/udp
    x11-6:
    - 6006/udp
    x11-7:
    - 6007/udp
    xdmcp:
    - 177/udp
    xinetd:
    - 9098/tcp
    xmms2:
    - 9667/udp
    xmpp-client:
    - 5222/udp
    - jabber-client
    xmpp-server:
    - 5269/udp
    - jabber-server
    xpilot:
    - 15345/udp
    xtel:
    - 1313/tcp
    xtell:
    - 4224/tcp
    xtelw:
    - 1314/tcp
    z3950:
    - 210/udp
    - wais
    zabbix-agent:
    - 10050/udp
    zabbix-trapper:
    - 10051/udp
    zebra:
    - 2601/tcp
    zebrasrv:
    - 2600/tcp
    zephyr-clt:
    - 2103/udp
    zephyr-hm:
    - 2104/udp
    zephyr-srv:
    - 2102/udp
    zip:
    - 6/ddp
    zope:
    - 9673/tcp
    zope-ftp:
    - 8021/tcp
    zserv:
    - 346/udp
group_names:
- bbb22_servers
groups:
    all:
    - ca.meet.coop
    - demo.meet.coop
    - dev.meet.coop
    - bbb.webarch.org.uk
    - turn.bbb.webarch.org.uk
    - turn.fosshost.org
    - monitor.webarch.net
    bbb22_servers:
    - ca.meet.coop
    - demo.meet.coop
    - dev.meet.coop
    - bbb.webarch.org.uk
    munin_servers:
    - monitor.webarch.net
    turn_servers:
    - turn.bbb.webarch.org.uk
    - turn.fosshost.org
    ungrouped: []
inventory_dir: /home/chris/meet/servers
inventory_file: /home/chris/meet/servers/hosts.yml
inventory_hostname: ca.meet.coop
inventory_hostname_short: ca
module_setup: true
mongodb_version: 3.4
munin_plugins_disabled:
-   dir: /usr/lib/munin-c/plugins
    file: munin-plugins-c
    name: entropy
-   name: nginx_request
-   name: nginx_status
-   dir: /usr/lib/munin-c/plugins
    file: munin-plugins-c
    name: open_files
-   dir: /usr/local/share/munin/plugins
    name: phpfpm_average
-   dir: /usr/local/share/munin/plugins
    name: phpfpm_connections
-   dir: /usr/local/share/munin/plugins
    name: phpfpm_memory
-   dir: /usr/local/share/munin/plugins
    name: phpfpm_processes
-   dir: /usr/local/share/munin/plugins
    name: phpfpm_status
munin_plugins_enabled:
-   name: apt_all
-   dir: /usr/lib/munin-c/plugins
    file: munin-plugins-c
    name: cpu
-   name: df
-   name: df_inode
-   name: diskstats
-   name: fail2ban
-   dir: /usr/lib/munin-c/plugins
    file: munin-plugins-c
    name: forks
-   dir: /usr/lib/munin-c/plugins
    file: munin-plugins-c
    name: fw_packets
-   dir: /usr/lib/munin-c/plugins
    file: munin-plugins-c
    name: interrupts
-   dir: /usr/lib/munin-c/plugins
    file: munin-plugins-c
    name: load
-   dir: /usr/lib/munin-c/plugins
    file: munin-plugins-c
    name: open_inodes
-   dir: /usr/lib/munin-c/plugins
    file: munin-plugins-c
    name: swap
-   dir: /usr/lib/munin-c/plugins
    file: munin-plugins-c
    name: threads
-   dir: /usr/lib/munin-c/plugins
    file: munin-plugins-c
    name: uptime
nginx_tls1_1: true
nginx_tls1_3: false
nodejs_version: 8
omit: __omit_place_holder__99e282c62f94b3d12d44258b247f5de8d670e4bc
packages:
    acl:
    -   arch: amd64
        category: utils
        name: acl
        origin: Ubuntu
        source: apt
        version: 2.2.52-3
    adduser:
    -   arch: all
        category: admin
        name: adduser
        origin: Ubuntu
        source: apt
        version: 3.113+nmu3ubuntu4
    adwaita-icon-theme:
    -   arch: all
        category: gnome
        name: adwaita-icon-theme
        origin: Ubuntu
        source: apt
        version: 3.18.0-2ubuntu3.1
    aglfn:
    -   arch: all
        category: universe/fonts
        name: aglfn
        origin: Ubuntu
        source: apt
        version: 1.7-3
    amd64-microcode:
    -   arch: amd64
        category: admin
        name: amd64-microcode
        origin: Ubuntu
        source: apt
        version: 3.20191021.1+really3.20180524.1~ubuntu0.16.04.2
    apparmor:
    -   arch: amd64
        category: admin
        name: apparmor
        origin: Ubuntu
        source: apt
        version: 2.10.95-0ubuntu2.11
    apt:
    -   arch: amd64
        category: admin
        name: apt
        origin: Ubuntu
        source: apt
        version: 1.2.32ubuntu0.1
    apt-listchanges:
    -   arch: all
        category: utils
        name: apt-listchanges
        origin: Ubuntu
        source: apt
        version: 2.85.14ubuntu1
    apt-show-versions:
    -   arch: all
        category: universe/admin
        name: apt-show-versions
        origin: Ubuntu
        source: apt
        version: 0.22.7
    apt-transport-https:
    -   arch: amd64
        category: admin
        name: apt-transport-https
        origin: Ubuntu
        source: apt
        version: 1.2.32ubuntu0.1
    apt-utils:
    -   arch: amd64
        category: admin
        name: apt-utils
        origin: Ubuntu
        source: apt
        version: 1.2.32ubuntu0.1
    apticron:
    -   arch: all
        category: universe/admin
        name: apticron
        origin: Ubuntu
        source: apt
        version: 1.1.58ubuntu1
    aptitude:
    -   arch: amd64
        category: admin
        name: aptitude
        origin: Ubuntu
        source: apt
        version: 0.7.4-2ubuntu2
    aptitude-common:
    -   arch: all
        category: admin
        name: aptitude-common
        origin: Ubuntu
        source: apt
        version: 0.7.4-2ubuntu2
    at-spi2-core:
    -   arch: amd64
        category: misc
        name: at-spi2-core
        origin: Ubuntu
        source: apt
        version: 2.18.3-4ubuntu1
    aufs-tools:
    -   arch: amd64
        category: universe/kernel
        name: aufs-tools
        origin: Ubuntu
        source: apt
        version: 1:3.2+20130722-1.1ubuntu1
    base-files:
    -   arch: amd64
        category: admin
        name: base-files
        origin: Ubuntu
        source: apt
        version: 9.4ubuntu4.11
    base-passwd:
    -   arch: amd64
        category: admin
        name: base-passwd
        origin: Ubuntu
        source: apt
        version: 3.5.39
    bash:
    -   arch: amd64
        category: shells
        name: bash
        origin: Ubuntu
        source: apt
        version: 4.3-14ubuntu1.4
    bbb-apps:
    -   arch: amd64
        category: default
        name: bbb-apps
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-45
    bbb-apps-akka:
    -   arch: all
        category: java
        name: bbb-apps-akka
        origin: BigBlueButton
        source: apt
        version: 2.2.0-87
    bbb-apps-screenshare:
    -   arch: amd64
        category: default
        name: bbb-apps-screenshare
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-45
    bbb-apps-sip:
    -   arch: amd64
        category: default
        name: bbb-apps-sip
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-11
    bbb-apps-video:
    -   arch: amd64
        category: default
        name: bbb-apps-video
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-42
    bbb-apps-video-broadcast:
    -   arch: amd64
        category: default
        name: bbb-apps-video-broadcast
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-8
    bbb-client:
    -   arch: amd64
        category: default
        name: bbb-client
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-40
    bbb-config:
    -   arch: amd64
        category: default
        name: bbb-config
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-189
    bbb-etherpad:
    -   arch: amd64
        category: default
        name: bbb-etherpad
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-51
    bbb-freeswitch-core:
    -   arch: amd64
        category: default
        name: bbb-freeswitch-core
        origin: BigBlueButton
        source: apt
        version: 2:2.2.0-112
    bbb-freeswitch-sounds:
    -   arch: amd64
        category: default
        name: bbb-freeswitch-sounds
        origin: BigBlueButton
        source: apt
        version: 1:1.6.7-6
    bbb-fsesl-akka:
    -   arch: all
        category: java
        name: bbb-fsesl-akka
        origin: BigBlueButton
        source: apt
        version: 2.2.0-65
    bbb-html5:
    -   arch: amd64
        category: default
        name: bbb-html5
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-934
    bbb-mkclean:
    -   arch: amd64
        category: default
        name: bbb-mkclean
        origin: BigBlueButton
        source: apt
        version: 1:0.8.7-3
    bbb-playback-presentation:
    -   arch: amd64
        category: default
        name: bbb-playback-presentation
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-30
    bbb-record-core:
    -   arch: amd64
        category: default
        name: bbb-record-core
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-66
    bbb-red5:
    -   arch: amd64
        category: default
        name: bbb-red5
        origin: BigBlueButton
        source: apt
        version: 1:1.0.10-16
    bbb-transcode-akka:
    -   arch: all
        category: java
        name: bbb-transcode-akka
        origin: BigBlueButton
        source: apt
        version: 2.2.0-8
    bbb-web:
    -   arch: amd64
        category: default
        name: bbb-web
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-130
    bbb-webrtc-sfu:
    -   arch: amd64
        category: default
        name: bbb-webrtc-sfu
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-99
    bc:
    -   arch: amd64
        category: math
        name: bc
        origin: Ubuntu
        source: apt
        version: 1.06.95-9build1
    bigbluebutton:
    -   arch: amd64
        category: default
        name: bigbluebutton
        origin: BigBlueButton
        source: apt
        version: 1:2.2.0-5
    bind9-host:
    -   arch: amd64
        category: net
        name: bind9-host
        origin: Ubuntu
        source: apt
        version: 1:9.10.3.dfsg.P4-8ubuntu1.16
    binutils:
    -   arch: amd64
        category: devel
        name: binutils
        origin: Ubuntu
        source: apt
        version: 2.26.1-1ubuntu1~16.04.8
    bsdutils:
    -   arch: amd64
        category: utils
        name: bsdutils
        origin: Ubuntu
        source: apt
        version: 1:2.27.1-6ubuntu3.10
    build-essential:
    -   arch: amd64
        category: devel
        name: build-essential
        origin: Ubuntu
        source: apt
        version: 12.1ubuntu2
    busybox-initramfs:
    -   arch: amd64
        category: shells
        name: busybox-initramfs
        origin: Ubuntu
        source: apt
        version: 1:1.22.0-15ubuntu1.4
    bzip2:
    -   arch: amd64
        category: utils
        name: bzip2
        origin: Ubuntu
        source: apt
        version: 1.0.6-8ubuntu0.2
    ca-certificates:
    -   arch: all
        category: misc
        name: ca-certificates
        origin: Ubuntu
        source: apt
        version: 20190110~16.04.1
    ca-certificates-java:
    -   arch: all
        category: misc
        name: ca-certificates-java
        origin: Ubuntu
        source: apt
        version: 20160321ubuntu1
    cabextract:
    -   arch: amd64
        category: universe/utils
        name: cabextract
        origin: Ubuntu
        source: apt
        version: 1.6-1
    cgroupfs-mount:
    -   arch: all
        category: universe/admin
        name: cgroupfs-mount
        origin: Ubuntu
        source: apt
        version: '1.2'
    colord:
    -   arch: amd64
        category: graphics
        name: colord
        origin: Ubuntu
        source: apt
        version: 1.2.12-1ubuntu1
    colord-data:
    -   arch: all
        category: graphics
        name: colord-data
        origin: Ubuntu
        source: apt
        version: 1.2.12-1ubuntu1
    console-setup:
    -   arch: all
        category: utils
        name: console-setup
        origin: Ubuntu
        source: apt
        version: 1.108ubuntu15.5
    console-setup-linux:
    -   arch: all
        category: utils
        name: console-setup-linux
        origin: Ubuntu
        source: apt
        version: 1.108ubuntu15.5
    containerd.io:
    -   arch: amd64
        category: devel
        name: containerd.io
        origin: Docker
        source: apt
        version: 1.2.13-2
    coreutils:
    -   arch: amd64
        category: utils
        name: coreutils
        origin: Ubuntu
        source: apt
        version: 8.25-2ubuntu3~16.04
    cpio:
    -   arch: amd64
        category: utils
        name: cpio
        origin: Ubuntu
        source: apt
        version: 2.11+dfsg-5ubuntu1.1
    cpp:
    -   arch: amd64
        category: interpreters
        name: cpp
        origin: Ubuntu
        source: apt
        version: 4:5.3.1-1ubuntu1
    cpp-5:
    -   arch: amd64
        category: interpreters
        name: cpp-5
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    crda:
    -   arch: amd64
        category: net
        name: crda
        origin: Ubuntu
        source: apt
        version: 3.13-1
    cron:
    -   arch: amd64
        category: admin
        name: cron
        origin: Ubuntu
        source: apt
        version: 3.0pl1-128ubuntu2
    cryptsetup:
    -   arch: amd64
        category: admin
        name: cryptsetup
        origin: Ubuntu
        source: apt
        version: 2:1.6.6-5ubuntu2.1
    cryptsetup-bin:
    -   arch: amd64
        category: admin
        name: cryptsetup-bin
        origin: Ubuntu
        source: apt
        version: 2:1.6.6-5ubuntu2.1
    curl:
    -   arch: amd64
        category: web
        name: curl
        origin: Ubuntu
        source: apt
        version: 7.47.0-1ubuntu2.14
    dash:
    -   arch: amd64
        category: shells
        name: dash
        origin: Ubuntu
        source: apt
        version: 0.5.8-2.1ubuntu2
    dbus:
    -   arch: amd64
        category: devel
        name: dbus
        origin: Ubuntu
        source: apt
        version: 1.10.6-1ubuntu3.5
    dconf-gsettings-backend:
    -   arch: amd64
        category: libs
        name: dconf-gsettings-backend
        origin: Ubuntu
        source: apt
        version: 0.24.0-2
    dconf-service:
    -   arch: amd64
        category: libs
        name: dconf-service
        origin: Ubuntu
        source: apt
        version: 0.24.0-2
    debconf:
    -   arch: all
        category: admin
        name: debconf
        origin: Ubuntu
        source: apt
        version: 1.5.58ubuntu2
    debconf-i18n:
    -   arch: all
        category: admin
        name: debconf-i18n
        origin: Ubuntu
        source: apt
        version: 1.5.58ubuntu2
    debconf-utils:
    -   arch: all
        category: universe/devel
        name: debconf-utils
        origin: Ubuntu
        source: apt
        version: 1.5.58ubuntu2
    debianutils:
    -   arch: amd64
        category: utils
        name: debianutils
        origin: Ubuntu
        source: apt
        version: '4.7'
    default-jre:
    -   arch: amd64
        category: interpreters
        name: default-jre
        origin: Ubuntu
        source: apt
        version: 2:1.8-56ubuntu2
    default-jre-headless:
    -   arch: amd64
        category: interpreters
        name: default-jre-headless
        origin: Ubuntu
        source: apt
        version: 2:1.8-56ubuntu2
    dh-python:
    -   arch: all
        category: python
        name: dh-python
        origin: Ubuntu
        source: apt
        version: 2.20151103ubuntu1.2
    dictionaries-common:
    -   arch: all
        category: text
        name: dictionaries-common
        origin: Ubuntu
        source: apt
        version: 1.26.3
    diffutils:
    -   arch: amd64
        category: utils
        name: diffutils
        origin: Ubuntu
        source: apt
        version: 1:3.3-3
    dirmngr:
    -   arch: amd64
        category: utils
        name: dirmngr
        origin: Ubuntu
        source: apt
        version: 2.1.11-6ubuntu2.1
    distro-info-data:
    -   arch: all
        category: devel
        name: distro-info-data
        origin: Ubuntu
        source: apt
        version: 0.28ubuntu0.14
    dmeventd:
    -   arch: amd64
        category: admin
        name: dmeventd
        origin: Ubuntu
        source: apt
        version: 2:1.02.110-1ubuntu10
    dmsetup:
    -   arch: amd64
        category: admin
        name: dmsetup
        origin: Ubuntu
        source: apt
        version: 2:1.02.110-1ubuntu10
    dnsutils:
    -   arch: amd64
        category: net
        name: dnsutils
        origin: Ubuntu
        source: apt
        version: 1:9.10.3.dfsg.P4-8ubuntu1.16
    docker-ce:
    -   arch: amd64
        category: admin
        name: docker-ce
        origin: Docker
        source: apt
        version: 5:19.03.11~3-0~ubuntu-xenial
    docker-ce-cli:
    -   arch: amd64
        category: admin
        name: docker-ce-cli
        origin: Docker
        source: apt
        version: 5:19.03.11~3-0~ubuntu-xenial
    dpkg:
    -   arch: amd64
        category: admin
        name: dpkg
        origin: Ubuntu
        source: apt
        version: 1.18.4ubuntu1.6
    dpkg-dev:
    -   arch: all
        category: utils
        name: dpkg-dev
        origin: Ubuntu
        source: apt
        version: 1.18.4ubuntu1.6
    dstat:
    -   arch: all
        category: universe/admin
        name: dstat
        origin: Ubuntu
        source: apt
        version: 0.7.2-4
    e2fslibs:
    -   arch: amd64
        category: libs
        name: e2fslibs
        origin: Ubuntu
        source: apt
        version: 1.42.13-1ubuntu1.2
    e2fsprogs:
    -   arch: amd64
        category: admin
        name: e2fsprogs
        origin: Ubuntu
        source: apt
        version: 1.42.13-1ubuntu1.2
    eject:
    -   arch: amd64
        category: utils
        name: eject
        origin: Ubuntu
        source: apt
        version: 2.1.5+deb1+cvs20081104-13.1ubuntu0.16.04.1
    emacsen-common:
    -   arch: all
        category: editors
        name: emacsen-common
        origin: Ubuntu
        source: apt
        version: 2.0.8
    esound-common:
    -   arch: all
        category: universe/sound
        name: esound-common
        origin: Ubuntu
        source: apt
        version: 0.2.41-11
    etckeeper:
    -   arch: all
        category: admin
        name: etckeeper
        origin: Ubuntu
        source: apt
        version: 1.18.2-1ubuntu1
    exim4-base:
    -   arch: amd64
        category: mail
        name: exim4-base
        origin: Ubuntu
        source: apt
        version: 4.86.2-2ubuntu2.6
    exim4-config:
    -   arch: all
        category: mail
        name: exim4-config
        origin: Ubuntu
        source: apt
        version: 4.86.2-2ubuntu2.6
    exim4-daemon-light:
    -   arch: amd64
        category: mail
        name: exim4-daemon-light
        origin: Ubuntu
        source: apt
        version: 4.86.2-2ubuntu2.6
    fakeroot:
    -   arch: amd64
        category: utils
        name: fakeroot
        origin: Ubuntu
        source: apt
        version: 1.20.2-1ubuntu1
    ffmpeg:
    -   arch: amd64
        category: video
        name: ffmpeg
        origin: LP-PPA-bigbluebutton-support
        source: apt
        version: 7:4.2.2-1bbb1~ubuntu16.04
    file:
    -   arch: amd64
        category: utils
        name: file
        origin: Ubuntu
        source: apt
        version: 1:5.25-2ubuntu1.4
    findutils:
    -   arch: amd64
        category: utils
        name: findutils
        origin: Ubuntu
        source: apt
        version: 4.6.0+git+20160126-2
    fontconfig:
    -   arch: amd64
        category: utils
        name: fontconfig
        origin: Ubuntu
        source: apt
        version: 2.11.94-0ubuntu1.1
    fontconfig-config:
    -   arch: all
        category: libs
        name: fontconfig-config
        origin: Ubuntu
        source: apt
        version: 2.11.94-0ubuntu1.1
    fonts-crosextra-caladea:
    -   arch: all
        category: universe/fonts
        name: fonts-crosextra-caladea
        origin: Ubuntu
        source: apt
        version: 20130214-1
    fonts-crosextra-carlito:
    -   arch: all
        category: universe/fonts
        name: fonts-crosextra-carlito
        origin: Ubuntu
        source: apt
        version: 20130920-1
    fonts-dejavu:
    -   arch: all
        category: universe/fonts
        name: fonts-dejavu
        origin: Ubuntu
        source: apt
        version: 2.35-1
    fonts-dejavu-core:
    -   arch: all
        category: fonts
        name: fonts-dejavu-core
        origin: Ubuntu
        source: apt
        version: 2.35-1
    fonts-dejavu-extra:
    -   arch: all
        category: fonts
        name: fonts-dejavu-extra
        origin: Ubuntu
        source: apt
        version: 2.35-1
    fonts-lato:
    -   arch: all
        category: fonts
        name: fonts-lato
        origin: Ubuntu
        source: apt
        version: 2.0-1
    fonts-liberation:
    -   arch: all
        category: fonts
        name: fonts-liberation
        origin: Ubuntu
        source: apt
        version: 1.07.4-1
    fonts-noto:
    -   arch: all
        category: universe/fonts
        name: fonts-noto
        origin: Ubuntu
        source: apt
        version: 20160116-1
    fonts-noto-cjk:
    -   arch: all
        category: fonts
        name: fonts-noto-cjk
        origin: Ubuntu
        source: apt
        version: 1:1.004+repack2-1~ubuntu1
    fonts-noto-hinted:
    -   arch: all
        category: universe/fonts
        name: fonts-noto-hinted
        origin: Ubuntu
        source: apt
        version: 20160116-1
    fonts-noto-mono:
    -   arch: all
        category: universe/fonts
        name: fonts-noto-mono
        origin: Ubuntu
        source: apt
        version: 20160116-1
    fonts-noto-unhinted:
    -   arch: all
        category: universe/fonts
        name: fonts-noto-unhinted
        origin: Ubuntu
        source: apt
        version: 20160116-1
    fonts-opensymbol:
    -   arch: all
        category: fonts
        name: fonts-opensymbol
        origin: Ubuntu
        source: apt
        version: 2:102.7+LibO5.1.6~rc2-0ubuntu1~xenial10
    fonts-sil-gentium:
    -   arch: all
        category: universe/fonts
        name: fonts-sil-gentium
        origin: Ubuntu
        source: apt
        version: 20081126:1.03-1
    fonts-sil-gentium-basic:
    -   arch: all
        category: universe/fonts
        name: fonts-sil-gentium-basic
        origin: Ubuntu
        source: apt
        version: 1.1-7
    fonts-stix:
    -   arch: all
        category: fonts
        name: fonts-stix
        origin: Ubuntu
        source: apt
        version: 1.1.1-4
    freepats:
    -   arch: all
        category: universe/sound
        name: freepats
        origin: Ubuntu
        source: apt
        version: 20060219-1
    g++:
    -   arch: amd64
        category: devel
        name: g++
        origin: Ubuntu
        source: apt
        version: 4:5.3.1-1ubuntu1
    g++-5:
    -   arch: amd64
        category: devel
        name: g++-5
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    gawk:
    -   arch: amd64
        category: interpreters
        name: gawk
        origin: Ubuntu
        source: apt
        version: 1:4.1.3+dfsg-0.1
    gcc:
    -   arch: amd64
        category: devel
        name: gcc
        origin: Ubuntu
        source: apt
        version: 4:5.3.1-1ubuntu1
    gcc-5:
    -   arch: amd64
        category: devel
        name: gcc-5
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    gcc-5-base:
    -   arch: amd64
        category: libs
        name: gcc-5-base
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    gcc-6-base:
    -   arch: amd64
        category: libs
        name: gcc-6-base
        origin: Ubuntu
        source: apt
        version: 6.0.1-0ubuntu1
    geoip-database:
    -   arch: all
        category: net
        name: geoip-database
        origin: Ubuntu
        source: apt
        version: 20160408-1
    gettext-base:
    -   arch: amd64
        category: utils
        name: gettext-base
        origin: Ubuntu
        source: apt
        version: 0.19.7-2ubuntu3.1
    ghostscript:
    -   arch: amd64
        category: text
        name: ghostscript
        origin: Ubuntu
        source: apt
        version: 9.26~dfsg+0-0ubuntu0.16.04.12
    gir1.2-glib-2.0:
    -   arch: amd64
        category: libs
        name: gir1.2-glib-2.0
        origin: Ubuntu
        source: apt
        version: 1.46.0-3ubuntu1
    git:
    -   arch: amd64
        category: vcs
        name: git
        origin: Ubuntu
        source: apt
        version: 1:2.7.4-0ubuntu1.9
    git-man:
    -   arch: all
        category: vcs
        name: git-man
        origin: Ubuntu
        source: apt
        version: 1:2.7.4-0ubuntu1.9
    glib-networking:
    -   arch: amd64
        category: libs
        name: glib-networking
        origin: Ubuntu
        source: apt
        version: 2.48.2-1~ubuntu16.04.1
    glib-networking-common:
    -   arch: all
        category: libs
        name: glib-networking-common
        origin: Ubuntu
        source: apt
        version: 2.48.2-1~ubuntu16.04.1
    glib-networking-services:
    -   arch: amd64
        category: libs
        name: glib-networking-services
        origin: Ubuntu
        source: apt
        version: 2.48.2-1~ubuntu16.04.1
    gnupg:
    -   arch: amd64
        category: utils
        name: gnupg
        origin: Ubuntu
        source: apt
        version: 1.4.20-1ubuntu3.3
    gnupg-agent:
    -   arch: amd64
        category: utils
        name: gnupg-agent
        origin: Ubuntu
        source: apt
        version: 2.1.11-6ubuntu2.1
    gnupg2:
    -   arch: amd64
        category: utils
        name: gnupg2
        origin: Ubuntu
        source: apt
        version: 2.1.11-6ubuntu2.1
    gnuplot:
    -   arch: all
        category: universe/math
        name: gnuplot
        origin: Ubuntu
        source: apt
        version: 4.6.6-3
    gnuplot-tex:
    -   arch: all
        category: universe/doc
        name: gnuplot-tex
        origin: Ubuntu
        source: apt
        version: 4.6.6-3
    gnuplot5-data:
    -   arch: all
        category: universe/doc
        name: gnuplot5-data
        origin: Ubuntu
        source: apt
        version: 5.0.3+dfsg2-1
    gnuplot5-qt:
    -   arch: amd64
        category: universe/math
        name: gnuplot5-qt
        origin: Ubuntu
        source: apt
        version: 5.0.3+dfsg2-1
    gpgv:
    -   arch: amd64
        category: utils
        name: gpgv
        origin: Ubuntu
        source: apt
        version: 1.4.20-1ubuntu3.3
    grep:
    -   arch: amd64
        category: utils
        name: grep
        origin: Ubuntu
        source: apt
        version: 2.25-1~16.04.1
    grub-common:
    -   arch: amd64
        category: admin
        name: grub-common
        origin: Ubuntu
        source: apt
        version: 2.02~beta2-36ubuntu3.23
    grub-gfxpayload-lists:
    -   arch: amd64
        category: admin
        name: grub-gfxpayload-lists
        origin: Ubuntu
        source: apt
        version: '0.7'
    grub-pc:
    -   arch: amd64
        category: admin
        name: grub-pc
        origin: Ubuntu
        source: apt
        version: 2.02~beta2-36ubuntu3.23
    grub-pc-bin:
    -   arch: amd64
        category: admin
        name: grub-pc-bin
        origin: Ubuntu
        source: apt
        version: 2.02~beta2-36ubuntu3.23
    grub2-common:
    -   arch: amd64
        category: admin
        name: grub2-common
        origin: Ubuntu
        source: apt
        version: 2.02~beta2-36ubuntu3.23
    gsettings-desktop-schemas:
    -   arch: all
        category: libs
        name: gsettings-desktop-schemas
        origin: Ubuntu
        source: apt
        version: 3.18.1-1ubuntu1
    gsfonts:
    -   arch: all
        category: text
        name: gsfonts
        origin: Ubuntu
        source: apt
        version: 1:8.11+urwcyr1.0.7~pre44-4.2ubuntu1
    gstreamer1.0-plugins-base:
    -   arch: amd64
        category: libs
        name: gstreamer1.0-plugins-base
        origin: Ubuntu
        source: apt
        version: 1.8.3-1ubuntu0.3
    gstreamer1.5-libav:
    -   arch: amd64
        category: libs
        name: gstreamer1.5-libav
        origin: BigBlueButton
        source: apt
        version: 1.8.1-1kurento1.16.04
    gstreamer1.5-nice:
    -   arch: amd64
        category: net
        name: gstreamer1.5-nice
        origin: BigBlueButton
        source: apt
        version: 0.1.15-1kurento3.16.04
    gstreamer1.5-plugins-bad:
    -   arch: amd64
        category: libs
        name: gstreamer1.5-plugins-bad
        origin: BigBlueButton
        source: apt
        version: 1.8.1-1kurento4.16.04
    gstreamer1.5-plugins-base:
    -   arch: amd64
        category: libs
        name: gstreamer1.5-plugins-base
        origin: BigBlueButton
        source: apt
        version: 1.8.1-1kurento2.16.04
    gstreamer1.5-plugins-good:
    -   arch: amd64
        category: libs
        name: gstreamer1.5-plugins-good
        origin: BigBlueButton
        source: apt
        version: 1.8.1-1kurento3.16.04
    gstreamer1.5-plugins-ugly:
    -   arch: amd64
        category: libs
        name: gstreamer1.5-plugins-ugly
        origin: BigBlueButton
        source: apt
        version: 1.8.1-1kurento1.16.04
    gstreamer1.5-pulseaudio:
    -   arch: amd64
        category: sound
        name: gstreamer1.5-pulseaudio
        origin: BigBlueButton
        source: apt
        version: 1.8.1-1kurento3.16.04
    gstreamer1.5-x:
    -   arch: amd64
        category: libs
        name: gstreamer1.5-x
        origin: BigBlueButton
        source: apt
        version: 1.8.1-1kurento2.16.04
    gzip:
    -   arch: amd64
        category: utils
        name: gzip
        origin: Ubuntu
        source: apt
        version: 1.6-4ubuntu1
    haveged:
    -   arch: amd64
        category: universe/misc
        name: haveged
        origin: Ubuntu
        source: apt
        version: 1.9.1-3
    hicolor-icon-theme:
    -   arch: all
        category: misc
        name: hicolor-icon-theme
        origin: Ubuntu
        source: apt
        version: 0.15-0ubuntu1.1
    hostname:
    -   arch: amd64
        category: admin
        name: hostname
        origin: Ubuntu
        source: apt
        version: 3.16ubuntu2
    htop:
    -   arch: amd64
        category: universe/utils
        name: htop
        origin: Ubuntu
        source: apt
        version: 2.0.1-1ubuntu1
    humanity-icon-theme:
    -   arch: all
        category: gnome
        name: humanity-icon-theme
        origin: Ubuntu
        source: apt
        version: 0.6.10.1
    hunspell-en-us:
    -   arch: all
        category: text
        name: hunspell-en-us
        origin: Ubuntu
        source: apt
        version: 20070829-6ubuntu3
    i965-va-driver:
    -   arch: amd64
        category: universe/oldlibs
        name: i965-va-driver
        origin: Ubuntu
        source: apt
        version: 1.7.0-1
    icu-devtools:
    -   arch: amd64
        category: libdevel
        name: icu-devtools
        origin: Ubuntu
        source: apt
        version: 55.1-7ubuntu0.5
    ifupdown:
    -   arch: amd64
        category: admin
        name: ifupdown
        origin: Ubuntu
        source: apt
        version: 0.8.10ubuntu1.4
    imagemagick:
    -   arch: amd64
        category: graphics
        name: imagemagick
        origin: Ubuntu
        source: apt
        version: 8:6.8.9.9-7ubuntu5.15
    imagemagick-6.q16:
    -   arch: amd64
        category: graphics
        name: imagemagick-6.q16
        origin: Ubuntu
        source: apt
        version: 8:6.8.9.9-7ubuntu5.15
    imagemagick-common:
    -   arch: all
        category: graphics
        name: imagemagick-common
        origin: Ubuntu
        source: apt
        version: 8:6.8.9.9-7ubuntu5.15
    init:
    -   arch: amd64
        category: metapackages
        name: init
        origin: Ubuntu
        source: apt
        version: 1.29ubuntu4
    init-system-helpers:
    -   arch: all
        category: admin
        name: init-system-helpers
        origin: Ubuntu
        source: apt
        version: 1.29ubuntu4
    initramfs-tools:
    -   arch: all
        category: utils
        name: initramfs-tools
        origin: Ubuntu
        source: apt
        version: 0.122ubuntu8.16
    initramfs-tools-bin:
    -   arch: amd64
        category: utils
        name: initramfs-tools-bin
        origin: Ubuntu
        source: apt
        version: 0.122ubuntu8.16
    initramfs-tools-core:
    -   arch: all
        category: utils
        name: initramfs-tools-core
        origin: Ubuntu
        source: apt
        version: 0.122ubuntu8.16
    initscripts:
    -   arch: amd64
        category: admin
        name: initscripts
        origin: Ubuntu
        source: apt
        version: 2.88dsf-59.3ubuntu2
    insserv:
    -   arch: amd64
        category: misc
        name: insserv
        origin: Ubuntu
        source: apt
        version: 1.14.0-5ubuntu3
    intel-microcode:
    -   arch: amd64
        category: admin
        name: intel-microcode
        origin: Ubuntu
        source: apt
        version: 3.20191115.1ubuntu0.16.04.2
    iotop:
    -   arch: amd64
        category: universe/admin
        name: iotop
        origin: Ubuntu
        source: apt
        version: 0.6-1
    iproute2:
    -   arch: amd64
        category: net
        name: iproute2
        origin: Ubuntu
        source: apt
        version: 4.3.0-1ubuntu3.16.04.5
    iptables:
    -   arch: amd64
        category: net
        name: iptables
        origin: Ubuntu
        source: apt
        version: 1.6.0-2ubuntu3
    iputils-ping:
    -   arch: amd64
        category: net
        name: iputils-ping
        origin: Ubuntu
        source: apt
        version: 3:20121221-5ubuntu2
    isc-dhcp-client:
    -   arch: amd64
        category: net
        name: isc-dhcp-client
        origin: Ubuntu
        source: apt
        version: 4.3.3-5ubuntu12.10
    isc-dhcp-common:
    -   arch: amd64
        category: net
        name: isc-dhcp-common
        origin: Ubuntu
        source: apt
        version: 4.3.3-5ubuntu12.10
    iso-codes:
    -   arch: all
        category: libs
        name: iso-codes
        origin: Ubuntu
        source: apt
        version: 3.65-1
    iucode-tool:
    -   arch: amd64
        category: utils
        name: iucode-tool
        origin: Ubuntu
        source: apt
        version: 1.5.1-1ubuntu0.1
    iw:
    -   arch: amd64
        category: net
        name: iw
        origin: Ubuntu
        source: apt
        version: 3.17-1
    java-common:
    -   arch: all
        category: misc
        name: java-common
        origin: Ubuntu
        source: apt
        version: 0.56ubuntu2
    javascript-common:
    -   arch: all
        category: web
        name: javascript-common
        origin: Ubuntu
        source: apt
        version: '11'
    jsvc:
    -   arch: amd64
        category: universe/net
        name: jsvc
        origin: Ubuntu
        source: apt
        version: 1.0.15-6
    kbd:
    -   arch: amd64
        category: utils
        name: kbd
        origin: Ubuntu
        source: apt
        version: 1.15.5-1ubuntu5
    keyboard-configuration:
    -   arch: all
        category: utils
        name: keyboard-configuration
        origin: Ubuntu
        source: apt
        version: 1.108ubuntu15.5
    klibc-utils:
    -   arch: amd64
        category: libs
        name: klibc-utils
        origin: Ubuntu
        source: apt
        version: 2.0.4-8ubuntu1.16.04.4
    kmod:
    -   arch: amd64
        category: admin
        name: kmod
        origin: Ubuntu
        source: apt
        version: 22-1ubuntu5.2
    kms-core:
    -   arch: amd64
        category: libs
        name: kms-core
        origin: BigBlueButton
        source: apt
        version: 6.13.0.xenial~20200506234430.57.5a9fdb5
    kms-elements:
    -   arch: amd64
        category: libs
        name: kms-elements
        origin: BigBlueButton
        source: apt
        version: 6.13.0.xenial~20200506235224.48.cc7e5c4
    kms-filters:
    -   arch: amd64
        category: libs
        name: kms-filters
        origin: BigBlueButton
        source: apt
        version: 6.13.0.xenial.20200506235848.48.72a06d5
    kms-jsonrpc:
    -   arch: amd64
        category: libs
        name: kms-jsonrpc
        origin: BigBlueButton
        source: apt
        version: 6.13.0-0kurento1.16.04
    kmsjsoncpp:
    -   arch: amd64
        category: utils
        name: kmsjsoncpp
        origin: BigBlueButton
        source: apt
        version: 1.6.3-1kurento1.16.04
    kurento-media-server:
    -   arch: amd64
        category: video
        name: kurento-media-server
        origin: BigBlueButton
        source: apt
        version: 6.13.0-0kurento1.16.04
    language-pack-en:
    -   arch: all
        category: translations
        name: language-pack-en
        origin: Ubuntu
        source: apt
        version: 1:16.04+20161009
    language-pack-en-base:
    -   arch: all
        category: translations
        name: language-pack-en-base
        origin: Ubuntu
        source: apt
        version: 1:16.04+20160627
    less:
    -   arch: amd64
        category: text
        name: less
        origin: Ubuntu
        source: apt
        version: 481-2.1ubuntu0.2
    liba52-0.7.4:
    -   arch: amd64
        category: universe/libs
        name: liba52-0.7.4
        origin: Ubuntu
        source: apt
        version: 0.7.4-18
    libaa1:
    -   arch: amd64
        category: libs
        name: libaa1
        origin: Ubuntu
        source: apt
        version: 1.4p5-44build1
    libaacs0:
    -   arch: amd64
        category: universe/video
        name: libaacs0
        origin: Ubuntu
        source: apt
        version: 0.8.1-1
    libabw-0.1-1v5:
    -   arch: amd64
        category: libs
        name: libabw-0.1-1v5
        origin: Ubuntu
        source: apt
        version: 0.1.1-2ubuntu2
    libacl1:
    -   arch: amd64
        category: libs
        name: libacl1
        origin: Ubuntu
        source: apt
        version: 2.2.52-3
    libalgorithm-diff-perl:
    -   arch: all
        category: perl
        name: libalgorithm-diff-perl
        origin: Ubuntu
        source: apt
        version: 1.19.03-1
    libalgorithm-diff-xs-perl:
    -   arch: amd64
        category: perl
        name: libalgorithm-diff-xs-perl
        origin: Ubuntu
        source: apt
        version: 0.04-4build1
    libalgorithm-merge-perl:
    -   arch: all
        category: perl
        name: libalgorithm-merge-perl
        origin: Ubuntu
        source: apt
        version: 0.08-3
    libao-common:
    -   arch: all
        category: libs
        name: libao-common
        origin: Ubuntu
        source: apt
        version: 1.1.0-3ubuntu1
    libao4:
    -   arch: amd64
        category: libs
        name: libao4
        origin: Ubuntu
        source: apt
        version: 1.1.0-3ubuntu1
    libapparmor-perl:
    -   arch: amd64
        category: perl
        name: libapparmor-perl
        origin: Ubuntu
        source: apt
        version: 2.10.95-0ubuntu2.11
    libapparmor1:
    -   arch: amd64
        category: libs
        name: libapparmor1
        origin: Ubuntu
        source: apt
        version: 2.10.95-0ubuntu2.11
    libapt-inst2.0:
    -   arch: amd64
        category: libs
        name: libapt-inst2.0
        origin: Ubuntu
        source: apt
        version: 1.2.32ubuntu0.1
    libapt-pkg-perl:
    -   arch: amd64
        category: perl
        name: libapt-pkg-perl
        origin: Ubuntu
        source: apt
        version: 0.1.29build7
    libapt-pkg5.0:
    -   arch: amd64
        category: libs
        name: libapt-pkg5.0
        origin: Ubuntu
        source: apt
        version: 1.2.32ubuntu0.1
    libasan2:
    -   arch: amd64
        category: libs
        name: libasan2
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libasn1-8-heimdal:
    -   arch: amd64
        category: libs
        name: libasn1-8-heimdal
        origin: Ubuntu
        source: apt
        version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
    libasound2:
    -   arch: amd64
        category: libs
        name: libasound2
        origin: Ubuntu
        source: apt
        version: 1.1.0-0ubuntu1
    libasound2-data:
    -   arch: all
        category: libs
        name: libasound2-data
        origin: Ubuntu
        source: apt
        version: 1.1.0-0ubuntu1
    libasprintf0v5:
    -   arch: amd64
        category: libs
        name: libasprintf0v5
        origin: Ubuntu
        source: apt
        version: 0.19.7-2ubuntu3.1
    libass5:
    -   arch: amd64
        category: universe/libs
        name: libass5
        origin: Ubuntu
        source: apt
        version: 0.13.1-1
    libassuan0:
    -   arch: amd64
        category: libs
        name: libassuan0
        origin: Ubuntu
        source: apt
        version: 2.4.2-2
    libasyncns0:
    -   arch: amd64
        category: libs
        name: libasyncns0
        origin: Ubuntu
        source: apt
        version: 0.8-5build1
    libatk-bridge2.0-0:
    -   arch: amd64
        category: libs
        name: libatk-bridge2.0-0
        origin: Ubuntu
        source: apt
        version: 2.18.1-2ubuntu1
    libatk1.0-0:
    -   arch: amd64
        category: libs
        name: libatk1.0-0
        origin: Ubuntu
        source: apt
        version: 2.18.0-1
    libatk1.0-data:
    -   arch: all
        category: misc
        name: libatk1.0-data
        origin: Ubuntu
        source: apt
        version: 2.18.0-1
    libatm1:
    -   arch: amd64
        category: libs
        name: libatm1
        origin: Ubuntu
        source: apt
        version: 1:2.5.1-1.5
    libatomic1:
    -   arch: amd64
        category: libs
        name: libatomic1
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libatspi2.0-0:
    -   arch: amd64
        category: misc
        name: libatspi2.0-0
        origin: Ubuntu
        source: apt
        version: 2.18.3-4ubuntu1
    libattr1:
    -   arch: amd64
        category: libs
        name: libattr1
        origin: Ubuntu
        source: apt
        version: 1:2.4.47-2
    libaudio2:
    -   arch: amd64
        category: libs
        name: libaudio2
        origin: Ubuntu
        source: apt
        version: 1.9.4-4
    libaudiofile1:
    -   arch: amd64
        category: universe/libs
        name: libaudiofile1
        origin: Ubuntu
        source: apt
        version: 0.3.6-2ubuntu0.16.04.1
    libaudit-common:
    -   arch: all
        category: libs
        name: libaudit-common
        origin: Ubuntu
        source: apt
        version: 1:2.4.5-1ubuntu2.1
    libaudit1:
    -   arch: amd64
        category: libs
        name: libaudit1
        origin: Ubuntu
        source: apt
        version: 1:2.4.5-1ubuntu2.1
    libauthen-sasl-perl:
    -   arch: all
        category: perl
        name: libauthen-sasl-perl
        origin: Ubuntu
        source: apt
        version: 2.1600-1
    libav-tools:
    -   arch: all
        category: universe/oldlibs
        name: libav-tools
        origin: Ubuntu
        source: apt
        version: 7:2.8.15-0ubuntu0.16.04.1
    libavahi-client3:
    -   arch: amd64
        category: libs
        name: libavahi-client3
        origin: Ubuntu
        source: apt
        version: 0.6.32~rc+dfsg-1ubuntu2.3
    libavahi-common-data:
    -   arch: amd64
        category: libs
        name: libavahi-common-data
        origin: Ubuntu
        source: apt
        version: 0.6.32~rc+dfsg-1ubuntu2.3
    libavahi-common3:
    -   arch: amd64
        category: libs
        name: libavahi-common3
        origin: Ubuntu
        source: apt
        version: 0.6.32~rc+dfsg-1ubuntu2.3
    libavc1394-0:
    -   arch: amd64
        category: libs
        name: libavc1394-0
        origin: Ubuntu
        source: apt
        version: 0.5.4-4
    libavcodec-ffmpeg56:
    -   arch: amd64
        category: universe/libs
        name: libavcodec-ffmpeg56
        origin: Ubuntu
        source: apt
        version: 7:2.8.15-0ubuntu0.16.04.1
    libavcodec58:
    -   arch: amd64
        category: libs
        name: libavcodec58
        origin: LP-PPA-bigbluebutton-support
        source: apt
        version: 7:4.2.2-1bbb1~ubuntu16.04
    libavdevice58:
    -   arch: amd64
        category: libs
        name: libavdevice58
        origin: LP-PPA-bigbluebutton-support
        source: apt
        version: 7:4.2.2-1bbb1~ubuntu16.04
    libavfilter7:
    -   arch: amd64
        category: libs
        name: libavfilter7
        origin: LP-PPA-bigbluebutton-support
        source: apt
        version: 7:4.2.2-1bbb1~ubuntu16.04
    libavformat-ffmpeg56:
    -   arch: amd64
        category: universe/libs
        name: libavformat-ffmpeg56
        origin: Ubuntu
        source: apt
        version: 7:2.8.15-0ubuntu0.16.04.1
    libavformat58:
    -   arch: amd64
        category: libs
        name: libavformat58
        origin: LP-PPA-bigbluebutton-support
        source: apt
        version: 7:4.2.2-1bbb1~ubuntu16.04
    libavresample4:
    -   arch: amd64
        category: libs
        name: libavresample4
        origin: LP-PPA-bigbluebutton-support
        source: apt
        version: 7:4.2.2-1bbb1~ubuntu16.04
    libavutil-ffmpeg54:
    -   arch: amd64
        category: universe/libs
        name: libavutil-ffmpeg54
        origin: Ubuntu
        source: apt
        version: 7:2.8.15-0ubuntu0.16.04.1
    libavutil56:
    -   arch: amd64
        category: libs
        name: libavutil56
        origin: LP-PPA-bigbluebutton-support
        source: apt
        version: 7:4.2.2-1bbb1~ubuntu16.04
    libbdplus0:
    -   arch: amd64
        category: universe/libs
        name: libbdplus0
        origin: Ubuntu
        source: apt
        version: 0.1.2-1
    libbind9-140:
    -   arch: amd64
        category: libs
        name: libbind9-140
        origin: Ubuntu
        source: apt
        version: 1:9.10.3.dfsg.P4-8ubuntu1.16
    libblkid1:
    -   arch: amd64
        category: libs
        name: libblkid1
        origin: Ubuntu
        source: apt
        version: 2.27.1-6ubuntu3.10
    libbluray1:
    -   arch: amd64
        category: universe/libs
        name: libbluray1
        origin: Ubuntu
        source: apt
        version: 1:0.9.2-2
    libboost-date-time1.58.0:
    -   arch: amd64
        category: libs
        name: libboost-date-time1.58.0
        origin: Ubuntu
        source: apt
        version: 1.58.0+dfsg-5ubuntu3.1
    libboost-filesystem1.58.0:
    -   arch: amd64
        category: libs
        name: libboost-filesystem1.58.0
        origin: Ubuntu
        source: apt
        version: 1.58.0+dfsg-5ubuntu3.1
    libboost-iostreams1.58.0:
    -   arch: amd64
        category: libs
        name: libboost-iostreams1.58.0
        origin: Ubuntu
        source: apt
        version: 1.58.0+dfsg-5ubuntu3.1
    libboost-log1.58.0:
    -   arch: amd64
        category: universe/libs
        name: libboost-log1.58.0
        origin: Ubuntu
        source: apt
        version: 1.58.0+dfsg-5ubuntu3.1
    libboost-program-options1.58.0:
    -   arch: amd64
        category: libs
        name: libboost-program-options1.58.0
        origin: Ubuntu
        source: apt
        version: 1.58.0+dfsg-5ubuntu3.1
    libboost-regex1.58.0:
    -   arch: amd64
        category: libs
        name: libboost-regex1.58.0
        origin: Ubuntu
        source: apt
        version: 1.58.0+dfsg-5ubuntu3.1
    libboost-system1.58.0:
    -   arch: amd64
        category: libs
        name: libboost-system1.58.0
        origin: Ubuntu
        source: apt
        version: 1.58.0+dfsg-5ubuntu3.1
    libboost-thread1.58.0:
    -   arch: amd64
        category: libs
        name: libboost-thread1.58.0
        origin: Ubuntu
        source: apt
        version: 1.58.0+dfsg-5ubuntu3.1
    libbs2b0:
    -   arch: amd64
        category: universe/libs
        name: libbs2b0
        origin: Ubuntu
        source: apt
        version: 3.1.0+dfsg-2.2
    libbsd0:
    -   arch: amd64
        category: libs
        name: libbsd0
        origin: Ubuntu
        source: apt
        version: 0.8.2-1ubuntu0.1
    libbz2-1.0:
    -   arch: amd64
        category: libs
        name: libbz2-1.0
        origin: Ubuntu
        source: apt
        version: 1.0.6-8ubuntu0.2
    libc-bin:
    -   arch: amd64
        category: libs
        name: libc-bin
        origin: Ubuntu
        source: apt
        version: 2.23-0ubuntu11
    libc-dev-bin:
    -   arch: amd64
        category: libdevel
        name: libc-dev-bin
        origin: Ubuntu
        source: apt
        version: 2.23-0ubuntu11
    libc6:
    -   arch: amd64
        category: libs
        name: libc6
        origin: Ubuntu
        source: apt
        version: 2.23-0ubuntu11
    libc6-dev:
    -   arch: amd64
        category: libdevel
        name: libc6-dev
        origin: Ubuntu
        source: apt
        version: 2.23-0ubuntu11
    libcaca0:
    -   arch: amd64
        category: libs
        name: libcaca0
        origin: Ubuntu
        source: apt
        version: 0.99.beta19-2ubuntu0.16.04.1
    libcache-cache-perl:
    -   arch: all
        category: universe/perl
        name: libcache-cache-perl
        origin: Ubuntu
        source: apt
        version: 1.08-2
    libcache-memcached-perl:
    -   arch: all
        category: universe/perl
        name: libcache-memcached-perl
        origin: Ubuntu
        source: apt
        version: 1.30-1
    libcairo-gobject2:
    -   arch: amd64
        category: libs
        name: libcairo-gobject2
        origin: Ubuntu
        source: apt
        version: 1.14.6-1
    libcairo2:
    -   arch: amd64
        category: libs
        name: libcairo2
        origin: Ubuntu
        source: apt
        version: 1.14.6-1
    libcap-ng0:
    -   arch: amd64
        category: libs
        name: libcap-ng0
        origin: Ubuntu
        source: apt
        version: 0.7.7-1
    libcap2:
    -   arch: amd64
        category: libs
        name: libcap2
        origin: Ubuntu
        source: apt
        version: 1:2.24-12
    libcap2-bin:
    -   arch: amd64
        category: utils
        name: libcap2-bin
        origin: Ubuntu
        source: apt
        version: 1:2.24-12
    libcapnp-0.5.3:
    -   arch: amd64
        category: libs
        name: libcapnp-0.5.3
        origin: Ubuntu
        source: apt
        version: 0.5.3-2ubuntu1.1
    libcc1-0:
    -   arch: amd64
        category: libs
        name: libcc1-0
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libcdio-cdda1:
    -   arch: amd64
        category: libs
        name: libcdio-cdda1
        origin: Ubuntu
        source: apt
        version: 0.83-4.2ubuntu1
    libcdio-paranoia1:
    -   arch: amd64
        category: libs
        name: libcdio-paranoia1
        origin: Ubuntu
        source: apt
        version: 0.83-4.2ubuntu1
    libcdio13:
    -   arch: amd64
        category: libs
        name: libcdio13
        origin: Ubuntu
        source: apt
        version: 0.83-4.2ubuntu1
    libcdparanoia0:
    -   arch: amd64
        category: sound
        name: libcdparanoia0
        origin: Ubuntu
        source: apt
        version: 3.10.2+debian-11
    libcdr-0.1-1:
    -   arch: amd64
        category: libs
        name: libcdr-0.1-1
        origin: Ubuntu
        source: apt
        version: 0.1.2-2ubuntu2
    libcgi-fast-perl:
    -   arch: all
        category: perl
        name: libcgi-fast-perl
        origin: Ubuntu
        source: apt
        version: 1:2.10-1
    libcgi-pm-perl:
    -   arch: all
        category: perl
        name: libcgi-pm-perl
        origin: Ubuntu
        source: apt
        version: 4.26-1
    libchromaprint0:
    -   arch: amd64
        category: universe/libs
        name: libchromaprint0
        origin: Ubuntu
        source: apt
        version: 1.3-1
    libcilkrts5:
    -   arch: amd64
        category: libs
        name: libcilkrts5
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libclass-accessor-perl:
    -   arch: all
        category: perl
        name: libclass-accessor-perl
        origin: Ubuntu
        source: apt
        version: 0.34-1
    libclucene-contribs1v5:
    -   arch: amd64
        category: libs
        name: libclucene-contribs1v5
        origin: Ubuntu
        source: apt
        version: 2.3.3.4-4.1
    libclucene-core1v5:
    -   arch: amd64
        category: libs
        name: libclucene-core1v5
        origin: Ubuntu
        source: apt
        version: 2.3.3.4-4.1
    libcmis-0.5-5v5:
    -   arch: amd64
        category: libs
        name: libcmis-0.5-5v5
        origin: Ubuntu
        source: apt
        version: 0.5.1-2ubuntu2
    libcolamd2.9.1:
    -   arch: amd64
        category: libs
        name: libcolamd2.9.1
        origin: Ubuntu
        source: apt
        version: 1:4.4.6-1
    libcolord2:
    -   arch: amd64
        category: libs
        name: libcolord2
        origin: Ubuntu
        source: apt
        version: 1.2.12-1ubuntu1
    libcolorhug2:
    -   arch: amd64
        category: libs
        name: libcolorhug2
        origin: Ubuntu
        source: apt
        version: 1.2.12-1ubuntu1
    libcomerr2:
    -   arch: amd64
        category: libs
        name: libcomerr2
        origin: Ubuntu
        source: apt
        version: 1.42.13-1ubuntu1.2
    libcommons-daemon-java:
    -   arch: all
        category: universe/libs
        name: libcommons-daemon-java
        origin: Ubuntu
        source: apt
        version: 1.0.15-6
    libcroco3:
    -   arch: amd64
        category: libs
        name: libcroco3
        origin: Ubuntu
        source: apt
        version: 0.6.11-1
    libcryptsetup4:
    -   arch: amd64
        category: libs
        name: libcryptsetup4
        origin: Ubuntu
        source: apt
        version: 2:1.6.6-5ubuntu2.1
    libcrystalhd3:
    -   arch: amd64
        category: universe/libs
        name: libcrystalhd3
        origin: Ubuntu
        source: apt
        version: 1:0.0~git20110715.fdd2f19-11build1
    libcups2:
    -   arch: amd64
        category: libs
        name: libcups2
        origin: Ubuntu
        source: apt
        version: 2.1.3-4ubuntu0.11
    libcupsfilters1:
    -   arch: amd64
        category: libs
        name: libcupsfilters1
        origin: Ubuntu
        source: apt
        version: 1.8.3-2ubuntu3.5
    libcupsimage2:
    -   arch: amd64
        category: libs
        name: libcupsimage2
        origin: Ubuntu
        source: apt
        version: 2.1.3-4ubuntu0.11
    libcurl3:
    -   arch: amd64
        category: libs
        name: libcurl3
        origin: Ubuntu
        source: apt
        version: 7.47.0-1ubuntu2.14
    libcurl3-gnutls:
    -   arch: amd64
        category: libs
        name: libcurl3-gnutls
        origin: Ubuntu
        source: apt
        version: 7.47.0-1ubuntu2.14
    libcurl4-openssl-dev:
    -   arch: amd64
        category: libdevel
        name: libcurl4-openssl-dev
        origin: Ubuntu
        source: apt
        version: 7.47.0-1ubuntu2.14
    libcwidget3v5:
    -   arch: amd64
        category: libs
        name: libcwidget3v5
        origin: Ubuntu
        source: apt
        version: 0.5.17-4ubuntu2
    libdatrie1:
    -   arch: amd64
        category: libs
        name: libdatrie1
        origin: Ubuntu
        source: apt
        version: 0.2.10-2
    libdb5.3:
    -   arch: amd64
        category: libs
        name: libdb5.3
        origin: Ubuntu
        source: apt
        version: 5.3.28-11ubuntu0.2
    libdbus-1-3:
    -   arch: amd64
        category: libs
        name: libdbus-1-3
        origin: Ubuntu
        source: apt
        version: 1.10.6-1ubuntu3.5
    libdbus-glib-1-2:
    -   arch: amd64
        category: libs
        name: libdbus-glib-1-2
        origin: Ubuntu
        source: apt
        version: 0.106-1
    libdc1394-22:
    -   arch: amd64
        category: universe/libs
        name: libdc1394-22
        origin: Ubuntu
        source: apt
        version: 2.2.4-1
    libdca0:
    -   arch: amd64
        category: universe/libs
        name: libdca0
        origin: Ubuntu
        source: apt
        version: 0.0.5-7build1
    libdconf1:
    -   arch: amd64
        category: libs
        name: libdconf1
        origin: Ubuntu
        source: apt
        version: 0.24.0-2
    libde265-0:
    -   arch: amd64
        category: universe/libs
        name: libde265-0
        origin: Ubuntu
        source: apt
        version: 1.0.2-2
    libdebconfclient0:
    -   arch: amd64
        category: libs
        name: libdebconfclient0
        origin: Ubuntu
        source: apt
        version: 0.198ubuntu1
    libdevmapper-event1.02.1:
    -   arch: amd64
        category: libs
        name: libdevmapper-event1.02.1
        origin: Ubuntu
        source: apt
        version: 2:1.02.110-1ubuntu10
    libdevmapper1.02.1:
    -   arch: amd64
        category: libs
        name: libdevmapper1.02.1
        origin: Ubuntu
        source: apt
        version: 2:1.02.110-1ubuntu10
    libdirectfb-1.2-9:
    -   arch: amd64
        category: universe/libs
        name: libdirectfb-1.2-9
        origin: Ubuntu
        source: apt
        version: 1.2.10.0-5.1
    libdjvulibre-text:
    -   arch: all
        category: libs
        name: libdjvulibre-text
        origin: Ubuntu
        source: apt
        version: 3.5.27.1-5ubuntu0.1
    libdjvulibre21:
    -   arch: amd64
        category: libs
        name: libdjvulibre21
        origin: Ubuntu
        source: apt
        version: 3.5.27.1-5ubuntu0.1
    libdns-export162:
    -   arch: amd64
        category: libs
        name: libdns-export162
        origin: Ubuntu
        source: apt
        version: 1:9.10.3.dfsg.P4-8ubuntu1.16
    libdns162:
    -   arch: amd64
        category: libs
        name: libdns162
        origin: Ubuntu
        source: apt
        version: 1:9.10.3.dfsg.P4-8ubuntu1.16
    libdpkg-perl:
    -   arch: all
        category: perl
        name: libdpkg-perl
        origin: Ubuntu
        source: apt
        version: 1.18.4ubuntu1.6
    libdrm-amdgpu1:
    -   arch: amd64
        category: libs
        name: libdrm-amdgpu1
        origin: Ubuntu
        source: apt
        version: 2.4.91-2~16.04.1
    libdrm-common:
    -   arch: all
        category: libs
        name: libdrm-common
        origin: Ubuntu
        source: apt
        version: 2.4.91-2~16.04.1
    libdrm-intel1:
    -   arch: amd64
        category: libs
        name: libdrm-intel1
        origin: Ubuntu
        source: apt
        version: 2.4.91-2~16.04.1
    libdrm-nouveau2:
    -   arch: amd64
        category: libs
        name: libdrm-nouveau2
        origin: Ubuntu
        source: apt
        version: 2.4.91-2~16.04.1
    libdrm-radeon1:
    -   arch: amd64
        category: libs
        name: libdrm-radeon1
        origin: Ubuntu
        source: apt
        version: 2.4.91-2~16.04.1
    libdrm2:
    -   arch: amd64
        category: libs
        name: libdrm2
        origin: Ubuntu
        source: apt
        version: 2.4.91-2~16.04.1
    libdv4:
    -   arch: amd64
        category: libs
        name: libdv4
        origin: Ubuntu
        source: apt
        version: 1.0.0-7
    libdvdnav4:
    -   arch: amd64
        category: universe/libs
        name: libdvdnav4
        origin: Ubuntu
        source: apt
        version: 5.0.3-1
    libdvdread4:
    -   arch: amd64
        category: universe/libs
        name: libdvdread4
        origin: Ubuntu
        source: apt
        version: 5.0.3-1
    libe-book-0.1-1:
    -   arch: amd64
        category: libs
        name: libe-book-0.1-1
        origin: Ubuntu
        source: apt
        version: 0.1.2-2ubuntu1
    libedit2:
    -   arch: amd64
        category: libs
        name: libedit2
        origin: Ubuntu
        source: apt
        version: 3.1-20150325-1ubuntu2
    libegl1-mesa:
    -   arch: amd64
        category: libs
        name: libegl1-mesa
        origin: Ubuntu
        source: apt
        version: 18.0.5-0ubuntu0~16.04.1
    libelf1:
    -   arch: amd64
        category: libs
        name: libelf1
        origin: Ubuntu
        source: apt
        version: 0.165-3ubuntu1.2
    libenca0:
    -   arch: amd64
        category: universe/libs
        name: libenca0
        origin: Ubuntu
        source: apt
        version: 1.18-1
    libencode-locale-perl:
    -   arch: all
        category: perl
        name: libencode-locale-perl
        origin: Ubuntu
        source: apt
        version: 1.05-1
    libeot0:
    -   arch: amd64
        category: libs
        name: libeot0
        origin: Ubuntu
        source: apt
        version: 0.01-3ubuntu1
    libepoxy0:
    -   arch: amd64
        category: libs
        name: libepoxy0
        origin: Ubuntu
        source: apt
        version: 1.3.1-1ubuntu0.16.04.2
    liberror-perl:
    -   arch: all
        category: perl
        name: liberror-perl
        origin: Ubuntu
        source: apt
        version: 0.17-1.2
    libesd0:
    -   arch: amd64
        category: universe/libs
        name: libesd0
        origin: Ubuntu
        source: apt
        version: 0.2.41-11
    libestr0:
    -   arch: amd64
        category: libs
        name: libestr0
        origin: Ubuntu
        source: apt
        version: 0.1.10-1
    libetonyek-0.1-1:
    -   arch: amd64
        category: libs
        name: libetonyek-0.1-1
        origin: Ubuntu
        source: apt
        version: 0.1.6-1ubuntu1
    libevdev2:
    -   arch: amd64
        category: libs
        name: libevdev2
        origin: Ubuntu
        source: apt
        version: 1.4.6+dfsg-1
    libevent-2.0-5:
    -   arch: amd64
        category: libs
        name: libevent-2.0-5
        origin: Ubuntu
        source: apt
        version: 2.0.21-stable-2ubuntu0.16.04.1
    libexif12:
    -   arch: amd64
        category: libs
        name: libexif12
        origin: Ubuntu
        source: apt
        version: 0.6.21-2ubuntu0.2
    libexpat1:
    -   arch: amd64
        category: libs
        name: libexpat1
        origin: Ubuntu
        source: apt
        version: 2.1.0-7ubuntu0.16.04.5
    libexporter-tiny-perl:
    -   arch: all
        category: perl
        name: libexporter-tiny-perl
        origin: Ubuntu
        source: apt
        version: 0.042-1
    libexttextcat-2.0-0:
    -   arch: amd64
        category: libs
        name: libexttextcat-2.0-0
        origin: Ubuntu
        source: apt
        version: 3.4.4-1ubuntu3
    libexttextcat-data:
    -   arch: all
        category: text
        name: libexttextcat-data
        origin: Ubuntu
        source: apt
        version: 3.4.4-1ubuntu3
    libfaad2:
    -   arch: amd64
        category: universe/libs
        name: libfaad2
        origin: Ubuntu
        source: apt
        version: 2.8.0~cvs20150510-1ubuntu0.1
    libfakeroot:
    -   arch: amd64
        category: utils
        name: libfakeroot
        origin: Ubuntu
        source: apt
        version: 1.20.2-1ubuntu1
    libfcgi-perl:
    -   arch: amd64
        category: perl
        name: libfcgi-perl
        origin: Ubuntu
        source: apt
        version: 0.77-1build1
    libfdisk1:
    -   arch: amd64
        category: libs
        name: libfdisk1
        origin: Ubuntu
        source: apt
        version: 2.27.1-6ubuntu3.10
    libffi6:
    -   arch: amd64
        category: libs
        name: libffi6
        origin: Ubuntu
        source: apt
        version: 3.2.1-4
    libfftw3-double3:
    -   arch: amd64
        category: libs
        name: libfftw3-double3
        origin: Ubuntu
        source: apt
        version: 3.3.4-2ubuntu1
    libfile-fcntllock-perl:
    -   arch: amd64
        category: perl
        name: libfile-fcntllock-perl
        origin: Ubuntu
        source: apt
        version: 0.22-3
    libfile-listing-perl:
    -   arch: all
        category: perl
        name: libfile-listing-perl
        origin: Ubuntu
        source: apt
        version: 6.04-1
    libflac8:
    -   arch: amd64
        category: libs
        name: libflac8
        origin: Ubuntu
        source: apt
        version: 1.3.1-4
    libflite1:
    -   arch: amd64
        category: universe/libs
        name: libflite1
        origin: Ubuntu
        source: apt
        version: 2.0.0-release-1
    libfluidsynth1:
    -   arch: amd64
        category: universe/libs
        name: libfluidsynth1
        origin: Ubuntu
        source: apt
        version: 1.1.6-3
    libfont-afm-perl:
    -   arch: all
        category: perl
        name: libfont-afm-perl
        origin: Ubuntu
        source: apt
        version: 1.20-1
    libfontconfig1:
    -   arch: amd64
        category: libs
        name: libfontconfig1
        origin: Ubuntu
        source: apt
        version: 2.11.94-0ubuntu1.1
    libfontenc1:
    -   arch: amd64
        category: x11
        name: libfontenc1
        origin: Ubuntu
        source: apt
        version: 1:1.1.3-1
    libfreehand-0.1-1:
    -   arch: amd64
        category: libs
        name: libfreehand-0.1-1
        origin: Ubuntu
        source: apt
        version: 0.1.1-1ubuntu1
    libfreetype6:
    -   arch: amd64
        category: libs
        name: libfreetype6
        origin: Ubuntu
        source: apt
        version: 2.6.1-0.1ubuntu2.4
    libfribidi0:
    -   arch: amd64
        category: libs
        name: libfribidi0
        origin: Ubuntu
        source: apt
        version: 0.19.7-1
    libfuse2:
    -   arch: amd64
        category: libs
        name: libfuse2
        origin: Ubuntu
        source: apt
        version: 2.9.4-1ubuntu3.1
    libgbm1:
    -   arch: amd64
        category: libs
        name: libgbm1
        origin: Ubuntu
        source: apt
        version: 18.0.5-0ubuntu0~16.04.1
    libgcc-5-dev:
    -   arch: amd64
        category: libdevel
        name: libgcc-5-dev
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libgcc1:
    -   arch: amd64
        category: libs
        name: libgcc1
        origin: Ubuntu
        source: apt
        version: 1:6.0.1-0ubuntu1
    libgcrypt20:
    -   arch: amd64
        category: libs
        name: libgcrypt20
        origin: Ubuntu
        source: apt
        version: 1.6.5-2ubuntu0.6
    libgd3:
    -   arch: amd64
        category: libs
        name: libgd3
        origin: Ubuntu
        source: apt
        version: 2.1.1-4ubuntu0.16.04.12
    libgdbm3:
    -   arch: amd64
        category: libs
        name: libgdbm3
        origin: Ubuntu
        source: apt
        version: 1.8.3-13.1
    libgdk-pixbuf2.0-0:
    -   arch: amd64
        category: libs
        name: libgdk-pixbuf2.0-0
        origin: Ubuntu
        source: apt
        version: 2.32.2-1ubuntu1.6
    libgdk-pixbuf2.0-common:
    -   arch: all
        category: libs
        name: libgdk-pixbuf2.0-common
        origin: Ubuntu
        source: apt
        version: 2.32.2-1ubuntu1.6
    libgeoip1:
    -   arch: amd64
        category: libs
        name: libgeoip1
        origin: Ubuntu
        source: apt
        version: 1.6.9-1
    libgif7:
    -   arch: amd64
        category: libs
        name: libgif7
        origin: Ubuntu
        source: apt
        version: 5.1.4-0.3~16.04.1
    libgirepository-1.0-1:
    -   arch: amd64
        category: libs
        name: libgirepository-1.0-1
        origin: Ubuntu
        source: apt
        version: 1.46.0-3ubuntu1
    libgl1-mesa-dri:
    -   arch: amd64
        category: libs
        name: libgl1-mesa-dri
        origin: Ubuntu
        source: apt
        version: 18.0.5-0ubuntu0~16.04.1
    libgl1-mesa-glx:
    -   arch: amd64
        category: libs
        name: libgl1-mesa-glx
        origin: Ubuntu
        source: apt
        version: 18.0.5-0ubuntu0~16.04.1
    libglapi-mesa:
    -   arch: amd64
        category: libs
        name: libglapi-mesa
        origin: Ubuntu
        source: apt
        version: 18.0.5-0ubuntu0~16.04.1
    libglew1.13:
    -   arch: amd64
        category: libs
        name: libglew1.13
        origin: Ubuntu
        source: apt
        version: 1.13.0-2
    libglib2.0-0:
    -   arch: amd64
        category: libs
        name: libglib2.0-0
        origin: Ubuntu
        source: apt
        version: 2.48.2-0ubuntu4.6
    libglib2.0-data:
    -   arch: all
        category: misc
        name: libglib2.0-data
        origin: Ubuntu
        source: apt
        version: 2.48.2-0ubuntu4.6
    libglibmm-2.4-1v5:
    -   arch: amd64
        category: libs
        name: libglibmm-2.4-1v5
        origin: Ubuntu
        source: apt
        version: 2.46.3-1
    libglu1-mesa:
    -   arch: amd64
        category: libs
        name: libglu1-mesa
        origin: Ubuntu
        source: apt
        version: 9.0.0-2.1
    libgme0:
    -   arch: amd64
        category: universe/libs
        name: libgme0
        origin: Ubuntu
        source: apt
        version: 0.6.0-3ubuntu0.16.04.1
    libgmp-dev:
    -   arch: amd64
        category: libdevel
        name: libgmp-dev
        origin: Ubuntu
        source: apt
        version: 2:6.1.0+dfsg-2
    libgmp10:
    -   arch: amd64
        category: libs
        name: libgmp10
        origin: Ubuntu
        source: apt
        version: 2:6.1.0+dfsg-2
    libgmpxx4ldbl:
    -   arch: amd64
        category: libs
        name: libgmpxx4ldbl
        origin: Ubuntu
        source: apt
        version: 2:6.1.0+dfsg-2
    libgnutls-openssl27:
    -   arch: amd64
        category: libs
        name: libgnutls-openssl27
        origin: Ubuntu
        source: apt
        version: 3.4.10-4ubuntu1.7
    libgnutls30:
    -   arch: amd64
        category: libs
        name: libgnutls30
        origin: Ubuntu
        source: apt
        version: 3.4.10-4ubuntu1.7
    libgomp1:
    -   arch: amd64
        category: libs
        name: libgomp1
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libgpg-error0:
    -   arch: amd64
        category: libs
        name: libgpg-error0
        origin: Ubuntu
        source: apt
        version: 1.21-2ubuntu1
    libgpgme11:
    -   arch: amd64
        category: libs
        name: libgpgme11
        origin: Ubuntu
        source: apt
        version: 1.6.0-1
    libgphoto2-6:
    -   arch: amd64
        category: libs
        name: libgphoto2-6
        origin: Ubuntu
        source: apt
        version: 2.5.9-3
    libgphoto2-l10n:
    -   arch: all
        category: localization
        name: libgphoto2-l10n
        origin: Ubuntu
        source: apt
        version: 2.5.9-3
    libgphoto2-port12:
    -   arch: amd64
        category: libs
        name: libgphoto2-port12
        origin: Ubuntu
        source: apt
        version: 2.5.9-3
    libgpm2:
    -   arch: amd64
        category: libs
        name: libgpm2
        origin: Ubuntu
        source: apt
        version: 1.20.4-6.1
    libgraphite2-3:
    -   arch: amd64
        category: libs
        name: libgraphite2-3
        origin: Ubuntu
        source: apt
        version: 1.3.10-0ubuntu0.16.04.1
    libgs9:
    -   arch: amd64
        category: libs
        name: libgs9
        origin: Ubuntu
        source: apt
        version: 9.26~dfsg+0-0ubuntu0.16.04.12
    libgs9-common:
    -   arch: all
        category: libs
        name: libgs9-common
        origin: Ubuntu
        source: apt
        version: 9.26~dfsg+0-0ubuntu0.16.04.12
    libgsm1:
    -   arch: amd64
        category: universe/libs
        name: libgsm1
        origin: Ubuntu
        source: apt
        version: 1.0.13-4
    libgssapi-krb5-2:
    -   arch: amd64
        category: libs
        name: libgssapi-krb5-2
        origin: Ubuntu
        source: apt
        version: 1.13.2+dfsg-5ubuntu2.1
    libgssapi3-heimdal:
    -   arch: amd64
        category: libs
        name: libgssapi3-heimdal
        origin: Ubuntu
        source: apt
        version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
    libgssdp-1.0-3:
    -   arch: amd64
        category: universe/libs
        name: libgssdp-1.0-3
        origin: Ubuntu
        source: apt
        version: 0.14.14-1ubuntu1
    libgstreamer-plugins-bad1.5-0:
    -   arch: amd64
        category: libs
        name: libgstreamer-plugins-bad1.5-0
        origin: BigBlueButton
        source: apt
        version: 1.8.1-1kurento4.16.04
    libgstreamer-plugins-base1.0-0:
    -   arch: amd64
        category: libs
        name: libgstreamer-plugins-base1.0-0
        origin: Ubuntu
        source: apt
        version: 1.8.3-1ubuntu0.3
    libgstreamer-plugins-base1.5-0:
    -   arch: amd64
        category: libs
        name: libgstreamer-plugins-base1.5-0
        origin: BigBlueButton
        source: apt
        version: 1.8.1-1kurento2.16.04
    libgstreamer1.0-0:
    -   arch: amd64
        category: libs
        name: libgstreamer1.0-0
        origin: Ubuntu
        source: apt
        version: 1.8.3-1~ubuntu0.1
    libgstreamer1.5-0:
    -   arch: amd64
        category: libs
        name: libgstreamer1.5-0
        origin: BigBlueButton
        source: apt
        version: 1.8.1-1kurento2.16.04
    libgtk-3-0:
    -   arch: amd64
        category: libs
        name: libgtk-3-0
        origin: Ubuntu
        source: apt
        version: 3.18.9-1ubuntu3.3
    libgtk-3-bin:
    -   arch: amd64
        category: misc
        name: libgtk-3-bin
        origin: Ubuntu
        source: apt
        version: 3.18.9-1ubuntu3.3
    libgtk-3-common:
    -   arch: all
        category: misc
        name: libgtk-3-common
        origin: Ubuntu
        source: apt
        version: 3.18.9-1ubuntu3.3
    libgtk2.0-0:
    -   arch: amd64
        category: libs
        name: libgtk2.0-0
        origin: Ubuntu
        source: apt
        version: 2.24.30-1ubuntu1.16.04.2
    libgtk2.0-bin:
    -   arch: amd64
        category: misc
        name: libgtk2.0-bin
        origin: Ubuntu
        source: apt
        version: 2.24.30-1ubuntu1.16.04.2
    libgtk2.0-common:
    -   arch: all
        category: misc
        name: libgtk2.0-common
        origin: Ubuntu
        source: apt
        version: 2.24.30-1ubuntu1.16.04.2
    libgtkglext1:
    -   arch: amd64
        category: universe/libs
        name: libgtkglext1
        origin: Ubuntu
        source: apt
        version: 1.2.0-3.2fakesync1ubuntu1
    libgudev-1.0-0:
    -   arch: amd64
        category: libs
        name: libgudev-1.0-0
        origin: Ubuntu
        source: apt
        version: 1:230-2
    libgupnp-1.0-4:
    -   arch: amd64
        category: universe/libs
        name: libgupnp-1.0-4
        origin: Ubuntu
        source: apt
        version: 0.20.16-1
    libgupnp-igd-1.0-4:
    -   arch: amd64
        category: universe/libs
        name: libgupnp-igd-1.0-4
        origin: Ubuntu
        source: apt
        version: 0.2.4-1
    libgusb2:
    -   arch: amd64
        category: libs
        name: libgusb2
        origin: Ubuntu
        source: apt
        version: 0.2.9-0ubuntu1
    libharfbuzz-icu0:
    -   arch: amd64
        category: libs
        name: libharfbuzz-icu0
        origin: Ubuntu
        source: apt
        version: 1.0.1-1ubuntu0.1
    libharfbuzz0b:
    -   arch: amd64
        category: libs
        name: libharfbuzz0b
        origin: Ubuntu
        source: apt
        version: 1.0.1-1ubuntu0.1
    libhavege1:
    -   arch: amd64
        category: universe/libs
        name: libhavege1
        origin: Ubuntu
        source: apt
        version: 1.9.1-3
    libhcrypto4-heimdal:
    -   arch: amd64
        category: libs
        name: libhcrypto4-heimdal
        origin: Ubuntu
        source: apt
        version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
    libheimbase1-heimdal:
    -   arch: amd64
        category: libs
        name: libheimbase1-heimdal
        origin: Ubuntu
        source: apt
        version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
    libheimntlm0-heimdal:
    -   arch: amd64
        category: libs
        name: libheimntlm0-heimdal
        origin: Ubuntu
        source: apt
        version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
    libhogweed4:
    -   arch: amd64
        category: libs
        name: libhogweed4
        origin: Ubuntu
        source: apt
        version: 3.2-1ubuntu0.16.04.1
    libhsqldb1.8.0-java:
    -   arch: all
        category: java
        name: libhsqldb1.8.0-java
        origin: Ubuntu
        source: apt
        version: 1.8.0.10+dfsg-6
    libhtml-form-perl:
    -   arch: all
        category: perl
        name: libhtml-form-perl
        origin: Ubuntu
        source: apt
        version: 6.03-1
    libhtml-format-perl:
    -   arch: all
        category: perl
        name: libhtml-format-perl
        origin: Ubuntu
        source: apt
        version: 2.11-2
    libhtml-parser-perl:
    -   arch: amd64
        category: perl
        name: libhtml-parser-perl
        origin: Ubuntu
        source: apt
        version: 3.72-1
    libhtml-tagset-perl:
    -   arch: all
        category: perl
        name: libhtml-tagset-perl
        origin: Ubuntu
        source: apt
        version: 3.20-2
    libhtml-tree-perl:
    -   arch: all
        category: perl
        name: libhtml-tree-perl
        origin: Ubuntu
        source: apt
        version: 5.03-2
    libhttp-cookies-perl:
    -   arch: all
        category: perl
        name: libhttp-cookies-perl
        origin: Ubuntu
        source: apt
        version: 6.01-1
    libhttp-daemon-perl:
    -   arch: all
        category: perl
        name: libhttp-daemon-perl
        origin: Ubuntu
        source: apt
        version: 6.01-1
    libhttp-date-perl:
    -   arch: all
        category: perl
        name: libhttp-date-perl
        origin: Ubuntu
        source: apt
        version: 6.02-1
    libhttp-message-perl:
    -   arch: all
        category: perl
        name: libhttp-message-perl
        origin: Ubuntu
        source: apt
        version: 6.11-1
    libhttp-negotiate-perl:
    -   arch: all
        category: perl
        name: libhttp-negotiate-perl
        origin: Ubuntu
        source: apt
        version: 6.00-2
    libhunspell-1.3-0:
    -   arch: amd64
        category: libs
        name: libhunspell-1.3-0
        origin: Ubuntu
        source: apt
        version: 1.3.3-4ubuntu1
    libhx509-5-heimdal:
    -   arch: amd64
        category: libs
        name: libhx509-5-heimdal
        origin: Ubuntu
        source: apt
        version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
    libhyphen0:
    -   arch: amd64
        category: libs
        name: libhyphen0
        origin: Ubuntu
        source: apt
        version: 2.8.8-2ubuntu1
    libice6:
    -   arch: amd64
        category: libs
        name: libice6
        origin: Ubuntu
        source: apt
        version: 2:1.0.9-1
    libicu-dev:
    -   arch: amd64
        category: libdevel
        name: libicu-dev
        origin: Ubuntu
        source: apt
        version: 55.1-7ubuntu0.5
    libicu55:
    -   arch: amd64
        category: libs
        name: libicu55
        origin: Ubuntu
        source: apt
        version: 55.1-7ubuntu0.5
    libidn11:
    -   arch: amd64
        category: libs
        name: libidn11
        origin: Ubuntu
        source: apt
        version: 1.32-3ubuntu1.2
    libiec61883-0:
    -   arch: amd64
        category: libs
        name: libiec61883-0
        origin: Ubuntu
        source: apt
        version: 1.2.0-0.2
    libieee1284-3:
    -   arch: amd64
        category: libs
        name: libieee1284-3
        origin: Ubuntu
        source: apt
        version: 0.2.11-12
    libijs-0.35:
    -   arch: amd64
        category: libs
        name: libijs-0.35
        origin: Ubuntu
        source: apt
        version: 0.35-12
    libilmbase12:
    -   arch: amd64
        category: libs
        name: libilmbase12
        origin: Ubuntu
        source: apt
        version: 2.2.0-11ubuntu2
    libinput-bin:
    -   arch: amd64
        category: libs
        name: libinput-bin
        origin: Ubuntu
        source: apt
        version: 1.6.3-1ubuntu1~16.04.1
    libinput10:
    -   arch: amd64
        category: libs
        name: libinput10
        origin: Ubuntu
        source: apt
        version: 1.6.3-1ubuntu1~16.04.1
    libio-html-perl:
    -   arch: all
        category: perl
        name: libio-html-perl
        origin: Ubuntu
        source: apt
        version: 1.001-1
    libio-multiplex-perl:
    -   arch: all
        category: perl
        name: libio-multiplex-perl
        origin: Ubuntu
        source: apt
        version: 1.16-1
    libio-socket-inet6-perl:
    -   arch: all
        category: perl
        name: libio-socket-inet6-perl
        origin: Ubuntu
        source: apt
        version: 2.72-2
    libio-socket-ssl-perl:
    -   arch: all
        category: perl
        name: libio-socket-ssl-perl
        origin: Ubuntu
        source: apt
        version: 2.024-1
    libio-string-perl:
    -   arch: all
        category: perl
        name: libio-string-perl
        origin: Ubuntu
        source: apt
        version: 1.08-3
    libipc-sharelite-perl:
    -   arch: amd64
        category: universe/perl
        name: libipc-sharelite-perl
        origin: Ubuntu
        source: apt
        version: 0.17-3build3
    libisc-export160:
    -   arch: amd64
        category: libs
        name: libisc-export160
        origin: Ubuntu
        source: apt
        version: 1:9.10.3.dfsg.P4-8ubuntu1.16
    libisc160:
    -   arch: amd64
        category: libs
        name: libisc160
        origin: Ubuntu
        source: apt
        version: 1:9.10.3.dfsg.P4-8ubuntu1.16
    libisccc140:
    -   arch: amd64
        category: libs
        name: libisccc140
        origin: Ubuntu
        source: apt
        version: 1:9.10.3.dfsg.P4-8ubuntu1.16
    libisccfg140:
    -   arch: amd64
        category: libs
        name: libisccfg140
        origin: Ubuntu
        source: apt
        version: 1:9.10.3.dfsg.P4-8ubuntu1.16
    libisl15:
    -   arch: amd64
        category: libs
        name: libisl15
        origin: Ubuntu
        source: apt
        version: 0.16.1-1
    libitm1:
    -   arch: amd64
        category: libs
        name: libitm1
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libjack-jackd2-0:
    -   arch: amd64
        category: libs
        name: libjack-jackd2-0
        origin: Ubuntu
        source: apt
        version: 1.9.10+20150825git1ed50c92~dfsg-1ubuntu1
    libjasper1:
    -   arch: amd64
        category: libs
        name: libjasper1
        origin: Ubuntu
        source: apt
        version: 1.900.1-debian1-2.4ubuntu1.2
    libjbig0:
    -   arch: amd64
        category: libs
        name: libjbig0
        origin: Ubuntu
        source: apt
        version: 2.1-3.1
    libjbig2dec0:
    -   arch: amd64
        category: libs
        name: libjbig2dec0
        origin: Ubuntu
        source: apt
        version: 0.12+20150918-1ubuntu0.1
    libjemalloc1:
    -   arch: amd64
        category: universe/libs
        name: libjemalloc1
        origin: Ubuntu
        source: apt
        version: 3.6.0-9ubuntu1
    libjpeg-turbo8:
    -   arch: amd64
        category: libs
        name: libjpeg-turbo8
        origin: Ubuntu
        source: apt
        version: 1.4.2-0ubuntu3.3
    libjpeg8:
    -   arch: amd64
        category: libs
        name: libjpeg8
        origin: Ubuntu
        source: apt
        version: 8c-2ubuntu8
    libjs-jquery:
    -   arch: all
        category: web
        name: libjs-jquery
        origin: Ubuntu
        source: apt
        version: 1.11.3+dfsg-4
    libjson-c2:
    -   arch: amd64
        category: libs
        name: libjson-c2
        origin: Ubuntu
        source: apt
        version: 0.11-4ubuntu2.6
    libjson-glib-1.0-0:
    -   arch: amd64
        category: libs
        name: libjson-glib-1.0-0
        origin: Ubuntu
        source: apt
        version: 1.1.2-0ubuntu1
    libjson-glib-1.0-common:
    -   arch: all
        category: libs
        name: libjson-glib-1.0-common
        origin: Ubuntu
        source: apt
        version: 1.1.2-0ubuntu1
    libk5crypto3:
    -   arch: amd64
        category: libs
        name: libk5crypto3
        origin: Ubuntu
        source: apt
        version: 1.13.2+dfsg-5ubuntu2.1
    libkate1:
    -   arch: amd64
        category: universe/libs
        name: libkate1
        origin: Ubuntu
        source: apt
        version: 0.4.1-7
    libkeyutils1:
    -   arch: amd64
        category: misc
        name: libkeyutils1
        origin: Ubuntu
        source: apt
        version: 1.5.9-8ubuntu1
    libklibc:
    -   arch: amd64
        category: libs
        name: libklibc
        origin: Ubuntu
        source: apt
        version: 2.0.4-8ubuntu1.16.04.4
    libkmod2:
    -   arch: amd64
        category: libs
        name: libkmod2
        origin: Ubuntu
        source: apt
        version: 22-1ubuntu5.2
    libkrb5-26-heimdal:
    -   arch: amd64
        category: libs
        name: libkrb5-26-heimdal
        origin: Ubuntu
        source: apt
        version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
    libkrb5-3:
    -   arch: amd64
        category: libs
        name: libkrb5-3
        origin: Ubuntu
        source: apt
        version: 1.13.2+dfsg-5ubuntu2.1
    libkrb5support0:
    -   arch: amd64
        category: libs
        name: libkrb5support0
        origin: Ubuntu
        source: apt
        version: 1.13.2+dfsg-5ubuntu2.1
    libksba8:
    -   arch: amd64
        category: libs
        name: libksba8
        origin: Ubuntu
        source: apt
        version: 1.3.3-1ubuntu0.16.04.1
    liblangtag-common:
    -   arch: all
        category: libs
        name: liblangtag-common
        origin: Ubuntu
        source: apt
        version: 0.5.7-2ubuntu1
    liblangtag1:
    -   arch: amd64
        category: libs
        name: liblangtag1
        origin: Ubuntu
        source: apt
        version: 0.5.7-2ubuntu1
    liblcms2-2:
    -   arch: amd64
        category: libs
        name: liblcms2-2
        origin: Ubuntu
        source: apt
        version: 2.6-3ubuntu2.1
    libldap-2.4-2:
    -   arch: amd64
        category: libs
        name: libldap-2.4-2
        origin: Ubuntu
        source: apt
        version: 2.4.42+dfsg-2ubuntu3.8
    libldb1:
    -   arch: amd64
        category: libs
        name: libldb1
        origin: Ubuntu
        source: apt
        version: 2:1.1.24-1ubuntu3.1
    libldns1:
    -   arch: amd64
        category: libs
        name: libldns1
        origin: Ubuntu
        source: apt
        version: 1.6.17-8ubuntu0.1
    liblircclient0:
    -   arch: amd64
        category: libs
        name: liblircclient0
        origin: Ubuntu
        source: apt
        version: 0.9.0-0ubuntu6
    liblist-moreutils-perl:
    -   arch: amd64
        category: perl
        name: liblist-moreutils-perl
        origin: Ubuntu
        source: apt
        version: 0.413-1build1
    libllvm6.0:
    -   arch: amd64
        category: libs
        name: libllvm6.0
        origin: Ubuntu
        source: apt
        version: 1:6.0-1ubuntu2~16.04.1
    liblocale-gettext-perl:
    -   arch: amd64
        category: perl
        name: liblocale-gettext-perl
        origin: Ubuntu
        source: apt
        version: 1.07-1build1
    liblqr-1-0:
    -   arch: amd64
        category: libs
        name: liblqr-1-0
        origin: Ubuntu
        source: apt
        version: 0.4.2-2
    liblsan0:
    -   arch: amd64
        category: libs
        name: liblsan0
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libltdl7:
    -   arch: amd64
        category: libs
        name: libltdl7
        origin: Ubuntu
        source: apt
        version: 2.4.6-0.1
    liblua5.1-0:
    -   arch: amd64
        category: libs
        name: liblua5.1-0
        origin: Ubuntu
        source: apt
        version: 5.1.5-8ubuntu1
    liblua5.2-0:
    -   arch: amd64
        category: libs
        name: liblua5.2-0
        origin: Ubuntu
        source: apt
        version: 5.2.4-1ubuntu1
    libluajit-5.1-2:
    -   arch: amd64
        category: universe/interpreters
        name: libluajit-5.1-2
        origin: Ubuntu
        source: apt
        version: 2.0.4+dfsg-1
    libluajit-5.1-common:
    -   arch: all
        category: universe/interpreters
        name: libluajit-5.1-common
        origin: Ubuntu
        source: apt
        version: 2.0.4+dfsg-1
    liblvm2app2.2:
    -   arch: amd64
        category: libs
        name: liblvm2app2.2
        origin: Ubuntu
        source: apt
        version: 2.02.133-1ubuntu10
    liblvm2cmd2.02:
    -   arch: amd64
        category: libs
        name: liblvm2cmd2.02
        origin: Ubuntu
        source: apt
        version: 2.02.133-1ubuntu10
    liblwp-mediatypes-perl:
    -   arch: all
        category: perl
        name: liblwp-mediatypes-perl
        origin: Ubuntu
        source: apt
        version: 6.02-1
    liblwp-protocol-https-perl:
    -   arch: all
        category: perl
        name: liblwp-protocol-https-perl
        origin: Ubuntu
        source: apt
        version: 6.06-2
    liblwres141:
    -   arch: amd64
        category: libs
        name: liblwres141
        origin: Ubuntu
        source: apt
        version: 1:9.10.3.dfsg.P4-8ubuntu1.16
    liblz4-1:
    -   arch: amd64
        category: libs
        name: liblz4-1
        origin: Ubuntu
        source: apt
        version: 0.0~r131-2ubuntu2
    liblzma5:
    -   arch: amd64
        category: libs
        name: liblzma5
        origin: Ubuntu
        source: apt
        version: 5.1.1alpha+20120614-2ubuntu2
    liblzo2-2:
    -   arch: amd64
        category: libs
        name: liblzo2-2
        origin: Ubuntu
        source: apt
        version: 2.08-1.2
    libmad0:
    -   arch: amd64
        category: universe/libs
        name: libmad0
        origin: Ubuntu
        source: apt
        version: 0.15.1b-9ubuntu16.04.1
    libmagic1:
    -   arch: amd64
        category: libs
        name: libmagic1
        origin: Ubuntu
        source: apt
        version: 1:5.25-2ubuntu1.4
    libmagickcore-6.q16-2:
    -   arch: amd64
        category: libs
        name: libmagickcore-6.q16-2
        origin: Ubuntu
        source: apt
        version: 8:6.8.9.9-7ubuntu5.15
    libmagickcore-6.q16-2-extra:
    -   arch: amd64
        category: libs
        name: libmagickcore-6.q16-2-extra
        origin: Ubuntu
        source: apt
        version: 8:6.8.9.9-7ubuntu5.15
    libmagickwand-6.q16-2:
    -   arch: amd64
        category: libs
        name: libmagickwand-6.q16-2
        origin: Ubuntu
        source: apt
        version: 8:6.8.9.9-7ubuntu5.15
    libmailtools-perl:
    -   arch: all
        category: perl
        name: libmailtools-perl
        origin: Ubuntu
        source: apt
        version: 2.13-1
    libmhash2:
    -   arch: amd64
        category: libs
        name: libmhash2
        origin: Ubuntu
        source: apt
        version: 0.9.9.9-7
    libmimic0:
    -   arch: amd64
        category: universe/libs
        name: libmimic0
        origin: Ubuntu
        source: apt
        version: 1.0.4-2.3
    libmirclient9:
    -   arch: amd64
        category: libs
        name: libmirclient9
        origin: Ubuntu
        source: apt
        version: 0.26.3+16.04.20170605-0ubuntu1.1
    libmircommon7:
    -   arch: amd64
        category: libs
        name: libmircommon7
        origin: Ubuntu
        source: apt
        version: 0.26.3+16.04.20170605-0ubuntu1.1
    libmircore1:
    -   arch: amd64
        category: libs
        name: libmircore1
        origin: Ubuntu
        source: apt
        version: 0.26.3+16.04.20170605-0ubuntu1.1
    libmirprotobuf3:
    -   arch: amd64
        category: libs
        name: libmirprotobuf3
        origin: Ubuntu
        source: apt
        version: 0.26.3+16.04.20170605-0ubuntu1.1
    libmjpegutils-2.1-0:
    -   arch: amd64
        category: universe/libs
        name: libmjpegutils-2.1-0
        origin: Ubuntu
        source: apt
        version: 1:2.1.0+debian-4
    libmms0:
    -   arch: amd64
        category: universe/libs
        name: libmms0
        origin: Ubuntu
        source: apt
        version: 0.6.4-1
    libmnl0:
    -   arch: amd64
        category: libs
        name: libmnl0
        origin: Ubuntu
        source: apt
        version: 1.0.3-5
    libmodplug1:
    -   arch: amd64
        category: universe/libs
        name: libmodplug1
        origin: Ubuntu
        source: apt
        version: 1:0.8.8.5-2
    libmount1:
    -   arch: amd64
        category: libs
        name: libmount1
        origin: Ubuntu
        source: apt
        version: 2.27.1-6ubuntu3.10
    libmp3lame0:
    -   arch: amd64
        category: universe/libs
        name: libmp3lame0
        origin: Ubuntu
        source: apt
        version: 3.99.5+repack1-9build1
    libmpc3:
    -   arch: amd64
        category: libs
        name: libmpc3
        origin: Ubuntu
        source: apt
        version: 1.0.3-1
    libmpdec2:
    -   arch: amd64
        category: libs
        name: libmpdec2
        origin: Ubuntu
        source: apt
        version: 2.4.2-1
    libmpeg2-4:
    -   arch: amd64
        category: universe/libs
        name: libmpeg2-4
        origin: Ubuntu
        source: apt
        version: 0.5.1-7
    libmpeg2encpp-2.1-0:
    -   arch: amd64
        category: universe/libs
        name: libmpeg2encpp-2.1-0
        origin: Ubuntu
        source: apt
        version: 1:2.1.0+debian-4
    libmpfr4:
    -   arch: amd64
        category: libs
        name: libmpfr4
        origin: Ubuntu
        source: apt
        version: 3.1.4-1
    libmpg123-0:
    -   arch: amd64
        category: universe/libs
        name: libmpg123-0
        origin: Ubuntu
        source: apt
        version: 1.22.4-1ubuntu0.1
    libmplex2-2.1-0:
    -   arch: amd64
        category: universe/libs
        name: libmplex2-2.1-0
        origin: Ubuntu
        source: apt
        version: 1:2.1.0+debian-4
    libmpx0:
    -   arch: amd64
        category: libs
        name: libmpx0
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libmspack0:
    -   arch: amd64
        category: libs
        name: libmspack0
        origin: Ubuntu
        source: apt
        version: 0.5-1ubuntu0.16.04.4
    libmspub-0.1-1:
    -   arch: amd64
        category: libs
        name: libmspub-0.1-1
        origin: Ubuntu
        source: apt
        version: 0.1.2-2ubuntu1
    libmtdev1:
    -   arch: amd64
        category: libs
        name: libmtdev1
        origin: Ubuntu
        source: apt
        version: 1.1.5-1ubuntu2
    libmwaw-0.3-3:
    -   arch: amd64
        category: libs
        name: libmwaw-0.3-3
        origin: Ubuntu
        source: apt
        version: 0.3.7-1ubuntu2.1
    libmythes-1.2-0:
    -   arch: amd64
        category: libs
        name: libmythes-1.2-0
        origin: Ubuntu
        source: apt
        version: 2:1.2.4-1ubuntu3
    libncurses5:
    -   arch: amd64
        category: libs
        name: libncurses5
        origin: Ubuntu
        source: apt
        version: 6.0+20160213-1ubuntu1
    libncurses5-dev:
    -   arch: amd64
        category: libdevel
        name: libncurses5-dev
        origin: Ubuntu
        source: apt
        version: 6.0+20160213-1ubuntu1
    libncursesw5:
    -   arch: amd64
        category: libs
        name: libncursesw5
        origin: Ubuntu
        source: apt
        version: 6.0+20160213-1ubuntu1
    libneon27-gnutls:
    -   arch: amd64
        category: libs
        name: libneon27-gnutls
        origin: Ubuntu
        source: apt
        version: 0.30.1-3build1
    libnet-cidr-perl:
    -   arch: all
        category: perl
        name: libnet-cidr-perl
        origin: Ubuntu
        source: apt
        version: 0.17-1
    libnet-http-perl:
    -   arch: all
        category: perl
        name: libnet-http-perl
        origin: Ubuntu
        source: apt
        version: 6.09-1
    libnet-server-perl:
    -   arch: all
        category: perl
        name: libnet-server-perl
        origin: Ubuntu
        source: apt
        version: 2.008-2
    libnet-smtp-ssl-perl:
    -   arch: all
        category: perl
        name: libnet-smtp-ssl-perl
        origin: Ubuntu
        source: apt
        version: 1.03-1
    libnet-snmp-perl:
    -   arch: all
        category: perl
        name: libnet-snmp-perl
        origin: Ubuntu
        source: apt
        version: 6.0.1-2
    libnet-ssleay-perl:
    -   arch: amd64
        category: perl
        name: libnet-ssleay-perl
        origin: Ubuntu
        source: apt
        version: 1.72-1build1
    libnetpbm10:
    -   arch: amd64
        category: libs
        name: libnetpbm10
        origin: Ubuntu
        source: apt
        version: 2:10.0-15.3
    libnettle6:
    -   arch: amd64
        category: libs
        name: libnettle6
        origin: Ubuntu
        source: apt
        version: 3.2-1ubuntu0.16.04.1
    libnewt0.52:
    -   arch: amd64
        category: libs
        name: libnewt0.52
        origin: Ubuntu
        source: apt
        version: 0.52.18-1ubuntu2
    libnfnetlink0:
    -   arch: amd64
        category: libs
        name: libnfnetlink0
        origin: Ubuntu
        source: apt
        version: 1.0.1-3
    libnice10:
    -   arch: amd64
        category: libs
        name: libnice10
        origin: BigBlueButton
        source: apt
        version: 0.1.15-1kurento3.16.04
    libnih1:
    -   arch: amd64
        category: libs
        name: libnih1
        origin: Ubuntu
        source: apt
        version: 1.0.3-4.3ubuntu1
    libnl-3-200:
    -   arch: amd64
        category: libs
        name: libnl-3-200
        origin: Ubuntu
        source: apt
        version: 3.2.27-1ubuntu0.16.04.1
    libnl-genl-3-200:
    -   arch: amd64
        category: libs
        name: libnl-genl-3-200
        origin: Ubuntu
        source: apt
        version: 3.2.27-1ubuntu0.16.04.1
    libnotify4:
    -   arch: amd64
        category: libs
        name: libnotify4
        origin: Ubuntu
        source: apt
        version: 0.7.6-2svn1
    libnpth0:
    -   arch: amd64
        category: libs
        name: libnpth0
        origin: Ubuntu
        source: apt
        version: 1.2-3
    libnspr4:
    -   arch: amd64
        category: libs
        name: libnspr4
        origin: Ubuntu
        source: apt
        version: 2:4.13.1-0ubuntu0.16.04.1
    libnss3:
    -   arch: amd64
        category: libs
        name: libnss3
        origin: Ubuntu
        source: apt
        version: 2:3.28.4-0ubuntu0.16.04.10
    libnss3-nssdb:
    -   arch: all
        category: admin
        name: libnss3-nssdb
        origin: Ubuntu
        source: apt
        version: 2:3.28.4-0ubuntu0.16.04.10
    libnuma1:
    -   arch: amd64
        category: libs
        name: libnuma1
        origin: Ubuntu
        source: apt
        version: 2.0.11-1ubuntu1.1
    libodfgen-0.1-1:
    -   arch: amd64
        category: libs
        name: libodfgen-0.1-1
        origin: Ubuntu
        source: apt
        version: 0.1.6-1ubuntu2
    libofa0:
    -   arch: amd64
        category: universe/libs
        name: libofa0
        origin: Ubuntu
        source: apt
        version: 0.9.3-10ubuntu1
    libogg0:
    -   arch: amd64
        category: libs
        name: libogg0
        origin: Ubuntu
        source: apt
        version: 1.3.2-1
    libopenal-data:
    -   arch: all
        category: universe/libs
        name: libopenal-data
        origin: Ubuntu
        source: apt
        version: 1:1.16.0-3
    libopenal1:
    -   arch: amd64
        category: universe/libs
        name: libopenal1
        origin: Ubuntu
        source: apt
        version: 1:1.16.0-3
    libopencore-amrnb0:
    -   arch: amd64
        category: universe/libs
        name: libopencore-amrnb0
        origin: Ubuntu
        source: apt
        version: 0.1.3-2.1
    libopencore-amrwb0:
    -   arch: amd64
        category: universe/libs
        name: libopencore-amrwb0
        origin: Ubuntu
        source: apt
        version: 0.1.3-2.1
    libopencv-core2.4v5:
    -   arch: amd64
        category: universe/libs
        name: libopencv-core2.4v5
        origin: Ubuntu
        source: apt
        version: 2.4.9.1+dfsg-1.5ubuntu1.1
    libopencv-highgui2.4v5:
    -   arch: amd64
        category: universe/libs
        name: libopencv-highgui2.4v5
        origin: Ubuntu
        source: apt
        version: 2.4.9.1+dfsg-1.5ubuntu1.1
    libopencv-imgproc2.4v5:
    -   arch: amd64
        category: universe/libs
        name: libopencv-imgproc2.4v5
        origin: Ubuntu
        source: apt
        version: 2.4.9.1+dfsg-1.5ubuntu1.1
    libopencv-objdetect2.4v5:
    -   arch: amd64
        category: universe/libs
        name: libopencv-objdetect2.4v5
        origin: Ubuntu
        source: apt
        version: 2.4.9.1+dfsg-1.5ubuntu1.1
    libopenexr22:
    -   arch: amd64
        category: libs
        name: libopenexr22
        origin: Ubuntu
        source: apt
        version: 2.2.0-10ubuntu2.2
    libopenjpeg5:
    -   arch: amd64
        category: universe/libs
        name: libopenjpeg5
        origin: Ubuntu
        source: apt
        version: 1:1.5.2-3.1
    libopus0:
    -   arch: amd64
        category: libs
        name: libopus0
        origin: Ubuntu
        source: apt
        version: 1.1.2-1ubuntu1
    libopusenc0:
    -   arch: amd64
        category: sound
        name: libopusenc0
        origin: LP-PPA-bigbluebutton-support
        source: apt
        version: 0.2.1-1bbb1
    libopusfile0:
    -   arch: amd64
        category: universe/libs
        name: libopusfile0
        origin: Ubuntu
        source: apt
        version: 0.7-1
    liborc-0.4-0:
    -   arch: amd64
        category: libs
        name: liborc-0.4-0
        origin: Ubuntu
        source: apt
        version: 1:0.4.25-1
    liborcus-0.10-0v5:
    -   arch: amd64
        category: libs
        name: liborcus-0.10-0v5
        origin: Ubuntu
        source: apt
        version: 0.9.2-4ubuntu2
    libp11-kit0:
    -   arch: amd64
        category: libs
        name: libp11-kit0
        origin: Ubuntu
        source: apt
        version: 0.23.2-5~ubuntu16.04.1
    libpagemaker-0.0-0:
    -   arch: amd64
        category: libs
        name: libpagemaker-0.0-0
        origin: Ubuntu
        source: apt
        version: 0.0.3-1ubuntu1
    libpam-modules:
    -   arch: amd64
        category: admin
        name: libpam-modules
        origin: Ubuntu
        source: apt
        version: 1.1.8-3.2ubuntu2.1
    libpam-modules-bin:
    -   arch: amd64
        category: admin
        name: libpam-modules-bin
        origin: Ubuntu
        source: apt
        version: 1.1.8-3.2ubuntu2.1
    libpam-runtime:
    -   arch: all
        category: admin
        name: libpam-runtime
        origin: Ubuntu
        source: apt
        version: 1.1.8-3.2ubuntu2.1
    libpam-systemd:
    -   arch: amd64
        category: admin
        name: libpam-systemd
        origin: Ubuntu
        source: apt
        version: 229-4ubuntu21.28
    libpam0g:
    -   arch: amd64
        category: libs
        name: libpam0g
        origin: Ubuntu
        source: apt
        version: 1.1.8-3.2ubuntu2.1
    libpango-1.0-0:
    -   arch: amd64
        category: libs
        name: libpango-1.0-0
        origin: Ubuntu
        source: apt
        version: 1.38.1-1
    libpangocairo-1.0-0:
    -   arch: amd64
        category: libs
        name: libpangocairo-1.0-0
        origin: Ubuntu
        source: apt
        version: 1.38.1-1
    libpangoft2-1.0-0:
    -   arch: amd64
        category: libs
        name: libpangoft2-1.0-0
        origin: Ubuntu
        source: apt
        version: 1.38.1-1
    libpangox-1.0-0:
    -   arch: amd64
        category: oldlibs
        name: libpangox-1.0-0
        origin: Ubuntu
        source: apt
        version: 0.0.2-5
    libpaper-utils:
    -   arch: amd64
        category: utils
        name: libpaper-utils
        origin: Ubuntu
        source: apt
        version: 1.1.24+nmu4ubuntu1
    libpaper1:
    -   arch: amd64
        category: libs
        name: libpaper1
        origin: Ubuntu
        source: apt
        version: 1.1.24+nmu4ubuntu1
    libparse-debianchangelog-perl:
    -   arch: all
        category: perl
        name: libparse-debianchangelog-perl
        origin: Ubuntu
        source: apt
        version: 1.2.0-8
    libpci3:
    -   arch: amd64
        category: libs
        name: libpci3
        origin: Ubuntu
        source: apt
        version: 1:3.3.1-1.1ubuntu1.3
    libpciaccess0:
    -   arch: amd64
        category: libs
        name: libpciaccess0
        origin: Ubuntu
        source: apt
        version: 0.13.4-1
    libpcre16-3:
    -   arch: amd64
        category: libs
        name: libpcre16-3
        origin: Ubuntu
        source: apt
        version: 2:8.38-3.1
    libpcre3:
    -   arch: amd64
        category: libs
        name: libpcre3
        origin: Ubuntu
        source: apt
        version: 2:8.38-3.1
    libpcsclite1:
    -   arch: amd64
        category: libs
        name: libpcsclite1
        origin: Ubuntu
        source: apt
        version: 1.8.14-1ubuntu1.16.04.1
    libperl5.22:
    -   arch: amd64
        category: libs
        name: libperl5.22
        origin: Ubuntu
        source: apt
        version: 5.22.1-9ubuntu0.6
    libpixman-1-0:
    -   arch: amd64
        category: libs
        name: libpixman-1-0
        origin: Ubuntu
        source: apt
        version: 0.33.6-1
    libplymouth4:
    -   arch: amd64
        category: libs
        name: libplymouth4
        origin: Ubuntu
        source: apt
        version: 0.9.2-3ubuntu13.5
    libpng12-0:
    -   arch: amd64
        category: libs
        name: libpng12-0
        origin: Ubuntu
        source: apt
        version: 1.2.54-1ubuntu1.1
    libpolkit-agent-1-0:
    -   arch: amd64
        category: libs
        name: libpolkit-agent-1-0
        origin: Ubuntu
        source: apt
        version: 0.105-14.1ubuntu0.5
    libpolkit-backend-1-0:
    -   arch: amd64
        category: libs
        name: libpolkit-backend-1-0
        origin: Ubuntu
        source: apt
        version: 0.105-14.1ubuntu0.5
    libpolkit-gobject-1-0:
    -   arch: amd64
        category: libs
        name: libpolkit-gobject-1-0
        origin: Ubuntu
        source: apt
        version: 0.105-14.1ubuntu0.5
    libpoppler58:
    -   arch: amd64
        category: libs
        name: libpoppler58
        origin: Ubuntu
        source: apt
        version: 0.41.0-0ubuntu1.14
    libpopt0:
    -   arch: amd64
        category: libs
        name: libpopt0
        origin: Ubuntu
        source: apt
        version: 1.16-10
    libpostproc-ffmpeg53:
    -   arch: amd64
        category: universe/libs
        name: libpostproc-ffmpeg53
        origin: Ubuntu
        source: apt
        version: 7:2.8.15-0ubuntu0.16.04.1
    libpostproc55:
    -   arch: amd64
        category: libs
        name: libpostproc55
        origin: LP-PPA-bigbluebutton-support
        source: apt
        version: 7:4.2.2-1bbb1~ubuntu16.04
    libprocps4:
    -   arch: amd64
        category: libs
        name: libprocps4
        origin: Ubuntu
        source: apt
        version: 2:3.3.10-4ubuntu2.5
    libprotobuf-lite9v5:
    -   arch: amd64
        category: libs
        name: libprotobuf-lite9v5
        origin: Ubuntu
        source: apt
        version: 2.6.1-1.3
    libproxy1v5:
    -   arch: amd64
        category: libs
        name: libproxy1v5
        origin: Ubuntu
        source: apt
        version: 0.4.11-5ubuntu1
    libpulse0:
    -   arch: amd64
        category: libs
        name: libpulse0
        origin: Ubuntu
        source: apt
        version: 1:8.0-0ubuntu3.12
    libpython-stdlib:
    -   arch: amd64
        category: python
        name: libpython-stdlib
        origin: Ubuntu
        source: apt
        version: 2.7.12-1~16.04
    libpython2.7:
    -   arch: amd64
        category: libs
        name: libpython2.7
        origin: Ubuntu
        source: apt
        version: 2.7.12-1ubuntu0~16.04.11
    libpython2.7-minimal:
    -   arch: amd64
        category: python
        name: libpython2.7-minimal
        origin: Ubuntu
        source: apt
        version: 2.7.12-1ubuntu0~16.04.11
    libpython2.7-stdlib:
    -   arch: amd64
        category: python
        name: libpython2.7-stdlib
        origin: Ubuntu
        source: apt
        version: 2.7.12-1ubuntu0~16.04.11
    libpython3-stdlib:
    -   arch: amd64
        category: python
        name: libpython3-stdlib
        origin: Ubuntu
        source: apt
        version: 3.5.1-3
    libpython3.5:
    -   arch: amd64
        category: libs
        name: libpython3.5
        origin: Ubuntu
        source: apt
        version: 3.5.2-2ubuntu0~16.04.10
    libpython3.5-minimal:
    -   arch: amd64
        category: python
        name: libpython3.5-minimal
        origin: Ubuntu
        source: apt
        version: 3.5.2-2ubuntu0~16.04.10
    libpython3.5-stdlib:
    -   arch: amd64
        category: python
        name: libpython3.5-stdlib
        origin: Ubuntu
        source: apt
        version: 3.5.2-2ubuntu0~16.04.10
    libqt5core5a:
    -   arch: amd64
        category: libs
        name: libqt5core5a
        origin: Ubuntu
        source: apt
        version: 5.5.1+dfsg-16ubuntu7.7
    libqt5dbus5:
    -   arch: amd64
        category: libs
        name: libqt5dbus5
        origin: Ubuntu
        source: apt
        version: 5.5.1+dfsg-16ubuntu7.7
    libqt5gui5:
    -   arch: amd64
        category: libs
        name: libqt5gui5
        origin: Ubuntu
        source: apt
        version: 5.5.1+dfsg-16ubuntu7.7
    libqt5network5:
    -   arch: amd64
        category: libs
        name: libqt5network5
        origin: Ubuntu
        source: apt
        version: 5.5.1+dfsg-16ubuntu7.7
    libqt5printsupport5:
    -   arch: amd64
        category: libs
        name: libqt5printsupport5
        origin: Ubuntu
        source: apt
        version: 5.5.1+dfsg-16ubuntu7.7
    libqt5svg5:
    -   arch: amd64
        category: libs
        name: libqt5svg5
        origin: Ubuntu
        source: apt
        version: 5.5.1-2build1
    libqt5widgets5:
    -   arch: amd64
        category: libs
        name: libqt5widgets5
        origin: Ubuntu
        source: apt
        version: 5.5.1+dfsg-16ubuntu7.7
    libquadmath0:
    -   arch: amd64
        category: libs
        name: libquadmath0
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libraptor2-0:
    -   arch: amd64
        category: libs
        name: libraptor2-0
        origin: Ubuntu
        source: apt
        version: 2.0.14-1
    librasqal3:
    -   arch: amd64
        category: libs
        name: librasqal3
        origin: Ubuntu
        source: apt
        version: 0.9.32-1
    libraw1394-11:
    -   arch: amd64
        category: libs
        name: libraw1394-11
        origin: Ubuntu
        source: apt
        version: 2.1.1-2
    librdf0:
    -   arch: amd64
        category: libs
        name: librdf0
        origin: Ubuntu
        source: apt
        version: 1.0.17-1build1
    libreadline5:
    -   arch: amd64
        category: libs
        name: libreadline5
        origin: Ubuntu
        source: apt
        version: 5.2+dfsg-3build1
    libreadline6:
    -   arch: amd64
        category: libs
        name: libreadline6
        origin: Ubuntu
        source: apt
        version: 6.3-8ubuntu2
    libreoffice:
    -   arch: amd64
        category: universe/editors
        name: libreoffice
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-avmedia-backend-gstreamer:
    -   arch: amd64
        category: misc
        name: libreoffice-avmedia-backend-gstreamer
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-base:
    -   arch: amd64
        category: database
        name: libreoffice-base
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-base-core:
    -   arch: amd64
        category: editors
        name: libreoffice-base-core
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-base-drivers:
    -   arch: amd64
        category: database
        name: libreoffice-base-drivers
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-calc:
    -   arch: amd64
        category: editors
        name: libreoffice-calc
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-common:
    -   arch: all
        category: editors
        name: libreoffice-common
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-core:
    -   arch: amd64
        category: editors
        name: libreoffice-core
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-draw:
    -   arch: amd64
        category: editors
        name: libreoffice-draw
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-gnome:
    -   arch: amd64
        category: gnome
        name: libreoffice-gnome
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-gtk:
    -   arch: amd64
        category: gnome
        name: libreoffice-gtk
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-impress:
    -   arch: amd64
        category: editors
        name: libreoffice-impress
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-java-common:
    -   arch: all
        category: editors
        name: libreoffice-java-common
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-math:
    -   arch: amd64
        category: editors
        name: libreoffice-math
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-pdfimport:
    -   arch: amd64
        category: misc
        name: libreoffice-pdfimport
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-report-builder-bin:
    -   arch: amd64
        category: universe/misc
        name: libreoffice-report-builder-bin
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-sdbc-firebird:
    -   arch: amd64
        category: database
        name: libreoffice-sdbc-firebird
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-sdbc-hsqldb:
    -   arch: amd64
        category: database
        name: libreoffice-sdbc-hsqldb
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-style-elementary:
    -   arch: all
        category: universe/x11
        name: libreoffice-style-elementary
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-style-galaxy:
    -   arch: all
        category: editors
        name: libreoffice-style-galaxy
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    libreoffice-writer:
    -   arch: amd64
        category: editors
        name: libreoffice-writer
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    librest-0.7-0:
    -   arch: amd64
        category: libs
        name: librest-0.7-0
        origin: Ubuntu
        source: apt
        version: 0.7.93-1
    librevenge-0.0-0:
    -   arch: amd64
        category: libs
        name: librevenge-0.0-0
        origin: Ubuntu
        source: apt
        version: 0.0.4-4ubuntu1
    libroken18-heimdal:
    -   arch: amd64
        category: libs
        name: libroken18-heimdal
        origin: Ubuntu
        source: apt
        version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
    librsvg2-2:
    -   arch: amd64
        category: libs
        name: librsvg2-2
        origin: Ubuntu
        source: apt
        version: 2.40.13-3
    librsvg2-common:
    -   arch: amd64
        category: libs
        name: librsvg2-common
        origin: Ubuntu
        source: apt
        version: 2.40.13-3
    librtmp1:
    -   arch: amd64
        category: libs
        name: librtmp1
        origin: Ubuntu
        source: apt
        version: 2.4+20151223.gitfa8646d-1ubuntu0.1
    librubberband2v5:
    -   arch: amd64
        category: universe/libs
        name: librubberband2v5
        origin: Ubuntu
        source: apt
        version: 1.8.1-6ubuntu2
    libruby2.3:
    -   arch: amd64
        category: libs
        name: libruby2.3
        origin: Ubuntu
        source: apt
        version: 2.3.1-2~ubuntu16.04.14
    libsamplerate0:
    -   arch: amd64
        category: libs
        name: libsamplerate0
        origin: Ubuntu
        source: apt
        version: 0.1.8-8
    libsane:
    -   arch: amd64
        category: libs
        name: libsane
        origin: Ubuntu
        source: apt
        version: 1.0.25+git20150528-1ubuntu2.16.04.1
    libsane-common:
    -   arch: all
        category: libs
        name: libsane-common
        origin: Ubuntu
        source: apt
        version: 1.0.25+git20150528-1ubuntu2.16.04.1
    libsasl2-2:
    -   arch: amd64
        category: libs
        name: libsasl2-2
        origin: Ubuntu
        source: apt
        version: 2.1.26.dfsg1-14ubuntu0.2
    libsasl2-modules:
    -   arch: amd64
        category: devel
        name: libsasl2-modules
        origin: Ubuntu
        source: apt
        version: 2.1.26.dfsg1-14ubuntu0.2
    libsasl2-modules-db:
    -   arch: amd64
        category: libs
        name: libsasl2-modules-db
        origin: Ubuntu
        source: apt
        version: 2.1.26.dfsg1-14ubuntu0.2
    libsbc1:
    -   arch: amd64
        category: libs
        name: libsbc1
        origin: Ubuntu
        source: apt
        version: 1.3-1
    libschroedinger-1.0-0:
    -   arch: amd64
        category: universe/libs
        name: libschroedinger-1.0-0
        origin: Ubuntu
        source: apt
        version: 1.0.11-2.1build1
    libsdl1.2debian:
    -   arch: amd64
        category: libs
        name: libsdl1.2debian
        origin: Ubuntu
        source: apt
        version: 1.2.15+dfsg1-3ubuntu0.1
    libsdl2-2.0-0:
    -   arch: amd64
        category: universe/libs
        name: libsdl2-2.0-0
        origin: Ubuntu
        source: apt
        version: 2.0.4+dfsg1-2ubuntu2.16.04.2
    libseccomp2:
    -   arch: amd64
        category: libs
        name: libseccomp2
        origin: Ubuntu
        source: apt
        version: 2.4.1-0ubuntu0.16.04.2
    libselinux1:
    -   arch: amd64
        category: libs
        name: libselinux1
        origin: Ubuntu
        source: apt
        version: 2.4-3build2
    libsemanage-common:
    -   arch: all
        category: libs
        name: libsemanage-common
        origin: Ubuntu
        source: apt
        version: 2.3-1build3
    libsemanage1:
    -   arch: amd64
        category: libs
        name: libsemanage1
        origin: Ubuntu
        source: apt
        version: 2.3-1build3
    libsensors4:
    -   arch: amd64
        category: libs
        name: libsensors4
        origin: Ubuntu
        source: apt
        version: 1:3.4.0-2
    libsepol1:
    -   arch: amd64
        category: libs
        name: libsepol1
        origin: Ubuntu
        source: apt
        version: 2.4-2
    libservlet3.1-java:
    -   arch: all
        category: java
        name: libservlet3.1-java
        origin: Ubuntu
        source: apt
        version: 8.0.32-1ubuntu1.11
    libshine3:
    -   arch: amd64
        category: universe/libs
        name: libshine3
        origin: Ubuntu
        source: apt
        version: 3.1.0-4
    libshout3:
    -   arch: amd64
        category: libs
        name: libshout3
        origin: Ubuntu
        source: apt
        version: 2.3.1-3
    libsidplay1v5:
    -   arch: amd64
        category: universe/libs
        name: libsidplay1v5
        origin: Ubuntu
        source: apt
        version: 1.36.59-8
    libsigc++-2.0-0v5:
    -   arch: amd64
        category: libs
        name: libsigc++-2.0-0v5
        origin: Ubuntu
        source: apt
        version: 2.6.2-1
    libsigsegv2:
    -   arch: amd64
        category: libs
        name: libsigsegv2
        origin: Ubuntu
        source: apt
        version: 2.10-4
    libslang2:
    -   arch: amd64
        category: libs
        name: libslang2
        origin: Ubuntu
        source: apt
        version: 2.3.0-2ubuntu1.1
    libsm6:
    -   arch: amd64
        category: libs
        name: libsm6
        origin: Ubuntu
        source: apt
        version: 2:1.2.2-1
    libsmartcols1:
    -   arch: amd64
        category: libs
        name: libsmartcols1
        origin: Ubuntu
        source: apt
        version: 2.27.1-6ubuntu3.10
    libsmbclient:
    -   arch: amd64
        category: libs
        name: libsmbclient
        origin: Ubuntu
        source: apt
        version: 2:4.3.11+dfsg-0ubuntu0.16.04.27
    libsnappy1v5:
    -   arch: amd64
        category: libs
        name: libsnappy1v5
        origin: Ubuntu
        source: apt
        version: 1.1.3-2
    libsndfile1:
    -   arch: amd64
        category: libs
        name: libsndfile1
        origin: Ubuntu
        source: apt
        version: 1.0.25-10ubuntu0.16.04.2
    libsndio6.1:
    -   arch: amd64
        category: universe/libs
        name: libsndio6.1
        origin: Ubuntu
        source: apt
        version: 1.1.0-2
    libsocket6-perl:
    -   arch: amd64
        category: perl
        name: libsocket6-perl
        origin: Ubuntu
        source: apt
        version: 0.25-1build2
    libsoundtouch1:
    -   arch: amd64
        category: universe/libs
        name: libsoundtouch1
        origin: Ubuntu
        source: apt
        version: 1.9.2-2+deb9u1build0.16.04.1
    libsoup-gnome2.4-1:
    -   arch: amd64
        category: libs
        name: libsoup-gnome2.4-1
        origin: Ubuntu
        source: apt
        version: 2.52.2-1ubuntu0.3
    libsoup2.4-1:
    -   arch: amd64
        category: libs
        name: libsoup2.4-1
        origin: Ubuntu
        source: apt
        version: 2.52.2-1ubuntu0.3
    libsox-fmt-alsa:
    -   arch: amd64
        category: universe/sound
        name: libsox-fmt-alsa
        origin: Ubuntu
        source: apt
        version: 14.4.1-5+deb8u4ubuntu0.1
    libsox-fmt-base:
    -   arch: amd64
        category: universe/sound
        name: libsox-fmt-base
        origin: Ubuntu
        source: apt
        version: 14.4.1-5+deb8u4ubuntu0.1
    libsox2:
    -   arch: amd64
        category: universe/sound
        name: libsox2
        origin: Ubuntu
        source: apt
        version: 14.4.1-5+deb8u4ubuntu0.1
    libsoxr0:
    -   arch: amd64
        category: universe/libs
        name: libsoxr0
        origin: Ubuntu
        source: apt
        version: 0.1.2-1
    libspandsp2:
    -   arch: amd64
        category: universe/libs
        name: libspandsp2
        origin: Ubuntu
        source: apt
        version: 0.0.6-2.1
    libspeex1:
    -   arch: amd64
        category: libs
        name: libspeex1
        origin: Ubuntu
        source: apt
        version: 1.2~rc1.2-1ubuntu1
    libspeexdsp1:
    -   arch: amd64
        category: libs
        name: libspeexdsp1
        origin: Ubuntu
        source: apt
        version: 1.2~rc1.2-1ubuntu1
    libsqlite3-0:
    -   arch: amd64
        category: libs
        name: libsqlite3-0
        origin: Ubuntu
        source: apt
        version: 3.11.0-1ubuntu1.4
    libsrtp0:
    -   arch: amd64
        category: libs
        name: libsrtp0
        origin: BigBlueButton
        source: apt
        version: 1.6.0-0kurento1.16.04
    libss2:
    -   arch: amd64
        category: libs
        name: libss2
        origin: Ubuntu
        source: apt
        version: 1.42.13-1ubuntu1.2
    libssh-gcrypt-4:
    -   arch: amd64
        category: libs
        name: libssh-gcrypt-4
        origin: Ubuntu
        source: apt
        version: 0.6.3-4.3ubuntu0.5
    libssl1.0.0:
    -   arch: amd64
        category: libs
        name: libssl1.0.0
        origin: Ubuntu
        source: apt
        version: 1.0.2g-1ubuntu4.16
    libstdc++-5-dev:
    -   arch: amd64
        category: libdevel
        name: libstdc++-5-dev
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libstdc++6:
    -   arch: amd64
        category: libs
        name: libstdc++6
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libstring-crc32-perl:
    -   arch: amd64
        category: universe/perl
        name: libstring-crc32-perl
        origin: Ubuntu
        source: apt
        version: 1.5-1build2
    libsub-name-perl:
    -   arch: amd64
        category: perl
        name: libsub-name-perl
        origin: Ubuntu
        source: apt
        version: 0.14-1build1
    libsuitesparseconfig4.4.6:
    -   arch: amd64
        category: libs
        name: libsuitesparseconfig4.4.6
        origin: Ubuntu
        source: apt
        version: 1:4.4.6-1
    libswresample-ffmpeg1:
    -   arch: amd64
        category: universe/libs
        name: libswresample-ffmpeg1
        origin: Ubuntu
        source: apt
        version: 7:2.8.15-0ubuntu0.16.04.1
    libswresample3:
    -   arch: amd64
        category: libs
        name: libswresample3
        origin: LP-PPA-bigbluebutton-support
        source: apt
        version: 7:4.2.2-1bbb1~ubuntu16.04
    libswscale-ffmpeg3:
    -   arch: amd64
        category: universe/libs
        name: libswscale-ffmpeg3
        origin: Ubuntu
        source: apt
        version: 7:2.8.15-0ubuntu0.16.04.1
    libswscale5:
    -   arch: amd64
        category: libs
        name: libswscale5
        origin: LP-PPA-bigbluebutton-support
        source: apt
        version: 7:4.2.2-1bbb1~ubuntu16.04
    libsystemd-dev:
    -   arch: amd64
        category: libdevel
        name: libsystemd-dev
        origin: Ubuntu
        source: apt
        version: 229-4ubuntu21.28
    libsystemd0:
    -   arch: amd64
        category: libs
        name: libsystemd0
        origin: Ubuntu
        source: apt
        version: 229-4ubuntu21.28
    libtag1v5:
    -   arch: amd64
        category: libs
        name: libtag1v5
        origin: Ubuntu
        source: apt
        version: 1.9.1-2.4ubuntu1
    libtag1v5-vanilla:
    -   arch: amd64
        category: libs
        name: libtag1v5-vanilla
        origin: Ubuntu
        source: apt
        version: 1.9.1-2.4ubuntu1
    libtalloc2:
    -   arch: amd64
        category: libs
        name: libtalloc2
        origin: Ubuntu
        source: apt
        version: 2.1.5-2
    libtasn1-6:
    -   arch: amd64
        category: libs
        name: libtasn1-6
        origin: Ubuntu
        source: apt
        version: 4.7-3ubuntu0.16.04.3
    libtbb2:
    -   arch: amd64
        category: universe/libs
        name: libtbb2
        origin: Ubuntu
        source: apt
        version: 4.4~20151115-0ubuntu3
    libtdb1:
    -   arch: amd64
        category: libs
        name: libtdb1
        origin: Ubuntu
        source: apt
        version: 1.3.8-2
    libtevent0:
    -   arch: amd64
        category: libs
        name: libtevent0
        origin: Ubuntu
        source: apt
        version: 0.9.28-0ubuntu0.16.04.1
    libtext-charwidth-perl:
    -   arch: amd64
        category: perl
        name: libtext-charwidth-perl
        origin: Ubuntu
        source: apt
        version: 0.04-7build5
    libtext-iconv-perl:
    -   arch: amd64
        category: perl
        name: libtext-iconv-perl
        origin: Ubuntu
        source: apt
        version: 1.7-5build4
    libtext-wrapi18n-perl:
    -   arch: all
        category: perl
        name: libtext-wrapi18n-perl
        origin: Ubuntu
        source: apt
        version: 0.06-7.1
    libthai-data:
    -   arch: all
        category: libs
        name: libthai-data
        origin: Ubuntu
        source: apt
        version: 0.1.24-2
    libthai0:
    -   arch: amd64
        category: libs
        name: libthai0
        origin: Ubuntu
        source: apt
        version: 0.1.24-2
    libtheora0:
    -   arch: amd64
        category: libs
        name: libtheora0
        origin: Ubuntu
        source: apt
        version: 1.1.1+dfsg.1-8
    libtidy-0.99-0:
    -   arch: amd64
        category: libs
        name: libtidy-0.99-0
        origin: Ubuntu
        source: apt
        version: 20091223cvs-1.5
    libtiff5:
    -   arch: amd64
        category: libs
        name: libtiff5
        origin: Ubuntu
        source: apt
        version: 4.0.6-1ubuntu0.7
    libtimedate-perl:
    -   arch: all
        category: perl
        name: libtimedate-perl
        origin: Ubuntu
        source: apt
        version: 2.3000-2
    libtinfo-dev:
    -   arch: amd64
        category: libdevel
        name: libtinfo-dev
        origin: Ubuntu
        source: apt
        version: 6.0+20160213-1ubuntu1
    libtinfo5:
    -   arch: amd64
        category: libs
        name: libtinfo5
        origin: Ubuntu
        source: apt
        version: 6.0+20160213-1ubuntu1
    libtokyocabinet9:
    -   arch: amd64
        category: libs
        name: libtokyocabinet9
        origin: Ubuntu
        source: apt
        version: 1.4.48-10
    libtsan0:
    -   arch: amd64
        category: libs
        name: libtsan0
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libtwolame0:
    -   arch: amd64
        category: universe/libs
        name: libtwolame0
        origin: Ubuntu
        source: apt
        version: 0.3.13-1.2
    libtxc-dxtn-s2tc0:
    -   arch: amd64
        category: libs
        name: libtxc-dxtn-s2tc0
        origin: Ubuntu
        source: apt
        version: 0~git20131104-1.1
    libubsan0:
    -   arch: amd64
        category: libs
        name: libubsan0
        origin: Ubuntu
        source: apt
        version: 5.4.0-6ubuntu1~16.04.12
    libudev1:
    -   arch: amd64
        category: libs
        name: libudev1
        origin: Ubuntu
        source: apt
        version: 229-4ubuntu21.28
    liburi-perl:
    -   arch: all
        category: perl
        name: liburi-perl
        origin: Ubuntu
        source: apt
        version: 1.71-1
    libusb-0.1-4:
    -   arch: amd64
        category: libs
        name: libusb-0.1-4
        origin: Ubuntu
        source: apt
        version: 2:0.1.12-28
    libusb-1.0-0:
    -   arch: amd64
        category: libs
        name: libusb-1.0-0
        origin: Ubuntu
        source: apt
        version: 2:1.0.20-1
    libusrsctp:
    -   arch: amd64
        category: utils
        name: libusrsctp
        origin: BigBlueButton
        source: apt
        version: 0.9.2-1kurento1.16.04
    libustr-1.0-1:
    -   arch: amd64
        category: libs
        name: libustr-1.0-1
        origin: Ubuntu
        source: apt
        version: 1.0.4-5
    libutempter0:
    -   arch: amd64
        category: libs
        name: libutempter0
        origin: Ubuntu
        source: apt
        version: 1.1.6-3
    libuuid1:
    -   arch: amd64
        category: libs
        name: libuuid1
        origin: Ubuntu
        source: apt
        version: 2.27.1-6ubuntu3.10
    libv4l-0:
    -   arch: amd64
        category: libs
        name: libv4l-0
        origin: Ubuntu
        source: apt
        version: 1.10.0-1
    libv4lconvert0:
    -   arch: amd64
        category: libs
        name: libv4lconvert0
        origin: Ubuntu
        source: apt
        version: 1.10.0-1
    libva-drm1:
    -   arch: amd64
        category: universe/libs
        name: libva-drm1
        origin: Ubuntu
        source: apt
        version: 1.7.0-1ubuntu0.1
    libva-x11-1:
    -   arch: amd64
        category: universe/libs
        name: libva-x11-1
        origin: Ubuntu
        source: apt
        version: 1.7.0-1ubuntu0.1
    libva1:
    -   arch: amd64
        category: universe/libs
        name: libva1
        origin: Ubuntu
        source: apt
        version: 1.7.0-1ubuntu0.1
    libvdpau1:
    -   arch: amd64
        category: libs
        name: libvdpau1
        origin: Ubuntu
        source: apt
        version: 1.1.1-3ubuntu1
    libvisio-0.1-1:
    -   arch: amd64
        category: libs
        name: libvisio-0.1-1
        origin: Ubuntu
        source: apt
        version: 0.1.5-1ubuntu1
    libvisual-0.4-0:
    -   arch: amd64
        category: libs
        name: libvisual-0.4-0
        origin: Ubuntu
        source: apt
        version: 0.4.0-8
    libvo-aacenc0:
    -   arch: amd64
        category: universe/libs
        name: libvo-aacenc0
        origin: Ubuntu
        source: apt
        version: 0.1.3-1
    libvo-amrwbenc0:
    -   arch: amd64
        category: universe/libs
        name: libvo-amrwbenc0
        origin: Ubuntu
        source: apt
        version: 0.1.3-1
    libvorbis0a:
    -   arch: amd64
        category: libs
        name: libvorbis0a
        origin: Ubuntu
        source: apt
        version: 1.3.5-3ubuntu0.2
    libvorbisenc2:
    -   arch: amd64
        category: libs
        name: libvorbisenc2
        origin: Ubuntu
        source: apt
        version: 1.3.5-3ubuntu0.2
    libvorbisfile3:
    -   arch: amd64
        category: libs
        name: libvorbisfile3
        origin: Ubuntu
        source: apt
        version: 1.3.5-3ubuntu0.2
    libvorbisidec1:
    -   arch: amd64
        category: universe/libs
        name: libvorbisidec1
        origin: Ubuntu
        source: apt
        version: 1.0.2+svn18153-0.2+deb7u1build0.16.04.1
    libvpx3:
    -   arch: amd64
        category: libs
        name: libvpx3
        origin: Ubuntu
        source: apt
        version: 1.5.0-2ubuntu1.1
    libwacom-bin:
    -   arch: amd64
        category: libs
        name: libwacom-bin
        origin: Ubuntu
        source: apt
        version: 0.22-1~ubuntu16.04.1
    libwacom-common:
    -   arch: all
        category: libs
        name: libwacom-common
        origin: Ubuntu
        source: apt
        version: 0.22-1~ubuntu16.04.1
    libwacom2:
    -   arch: amd64
        category: libs
        name: libwacom2
        origin: Ubuntu
        source: apt
        version: 0.22-1~ubuntu16.04.1
    libwavpack1:
    -   arch: amd64
        category: libs
        name: libwavpack1
        origin: Ubuntu
        source: apt
        version: 4.75.2-2ubuntu0.2
    libwayland-client0:
    -   arch: amd64
        category: libs
        name: libwayland-client0
        origin: Ubuntu
        source: apt
        version: 1.12.0-1~ubuntu16.04.3
    libwayland-cursor0:
    -   arch: amd64
        category: libs
        name: libwayland-cursor0
        origin: Ubuntu
        source: apt
        version: 1.12.0-1~ubuntu16.04.3
    libwayland-egl1-mesa:
    -   arch: amd64
        category: libs
        name: libwayland-egl1-mesa
        origin: Ubuntu
        source: apt
        version: 18.0.5-0ubuntu0~16.04.1
    libwayland-server0:
    -   arch: amd64
        category: libs
        name: libwayland-server0
        origin: Ubuntu
        source: apt
        version: 1.12.0-1~ubuntu16.04.3
    libwbclient0:
    -   arch: amd64
        category: libs
        name: libwbclient0
        origin: Ubuntu
        source: apt
        version: 2:4.3.11+dfsg-0ubuntu0.16.04.27
    libwebp5:
    -   arch: amd64
        category: libs
        name: libwebp5
        origin: Ubuntu
        source: apt
        version: 0.4.4-1
    libwildmidi-config:
    -   arch: all
        category: universe/misc
        name: libwildmidi-config
        origin: Ubuntu
        source: apt
        version: 0.3.8-2
    libwildmidi1:
    -   arch: amd64
        category: universe/libs
        name: libwildmidi1
        origin: Ubuntu
        source: apt
        version: 0.3.8-2
    libwind0-heimdal:
    -   arch: amd64
        category: libs
        name: libwind0-heimdal
        origin: Ubuntu
        source: apt
        version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
    libwmf0.2-7:
    -   arch: amd64
        category: libs
        name: libwmf0.2-7
        origin: Ubuntu
        source: apt
        version: 0.2.8.4-10.5ubuntu1
    libwpd-0.10-10:
    -   arch: amd64
        category: libs
        name: libwpd-0.10-10
        origin: Ubuntu
        source: apt
        version: 0.10.1-1ubuntu1
    libwpg-0.3-3:
    -   arch: amd64
        category: libs
        name: libwpg-0.3-3
        origin: Ubuntu
        source: apt
        version: 0.3.1-1ubuntu1
    libwps-0.4-4:
    -   arch: amd64
        category: libs
        name: libwps-0.4-4
        origin: Ubuntu
        source: apt
        version: 0.4.3-1ubuntu1
    libwrap0:
    -   arch: amd64
        category: libs
        name: libwrap0
        origin: Ubuntu
        source: apt
        version: 7.6.q-25
    libwww-perl:
    -   arch: all
        category: perl
        name: libwww-perl
        origin: Ubuntu
        source: apt
        version: 6.15-1
    libwww-robotrules-perl:
    -   arch: all
        category: perl
        name: libwww-robotrules-perl
        origin: Ubuntu
        source: apt
        version: 6.01-1
    libwxbase3.0-0v5:
    -   arch: amd64
        category: universe/libs
        name: libwxbase3.0-0v5
        origin: Ubuntu
        source: apt
        version: 3.0.2+dfsg-1.3ubuntu0.1
    libwxgtk3.0-0v5:
    -   arch: amd64
        category: universe/libs
        name: libwxgtk3.0-0v5
        origin: Ubuntu
        source: apt
        version: 3.0.2+dfsg-1.3ubuntu0.1
    libx11-6:
    -   arch: amd64
        category: libs
        name: libx11-6
        origin: Ubuntu
        source: apt
        version: 2:1.6.3-1ubuntu2.1
    libx11-data:
    -   arch: all
        category: x11
        name: libx11-data
        origin: Ubuntu
        source: apt
        version: 2:1.6.3-1ubuntu2.1
    libx11-xcb1:
    -   arch: amd64
        category: libs
        name: libx11-xcb1
        origin: Ubuntu
        source: apt
        version: 2:1.6.3-1ubuntu2.1
    libx264-148:
    -   arch: amd64
        category: universe/libs
        name: libx264-148
        origin: Ubuntu
        source: apt
        version: 2:0.148.2643+git5c65704-1
    libx265-79:
    -   arch: amd64
        category: universe/libs
        name: libx265-79
        origin: Ubuntu
        source: apt
        version: 1.9-3
    libxapian22v5:
    -   arch: amd64
        category: libs
        name: libxapian22v5
        origin: Ubuntu
        source: apt
        version: 1.2.22-2
    libxau6:
    -   arch: amd64
        category: libs
        name: libxau6
        origin: Ubuntu
        source: apt
        version: 1:1.0.8-1
    libxcb-dri2-0:
    -   arch: amd64
        category: libs
        name: libxcb-dri2-0
        origin: Ubuntu
        source: apt
        version: 1.11.1-1ubuntu1
    libxcb-dri3-0:
    -   arch: amd64
        category: libs
        name: libxcb-dri3-0
        origin: Ubuntu
        source: apt
        version: 1.11.1-1ubuntu1
    libxcb-glx0:
    -   arch: amd64
        category: libs
        name: libxcb-glx0
        origin: Ubuntu
        source: apt
        version: 1.11.1-1ubuntu1
    libxcb-icccm4:
    -   arch: amd64
        category: libs
        name: libxcb-icccm4
        origin: Ubuntu
        source: apt
        version: 0.4.1-1ubuntu1
    libxcb-image0:
    -   arch: amd64
        category: libdevel
        name: libxcb-image0
        origin: Ubuntu
        source: apt
        version: 0.4.0-1build1
    libxcb-keysyms1:
    -   arch: amd64
        category: libs
        name: libxcb-keysyms1
        origin: Ubuntu
        source: apt
        version: 0.4.0-1
    libxcb-present0:
    -   arch: amd64
        category: libs
        name: libxcb-present0
        origin: Ubuntu
        source: apt
        version: 1.11.1-1ubuntu1
    libxcb-randr0:
    -   arch: amd64
        category: libs
        name: libxcb-randr0
        origin: Ubuntu
        source: apt
        version: 1.11.1-1ubuntu1
    libxcb-render-util0:
    -   arch: amd64
        category: libs
        name: libxcb-render-util0
        origin: Ubuntu
        source: apt
        version: 0.3.9-1
    libxcb-render0:
    -   arch: amd64
        category: libs
        name: libxcb-render0
        origin: Ubuntu
        source: apt
        version: 1.11.1-1ubuntu1
    libxcb-shape0:
    -   arch: amd64
        category: libs
        name: libxcb-shape0
        origin: Ubuntu
        source: apt
        version: 1.11.1-1ubuntu1
    libxcb-shm0:
    -   arch: amd64
        category: libs
        name: libxcb-shm0
        origin: Ubuntu
        source: apt
        version: 1.11.1-1ubuntu1
    libxcb-sync1:
    -   arch: amd64
        category: libs
        name: libxcb-sync1
        origin: Ubuntu
        source: apt
        version: 1.11.1-1ubuntu1
    libxcb-util1:
    -   arch: amd64
        category: libs
        name: libxcb-util1
        origin: Ubuntu
        source: apt
        version: 0.4.0-0ubuntu3
    libxcb-xfixes0:
    -   arch: amd64
        category: libs
        name: libxcb-xfixes0
        origin: Ubuntu
        source: apt
        version: 1.11.1-1ubuntu1
    libxcb-xkb1:
    -   arch: amd64
        category: libs
        name: libxcb-xkb1
        origin: Ubuntu
        source: apt
        version: 1.11.1-1ubuntu1
    libxcb1:
    -   arch: amd64
        category: libs
        name: libxcb1
        origin: Ubuntu
        source: apt
        version: 1.11.1-1ubuntu1
    libxcomposite1:
    -   arch: amd64
        category: libs
        name: libxcomposite1
        origin: Ubuntu
        source: apt
        version: 1:0.4.4-1
    libxcursor1:
    -   arch: amd64
        category: libs
        name: libxcursor1
        origin: Ubuntu
        source: apt
        version: 1:1.1.14-1ubuntu0.16.04.2
    libxdamage1:
    -   arch: amd64
        category: libs
        name: libxdamage1
        origin: Ubuntu
        source: apt
        version: 1:1.1.4-2
    libxdmcp6:
    -   arch: amd64
        category: libs
        name: libxdmcp6
        origin: Ubuntu
        source: apt
        version: 1:1.1.2-1.1
    libxext6:
    -   arch: amd64
        category: libs
        name: libxext6
        origin: Ubuntu
        source: apt
        version: 2:1.3.3-1
    libxfixes3:
    -   arch: amd64
        category: libs
        name: libxfixes3
        origin: Ubuntu
        source: apt
        version: 1:5.0.1-2
    libxfont1:
    -   arch: amd64
        category: libs
        name: libxfont1
        origin: Ubuntu
        source: apt
        version: 1:1.5.1-1ubuntu0.16.04.4
    libxi6:
    -   arch: amd64
        category: libs
        name: libxi6
        origin: Ubuntu
        source: apt
        version: 2:1.7.6-1
    libxinerama1:
    -   arch: amd64
        category: libs
        name: libxinerama1
        origin: Ubuntu
        source: apt
        version: 2:1.1.3-1
    libxkbcommon-x11-0:
    -   arch: amd64
        category: libs
        name: libxkbcommon-x11-0
        origin: Ubuntu
        source: apt
        version: 0.5.0-1ubuntu2.1
    libxkbcommon0:
    -   arch: amd64
        category: libs
        name: libxkbcommon0
        origin: Ubuntu
        source: apt
        version: 0.5.0-1ubuntu2.1
    libxml2:
    -   arch: amd64
        category: libs
        name: libxml2
        origin: Ubuntu
        source: apt
        version: 2.9.3+dfsg1-1ubuntu0.7
    libxml2-dev:
    -   arch: amd64
        category: libdevel
        name: libxml2-dev
        origin: Ubuntu
        source: apt
        version: 2.9.3+dfsg1-1ubuntu0.7
    libxmu6:
    -   arch: amd64
        category: libs
        name: libxmu6
        origin: Ubuntu
        source: apt
        version: 2:1.1.2-2
    libxpm4:
    -   arch: amd64
        category: libs
        name: libxpm4
        origin: Ubuntu
        source: apt
        version: 1:3.5.11-1ubuntu0.16.04.1
    libxrandr2:
    -   arch: amd64
        category: libs
        name: libxrandr2
        origin: Ubuntu
        source: apt
        version: 2:1.5.0-1
    libxrender1:
    -   arch: amd64
        category: libs
        name: libxrender1
        origin: Ubuntu
        source: apt
        version: 1:0.9.9-0ubuntu1
    libxshmfence1:
    -   arch: amd64
        category: libs
        name: libxshmfence1
        origin: Ubuntu
        source: apt
        version: 1.2-1
    libxslt1-dev:
    -   arch: amd64
        category: libdevel
        name: libxslt1-dev
        origin: Ubuntu
        source: apt
        version: 1.1.28-2.1ubuntu0.3
    libxslt1.1:
    -   arch: amd64
        category: libs
        name: libxslt1.1
        origin: Ubuntu
        source: apt
        version: 1.1.28-2.1ubuntu0.3
    libxss1:
    -   arch: amd64
        category: libs
        name: libxss1
        origin: Ubuntu
        source: apt
        version: 1:1.2.2-1
    libxt6:
    -   arch: amd64
        category: libs
        name: libxt6
        origin: Ubuntu
        source: apt
        version: 1:1.1.5-0ubuntu1
    libxtables11:
    -   arch: amd64
        category: net
        name: libxtables11
        origin: Ubuntu
        source: apt
        version: 1.6.0-2ubuntu3
    libxtst6:
    -   arch: amd64
        category: libs
        name: libxtst6
        origin: Ubuntu
        source: apt
        version: 2:1.2.2-1
    libxv1:
    -   arch: amd64
        category: libs
        name: libxv1
        origin: Ubuntu
        source: apt
        version: 2:1.0.10-1
    libxvidcore4:
    -   arch: amd64
        category: universe/libs
        name: libxvidcore4
        origin: Ubuntu
        source: apt
        version: 2:1.3.4-1
    libxvmc1:
    -   arch: amd64
        category: libs
        name: libxvmc1
        origin: Ubuntu
        source: apt
        version: 2:1.0.9-1ubuntu1
    libxxf86dga1:
    -   arch: amd64
        category: libs
        name: libxxf86dga1
        origin: Ubuntu
        source: apt
        version: 2:1.1.4-1
    libxxf86vm1:
    -   arch: amd64
        category: libs
        name: libxxf86vm1
        origin: Ubuntu
        source: apt
        version: 1:1.1.4-1
    libyajl2:
    -   arch: amd64
        category: libs
        name: libyajl2
        origin: Ubuntu
        source: apt
        version: 2.1.0-2
    libyaml-0-2:
    -   arch: amd64
        category: libs
        name: libyaml-0-2
        origin: Ubuntu
        source: apt
        version: 0.1.6-3
    libzbar0:
    -   arch: amd64
        category: universe/libs
        name: libzbar0
        origin: Ubuntu
        source: apt
        version: 0.10+doc-10ubuntu1
    libzvbi-common:
    -   arch: all
        category: universe/devel
        name: libzvbi-common
        origin: Ubuntu
        source: apt
        version: 0.2.35-10
    libzvbi0:
    -   arch: amd64
        category: universe/libs
        name: libzvbi0
        origin: Ubuntu
        source: apt
        version: 0.2.35-10
    linux-base:
    -   arch: all
        category: kernel
        name: linux-base
        origin: Ubuntu
        source: apt
        version: 4.5ubuntu1.1~16.04.1
    linux-firmware:
    -   arch: all
        category: misc
        name: linux-firmware
        origin: Ubuntu
        source: apt
        version: 1.157.23
    linux-image-4.4.0-179-generic:
    -   arch: amd64
        category: kernel
        name: linux-image-4.4.0-179-generic
        origin: Ubuntu
        source: apt
        version: 4.4.0-179.209
    linux-image-generic:
    -   arch: amd64
        category: kernel
        name: linux-image-generic
        origin: Ubuntu
        source: apt
        version: 4.4.0.179.187
    linux-libc-dev:
    -   arch: amd64
        category: devel
        name: linux-libc-dev
        origin: Ubuntu
        source: apt
        version: 4.4.0-179.209
    linux-modules-4.4.0-179-generic:
    -   arch: amd64
        category: kernel
        name: linux-modules-4.4.0-179-generic
        origin: Ubuntu
        source: apt
        version: 4.4.0-179.209
    linux-modules-extra-4.4.0-179-generic:
    -   arch: amd64
        category: kernel
        name: linux-modules-extra-4.4.0-179-generic
        origin: Ubuntu
        source: apt
        version: 4.4.0-179.209
    linuxlogo:
    -   arch: amd64
        category: universe/misc
        name: linuxlogo
        origin: Ubuntu
        source: apt
        version: 5.11-8
    locales:
    -   arch: all
        category: libs
        name: locales
        origin: Ubuntu
        source: apt
        version: 2.23-0ubuntu11
    login:
    -   arch: amd64
        category: admin
        name: login
        origin: Ubuntu
        source: apt
        version: 1:4.2-3.1ubuntu5.4
    logrotate:
    -   arch: amd64
        category: admin
        name: logrotate
        origin: Ubuntu
        source: apt
        version: 3.8.7-2ubuntu2.16.04.2
    lp-solve:
    -   arch: amd64
        category: math
        name: lp-solve
        origin: Ubuntu
        source: apt
        version: 5.5.0.13-7build2
    lsb-base:
    -   arch: all
        category: misc
        name: lsb-base
        origin: Ubuntu
        source: apt
        version: 9.20160110ubuntu0.2
    lsb-release:
    -   arch: all
        category: misc
        name: lsb-release
        origin: Ubuntu
        source: apt
        version: 9.20160110ubuntu0.2
    lvm2:
    -   arch: amd64
        category: admin
        name: lvm2
        origin: Ubuntu
        source: apt
        version: 2.02.133-1ubuntu10
    make:
    -   arch: amd64
        category: devel
        name: make
        origin: Ubuntu
        source: apt
        version: 4.1-6
    makedev:
    -   arch: all
        category: admin
        name: makedev
        origin: Ubuntu
        source: apt
        version: 2.3.1-93ubuntu2~ubuntu16.04.1
    manpages:
    -   arch: all
        category: doc
        name: manpages
        origin: Ubuntu
        source: apt
        version: 4.04-2
    manpages-dev:
    -   arch: all
        category: doc
        name: manpages-dev
        origin: Ubuntu
        source: apt
        version: 4.04-2
    mawk:
    -   arch: amd64
        category: utils
        name: mawk
        origin: Ubuntu
        source: apt
        version: 1.3.3-17ubuntu2
    mdadm:
    -   arch: amd64
        category: admin
        name: mdadm
        origin: Ubuntu
        source: apt
        version: 3.3-2ubuntu7.6
    memtest86+:
    -   arch: amd64
        category: misc
        name: memtest86+
        origin: Ubuntu
        source: apt
        version: 5.01-3ubuntu2
    mencoder:
    -   arch: amd64
        category: universe/video
        name: mencoder
        origin: Ubuntu
        source: apt
        version: 2:1.2.1-1ubuntu1.1
    mesa-va-drivers:
    -   arch: amd64
        category: universe/libs
        name: mesa-va-drivers
        origin: Ubuntu
        source: apt
        version: 18.0.5-0ubuntu0~16.04.1
    mesa-vdpau-drivers:
    -   arch: amd64
        category: libs
        name: mesa-vdpau-drivers
        origin: Ubuntu
        source: apt
        version: 18.0.5-0ubuntu0~16.04.1
    mime-support:
    -   arch: all
        category: net
        name: mime-support
        origin: Ubuntu
        source: apt
        version: 3.59ubuntu1
    mongodb-org:
    -   arch: amd64
        category: database
        name: mongodb-org
        origin: mongodb
        source: apt
        version: 3.4.24
    mongodb-org-mongos:
    -   arch: amd64
        category: database
        name: mongodb-org-mongos
        origin: mongodb
        source: apt
        version: 3.4.24
    mongodb-org-server:
    -   arch: amd64
        category: database
        name: mongodb-org-server
        origin: mongodb
        source: apt
        version: 3.4.24
    mongodb-org-shell:
    -   arch: amd64
        category: database
        name: mongodb-org-shell
        origin: mongodb
        source: apt
        version: 3.4.24
    mongodb-org-tools:
    -   arch: amd64
        category: database
        name: mongodb-org-tools
        origin: mongodb
        source: apt
        version: 3.4.24
    mount:
    -   arch: amd64
        category: admin
        name: mount
        origin: Ubuntu
        source: apt
        version: 2.27.1-6ubuntu3.10
    mplayer:
    -   arch: amd64
        category: universe/video
        name: mplayer
        origin: Ubuntu
        source: apt
        version: 2:1.2.1-1ubuntu1.1
    multiarch-support:
    -   arch: amd64
        category: libs
        name: multiarch-support
        origin: Ubuntu
        source: apt
        version: 2.23-0ubuntu11
    munin-common:
    -   arch: all
        category: universe/net
        name: munin-common
        origin: Ubuntu
        source: apt
        version: 2.0.25-2ubuntu0.16.04.3
    munin-node:
    -   arch: all
        category: universe/net
        name: munin-node
        origin: Ubuntu
        source: apt
        version: 2.0.25-2ubuntu0.16.04.3
    munin-plugins-c:
    -   arch: amd64
        category: universe/net
        name: munin-plugins-c
        origin: Ubuntu
        source: apt
        version: 0.0.9-1
    munin-plugins-core:
    -   arch: all
        category: universe/net
        name: munin-plugins-core
        origin: Ubuntu
        source: apt
        version: 2.0.25-2ubuntu0.16.04.3
    munin-plugins-extra:
    -   arch: all
        category: universe/net
        name: munin-plugins-extra
        origin: Ubuntu
        source: apt
        version: 2.0.25-2ubuntu0.16.04.3
    mutt:
    -   arch: amd64
        category: mail
        name: mutt
        origin: Ubuntu
        source: apt
        version: 1.5.24-1ubuntu0.2
    ncurses-base:
    -   arch: all
        category: utils
        name: ncurses-base
        origin: Ubuntu
        source: apt
        version: 6.0+20160213-1ubuntu1
    ncurses-bin:
    -   arch: amd64
        category: utils
        name: ncurses-bin
        origin: Ubuntu
        source: apt
        version: 6.0+20160213-1ubuntu1
    net-tools:
    -   arch: amd64
        category: net
        name: net-tools
        origin: Ubuntu
        source: apt
        version: 1.60-26ubuntu1
    netbase:
    -   arch: all
        category: admin
        name: netbase
        origin: Ubuntu
        source: apt
        version: '5.3'
    netcat-openbsd:
    -   arch: amd64
        category: net
        name: netcat-openbsd
        origin: Ubuntu
        source: apt
        version: 1.105-7ubuntu1
    netpbm:
    -   arch: amd64
        category: graphics
        name: netpbm
        origin: Ubuntu
        source: apt
        version: 2:10.0-15.3
    nginx-common:
    -   arch: all
        category: httpd
        name: nginx-common
        origin: Ubuntu
        source: apt
        version: 1.10.3-0ubuntu0.16.04.5
    nginx-extras:
    -   arch: amd64
        category: universe/httpd
        name: nginx-extras
        origin: Ubuntu
        source: apt
        version: 1.10.3-0ubuntu0.16.04.5
    nmon:
    -   arch: amd64
        category: universe/utils
        name: nmon
        origin: Ubuntu
        source: apt
        version: 14g+debian-1build1
    nodejs:
    -   arch: amd64
        category: web
        name: nodejs
        origin: Node Source
        source: apt
        version: 8.17.0-1nodesource1
    notification-daemon:
    -   arch: amd64
        category: x11
        name: notification-daemon
        origin: Ubuntu
        source: apt
        version: 3.18.2-1
    opencv-data:
    -   arch: all
        category: universe/libdevel
        name: opencv-data
        origin: Ubuntu
        source: apt
        version: 2.4.9.1+dfsg-1.5ubuntu1.1
    openh264:
    -   arch: amd64
        category: utils
        name: openh264
        origin: BigBlueButton
        source: apt
        version: 1.4.0-1kurento1.16.04
    openh264-gst-plugins-bad-1.5:
    -   arch: amd64
        category: libs
        name: openh264-gst-plugins-bad-1.5
        origin: BigBlueButton
        source: apt
        version: 1.8.1-1kurento4.16.04
    openjdk-8-jre:
    -   arch: amd64
        category: java
        name: openjdk-8-jre
        origin: Ubuntu
        source: apt
        version: 8u252-b09-1~16.04
    openjdk-8-jre-headless:
    -   arch: amd64
        category: java
        name: openjdk-8-jre-headless
        origin: Ubuntu
        source: apt
        version: 8u252-b09-1~16.04
    openssh-client:
    -   arch: amd64
        category: net
        name: openssh-client
        origin: Ubuntu
        source: apt
        version: 1:7.2p2-4ubuntu2.8
    openssh-server:
    -   arch: amd64
        category: net
        name: openssh-server
        origin: Ubuntu
        source: apt
        version: 1:7.2p2-4ubuntu2.8
    openssh-sftp-server:
    -   arch: amd64
        category: net
        name: openssh-sftp-server
        origin: Ubuntu
        source: apt
        version: 1:7.2p2-4ubuntu2.8
    openssl:
    -   arch: amd64
        category: utils
        name: openssl
        origin: Ubuntu
        source: apt
        version: 1.0.2g-1ubuntu4.16
    openwebrtc-gst-plugins:
    -   arch: amd64
        category: utils
        name: openwebrtc-gst-plugins
        origin: BigBlueButton
        source: apt
        version: 0.10.0-1kurento1.16.04
    os-prober:
    -   arch: amd64
        category: utils
        name: os-prober
        origin: Ubuntu
        source: apt
        version: 1.70ubuntu3.3
    passwd:
    -   arch: amd64
        category: admin
        name: passwd
        origin: Ubuntu
        source: apt
        version: 1:4.2-3.1ubuntu5.4
    patch:
    -   arch: amd64
        category: utils
        name: patch
        origin: Ubuntu
        source: apt
        version: 2.7.5-1ubuntu0.16.04.2
    pciutils:
    -   arch: amd64
        category: admin
        name: pciutils
        origin: Ubuntu
        source: apt
        version: 1:3.3.1-1.1ubuntu1.3
    perl:
    -   arch: amd64
        category: perl
        name: perl
        origin: Ubuntu
        source: apt
        version: 5.22.1-9ubuntu0.6
    perl-base:
    -   arch: amd64
        category: perl
        name: perl-base
        origin: Ubuntu
        source: apt
        version: 5.22.1-9ubuntu0.6
    perl-modules-5.22:
    -   arch: all
        category: perl
        name: perl-modules-5.22
        origin: Ubuntu
        source: apt
        version: 5.22.1-9ubuntu0.6
    pigz:
    -   arch: amd64
        category: universe/utils
        name: pigz
        origin: Ubuntu
        source: apt
        version: 2.3.1-2
    pinentry-curses:
    -   arch: amd64
        category: utils
        name: pinentry-curses
        origin: Ubuntu
        source: apt
        version: 0.9.7-3
    plymouth:
    -   arch: amd64
        category: x11
        name: plymouth
        origin: Ubuntu
        source: apt
        version: 0.9.2-3ubuntu13.5
    policykit-1:
    -   arch: amd64
        category: admin
        name: policykit-1
        origin: Ubuntu
        source: apt
        version: 0.105-14.1ubuntu0.5
    poppler-data:
    -   arch: all
        category: misc
        name: poppler-data
        origin: Ubuntu
        source: apt
        version: 0.4.7-7
    poppler-utils:
    -   arch: amd64
        category: utils
        name: poppler-utils
        origin: Ubuntu
        source: apt
        version: 0.41.0-0ubuntu1.14
    powermgmt-base:
    -   arch: all
        category: utils
        name: powermgmt-base
        origin: Ubuntu
        source: apt
        version: 1.31+nmu1
    procinfo:
    -   arch: amd64
        category: universe/utils
        name: procinfo
        origin: Ubuntu
        source: apt
        version: 1:2.0.304-1ubuntu2
    procps:
    -   arch: amd64
        category: admin
        name: procps
        origin: Ubuntu
        source: apt
        version: 2:3.3.10-4ubuntu2.5
    psmisc:
    -   arch: amd64
        category: admin
        name: psmisc
        origin: Ubuntu
        source: apt
        version: 22.21-2.1ubuntu0.1
    pwgen:
    -   arch: amd64
        category: universe/admin
        name: pwgen
        origin: Ubuntu
        source: apt
        version: 2.07-1.1ubuntu1
    python:
    -   arch: amd64
        category: python
        name: python
        origin: Ubuntu
        source: apt
        version: 2.7.12-1~16.04
    python-apt:
    -   arch: amd64
        category: python
        name: python-apt
        origin: Ubuntu
        source: apt
        version: 1.1.0~beta1ubuntu0.16.04.9
    python-apt-common:
    -   arch: all
        category: python
        name: python-apt-common
        origin: Ubuntu
        source: apt
        version: 1.1.0~beta1ubuntu0.16.04.9
    python-backports.ssl-match-hostname:
    -   arch: all
        category: universe/python
        name: python-backports.ssl-match-hostname
        origin: Ubuntu
        source: apt
        version: 3.4.0.2-1
    python-cffi-backend:
    -   arch: amd64
        category: python
        name: python-cffi-backend
        origin: Ubuntu
        source: apt
        version: 1.5.2-1ubuntu1
    python-chardet:
    -   arch: all
        category: python
        name: python-chardet
        origin: Ubuntu
        source: apt
        version: 2.3.0-2
    python-cryptography:
    -   arch: amd64
        category: python
        name: python-cryptography
        origin: Ubuntu
        source: apt
        version: 1.2.3-1ubuntu0.2
    python-dns:
    -   arch: all
        category: universe/python
        name: python-dns
        origin: Ubuntu
        source: apt
        version: 2.3.6-3
    python-dnspython:
    -   arch: all
        category: python
        name: python-dnspython
        origin: Ubuntu
        source: apt
        version: 1.12.0-1
    python-docker:
    -   arch: all
        category: universe/python
        name: python-docker
        origin: Ubuntu
        source: apt
        version: 1.9.0-1~16.04.1
    python-enum34:
    -   arch: all
        category: python
        name: python-enum34
        origin: Ubuntu
        source: apt
        version: 1.1.2-1
    python-idna:
    -   arch: all
        category: python
        name: python-idna
        origin: Ubuntu
        source: apt
        version: 2.0-3
    python-ipaddress:
    -   arch: all
        category: python
        name: python-ipaddress
        origin: Ubuntu
        source: apt
        version: 1.0.16-1
    python-minimal:
    -   arch: amd64
        category: python
        name: python-minimal
        origin: Ubuntu
        source: apt
        version: 2.7.12-1~16.04
    python-ndg-httpsclient:
    -   arch: all
        category: python
        name: python-ndg-httpsclient
        origin: Ubuntu
        source: apt
        version: 0.4.0-3
    python-openssl:
    -   arch: all
        category: python
        name: python-openssl
        origin: Ubuntu
        source: apt
        version: 0.15.1-2ubuntu0.2
    python-passlib:
    -   arch: all
        category: python
        name: python-passlib
        origin: Ubuntu
        source: apt
        version: 1.6.5-4
    python-pkg-resources:
    -   arch: all
        category: python
        name: python-pkg-resources
        origin: Ubuntu
        source: apt
        version: 20.7.0-1
    python-pyasn1:
    -   arch: all
        category: python
        name: python-pyasn1
        origin: Ubuntu
        source: apt
        version: 0.1.9-1
    python-requests:
    -   arch: all
        category: python
        name: python-requests
        origin: Ubuntu
        source: apt
        version: 2.9.1-3ubuntu0.1
    python-six:
    -   arch: all
        category: python
        name: python-six
        origin: Ubuntu
        source: apt
        version: 1.10.0-3
    python-talloc:
    -   arch: amd64
        category: python
        name: python-talloc
        origin: Ubuntu
        source: apt
        version: 2.1.5-2
    python-urllib3:
    -   arch: all
        category: python
        name: python-urllib3
        origin: Ubuntu
        source: apt
        version: 1.13.1-2ubuntu0.16.04.3
    python-websocket:
    -   arch: all
        category: universe/python
        name: python-websocket
        origin: Ubuntu
        source: apt
        version: 0.18.0-2
    python2.7:
    -   arch: amd64
        category: python
        name: python2.7
        origin: Ubuntu
        source: apt
        version: 2.7.12-1ubuntu0~16.04.11
    python2.7-minimal:
    -   arch: amd64
        category: python
        name: python2.7-minimal
        origin: Ubuntu
        source: apt
        version: 2.7.12-1ubuntu0~16.04.11
    python3:
    -   arch: amd64
        category: python
        name: python3
        origin: Ubuntu
        source: apt
        version: 3.5.1-3
    python3-apt:
    -   arch: amd64
        category: python
        name: python3-apt
        origin: Ubuntu
        source: apt
        version: 1.1.0~beta1ubuntu0.16.04.9
    python3-bs4:
    -   arch: all
        category: python
        name: python3-bs4
        origin: Ubuntu
        source: apt
        version: 4.4.1-1
    python3-chardet:
    -   arch: all
        category: python
        name: python3-chardet
        origin: Ubuntu
        source: apt
        version: 2.3.0-2
    python3-dbus:
    -   arch: amd64
        category: python
        name: python3-dbus
        origin: Ubuntu
        source: apt
        version: 1.2.0-3
    python3-debian:
    -   arch: all
        category: python
        name: python3-debian
        origin: Ubuntu
        source: apt
        version: 0.1.27ubuntu2
    python3-distupgrade:
    -   arch: all
        category: python
        name: python3-distupgrade
        origin: Ubuntu
        source: apt
        version: 1:16.04.30
    python3-dnspython:
    -   arch: all
        category: python
        name: python3-dnspython
        origin: Ubuntu
        source: apt
        version: 1.12.0-0ubuntu3
    python3-docker:
    -   arch: all
        category: universe/python
        name: python3-docker
        origin: Ubuntu
        source: apt
        version: 1.9.0-1~16.04.1
    python3-gi:
    -   arch: amd64
        category: python
        name: python3-gi
        origin: Ubuntu
        source: apt
        version: 3.20.0-0ubuntu1
    python3-html5lib:
    -   arch: all
        category: python
        name: python3-html5lib
        origin: Ubuntu
        source: apt
        version: 0.999-4
    python3-icu:
    -   arch: amd64
        category: python
        name: python3-icu
        origin: Ubuntu
        source: apt
        version: 1.9.2-2build1
    python3-lxml:
    -   arch: amd64
        category: python
        name: python3-lxml
        origin: Ubuntu
        source: apt
        version: 3.5.0-1ubuntu0.1
    python3-minimal:
    -   arch: amd64
        category: python
        name: python3-minimal
        origin: Ubuntu
        source: apt
        version: 3.5.1-3
    python3-pkg-resources:
    -   arch: all
        category: python
        name: python3-pkg-resources
        origin: Ubuntu
        source: apt
        version: 20.7.0-1
    python3-pycurl:
    -   arch: amd64
        category: python
        name: python3-pycurl
        origin: Ubuntu
        source: apt
        version: 7.43.0-1ubuntu1
    python3-requests:
    -   arch: all
        category: python
        name: python3-requests
        origin: Ubuntu
        source: apt
        version: 2.9.1-3ubuntu0.1
    python3-six:
    -   arch: all
        category: python
        name: python3-six
        origin: Ubuntu
        source: apt
        version: 1.10.0-3
    python3-software-properties:
    -   arch: all
        category: python
        name: python3-software-properties
        origin: Ubuntu
        source: apt
        version: 0.96.20.9
    python3-uno:
    -   arch: amd64
        category: python
        name: python3-uno
        origin: Ubuntu
        source: apt
        version: 1:5.1.6~rc2-0ubuntu1~xenial10
    python3-update-manager:
    -   arch: all
        category: python
        name: python3-update-manager
        origin: Ubuntu
        source: apt
        version: 1:16.04.17
    python3-urllib3:
    -   arch: all
        category: python
        name: python3-urllib3
        origin: Ubuntu
        source: apt
        version: 1.13.1-2ubuntu0.16.04.3
    python3-websocket:
    -   arch: all
        category: universe/python
        name: python3-websocket
        origin: Ubuntu
        source: apt
        version: 0.18.0-2
    python3.5:
    -   arch: amd64
        category: python
        name: python3.5
        origin: Ubuntu
        source: apt
        version: 3.5.2-2ubuntu0~16.04.10
    python3.5-minimal:
    -   arch: amd64
        category: python
        name: python3.5-minimal
        origin: Ubuntu
        source: apt
        version: 3.5.2-2ubuntu0~16.04.10
    qttranslations5-l10n:
    -   arch: all
        category: localization
        name: qttranslations5-l10n
        origin: Ubuntu
        source: apt
        version: 5.5.1-2build1
    rake:
    -   arch: all
        category: devel
        name: rake
        origin: Ubuntu
        source: apt
        version: 10.5.0-2ubuntu0.1
    readline-common:
    -   arch: all
        category: utils
        name: readline-common
        origin: Ubuntu
        source: apt
        version: 6.3-8ubuntu2
    redis-server:
    -   arch: amd64
        category: universe/misc
        name: redis-server
        origin: Ubuntu
        source: apt
        version: 2:3.0.6-1ubuntu0.4
    redis-tools:
    -   arch: amd64
        category: universe/database
        name: redis-tools
        origin: Ubuntu
        source: apt
        version: 2:3.0.6-1ubuntu0.4
    rename:
    -   arch: all
        category: perl
        name: rename
        origin: Ubuntu
        source: apt
        version: 0.20-4
    resolvconf:
    -   arch: all
        category: net
        name: resolvconf
        origin: Ubuntu
        source: apt
        version: 1.78ubuntu7
    rsync:
    -   arch: amd64
        category: net
        name: rsync
        origin: Ubuntu
        source: apt
        version: 3.1.1-3ubuntu1.3
    rsyslog:
    -   arch: amd64
        category: admin
        name: rsyslog
        origin: Ubuntu
        source: apt
        version: 8.16.0-1ubuntu3.1
    ruby:
    -   arch: all
        category: interpreters
        name: ruby
        origin: Ubuntu
        source: apt
        version: 1:2.3.0+1
    ruby-dev:
    -   arch: amd64
        category: devel
        name: ruby-dev
        origin: Ubuntu
        source: apt
        version: 1:2.3.0+1
    ruby-did-you-mean:
    -   arch: all
        category: ruby
        name: ruby-did-you-mean
        origin: Ubuntu
        source: apt
        version: 1.0.0-2
    ruby-minitest:
    -   arch: all
        category: ruby
        name: ruby-minitest
        origin: Ubuntu
        source: apt
        version: 5.8.4-2
    ruby-net-telnet:
    -   arch: all
        category: ruby
        name: ruby-net-telnet
        origin: Ubuntu
        source: apt
        version: 0.1.1-2
    ruby-power-assert:
    -   arch: all
        category: ruby
        name: ruby-power-assert
        origin: Ubuntu
        source: apt
        version: 0.2.7-1
    ruby-test-unit:
    -   arch: all
        category: ruby
        name: ruby-test-unit
        origin: Ubuntu
        source: apt
        version: 3.1.7-2
    ruby2.3:
    -   arch: amd64
        category: ruby
        name: ruby2.3
        origin: Ubuntu
        source: apt
        version: 2.3.1-2~ubuntu16.04.14
    ruby2.3-dev:
    -   arch: amd64
        category: ruby
        name: ruby2.3-dev
        origin: Ubuntu
        source: apt
        version: 2.3.1-2~ubuntu16.04.14
    rubygems-integration:
    -   arch: all
        category: ruby
        name: rubygems-integration
        origin: Ubuntu
        source: apt
        version: '1.10'
    s-nail:
    -   arch: amd64
        category: universe/mail
        name: s-nail
        origin: Ubuntu
        source: apt
        version: 14.8.6-1
    samba-libs:
    -   arch: amd64
        category: libs
        name: samba-libs
        origin: Ubuntu
        source: apt
        version: 2:4.3.11+dfsg-0ubuntu0.16.04.27
    sed:
    -   arch: amd64
        category: utils
        name: sed
        origin: Ubuntu
        source: apt
        version: 4.2.2-7
    sensible-utils:
    -   arch: all
        category: utils
        name: sensible-utils
        origin: Ubuntu
        source: apt
        version: 0.0.9ubuntu0.16.04.1
    sgml-base:
    -   arch: all
        category: text
        name: sgml-base
        origin: Ubuntu
        source: apt
        version: 1.26+nmu4ubuntu1
    shared-mime-info:
    -   arch: amd64
        category: misc
        name: shared-mime-info
        origin: Ubuntu
        source: apt
        version: 1.5-2ubuntu0.2
    slay:
    -   arch: all
        category: universe/admin
        name: slay
        origin: Ubuntu
        source: apt
        version: 2.7.0
    software-properties-common:
    -   arch: all
        category: admin
        name: software-properties-common
        origin: Ubuntu
        source: apt
        version: 0.96.20.9
    sox:
    -   arch: amd64
        category: universe/sound
        name: sox
        origin: Ubuntu
        source: apt
        version: 14.4.1-5+deb8u4ubuntu0.1
    ssh-import-id:
    -   arch: all
        category: misc
        name: ssh-import-id
        origin: Ubuntu
        source: apt
        version: 5.5-0ubuntu1
    ssl-cert:
    -   arch: all
        category: utils
        name: ssl-cert
        origin: Ubuntu
        source: apt
        version: 1.0.37
    ssl-cert-check:
    -   arch: all
        category: universe/net
        name: ssl-cert-check
        origin: Ubuntu
        source: apt
        version: 3.27-2
    sudo:
    -   arch: amd64
        category: admin
        name: sudo
        origin: Ubuntu
        source: apt
        version: 1.8.16-0ubuntu1.9
    systemd:
    -   arch: amd64
        category: admin
        name: systemd
        origin: Ubuntu
        source: apt
        version: 229-4ubuntu21.28
    systemd-sysv:
    -   arch: amd64
        category: admin
        name: systemd-sysv
        origin: Ubuntu
        source: apt
        version: 229-4ubuntu21.28
    sysv-rc:
    -   arch: all
        category: admin
        name: sysv-rc
        origin: Ubuntu
        source: apt
        version: 2.88dsf-59.3ubuntu2
    sysvinit-utils:
    -   arch: amd64
        category: admin
        name: sysvinit-utils
        origin: Ubuntu
        source: apt
        version: 2.88dsf-59.3ubuntu2
    tar:
    -   arch: amd64
        category: utils
        name: tar
        origin: Ubuntu
        source: apt
        version: 1.28-2.1ubuntu0.1
    thermald:
    -   arch: amd64
        category: admin
        name: thermald
        origin: Ubuntu
        source: apt
        version: 1.5-2ubuntu4
    tidy:
    -   arch: amd64
        category: universe/web
        name: tidy
        origin: Ubuntu
        source: apt
        version: 20091223cvs-1.5
    time:
    -   arch: amd64
        category: utils
        name: time
        origin: Ubuntu
        source: apt
        version: 1.7-25.1
    tmux:
    -   arch: amd64
        category: admin
        name: tmux
        origin: Ubuntu
        source: apt
        version: 2.1-3build1
    ttf-mscorefonts-installer:
    -   arch: all
        category: multiverse/x11
        name: ttf-mscorefonts-installer
        origin: Ubuntu
        source: apt
        version: 3.4+nmu1ubuntu2
    tzdata:
    -   arch: all
        category: libs
        name: tzdata
        origin: Ubuntu
        source: apt
        version: 2020a-0ubuntu0.16.04
    ubuntu-advantage-tools:
    -   arch: all
        category: misc
        name: ubuntu-advantage-tools
        origin: Ubuntu
        source: apt
        version: 10ubuntu0.16.04.1
    ubuntu-keyring:
    -   arch: all
        category: misc
        name: ubuntu-keyring
        origin: Ubuntu
        source: apt
        version: 2012.05.19
    ubuntu-minimal:
    -   arch: amd64
        category: metapackages
        name: ubuntu-minimal
        origin: Ubuntu
        source: apt
        version: 1.361.4
    ubuntu-mono:
    -   arch: all
        category: gnome
        name: ubuntu-mono
        origin: Ubuntu
        source: apt
        version: 14.04+16.04.20180326-0ubuntu1
    ubuntu-release-upgrader-core:
    -   arch: all
        category: admin
        name: ubuntu-release-upgrader-core
        origin: Ubuntu
        source: apt
        version: 1:16.04.30
    ucf:
    -   arch: all
        category: utils
        name: ucf
        origin: Ubuntu
        source: apt
        version: '3.0036'
    udev:
    -   arch: amd64
        category: admin
        name: udev
        origin: Ubuntu
        source: apt
        version: 229-4ubuntu21.28
    ufw:
    -   arch: all
        category: admin
        name: ufw
        origin: Ubuntu
        source: apt
        version: 0.35-0ubuntu2
    unattended-upgrades:
    -   arch: all
        category: admin
        name: unattended-upgrades
        origin: Ubuntu
        source: apt
        version: 1.1ubuntu1.18.04.7~16.04.6
    uno-libs3:
    -   arch: amd64
        category: libs
        name: uno-libs3
        origin: Ubuntu
        source: apt
        version: 5.1.6~rc2-0ubuntu1~xenial10
    unzip:
    -   arch: amd64
        category: utils
        name: unzip
        origin: Ubuntu
        source: apt
        version: 6.0-20ubuntu1
    update-manager-core:
    -   arch: all
        category: admin
        name: update-manager-core
        origin: Ubuntu
        source: apt
        version: 1:16.04.17
    update-notifier-common:
    -   arch: all
        category: gnome
        name: update-notifier-common
        origin: Ubuntu
        source: apt
        version: 3.168.10
    ure:
    -   arch: amd64
        category: libs
        name: ure
        origin: Ubuntu
        source: apt
        version: 5.1.6~rc2-0ubuntu1~xenial10
    ureadahead:
    -   arch: amd64
        category: admin
        name: ureadahead
        origin: Ubuntu
        source: apt
        version: 0.100.0-19.1
    usbutils:
    -   arch: amd64
        category: utils
        name: usbutils
        origin: Ubuntu
        source: apt
        version: 1:007-4
    util-linux:
    -   arch: amd64
        category: utils
        name: util-linux
        origin: Ubuntu
        source: apt
        version: 2.27.1-6ubuntu3.10
    va-driver-all:
    -   arch: amd64
        category: universe/video
        name: va-driver-all
        origin: Ubuntu
        source: apt
        version: 1.7.0-1ubuntu0.1
    vdpau-driver-all:
    -   arch: amd64
        category: video
        name: vdpau-driver-all
        origin: Ubuntu
        source: apt
        version: 1.1.1-3ubuntu1
    vim:
    -   arch: amd64
        category: editors
        name: vim
        origin: Ubuntu
        source: apt
        version: 2:7.4.1689-3ubuntu1.4
    vim-common:
    -   arch: amd64
        category: editors
        name: vim-common
        origin: Ubuntu
        source: apt
        version: 2:7.4.1689-3ubuntu1.4
    vim-runtime:
    -   arch: all
        category: editors
        name: vim-runtime
        origin: Ubuntu
        source: apt
        version: 2:7.4.1689-3ubuntu1.4
    vim-tiny:
    -   arch: amd64
        category: editors
        name: vim-tiny
        origin: Ubuntu
        source: apt
        version: 2:7.4.1689-3ubuntu1.4
    vorbis-tools:
    -   arch: amd64
        category: universe/sound
        name: vorbis-tools
        origin: Ubuntu
        source: apt
        version: 1.4.0-7ubuntu1
    wget:
    -   arch: amd64
        category: web
        name: wget
        origin: Ubuntu
        source: apt
        version: 1.17.1-1ubuntu1.5
    whiptail:
    -   arch: amd64
        category: utils
        name: whiptail
        origin: Ubuntu
        source: apt
        version: 0.52.18-1ubuntu2
    whois:
    -   arch: amd64
        category: net
        name: whois
        origin: Ubuntu
        source: apt
        version: 5.2.11
    wireless-regdb:
    -   arch: all
        category: net
        name: wireless-regdb
        origin: Ubuntu
        source: apt
        version: 2018.05.09-0ubuntu1~16.04.1
    x11-common:
    -   arch: all
        category: x11
        name: x11-common
        origin: Ubuntu
        source: apt
        version: 1:7.7+13ubuntu3.1
    xdg-user-dirs:
    -   arch: amd64
        category: utils
        name: xdg-user-dirs
        origin: Ubuntu
        source: apt
        version: 0.15-2ubuntu6.16.04.1
    xfonts-encodings:
    -   arch: all
        category: x11
        name: xfonts-encodings
        origin: Ubuntu
        source: apt
        version: 1:1.0.4-2
    xfonts-utils:
    -   arch: amd64
        category: x11
        name: xfonts-utils
        origin: Ubuntu
        source: apt
        version: 1:7.7+3ubuntu0.16.04.2
    xkb-data:
    -   arch: all
        category: x11
        name: xkb-data
        origin: Ubuntu
        source: apt
        version: 2.16-1ubuntu1
    xml-core:
    -   arch: all
        category: text
        name: xml-core
        origin: Ubuntu
        source: apt
        version: 0.13+nmu2
    xmlstarlet:
    -   arch: amd64
        category: universe/text
        name: xmlstarlet
        origin: Ubuntu
        source: apt
        version: 1.6.1-1ubuntu1
    xz-utils:
    -   arch: amd64
        category: utils
        name: xz-utils
        origin: Ubuntu
        source: apt
        version: 5.1.1alpha+20120614-2ubuntu2
    yq:
    -   arch: amd64
        category: devel
        name: yq
        origin: LP-PPA-rmescandon-yq
        source: apt
        version: 3.1-2
    zip:
    -   arch: amd64
        category: utils
        name: zip
        origin: Ubuntu
        source: apt
        version: 3.0-11
    zlib1g:
    -   arch: amd64
        category: libs
        name: zlib1g
        origin: Ubuntu
        source: apt
        version: 1:1.2.8.dfsg-2ubuntu4.3
    zlib1g-dev:
    -   arch: amd64
        category: libdevel
        name: zlib1g-dev
        origin: Ubuntu
        source: apt
        version: 1:1.2.8.dfsg-2ubuntu4.3
playbook_dir: /home/chris/meet/servers
users:
    chris:
        users_editor: vim
        users_email: chris@webarchitects.co.uk
        users_groups:
        - sudo
        users_home_mode: '0750'
        users_name: Chris Croome
        users_ssh_public_keys:
        - https://git.coop/chris.keys
        users_state: present
    decentral1se:
        users_editor: vim
        users_groups:
        - sudo
        users_home_mode: '0750'
        users_ssh_public_keys:
        - https://git.coop/decentral1se.keys
        users_state: present
    root:
        users_editor: vim
        users_email: chris@webarchitects.co.uk
        users_home: /root
        users_home_mode: '0700'
        users_ssh_public_keys:
        - https://git.coop/chris.keys
        - https://git.coop/decentral1se.keys
        users_state: present


# Package facts
# The following dictionary is generated from the package_facts module
# https://docs.ansible.com/ansible/latest/modules/package_facts_module.html
acl:
-   arch: amd64
    category: utils
    name: acl
    origin: Ubuntu
    source: apt
    version: 2.2.52-3
adduser:
-   arch: all
    category: admin
    name: adduser
    origin: Ubuntu
    source: apt
    version: 3.113+nmu3ubuntu4
adwaita-icon-theme:
-   arch: all
    category: gnome
    name: adwaita-icon-theme
    origin: Ubuntu
    source: apt
    version: 3.18.0-2ubuntu3.1
aglfn:
-   arch: all
    category: universe/fonts
    name: aglfn
    origin: Ubuntu
    source: apt
    version: 1.7-3
amd64-microcode:
-   arch: amd64
    category: admin
    name: amd64-microcode
    origin: Ubuntu
    source: apt
    version: 3.20191021.1+really3.20180524.1~ubuntu0.16.04.2
apparmor:
-   arch: amd64
    category: admin
    name: apparmor
    origin: Ubuntu
    source: apt
    version: 2.10.95-0ubuntu2.11
apt:
-   arch: amd64
    category: admin
    name: apt
    origin: Ubuntu
    source: apt
    version: 1.2.32ubuntu0.1
apt-listchanges:
-   arch: all
    category: utils
    name: apt-listchanges
    origin: Ubuntu
    source: apt
    version: 2.85.14ubuntu1
apt-show-versions:
-   arch: all
    category: universe/admin
    name: apt-show-versions
    origin: Ubuntu
    source: apt
    version: 0.22.7
apt-transport-https:
-   arch: amd64
    category: admin
    name: apt-transport-https
    origin: Ubuntu
    source: apt
    version: 1.2.32ubuntu0.1
apt-utils:
-   arch: amd64
    category: admin
    name: apt-utils
    origin: Ubuntu
    source: apt
    version: 1.2.32ubuntu0.1
apticron:
-   arch: all
    category: universe/admin
    name: apticron
    origin: Ubuntu
    source: apt
    version: 1.1.58ubuntu1
aptitude:
-   arch: amd64
    category: admin
    name: aptitude
    origin: Ubuntu
    source: apt
    version: 0.7.4-2ubuntu2
aptitude-common:
-   arch: all
    category: admin
    name: aptitude-common
    origin: Ubuntu
    source: apt
    version: 0.7.4-2ubuntu2
at-spi2-core:
-   arch: amd64
    category: misc
    name: at-spi2-core
    origin: Ubuntu
    source: apt
    version: 2.18.3-4ubuntu1
aufs-tools:
-   arch: amd64
    category: universe/kernel
    name: aufs-tools
    origin: Ubuntu
    source: apt
    version: 1:3.2+20130722-1.1ubuntu1
base-files:
-   arch: amd64
    category: admin
    name: base-files
    origin: Ubuntu
    source: apt
    version: 9.4ubuntu4.11
base-passwd:
-   arch: amd64
    category: admin
    name: base-passwd
    origin: Ubuntu
    source: apt
    version: 3.5.39
bash:
-   arch: amd64
    category: shells
    name: bash
    origin: Ubuntu
    source: apt
    version: 4.3-14ubuntu1.4
bbb-apps:
-   arch: amd64
    category: default
    name: bbb-apps
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-45
bbb-apps-akka:
-   arch: all
    category: java
    name: bbb-apps-akka
    origin: BigBlueButton
    source: apt
    version: 2.2.0-87
bbb-apps-screenshare:
-   arch: amd64
    category: default
    name: bbb-apps-screenshare
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-45
bbb-apps-sip:
-   arch: amd64
    category: default
    name: bbb-apps-sip
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-11
bbb-apps-video:
-   arch: amd64
    category: default
    name: bbb-apps-video
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-42
bbb-apps-video-broadcast:
-   arch: amd64
    category: default
    name: bbb-apps-video-broadcast
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-8
bbb-client:
-   arch: amd64
    category: default
    name: bbb-client
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-40
bbb-config:
-   arch: amd64
    category: default
    name: bbb-config
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-189
bbb-etherpad:
-   arch: amd64
    category: default
    name: bbb-etherpad
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-51
bbb-freeswitch-core:
-   arch: amd64
    category: default
    name: bbb-freeswitch-core
    origin: BigBlueButton
    source: apt
    version: 2:2.2.0-112
bbb-freeswitch-sounds:
-   arch: amd64
    category: default
    name: bbb-freeswitch-sounds
    origin: BigBlueButton
    source: apt
    version: 1:1.6.7-6
bbb-fsesl-akka:
-   arch: all
    category: java
    name: bbb-fsesl-akka
    origin: BigBlueButton
    source: apt
    version: 2.2.0-65
bbb-html5:
-   arch: amd64
    category: default
    name: bbb-html5
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-934
bbb-mkclean:
-   arch: amd64
    category: default
    name: bbb-mkclean
    origin: BigBlueButton
    source: apt
    version: 1:0.8.7-3
bbb-playback-presentation:
-   arch: amd64
    category: default
    name: bbb-playback-presentation
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-30
bbb-record-core:
-   arch: amd64
    category: default
    name: bbb-record-core
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-66
bbb-red5:
-   arch: amd64
    category: default
    name: bbb-red5
    origin: BigBlueButton
    source: apt
    version: 1:1.0.10-16
bbb-transcode-akka:
-   arch: all
    category: java
    name: bbb-transcode-akka
    origin: BigBlueButton
    source: apt
    version: 2.2.0-8
bbb-web:
-   arch: amd64
    category: default
    name: bbb-web
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-130
bbb-webrtc-sfu:
-   arch: amd64
    category: default
    name: bbb-webrtc-sfu
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-99
bc:
-   arch: amd64
    category: math
    name: bc
    origin: Ubuntu
    source: apt
    version: 1.06.95-9build1
bigbluebutton:
-   arch: amd64
    category: default
    name: bigbluebutton
    origin: BigBlueButton
    source: apt
    version: 1:2.2.0-5
bind9-host:
-   arch: amd64
    category: net
    name: bind9-host
    origin: Ubuntu
    source: apt
    version: 1:9.10.3.dfsg.P4-8ubuntu1.16
binutils:
-   arch: amd64
    category: devel
    name: binutils
    origin: Ubuntu
    source: apt
    version: 2.26.1-1ubuntu1~16.04.8
bsdutils:
-   arch: amd64
    category: utils
    name: bsdutils
    origin: Ubuntu
    source: apt
    version: 1:2.27.1-6ubuntu3.10
build-essential:
-   arch: amd64
    category: devel
    name: build-essential
    origin: Ubuntu
    source: apt
    version: 12.1ubuntu2
busybox-initramfs:
-   arch: amd64
    category: shells
    name: busybox-initramfs
    origin: Ubuntu
    source: apt
    version: 1:1.22.0-15ubuntu1.4
bzip2:
-   arch: amd64
    category: utils
    name: bzip2
    origin: Ubuntu
    source: apt
    version: 1.0.6-8ubuntu0.2
ca-certificates:
-   arch: all
    category: misc
    name: ca-certificates
    origin: Ubuntu
    source: apt
    version: 20190110~16.04.1
ca-certificates-java:
-   arch: all
    category: misc
    name: ca-certificates-java
    origin: Ubuntu
    source: apt
    version: 20160321ubuntu1
cabextract:
-   arch: amd64
    category: universe/utils
    name: cabextract
    origin: Ubuntu
    source: apt
    version: 1.6-1
cgroupfs-mount:
-   arch: all
    category: universe/admin
    name: cgroupfs-mount
    origin: Ubuntu
    source: apt
    version: '1.2'
colord:
-   arch: amd64
    category: graphics
    name: colord
    origin: Ubuntu
    source: apt
    version: 1.2.12-1ubuntu1
colord-data:
-   arch: all
    category: graphics
    name: colord-data
    origin: Ubuntu
    source: apt
    version: 1.2.12-1ubuntu1
console-setup:
-   arch: all
    category: utils
    name: console-setup
    origin: Ubuntu
    source: apt
    version: 1.108ubuntu15.5
console-setup-linux:
-   arch: all
    category: utils
    name: console-setup-linux
    origin: Ubuntu
    source: apt
    version: 1.108ubuntu15.5
containerd.io:
-   arch: amd64
    category: devel
    name: containerd.io
    origin: Docker
    source: apt
    version: 1.2.13-2
coreutils:
-   arch: amd64
    category: utils
    name: coreutils
    origin: Ubuntu
    source: apt
    version: 8.25-2ubuntu3~16.04
cpio:
-   arch: amd64
    category: utils
    name: cpio
    origin: Ubuntu
    source: apt
    version: 2.11+dfsg-5ubuntu1.1
cpp:
-   arch: amd64
    category: interpreters
    name: cpp
    origin: Ubuntu
    source: apt
    version: 4:5.3.1-1ubuntu1
cpp-5:
-   arch: amd64
    category: interpreters
    name: cpp-5
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
crda:
-   arch: amd64
    category: net
    name: crda
    origin: Ubuntu
    source: apt
    version: 3.13-1
cron:
-   arch: amd64
    category: admin
    name: cron
    origin: Ubuntu
    source: apt
    version: 3.0pl1-128ubuntu2
cryptsetup:
-   arch: amd64
    category: admin
    name: cryptsetup
    origin: Ubuntu
    source: apt
    version: 2:1.6.6-5ubuntu2.1
cryptsetup-bin:
-   arch: amd64
    category: admin
    name: cryptsetup-bin
    origin: Ubuntu
    source: apt
    version: 2:1.6.6-5ubuntu2.1
curl:
-   arch: amd64
    category: web
    name: curl
    origin: Ubuntu
    source: apt
    version: 7.47.0-1ubuntu2.14
dash:
-   arch: amd64
    category: shells
    name: dash
    origin: Ubuntu
    source: apt
    version: 0.5.8-2.1ubuntu2
dbus:
-   arch: amd64
    category: devel
    name: dbus
    origin: Ubuntu
    source: apt
    version: 1.10.6-1ubuntu3.5
dconf-gsettings-backend:
-   arch: amd64
    category: libs
    name: dconf-gsettings-backend
    origin: Ubuntu
    source: apt
    version: 0.24.0-2
dconf-service:
-   arch: amd64
    category: libs
    name: dconf-service
    origin: Ubuntu
    source: apt
    version: 0.24.0-2
debconf:
-   arch: all
    category: admin
    name: debconf
    origin: Ubuntu
    source: apt
    version: 1.5.58ubuntu2
debconf-i18n:
-   arch: all
    category: admin
    name: debconf-i18n
    origin: Ubuntu
    source: apt
    version: 1.5.58ubuntu2
debconf-utils:
-   arch: all
    category: universe/devel
    name: debconf-utils
    origin: Ubuntu
    source: apt
    version: 1.5.58ubuntu2
debianutils:
-   arch: amd64
    category: utils
    name: debianutils
    origin: Ubuntu
    source: apt
    version: '4.7'
default-jre:
-   arch: amd64
    category: interpreters
    name: default-jre
    origin: Ubuntu
    source: apt
    version: 2:1.8-56ubuntu2
default-jre-headless:
-   arch: amd64
    category: interpreters
    name: default-jre-headless
    origin: Ubuntu
    source: apt
    version: 2:1.8-56ubuntu2
dh-python:
-   arch: all
    category: python
    name: dh-python
    origin: Ubuntu
    source: apt
    version: 2.20151103ubuntu1.2
dictionaries-common:
-   arch: all
    category: text
    name: dictionaries-common
    origin: Ubuntu
    source: apt
    version: 1.26.3
diffutils:
-   arch: amd64
    category: utils
    name: diffutils
    origin: Ubuntu
    source: apt
    version: 1:3.3-3
dirmngr:
-   arch: amd64
    category: utils
    name: dirmngr
    origin: Ubuntu
    source: apt
    version: 2.1.11-6ubuntu2.1
distro-info-data:
-   arch: all
    category: devel
    name: distro-info-data
    origin: Ubuntu
    source: apt
    version: 0.28ubuntu0.14
dmeventd:
-   arch: amd64
    category: admin
    name: dmeventd
    origin: Ubuntu
    source: apt
    version: 2:1.02.110-1ubuntu10
dmsetup:
-   arch: amd64
    category: admin
    name: dmsetup
    origin: Ubuntu
    source: apt
    version: 2:1.02.110-1ubuntu10
dnsutils:
-   arch: amd64
    category: net
    name: dnsutils
    origin: Ubuntu
    source: apt
    version: 1:9.10.3.dfsg.P4-8ubuntu1.16
docker-ce:
-   arch: amd64
    category: admin
    name: docker-ce
    origin: Docker
    source: apt
    version: 5:19.03.11~3-0~ubuntu-xenial
docker-ce-cli:
-   arch: amd64
    category: admin
    name: docker-ce-cli
    origin: Docker
    source: apt
    version: 5:19.03.11~3-0~ubuntu-xenial
dpkg:
-   arch: amd64
    category: admin
    name: dpkg
    origin: Ubuntu
    source: apt
    version: 1.18.4ubuntu1.6
dpkg-dev:
-   arch: all
    category: utils
    name: dpkg-dev
    origin: Ubuntu
    source: apt
    version: 1.18.4ubuntu1.6
dstat:
-   arch: all
    category: universe/admin
    name: dstat
    origin: Ubuntu
    source: apt
    version: 0.7.2-4
e2fslibs:
-   arch: amd64
    category: libs
    name: e2fslibs
    origin: Ubuntu
    source: apt
    version: 1.42.13-1ubuntu1.2
e2fsprogs:
-   arch: amd64
    category: admin
    name: e2fsprogs
    origin: Ubuntu
    source: apt
    version: 1.42.13-1ubuntu1.2
eject:
-   arch: amd64
    category: utils
    name: eject
    origin: Ubuntu
    source: apt
    version: 2.1.5+deb1+cvs20081104-13.1ubuntu0.16.04.1
emacsen-common:
-   arch: all
    category: editors
    name: emacsen-common
    origin: Ubuntu
    source: apt
    version: 2.0.8
esound-common:
-   arch: all
    category: universe/sound
    name: esound-common
    origin: Ubuntu
    source: apt
    version: 0.2.41-11
etckeeper:
-   arch: all
    category: admin
    name: etckeeper
    origin: Ubuntu
    source: apt
    version: 1.18.2-1ubuntu1
exim4-base:
-   arch: amd64
    category: mail
    name: exim4-base
    origin: Ubuntu
    source: apt
    version: 4.86.2-2ubuntu2.6
exim4-config:
-   arch: all
    category: mail
    name: exim4-config
    origin: Ubuntu
    source: apt
    version: 4.86.2-2ubuntu2.6
exim4-daemon-light:
-   arch: amd64
    category: mail
    name: exim4-daemon-light
    origin: Ubuntu
    source: apt
    version: 4.86.2-2ubuntu2.6
fakeroot:
-   arch: amd64
    category: utils
    name: fakeroot
    origin: Ubuntu
    source: apt
    version: 1.20.2-1ubuntu1
ffmpeg:
-   arch: amd64
    category: video
    name: ffmpeg
    origin: LP-PPA-bigbluebutton-support
    source: apt
    version: 7:4.2.2-1bbb1~ubuntu16.04
file:
-   arch: amd64
    category: utils
    name: file
    origin: Ubuntu
    source: apt
    version: 1:5.25-2ubuntu1.4
findutils:
-   arch: amd64
    category: utils
    name: findutils
    origin: Ubuntu
    source: apt
    version: 4.6.0+git+20160126-2
fontconfig:
-   arch: amd64
    category: utils
    name: fontconfig
    origin: Ubuntu
    source: apt
    version: 2.11.94-0ubuntu1.1
fontconfig-config:
-   arch: all
    category: libs
    name: fontconfig-config
    origin: Ubuntu
    source: apt
    version: 2.11.94-0ubuntu1.1
fonts-crosextra-caladea:
-   arch: all
    category: universe/fonts
    name: fonts-crosextra-caladea
    origin: Ubuntu
    source: apt
    version: 20130214-1
fonts-crosextra-carlito:
-   arch: all
    category: universe/fonts
    name: fonts-crosextra-carlito
    origin: Ubuntu
    source: apt
    version: 20130920-1
fonts-dejavu:
-   arch: all
    category: universe/fonts
    name: fonts-dejavu
    origin: Ubuntu
    source: apt
    version: 2.35-1
fonts-dejavu-core:
-   arch: all
    category: fonts
    name: fonts-dejavu-core
    origin: Ubuntu
    source: apt
    version: 2.35-1
fonts-dejavu-extra:
-   arch: all
    category: fonts
    name: fonts-dejavu-extra
    origin: Ubuntu
    source: apt
    version: 2.35-1
fonts-lato:
-   arch: all
    category: fonts
    name: fonts-lato
    origin: Ubuntu
    source: apt
    version: 2.0-1
fonts-liberation:
-   arch: all
    category: fonts
    name: fonts-liberation
    origin: Ubuntu
    source: apt
    version: 1.07.4-1
fonts-noto:
-   arch: all
    category: universe/fonts
    name: fonts-noto
    origin: Ubuntu
    source: apt
    version: 20160116-1
fonts-noto-cjk:
-   arch: all
    category: fonts
    name: fonts-noto-cjk
    origin: Ubuntu
    source: apt
    version: 1:1.004+repack2-1~ubuntu1
fonts-noto-hinted:
-   arch: all
    category: universe/fonts
    name: fonts-noto-hinted
    origin: Ubuntu
    source: apt
    version: 20160116-1
fonts-noto-mono:
-   arch: all
    category: universe/fonts
    name: fonts-noto-mono
    origin: Ubuntu
    source: apt
    version: 20160116-1
fonts-noto-unhinted:
-   arch: all
    category: universe/fonts
    name: fonts-noto-unhinted
    origin: Ubuntu
    source: apt
    version: 20160116-1
fonts-opensymbol:
-   arch: all
    category: fonts
    name: fonts-opensymbol
    origin: Ubuntu
    source: apt
    version: 2:102.7+LibO5.1.6~rc2-0ubuntu1~xenial10
fonts-sil-gentium:
-   arch: all
    category: universe/fonts
    name: fonts-sil-gentium
    origin: Ubuntu
    source: apt
    version: 20081126:1.03-1
fonts-sil-gentium-basic:
-   arch: all
    category: universe/fonts
    name: fonts-sil-gentium-basic
    origin: Ubuntu
    source: apt
    version: 1.1-7
fonts-stix:
-   arch: all
    category: fonts
    name: fonts-stix
    origin: Ubuntu
    source: apt
    version: 1.1.1-4
freepats:
-   arch: all
    category: universe/sound
    name: freepats
    origin: Ubuntu
    source: apt
    version: 20060219-1
g++:
-   arch: amd64
    category: devel
    name: g++
    origin: Ubuntu
    source: apt
    version: 4:5.3.1-1ubuntu1
g++-5:
-   arch: amd64
    category: devel
    name: g++-5
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
gawk:
-   arch: amd64
    category: interpreters
    name: gawk
    origin: Ubuntu
    source: apt
    version: 1:4.1.3+dfsg-0.1
gcc:
-   arch: amd64
    category: devel
    name: gcc
    origin: Ubuntu
    source: apt
    version: 4:5.3.1-1ubuntu1
gcc-5:
-   arch: amd64
    category: devel
    name: gcc-5
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
gcc-5-base:
-   arch: amd64
    category: libs
    name: gcc-5-base
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
gcc-6-base:
-   arch: amd64
    category: libs
    name: gcc-6-base
    origin: Ubuntu
    source: apt
    version: 6.0.1-0ubuntu1
geoip-database:
-   arch: all
    category: net
    name: geoip-database
    origin: Ubuntu
    source: apt
    version: 20160408-1
gettext-base:
-   arch: amd64
    category: utils
    name: gettext-base
    origin: Ubuntu
    source: apt
    version: 0.19.7-2ubuntu3.1
ghostscript:
-   arch: amd64
    category: text
    name: ghostscript
    origin: Ubuntu
    source: apt
    version: 9.26~dfsg+0-0ubuntu0.16.04.12
gir1.2-glib-2.0:
-   arch: amd64
    category: libs
    name: gir1.2-glib-2.0
    origin: Ubuntu
    source: apt
    version: 1.46.0-3ubuntu1
git:
-   arch: amd64
    category: vcs
    name: git
    origin: Ubuntu
    source: apt
    version: 1:2.7.4-0ubuntu1.9
git-man:
-   arch: all
    category: vcs
    name: git-man
    origin: Ubuntu
    source: apt
    version: 1:2.7.4-0ubuntu1.9
glib-networking:
-   arch: amd64
    category: libs
    name: glib-networking
    origin: Ubuntu
    source: apt
    version: 2.48.2-1~ubuntu16.04.1
glib-networking-common:
-   arch: all
    category: libs
    name: glib-networking-common
    origin: Ubuntu
    source: apt
    version: 2.48.2-1~ubuntu16.04.1
glib-networking-services:
-   arch: amd64
    category: libs
    name: glib-networking-services
    origin: Ubuntu
    source: apt
    version: 2.48.2-1~ubuntu16.04.1
gnupg:
-   arch: amd64
    category: utils
    name: gnupg
    origin: Ubuntu
    source: apt
    version: 1.4.20-1ubuntu3.3
gnupg-agent:
-   arch: amd64
    category: utils
    name: gnupg-agent
    origin: Ubuntu
    source: apt
    version: 2.1.11-6ubuntu2.1
gnupg2:
-   arch: amd64
    category: utils
    name: gnupg2
    origin: Ubuntu
    source: apt
    version: 2.1.11-6ubuntu2.1
gnuplot:
-   arch: all
    category: universe/math
    name: gnuplot
    origin: Ubuntu
    source: apt
    version: 4.6.6-3
gnuplot-tex:
-   arch: all
    category: universe/doc
    name: gnuplot-tex
    origin: Ubuntu
    source: apt
    version: 4.6.6-3
gnuplot5-data:
-   arch: all
    category: universe/doc
    name: gnuplot5-data
    origin: Ubuntu
    source: apt
    version: 5.0.3+dfsg2-1
gnuplot5-qt:
-   arch: amd64
    category: universe/math
    name: gnuplot5-qt
    origin: Ubuntu
    source: apt
    version: 5.0.3+dfsg2-1
gpgv:
-   arch: amd64
    category: utils
    name: gpgv
    origin: Ubuntu
    source: apt
    version: 1.4.20-1ubuntu3.3
grep:
-   arch: amd64
    category: utils
    name: grep
    origin: Ubuntu
    source: apt
    version: 2.25-1~16.04.1
grub-common:
-   arch: amd64
    category: admin
    name: grub-common
    origin: Ubuntu
    source: apt
    version: 2.02~beta2-36ubuntu3.23
grub-gfxpayload-lists:
-   arch: amd64
    category: admin
    name: grub-gfxpayload-lists
    origin: Ubuntu
    source: apt
    version: '0.7'
grub-pc:
-   arch: amd64
    category: admin
    name: grub-pc
    origin: Ubuntu
    source: apt
    version: 2.02~beta2-36ubuntu3.23
grub-pc-bin:
-   arch: amd64
    category: admin
    name: grub-pc-bin
    origin: Ubuntu
    source: apt
    version: 2.02~beta2-36ubuntu3.23
grub2-common:
-   arch: amd64
    category: admin
    name: grub2-common
    origin: Ubuntu
    source: apt
    version: 2.02~beta2-36ubuntu3.23
gsettings-desktop-schemas:
-   arch: all
    category: libs
    name: gsettings-desktop-schemas
    origin: Ubuntu
    source: apt
    version: 3.18.1-1ubuntu1
gsfonts:
-   arch: all
    category: text
    name: gsfonts
    origin: Ubuntu
    source: apt
    version: 1:8.11+urwcyr1.0.7~pre44-4.2ubuntu1
gstreamer1.0-plugins-base:
-   arch: amd64
    category: libs
    name: gstreamer1.0-plugins-base
    origin: Ubuntu
    source: apt
    version: 1.8.3-1ubuntu0.3
gstreamer1.5-libav:
-   arch: amd64
    category: libs
    name: gstreamer1.5-libav
    origin: BigBlueButton
    source: apt
    version: 1.8.1-1kurento1.16.04
gstreamer1.5-nice:
-   arch: amd64
    category: net
    name: gstreamer1.5-nice
    origin: BigBlueButton
    source: apt
    version: 0.1.15-1kurento3.16.04
gstreamer1.5-plugins-bad:
-   arch: amd64
    category: libs
    name: gstreamer1.5-plugins-bad
    origin: BigBlueButton
    source: apt
    version: 1.8.1-1kurento4.16.04
gstreamer1.5-plugins-base:
-   arch: amd64
    category: libs
    name: gstreamer1.5-plugins-base
    origin: BigBlueButton
    source: apt
    version: 1.8.1-1kurento2.16.04
gstreamer1.5-plugins-good:
-   arch: amd64
    category: libs
    name: gstreamer1.5-plugins-good
    origin: BigBlueButton
    source: apt
    version: 1.8.1-1kurento3.16.04
gstreamer1.5-plugins-ugly:
-   arch: amd64
    category: libs
    name: gstreamer1.5-plugins-ugly
    origin: BigBlueButton
    source: apt
    version: 1.8.1-1kurento1.16.04
gstreamer1.5-pulseaudio:
-   arch: amd64
    category: sound
    name: gstreamer1.5-pulseaudio
    origin: BigBlueButton
    source: apt
    version: 1.8.1-1kurento3.16.04
gstreamer1.5-x:
-   arch: amd64
    category: libs
    name: gstreamer1.5-x
    origin: BigBlueButton
    source: apt
    version: 1.8.1-1kurento2.16.04
gzip:
-   arch: amd64
    category: utils
    name: gzip
    origin: Ubuntu
    source: apt
    version: 1.6-4ubuntu1
haveged:
-   arch: amd64
    category: universe/misc
    name: haveged
    origin: Ubuntu
    source: apt
    version: 1.9.1-3
hicolor-icon-theme:
-   arch: all
    category: misc
    name: hicolor-icon-theme
    origin: Ubuntu
    source: apt
    version: 0.15-0ubuntu1.1
hostname:
-   arch: amd64
    category: admin
    name: hostname
    origin: Ubuntu
    source: apt
    version: 3.16ubuntu2
htop:
-   arch: amd64
    category: universe/utils
    name: htop
    origin: Ubuntu
    source: apt
    version: 2.0.1-1ubuntu1
humanity-icon-theme:
-   arch: all
    category: gnome
    name: humanity-icon-theme
    origin: Ubuntu
    source: apt
    version: 0.6.10.1
hunspell-en-us:
-   arch: all
    category: text
    name: hunspell-en-us
    origin: Ubuntu
    source: apt
    version: 20070829-6ubuntu3
i965-va-driver:
-   arch: amd64
    category: universe/oldlibs
    name: i965-va-driver
    origin: Ubuntu
    source: apt
    version: 1.7.0-1
icu-devtools:
-   arch: amd64
    category: libdevel
    name: icu-devtools
    origin: Ubuntu
    source: apt
    version: 55.1-7ubuntu0.5
ifupdown:
-   arch: amd64
    category: admin
    name: ifupdown
    origin: Ubuntu
    source: apt
    version: 0.8.10ubuntu1.4
imagemagick:
-   arch: amd64
    category: graphics
    name: imagemagick
    origin: Ubuntu
    source: apt
    version: 8:6.8.9.9-7ubuntu5.15
imagemagick-6.q16:
-   arch: amd64
    category: graphics
    name: imagemagick-6.q16
    origin: Ubuntu
    source: apt
    version: 8:6.8.9.9-7ubuntu5.15
imagemagick-common:
-   arch: all
    category: graphics
    name: imagemagick-common
    origin: Ubuntu
    source: apt
    version: 8:6.8.9.9-7ubuntu5.15
init:
-   arch: amd64
    category: metapackages
    name: init
    origin: Ubuntu
    source: apt
    version: 1.29ubuntu4
init-system-helpers:
-   arch: all
    category: admin
    name: init-system-helpers
    origin: Ubuntu
    source: apt
    version: 1.29ubuntu4
initramfs-tools:
-   arch: all
    category: utils
    name: initramfs-tools
    origin: Ubuntu
    source: apt
    version: 0.122ubuntu8.16
initramfs-tools-bin:
-   arch: amd64
    category: utils
    name: initramfs-tools-bin
    origin: Ubuntu
    source: apt
    version: 0.122ubuntu8.16
initramfs-tools-core:
-   arch: all
    category: utils
    name: initramfs-tools-core
    origin: Ubuntu
    source: apt
    version: 0.122ubuntu8.16
initscripts:
-   arch: amd64
    category: admin
    name: initscripts
    origin: Ubuntu
    source: apt
    version: 2.88dsf-59.3ubuntu2
insserv:
-   arch: amd64
    category: misc
    name: insserv
    origin: Ubuntu
    source: apt
    version: 1.14.0-5ubuntu3
intel-microcode:
-   arch: amd64
    category: admin
    name: intel-microcode
    origin: Ubuntu
    source: apt
    version: 3.20191115.1ubuntu0.16.04.2
iotop:
-   arch: amd64
    category: universe/admin
    name: iotop
    origin: Ubuntu
    source: apt
    version: 0.6-1
iproute2:
-   arch: amd64
    category: net
    name: iproute2
    origin: Ubuntu
    source: apt
    version: 4.3.0-1ubuntu3.16.04.5
iptables:
-   arch: amd64
    category: net
    name: iptables
    origin: Ubuntu
    source: apt
    version: 1.6.0-2ubuntu3
iputils-ping:
-   arch: amd64
    category: net
    name: iputils-ping
    origin: Ubuntu
    source: apt
    version: 3:20121221-5ubuntu2
isc-dhcp-client:
-   arch: amd64
    category: net
    name: isc-dhcp-client
    origin: Ubuntu
    source: apt
    version: 4.3.3-5ubuntu12.10
isc-dhcp-common:
-   arch: amd64
    category: net
    name: isc-dhcp-common
    origin: Ubuntu
    source: apt
    version: 4.3.3-5ubuntu12.10
iso-codes:
-   arch: all
    category: libs
    name: iso-codes
    origin: Ubuntu
    source: apt
    version: 3.65-1
iucode-tool:
-   arch: amd64
    category: utils
    name: iucode-tool
    origin: Ubuntu
    source: apt
    version: 1.5.1-1ubuntu0.1
iw:
-   arch: amd64
    category: net
    name: iw
    origin: Ubuntu
    source: apt
    version: 3.17-1
java-common:
-   arch: all
    category: misc
    name: java-common
    origin: Ubuntu
    source: apt
    version: 0.56ubuntu2
javascript-common:
-   arch: all
    category: web
    name: javascript-common
    origin: Ubuntu
    source: apt
    version: '11'
jsvc:
-   arch: amd64
    category: universe/net
    name: jsvc
    origin: Ubuntu
    source: apt
    version: 1.0.15-6
kbd:
-   arch: amd64
    category: utils
    name: kbd
    origin: Ubuntu
    source: apt
    version: 1.15.5-1ubuntu5
keyboard-configuration:
-   arch: all
    category: utils
    name: keyboard-configuration
    origin: Ubuntu
    source: apt
    version: 1.108ubuntu15.5
klibc-utils:
-   arch: amd64
    category: libs
    name: klibc-utils
    origin: Ubuntu
    source: apt
    version: 2.0.4-8ubuntu1.16.04.4
kmod:
-   arch: amd64
    category: admin
    name: kmod
    origin: Ubuntu
    source: apt
    version: 22-1ubuntu5.2
kms-core:
-   arch: amd64
    category: libs
    name: kms-core
    origin: BigBlueButton
    source: apt
    version: 6.13.0.xenial~20200506234430.57.5a9fdb5
kms-elements:
-   arch: amd64
    category: libs
    name: kms-elements
    origin: BigBlueButton
    source: apt
    version: 6.13.0.xenial~20200506235224.48.cc7e5c4
kms-filters:
-   arch: amd64
    category: libs
    name: kms-filters
    origin: BigBlueButton
    source: apt
    version: 6.13.0.xenial.20200506235848.48.72a06d5
kms-jsonrpc:
-   arch: amd64
    category: libs
    name: kms-jsonrpc
    origin: BigBlueButton
    source: apt
    version: 6.13.0-0kurento1.16.04
kmsjsoncpp:
-   arch: amd64
    category: utils
    name: kmsjsoncpp
    origin: BigBlueButton
    source: apt
    version: 1.6.3-1kurento1.16.04
kurento-media-server:
-   arch: amd64
    category: video
    name: kurento-media-server
    origin: BigBlueButton
    source: apt
    version: 6.13.0-0kurento1.16.04
language-pack-en:
-   arch: all
    category: translations
    name: language-pack-en
    origin: Ubuntu
    source: apt
    version: 1:16.04+20161009
language-pack-en-base:
-   arch: all
    category: translations
    name: language-pack-en-base
    origin: Ubuntu
    source: apt
    version: 1:16.04+20160627
less:
-   arch: amd64
    category: text
    name: less
    origin: Ubuntu
    source: apt
    version: 481-2.1ubuntu0.2
liba52-0.7.4:
-   arch: amd64
    category: universe/libs
    name: liba52-0.7.4
    origin: Ubuntu
    source: apt
    version: 0.7.4-18
libaa1:
-   arch: amd64
    category: libs
    name: libaa1
    origin: Ubuntu
    source: apt
    version: 1.4p5-44build1
libaacs0:
-   arch: amd64
    category: universe/video
    name: libaacs0
    origin: Ubuntu
    source: apt
    version: 0.8.1-1
libabw-0.1-1v5:
-   arch: amd64
    category: libs
    name: libabw-0.1-1v5
    origin: Ubuntu
    source: apt
    version: 0.1.1-2ubuntu2
libacl1:
-   arch: amd64
    category: libs
    name: libacl1
    origin: Ubuntu
    source: apt
    version: 2.2.52-3
libalgorithm-diff-perl:
-   arch: all
    category: perl
    name: libalgorithm-diff-perl
    origin: Ubuntu
    source: apt
    version: 1.19.03-1
libalgorithm-diff-xs-perl:
-   arch: amd64
    category: perl
    name: libalgorithm-diff-xs-perl
    origin: Ubuntu
    source: apt
    version: 0.04-4build1
libalgorithm-merge-perl:
-   arch: all
    category: perl
    name: libalgorithm-merge-perl
    origin: Ubuntu
    source: apt
    version: 0.08-3
libao-common:
-   arch: all
    category: libs
    name: libao-common
    origin: Ubuntu
    source: apt
    version: 1.1.0-3ubuntu1
libao4:
-   arch: amd64
    category: libs
    name: libao4
    origin: Ubuntu
    source: apt
    version: 1.1.0-3ubuntu1
libapparmor-perl:
-   arch: amd64
    category: perl
    name: libapparmor-perl
    origin: Ubuntu
    source: apt
    version: 2.10.95-0ubuntu2.11
libapparmor1:
-   arch: amd64
    category: libs
    name: libapparmor1
    origin: Ubuntu
    source: apt
    version: 2.10.95-0ubuntu2.11
libapt-inst2.0:
-   arch: amd64
    category: libs
    name: libapt-inst2.0
    origin: Ubuntu
    source: apt
    version: 1.2.32ubuntu0.1
libapt-pkg-perl:
-   arch: amd64
    category: perl
    name: libapt-pkg-perl
    origin: Ubuntu
    source: apt
    version: 0.1.29build7
libapt-pkg5.0:
-   arch: amd64
    category: libs
    name: libapt-pkg5.0
    origin: Ubuntu
    source: apt
    version: 1.2.32ubuntu0.1
libasan2:
-   arch: amd64
    category: libs
    name: libasan2
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libasn1-8-heimdal:
-   arch: amd64
    category: libs
    name: libasn1-8-heimdal
    origin: Ubuntu
    source: apt
    version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
libasound2:
-   arch: amd64
    category: libs
    name: libasound2
    origin: Ubuntu
    source: apt
    version: 1.1.0-0ubuntu1
libasound2-data:
-   arch: all
    category: libs
    name: libasound2-data
    origin: Ubuntu
    source: apt
    version: 1.1.0-0ubuntu1
libasprintf0v5:
-   arch: amd64
    category: libs
    name: libasprintf0v5
    origin: Ubuntu
    source: apt
    version: 0.19.7-2ubuntu3.1
libass5:
-   arch: amd64
    category: universe/libs
    name: libass5
    origin: Ubuntu
    source: apt
    version: 0.13.1-1
libassuan0:
-   arch: amd64
    category: libs
    name: libassuan0
    origin: Ubuntu
    source: apt
    version: 2.4.2-2
libasyncns0:
-   arch: amd64
    category: libs
    name: libasyncns0
    origin: Ubuntu
    source: apt
    version: 0.8-5build1
libatk-bridge2.0-0:
-   arch: amd64
    category: libs
    name: libatk-bridge2.0-0
    origin: Ubuntu
    source: apt
    version: 2.18.1-2ubuntu1
libatk1.0-0:
-   arch: amd64
    category: libs
    name: libatk1.0-0
    origin: Ubuntu
    source: apt
    version: 2.18.0-1
libatk1.0-data:
-   arch: all
    category: misc
    name: libatk1.0-data
    origin: Ubuntu
    source: apt
    version: 2.18.0-1
libatm1:
-   arch: amd64
    category: libs
    name: libatm1
    origin: Ubuntu
    source: apt
    version: 1:2.5.1-1.5
libatomic1:
-   arch: amd64
    category: libs
    name: libatomic1
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libatspi2.0-0:
-   arch: amd64
    category: misc
    name: libatspi2.0-0
    origin: Ubuntu
    source: apt
    version: 2.18.3-4ubuntu1
libattr1:
-   arch: amd64
    category: libs
    name: libattr1
    origin: Ubuntu
    source: apt
    version: 1:2.4.47-2
libaudio2:
-   arch: amd64
    category: libs
    name: libaudio2
    origin: Ubuntu
    source: apt
    version: 1.9.4-4
libaudiofile1:
-   arch: amd64
    category: universe/libs
    name: libaudiofile1
    origin: Ubuntu
    source: apt
    version: 0.3.6-2ubuntu0.16.04.1
libaudit-common:
-   arch: all
    category: libs
    name: libaudit-common
    origin: Ubuntu
    source: apt
    version: 1:2.4.5-1ubuntu2.1
libaudit1:
-   arch: amd64
    category: libs
    name: libaudit1
    origin: Ubuntu
    source: apt
    version: 1:2.4.5-1ubuntu2.1
libauthen-sasl-perl:
-   arch: all
    category: perl
    name: libauthen-sasl-perl
    origin: Ubuntu
    source: apt
    version: 2.1600-1
libav-tools:
-   arch: all
    category: universe/oldlibs
    name: libav-tools
    origin: Ubuntu
    source: apt
    version: 7:2.8.15-0ubuntu0.16.04.1
libavahi-client3:
-   arch: amd64
    category: libs
    name: libavahi-client3
    origin: Ubuntu
    source: apt
    version: 0.6.32~rc+dfsg-1ubuntu2.3
libavahi-common-data:
-   arch: amd64
    category: libs
    name: libavahi-common-data
    origin: Ubuntu
    source: apt
    version: 0.6.32~rc+dfsg-1ubuntu2.3
libavahi-common3:
-   arch: amd64
    category: libs
    name: libavahi-common3
    origin: Ubuntu
    source: apt
    version: 0.6.32~rc+dfsg-1ubuntu2.3
libavc1394-0:
-   arch: amd64
    category: libs
    name: libavc1394-0
    origin: Ubuntu
    source: apt
    version: 0.5.4-4
libavcodec-ffmpeg56:
-   arch: amd64
    category: universe/libs
    name: libavcodec-ffmpeg56
    origin: Ubuntu
    source: apt
    version: 7:2.8.15-0ubuntu0.16.04.1
libavcodec58:
-   arch: amd64
    category: libs
    name: libavcodec58
    origin: LP-PPA-bigbluebutton-support
    source: apt
    version: 7:4.2.2-1bbb1~ubuntu16.04
libavdevice58:
-   arch: amd64
    category: libs
    name: libavdevice58
    origin: LP-PPA-bigbluebutton-support
    source: apt
    version: 7:4.2.2-1bbb1~ubuntu16.04
libavfilter7:
-   arch: amd64
    category: libs
    name: libavfilter7
    origin: LP-PPA-bigbluebutton-support
    source: apt
    version: 7:4.2.2-1bbb1~ubuntu16.04
libavformat-ffmpeg56:
-   arch: amd64
    category: universe/libs
    name: libavformat-ffmpeg56
    origin: Ubuntu
    source: apt
    version: 7:2.8.15-0ubuntu0.16.04.1
libavformat58:
-   arch: amd64
    category: libs
    name: libavformat58
    origin: LP-PPA-bigbluebutton-support
    source: apt
    version: 7:4.2.2-1bbb1~ubuntu16.04
libavresample4:
-   arch: amd64
    category: libs
    name: libavresample4
    origin: LP-PPA-bigbluebutton-support
    source: apt
    version: 7:4.2.2-1bbb1~ubuntu16.04
libavutil-ffmpeg54:
-   arch: amd64
    category: universe/libs
    name: libavutil-ffmpeg54
    origin: Ubuntu
    source: apt
    version: 7:2.8.15-0ubuntu0.16.04.1
libavutil56:
-   arch: amd64
    category: libs
    name: libavutil56
    origin: LP-PPA-bigbluebutton-support
    source: apt
    version: 7:4.2.2-1bbb1~ubuntu16.04
libbdplus0:
-   arch: amd64
    category: universe/libs
    name: libbdplus0
    origin: Ubuntu
    source: apt
    version: 0.1.2-1
libbind9-140:
-   arch: amd64
    category: libs
    name: libbind9-140
    origin: Ubuntu
    source: apt
    version: 1:9.10.3.dfsg.P4-8ubuntu1.16
libblkid1:
-   arch: amd64
    category: libs
    name: libblkid1
    origin: Ubuntu
    source: apt
    version: 2.27.1-6ubuntu3.10
libbluray1:
-   arch: amd64
    category: universe/libs
    name: libbluray1
    origin: Ubuntu
    source: apt
    version: 1:0.9.2-2
libboost-date-time1.58.0:
-   arch: amd64
    category: libs
    name: libboost-date-time1.58.0
    origin: Ubuntu
    source: apt
    version: 1.58.0+dfsg-5ubuntu3.1
libboost-filesystem1.58.0:
-   arch: amd64
    category: libs
    name: libboost-filesystem1.58.0
    origin: Ubuntu
    source: apt
    version: 1.58.0+dfsg-5ubuntu3.1
libboost-iostreams1.58.0:
-   arch: amd64
    category: libs
    name: libboost-iostreams1.58.0
    origin: Ubuntu
    source: apt
    version: 1.58.0+dfsg-5ubuntu3.1
libboost-log1.58.0:
-   arch: amd64
    category: universe/libs
    name: libboost-log1.58.0
    origin: Ubuntu
    source: apt
    version: 1.58.0+dfsg-5ubuntu3.1
libboost-program-options1.58.0:
-   arch: amd64
    category: libs
    name: libboost-program-options1.58.0
    origin: Ubuntu
    source: apt
    version: 1.58.0+dfsg-5ubuntu3.1
libboost-regex1.58.0:
-   arch: amd64
    category: libs
    name: libboost-regex1.58.0
    origin: Ubuntu
    source: apt
    version: 1.58.0+dfsg-5ubuntu3.1
libboost-system1.58.0:
-   arch: amd64
    category: libs
    name: libboost-system1.58.0
    origin: Ubuntu
    source: apt
    version: 1.58.0+dfsg-5ubuntu3.1
libboost-thread1.58.0:
-   arch: amd64
    category: libs
    name: libboost-thread1.58.0
    origin: Ubuntu
    source: apt
    version: 1.58.0+dfsg-5ubuntu3.1
libbs2b0:
-   arch: amd64
    category: universe/libs
    name: libbs2b0
    origin: Ubuntu
    source: apt
    version: 3.1.0+dfsg-2.2
libbsd0:
-   arch: amd64
    category: libs
    name: libbsd0
    origin: Ubuntu
    source: apt
    version: 0.8.2-1ubuntu0.1
libbz2-1.0:
-   arch: amd64
    category: libs
    name: libbz2-1.0
    origin: Ubuntu
    source: apt
    version: 1.0.6-8ubuntu0.2
libc-bin:
-   arch: amd64
    category: libs
    name: libc-bin
    origin: Ubuntu
    source: apt
    version: 2.23-0ubuntu11
libc-dev-bin:
-   arch: amd64
    category: libdevel
    name: libc-dev-bin
    origin: Ubuntu
    source: apt
    version: 2.23-0ubuntu11
libc6:
-   arch: amd64
    category: libs
    name: libc6
    origin: Ubuntu
    source: apt
    version: 2.23-0ubuntu11
libc6-dev:
-   arch: amd64
    category: libdevel
    name: libc6-dev
    origin: Ubuntu
    source: apt
    version: 2.23-0ubuntu11
libcaca0:
-   arch: amd64
    category: libs
    name: libcaca0
    origin: Ubuntu
    source: apt
    version: 0.99.beta19-2ubuntu0.16.04.1
libcache-cache-perl:
-   arch: all
    category: universe/perl
    name: libcache-cache-perl
    origin: Ubuntu
    source: apt
    version: 1.08-2
libcache-memcached-perl:
-   arch: all
    category: universe/perl
    name: libcache-memcached-perl
    origin: Ubuntu
    source: apt
    version: 1.30-1
libcairo-gobject2:
-   arch: amd64
    category: libs
    name: libcairo-gobject2
    origin: Ubuntu
    source: apt
    version: 1.14.6-1
libcairo2:
-   arch: amd64
    category: libs
    name: libcairo2
    origin: Ubuntu
    source: apt
    version: 1.14.6-1
libcap-ng0:
-   arch: amd64
    category: libs
    name: libcap-ng0
    origin: Ubuntu
    source: apt
    version: 0.7.7-1
libcap2:
-   arch: amd64
    category: libs
    name: libcap2
    origin: Ubuntu
    source: apt
    version: 1:2.24-12
libcap2-bin:
-   arch: amd64
    category: utils
    name: libcap2-bin
    origin: Ubuntu
    source: apt
    version: 1:2.24-12
libcapnp-0.5.3:
-   arch: amd64
    category: libs
    name: libcapnp-0.5.3
    origin: Ubuntu
    source: apt
    version: 0.5.3-2ubuntu1.1
libcc1-0:
-   arch: amd64
    category: libs
    name: libcc1-0
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libcdio-cdda1:
-   arch: amd64
    category: libs
    name: libcdio-cdda1
    origin: Ubuntu
    source: apt
    version: 0.83-4.2ubuntu1
libcdio-paranoia1:
-   arch: amd64
    category: libs
    name: libcdio-paranoia1
    origin: Ubuntu
    source: apt
    version: 0.83-4.2ubuntu1
libcdio13:
-   arch: amd64
    category: libs
    name: libcdio13
    origin: Ubuntu
    source: apt
    version: 0.83-4.2ubuntu1
libcdparanoia0:
-   arch: amd64
    category: sound
    name: libcdparanoia0
    origin: Ubuntu
    source: apt
    version: 3.10.2+debian-11
libcdr-0.1-1:
-   arch: amd64
    category: libs
    name: libcdr-0.1-1
    origin: Ubuntu
    source: apt
    version: 0.1.2-2ubuntu2
libcgi-fast-perl:
-   arch: all
    category: perl
    name: libcgi-fast-perl
    origin: Ubuntu
    source: apt
    version: 1:2.10-1
libcgi-pm-perl:
-   arch: all
    category: perl
    name: libcgi-pm-perl
    origin: Ubuntu
    source: apt
    version: 4.26-1
libchromaprint0:
-   arch: amd64
    category: universe/libs
    name: libchromaprint0
    origin: Ubuntu
    source: apt
    version: 1.3-1
libcilkrts5:
-   arch: amd64
    category: libs
    name: libcilkrts5
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libclass-accessor-perl:
-   arch: all
    category: perl
    name: libclass-accessor-perl
    origin: Ubuntu
    source: apt
    version: 0.34-1
libclucene-contribs1v5:
-   arch: amd64
    category: libs
    name: libclucene-contribs1v5
    origin: Ubuntu
    source: apt
    version: 2.3.3.4-4.1
libclucene-core1v5:
-   arch: amd64
    category: libs
    name: libclucene-core1v5
    origin: Ubuntu
    source: apt
    version: 2.3.3.4-4.1
libcmis-0.5-5v5:
-   arch: amd64
    category: libs
    name: libcmis-0.5-5v5
    origin: Ubuntu
    source: apt
    version: 0.5.1-2ubuntu2
libcolamd2.9.1:
-   arch: amd64
    category: libs
    name: libcolamd2.9.1
    origin: Ubuntu
    source: apt
    version: 1:4.4.6-1
libcolord2:
-   arch: amd64
    category: libs
    name: libcolord2
    origin: Ubuntu
    source: apt
    version: 1.2.12-1ubuntu1
libcolorhug2:
-   arch: amd64
    category: libs
    name: libcolorhug2
    origin: Ubuntu
    source: apt
    version: 1.2.12-1ubuntu1
libcomerr2:
-   arch: amd64
    category: libs
    name: libcomerr2
    origin: Ubuntu
    source: apt
    version: 1.42.13-1ubuntu1.2
libcommons-daemon-java:
-   arch: all
    category: universe/libs
    name: libcommons-daemon-java
    origin: Ubuntu
    source: apt
    version: 1.0.15-6
libcroco3:
-   arch: amd64
    category: libs
    name: libcroco3
    origin: Ubuntu
    source: apt
    version: 0.6.11-1
libcryptsetup4:
-   arch: amd64
    category: libs
    name: libcryptsetup4
    origin: Ubuntu
    source: apt
    version: 2:1.6.6-5ubuntu2.1
libcrystalhd3:
-   arch: amd64
    category: universe/libs
    name: libcrystalhd3
    origin: Ubuntu
    source: apt
    version: 1:0.0~git20110715.fdd2f19-11build1
libcups2:
-   arch: amd64
    category: libs
    name: libcups2
    origin: Ubuntu
    source: apt
    version: 2.1.3-4ubuntu0.11
libcupsfilters1:
-   arch: amd64
    category: libs
    name: libcupsfilters1
    origin: Ubuntu
    source: apt
    version: 1.8.3-2ubuntu3.5
libcupsimage2:
-   arch: amd64
    category: libs
    name: libcupsimage2
    origin: Ubuntu
    source: apt
    version: 2.1.3-4ubuntu0.11
libcurl3:
-   arch: amd64
    category: libs
    name: libcurl3
    origin: Ubuntu
    source: apt
    version: 7.47.0-1ubuntu2.14
libcurl3-gnutls:
-   arch: amd64
    category: libs
    name: libcurl3-gnutls
    origin: Ubuntu
    source: apt
    version: 7.47.0-1ubuntu2.14
libcurl4-openssl-dev:
-   arch: amd64
    category: libdevel
    name: libcurl4-openssl-dev
    origin: Ubuntu
    source: apt
    version: 7.47.0-1ubuntu2.14
libcwidget3v5:
-   arch: amd64
    category: libs
    name: libcwidget3v5
    origin: Ubuntu
    source: apt
    version: 0.5.17-4ubuntu2
libdatrie1:
-   arch: amd64
    category: libs
    name: libdatrie1
    origin: Ubuntu
    source: apt
    version: 0.2.10-2
libdb5.3:
-   arch: amd64
    category: libs
    name: libdb5.3
    origin: Ubuntu
    source: apt
    version: 5.3.28-11ubuntu0.2
libdbus-1-3:
-   arch: amd64
    category: libs
    name: libdbus-1-3
    origin: Ubuntu
    source: apt
    version: 1.10.6-1ubuntu3.5
libdbus-glib-1-2:
-   arch: amd64
    category: libs
    name: libdbus-glib-1-2
    origin: Ubuntu
    source: apt
    version: 0.106-1
libdc1394-22:
-   arch: amd64
    category: universe/libs
    name: libdc1394-22
    origin: Ubuntu
    source: apt
    version: 2.2.4-1
libdca0:
-   arch: amd64
    category: universe/libs
    name: libdca0
    origin: Ubuntu
    source: apt
    version: 0.0.5-7build1
libdconf1:
-   arch: amd64
    category: libs
    name: libdconf1
    origin: Ubuntu
    source: apt
    version: 0.24.0-2
libde265-0:
-   arch: amd64
    category: universe/libs
    name: libde265-0
    origin: Ubuntu
    source: apt
    version: 1.0.2-2
libdebconfclient0:
-   arch: amd64
    category: libs
    name: libdebconfclient0
    origin: Ubuntu
    source: apt
    version: 0.198ubuntu1
libdevmapper-event1.02.1:
-   arch: amd64
    category: libs
    name: libdevmapper-event1.02.1
    origin: Ubuntu
    source: apt
    version: 2:1.02.110-1ubuntu10
libdevmapper1.02.1:
-   arch: amd64
    category: libs
    name: libdevmapper1.02.1
    origin: Ubuntu
    source: apt
    version: 2:1.02.110-1ubuntu10
libdirectfb-1.2-9:
-   arch: amd64
    category: universe/libs
    name: libdirectfb-1.2-9
    origin: Ubuntu
    source: apt
    version: 1.2.10.0-5.1
libdjvulibre-text:
-   arch: all
    category: libs
    name: libdjvulibre-text
    origin: Ubuntu
    source: apt
    version: 3.5.27.1-5ubuntu0.1
libdjvulibre21:
-   arch: amd64
    category: libs
    name: libdjvulibre21
    origin: Ubuntu
    source: apt
    version: 3.5.27.1-5ubuntu0.1
libdns-export162:
-   arch: amd64
    category: libs
    name: libdns-export162
    origin: Ubuntu
    source: apt
    version: 1:9.10.3.dfsg.P4-8ubuntu1.16
libdns162:
-   arch: amd64
    category: libs
    name: libdns162
    origin: Ubuntu
    source: apt
    version: 1:9.10.3.dfsg.P4-8ubuntu1.16
libdpkg-perl:
-   arch: all
    category: perl
    name: libdpkg-perl
    origin: Ubuntu
    source: apt
    version: 1.18.4ubuntu1.6
libdrm-amdgpu1:
-   arch: amd64
    category: libs
    name: libdrm-amdgpu1
    origin: Ubuntu
    source: apt
    version: 2.4.91-2~16.04.1
libdrm-common:
-   arch: all
    category: libs
    name: libdrm-common
    origin: Ubuntu
    source: apt
    version: 2.4.91-2~16.04.1
libdrm-intel1:
-   arch: amd64
    category: libs
    name: libdrm-intel1
    origin: Ubuntu
    source: apt
    version: 2.4.91-2~16.04.1
libdrm-nouveau2:
-   arch: amd64
    category: libs
    name: libdrm-nouveau2
    origin: Ubuntu
    source: apt
    version: 2.4.91-2~16.04.1
libdrm-radeon1:
-   arch: amd64
    category: libs
    name: libdrm-radeon1
    origin: Ubuntu
    source: apt
    version: 2.4.91-2~16.04.1
libdrm2:
-   arch: amd64
    category: libs
    name: libdrm2
    origin: Ubuntu
    source: apt
    version: 2.4.91-2~16.04.1
libdv4:
-   arch: amd64
    category: libs
    name: libdv4
    origin: Ubuntu
    source: apt
    version: 1.0.0-7
libdvdnav4:
-   arch: amd64
    category: universe/libs
    name: libdvdnav4
    origin: Ubuntu
    source: apt
    version: 5.0.3-1
libdvdread4:
-   arch: amd64
    category: universe/libs
    name: libdvdread4
    origin: Ubuntu
    source: apt
    version: 5.0.3-1
libe-book-0.1-1:
-   arch: amd64
    category: libs
    name: libe-book-0.1-1
    origin: Ubuntu
    source: apt
    version: 0.1.2-2ubuntu1
libedit2:
-   arch: amd64
    category: libs
    name: libedit2
    origin: Ubuntu
    source: apt
    version: 3.1-20150325-1ubuntu2
libegl1-mesa:
-   arch: amd64
    category: libs
    name: libegl1-mesa
    origin: Ubuntu
    source: apt
    version: 18.0.5-0ubuntu0~16.04.1
libelf1:
-   arch: amd64
    category: libs
    name: libelf1
    origin: Ubuntu
    source: apt
    version: 0.165-3ubuntu1.2
libenca0:
-   arch: amd64
    category: universe/libs
    name: libenca0
    origin: Ubuntu
    source: apt
    version: 1.18-1
libencode-locale-perl:
-   arch: all
    category: perl
    name: libencode-locale-perl
    origin: Ubuntu
    source: apt
    version: 1.05-1
libeot0:
-   arch: amd64
    category: libs
    name: libeot0
    origin: Ubuntu
    source: apt
    version: 0.01-3ubuntu1
libepoxy0:
-   arch: amd64
    category: libs
    name: libepoxy0
    origin: Ubuntu
    source: apt
    version: 1.3.1-1ubuntu0.16.04.2
liberror-perl:
-   arch: all
    category: perl
    name: liberror-perl
    origin: Ubuntu
    source: apt
    version: 0.17-1.2
libesd0:
-   arch: amd64
    category: universe/libs
    name: libesd0
    origin: Ubuntu
    source: apt
    version: 0.2.41-11
libestr0:
-   arch: amd64
    category: libs
    name: libestr0
    origin: Ubuntu
    source: apt
    version: 0.1.10-1
libetonyek-0.1-1:
-   arch: amd64
    category: libs
    name: libetonyek-0.1-1
    origin: Ubuntu
    source: apt
    version: 0.1.6-1ubuntu1
libevdev2:
-   arch: amd64
    category: libs
    name: libevdev2
    origin: Ubuntu
    source: apt
    version: 1.4.6+dfsg-1
libevent-2.0-5:
-   arch: amd64
    category: libs
    name: libevent-2.0-5
    origin: Ubuntu
    source: apt
    version: 2.0.21-stable-2ubuntu0.16.04.1
libexif12:
-   arch: amd64
    category: libs
    name: libexif12
    origin: Ubuntu
    source: apt
    version: 0.6.21-2ubuntu0.2
libexpat1:
-   arch: amd64
    category: libs
    name: libexpat1
    origin: Ubuntu
    source: apt
    version: 2.1.0-7ubuntu0.16.04.5
libexporter-tiny-perl:
-   arch: all
    category: perl
    name: libexporter-tiny-perl
    origin: Ubuntu
    source: apt
    version: 0.042-1
libexttextcat-2.0-0:
-   arch: amd64
    category: libs
    name: libexttextcat-2.0-0
    origin: Ubuntu
    source: apt
    version: 3.4.4-1ubuntu3
libexttextcat-data:
-   arch: all
    category: text
    name: libexttextcat-data
    origin: Ubuntu
    source: apt
    version: 3.4.4-1ubuntu3
libfaad2:
-   arch: amd64
    category: universe/libs
    name: libfaad2
    origin: Ubuntu
    source: apt
    version: 2.8.0~cvs20150510-1ubuntu0.1
libfakeroot:
-   arch: amd64
    category: utils
    name: libfakeroot
    origin: Ubuntu
    source: apt
    version: 1.20.2-1ubuntu1
libfcgi-perl:
-   arch: amd64
    category: perl
    name: libfcgi-perl
    origin: Ubuntu
    source: apt
    version: 0.77-1build1
libfdisk1:
-   arch: amd64
    category: libs
    name: libfdisk1
    origin: Ubuntu
    source: apt
    version: 2.27.1-6ubuntu3.10
libffi6:
-   arch: amd64
    category: libs
    name: libffi6
    origin: Ubuntu
    source: apt
    version: 3.2.1-4
libfftw3-double3:
-   arch: amd64
    category: libs
    name: libfftw3-double3
    origin: Ubuntu
    source: apt
    version: 3.3.4-2ubuntu1
libfile-fcntllock-perl:
-   arch: amd64
    category: perl
    name: libfile-fcntllock-perl
    origin: Ubuntu
    source: apt
    version: 0.22-3
libfile-listing-perl:
-   arch: all
    category: perl
    name: libfile-listing-perl
    origin: Ubuntu
    source: apt
    version: 6.04-1
libflac8:
-   arch: amd64
    category: libs
    name: libflac8
    origin: Ubuntu
    source: apt
    version: 1.3.1-4
libflite1:
-   arch: amd64
    category: universe/libs
    name: libflite1
    origin: Ubuntu
    source: apt
    version: 2.0.0-release-1
libfluidsynth1:
-   arch: amd64
    category: universe/libs
    name: libfluidsynth1
    origin: Ubuntu
    source: apt
    version: 1.1.6-3
libfont-afm-perl:
-   arch: all
    category: perl
    name: libfont-afm-perl
    origin: Ubuntu
    source: apt
    version: 1.20-1
libfontconfig1:
-   arch: amd64
    category: libs
    name: libfontconfig1
    origin: Ubuntu
    source: apt
    version: 2.11.94-0ubuntu1.1
libfontenc1:
-   arch: amd64
    category: x11
    name: libfontenc1
    origin: Ubuntu
    source: apt
    version: 1:1.1.3-1
libfreehand-0.1-1:
-   arch: amd64
    category: libs
    name: libfreehand-0.1-1
    origin: Ubuntu
    source: apt
    version: 0.1.1-1ubuntu1
libfreetype6:
-   arch: amd64
    category: libs
    name: libfreetype6
    origin: Ubuntu
    source: apt
    version: 2.6.1-0.1ubuntu2.4
libfribidi0:
-   arch: amd64
    category: libs
    name: libfribidi0
    origin: Ubuntu
    source: apt
    version: 0.19.7-1
libfuse2:
-   arch: amd64
    category: libs
    name: libfuse2
    origin: Ubuntu
    source: apt
    version: 2.9.4-1ubuntu3.1
libgbm1:
-   arch: amd64
    category: libs
    name: libgbm1
    origin: Ubuntu
    source: apt
    version: 18.0.5-0ubuntu0~16.04.1
libgcc-5-dev:
-   arch: amd64
    category: libdevel
    name: libgcc-5-dev
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libgcc1:
-   arch: amd64
    category: libs
    name: libgcc1
    origin: Ubuntu
    source: apt
    version: 1:6.0.1-0ubuntu1
libgcrypt20:
-   arch: amd64
    category: libs
    name: libgcrypt20
    origin: Ubuntu
    source: apt
    version: 1.6.5-2ubuntu0.6
libgd3:
-   arch: amd64
    category: libs
    name: libgd3
    origin: Ubuntu
    source: apt
    version: 2.1.1-4ubuntu0.16.04.12
libgdbm3:
-   arch: amd64
    category: libs
    name: libgdbm3
    origin: Ubuntu
    source: apt
    version: 1.8.3-13.1
libgdk-pixbuf2.0-0:
-   arch: amd64
    category: libs
    name: libgdk-pixbuf2.0-0
    origin: Ubuntu
    source: apt
    version: 2.32.2-1ubuntu1.6
libgdk-pixbuf2.0-common:
-   arch: all
    category: libs
    name: libgdk-pixbuf2.0-common
    origin: Ubuntu
    source: apt
    version: 2.32.2-1ubuntu1.6
libgeoip1:
-   arch: amd64
    category: libs
    name: libgeoip1
    origin: Ubuntu
    source: apt
    version: 1.6.9-1
libgif7:
-   arch: amd64
    category: libs
    name: libgif7
    origin: Ubuntu
    source: apt
    version: 5.1.4-0.3~16.04.1
libgirepository-1.0-1:
-   arch: amd64
    category: libs
    name: libgirepository-1.0-1
    origin: Ubuntu
    source: apt
    version: 1.46.0-3ubuntu1
libgl1-mesa-dri:
-   arch: amd64
    category: libs
    name: libgl1-mesa-dri
    origin: Ubuntu
    source: apt
    version: 18.0.5-0ubuntu0~16.04.1
libgl1-mesa-glx:
-   arch: amd64
    category: libs
    name: libgl1-mesa-glx
    origin: Ubuntu
    source: apt
    version: 18.0.5-0ubuntu0~16.04.1
libglapi-mesa:
-   arch: amd64
    category: libs
    name: libglapi-mesa
    origin: Ubuntu
    source: apt
    version: 18.0.5-0ubuntu0~16.04.1
libglew1.13:
-   arch: amd64
    category: libs
    name: libglew1.13
    origin: Ubuntu
    source: apt
    version: 1.13.0-2
libglib2.0-0:
-   arch: amd64
    category: libs
    name: libglib2.0-0
    origin: Ubuntu
    source: apt
    version: 2.48.2-0ubuntu4.6
libglib2.0-data:
-   arch: all
    category: misc
    name: libglib2.0-data
    origin: Ubuntu
    source: apt
    version: 2.48.2-0ubuntu4.6
libglibmm-2.4-1v5:
-   arch: amd64
    category: libs
    name: libglibmm-2.4-1v5
    origin: Ubuntu
    source: apt
    version: 2.46.3-1
libglu1-mesa:
-   arch: amd64
    category: libs
    name: libglu1-mesa
    origin: Ubuntu
    source: apt
    version: 9.0.0-2.1
libgme0:
-   arch: amd64
    category: universe/libs
    name: libgme0
    origin: Ubuntu
    source: apt
    version: 0.6.0-3ubuntu0.16.04.1
libgmp-dev:
-   arch: amd64
    category: libdevel
    name: libgmp-dev
    origin: Ubuntu
    source: apt
    version: 2:6.1.0+dfsg-2
libgmp10:
-   arch: amd64
    category: libs
    name: libgmp10
    origin: Ubuntu
    source: apt
    version: 2:6.1.0+dfsg-2
libgmpxx4ldbl:
-   arch: amd64
    category: libs
    name: libgmpxx4ldbl
    origin: Ubuntu
    source: apt
    version: 2:6.1.0+dfsg-2
libgnutls-openssl27:
-   arch: amd64
    category: libs
    name: libgnutls-openssl27
    origin: Ubuntu
    source: apt
    version: 3.4.10-4ubuntu1.7
libgnutls30:
-   arch: amd64
    category: libs
    name: libgnutls30
    origin: Ubuntu
    source: apt
    version: 3.4.10-4ubuntu1.7
libgomp1:
-   arch: amd64
    category: libs
    name: libgomp1
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libgpg-error0:
-   arch: amd64
    category: libs
    name: libgpg-error0
    origin: Ubuntu
    source: apt
    version: 1.21-2ubuntu1
libgpgme11:
-   arch: amd64
    category: libs
    name: libgpgme11
    origin: Ubuntu
    source: apt
    version: 1.6.0-1
libgphoto2-6:
-   arch: amd64
    category: libs
    name: libgphoto2-6
    origin: Ubuntu
    source: apt
    version: 2.5.9-3
libgphoto2-l10n:
-   arch: all
    category: localization
    name: libgphoto2-l10n
    origin: Ubuntu
    source: apt
    version: 2.5.9-3
libgphoto2-port12:
-   arch: amd64
    category: libs
    name: libgphoto2-port12
    origin: Ubuntu
    source: apt
    version: 2.5.9-3
libgpm2:
-   arch: amd64
    category: libs
    name: libgpm2
    origin: Ubuntu
    source: apt
    version: 1.20.4-6.1
libgraphite2-3:
-   arch: amd64
    category: libs
    name: libgraphite2-3
    origin: Ubuntu
    source: apt
    version: 1.3.10-0ubuntu0.16.04.1
libgs9:
-   arch: amd64
    category: libs
    name: libgs9
    origin: Ubuntu
    source: apt
    version: 9.26~dfsg+0-0ubuntu0.16.04.12
libgs9-common:
-   arch: all
    category: libs
    name: libgs9-common
    origin: Ubuntu
    source: apt
    version: 9.26~dfsg+0-0ubuntu0.16.04.12
libgsm1:
-   arch: amd64
    category: universe/libs
    name: libgsm1
    origin: Ubuntu
    source: apt
    version: 1.0.13-4
libgssapi-krb5-2:
-   arch: amd64
    category: libs
    name: libgssapi-krb5-2
    origin: Ubuntu
    source: apt
    version: 1.13.2+dfsg-5ubuntu2.1
libgssapi3-heimdal:
-   arch: amd64
    category: libs
    name: libgssapi3-heimdal
    origin: Ubuntu
    source: apt
    version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
libgssdp-1.0-3:
-   arch: amd64
    category: universe/libs
    name: libgssdp-1.0-3
    origin: Ubuntu
    source: apt
    version: 0.14.14-1ubuntu1
libgstreamer-plugins-bad1.5-0:
-   arch: amd64
    category: libs
    name: libgstreamer-plugins-bad1.5-0
    origin: BigBlueButton
    source: apt
    version: 1.8.1-1kurento4.16.04
libgstreamer-plugins-base1.0-0:
-   arch: amd64
    category: libs
    name: libgstreamer-plugins-base1.0-0
    origin: Ubuntu
    source: apt
    version: 1.8.3-1ubuntu0.3
libgstreamer-plugins-base1.5-0:
-   arch: amd64
    category: libs
    name: libgstreamer-plugins-base1.5-0
    origin: BigBlueButton
    source: apt
    version: 1.8.1-1kurento2.16.04
libgstreamer1.0-0:
-   arch: amd64
    category: libs
    name: libgstreamer1.0-0
    origin: Ubuntu
    source: apt
    version: 1.8.3-1~ubuntu0.1
libgstreamer1.5-0:
-   arch: amd64
    category: libs
    name: libgstreamer1.5-0
    origin: BigBlueButton
    source: apt
    version: 1.8.1-1kurento2.16.04
libgtk-3-0:
-   arch: amd64
    category: libs
    name: libgtk-3-0
    origin: Ubuntu
    source: apt
    version: 3.18.9-1ubuntu3.3
libgtk-3-bin:
-   arch: amd64
    category: misc
    name: libgtk-3-bin
    origin: Ubuntu
    source: apt
    version: 3.18.9-1ubuntu3.3
libgtk-3-common:
-   arch: all
    category: misc
    name: libgtk-3-common
    origin: Ubuntu
    source: apt
    version: 3.18.9-1ubuntu3.3
libgtk2.0-0:
-   arch: amd64
    category: libs
    name: libgtk2.0-0
    origin: Ubuntu
    source: apt
    version: 2.24.30-1ubuntu1.16.04.2
libgtk2.0-bin:
-   arch: amd64
    category: misc
    name: libgtk2.0-bin
    origin: Ubuntu
    source: apt
    version: 2.24.30-1ubuntu1.16.04.2
libgtk2.0-common:
-   arch: all
    category: misc
    name: libgtk2.0-common
    origin: Ubuntu
    source: apt
    version: 2.24.30-1ubuntu1.16.04.2
libgtkglext1:
-   arch: amd64
    category: universe/libs
    name: libgtkglext1
    origin: Ubuntu
    source: apt
    version: 1.2.0-3.2fakesync1ubuntu1
libgudev-1.0-0:
-   arch: amd64
    category: libs
    name: libgudev-1.0-0
    origin: Ubuntu
    source: apt
    version: 1:230-2
libgupnp-1.0-4:
-   arch: amd64
    category: universe/libs
    name: libgupnp-1.0-4
    origin: Ubuntu
    source: apt
    version: 0.20.16-1
libgupnp-igd-1.0-4:
-   arch: amd64
    category: universe/libs
    name: libgupnp-igd-1.0-4
    origin: Ubuntu
    source: apt
    version: 0.2.4-1
libgusb2:
-   arch: amd64
    category: libs
    name: libgusb2
    origin: Ubuntu
    source: apt
    version: 0.2.9-0ubuntu1
libharfbuzz-icu0:
-   arch: amd64
    category: libs
    name: libharfbuzz-icu0
    origin: Ubuntu
    source: apt
    version: 1.0.1-1ubuntu0.1
libharfbuzz0b:
-   arch: amd64
    category: libs
    name: libharfbuzz0b
    origin: Ubuntu
    source: apt
    version: 1.0.1-1ubuntu0.1
libhavege1:
-   arch: amd64
    category: universe/libs
    name: libhavege1
    origin: Ubuntu
    source: apt
    version: 1.9.1-3
libhcrypto4-heimdal:
-   arch: amd64
    category: libs
    name: libhcrypto4-heimdal
    origin: Ubuntu
    source: apt
    version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
libheimbase1-heimdal:
-   arch: amd64
    category: libs
    name: libheimbase1-heimdal
    origin: Ubuntu
    source: apt
    version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
libheimntlm0-heimdal:
-   arch: amd64
    category: libs
    name: libheimntlm0-heimdal
    origin: Ubuntu
    source: apt
    version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
libhogweed4:
-   arch: amd64
    category: libs
    name: libhogweed4
    origin: Ubuntu
    source: apt
    version: 3.2-1ubuntu0.16.04.1
libhsqldb1.8.0-java:
-   arch: all
    category: java
    name: libhsqldb1.8.0-java
    origin: Ubuntu
    source: apt
    version: 1.8.0.10+dfsg-6
libhtml-form-perl:
-   arch: all
    category: perl
    name: libhtml-form-perl
    origin: Ubuntu
    source: apt
    version: 6.03-1
libhtml-format-perl:
-   arch: all
    category: perl
    name: libhtml-format-perl
    origin: Ubuntu
    source: apt
    version: 2.11-2
libhtml-parser-perl:
-   arch: amd64
    category: perl
    name: libhtml-parser-perl
    origin: Ubuntu
    source: apt
    version: 3.72-1
libhtml-tagset-perl:
-   arch: all
    category: perl
    name: libhtml-tagset-perl
    origin: Ubuntu
    source: apt
    version: 3.20-2
libhtml-tree-perl:
-   arch: all
    category: perl
    name: libhtml-tree-perl
    origin: Ubuntu
    source: apt
    version: 5.03-2
libhttp-cookies-perl:
-   arch: all
    category: perl
    name: libhttp-cookies-perl
    origin: Ubuntu
    source: apt
    version: 6.01-1
libhttp-daemon-perl:
-   arch: all
    category: perl
    name: libhttp-daemon-perl
    origin: Ubuntu
    source: apt
    version: 6.01-1
libhttp-date-perl:
-   arch: all
    category: perl
    name: libhttp-date-perl
    origin: Ubuntu
    source: apt
    version: 6.02-1
libhttp-message-perl:
-   arch: all
    category: perl
    name: libhttp-message-perl
    origin: Ubuntu
    source: apt
    version: 6.11-1
libhttp-negotiate-perl:
-   arch: all
    category: perl
    name: libhttp-negotiate-perl
    origin: Ubuntu
    source: apt
    version: 6.00-2
libhunspell-1.3-0:
-   arch: amd64
    category: libs
    name: libhunspell-1.3-0
    origin: Ubuntu
    source: apt
    version: 1.3.3-4ubuntu1
libhx509-5-heimdal:
-   arch: amd64
    category: libs
    name: libhx509-5-heimdal
    origin: Ubuntu
    source: apt
    version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
libhyphen0:
-   arch: amd64
    category: libs
    name: libhyphen0
    origin: Ubuntu
    source: apt
    version: 2.8.8-2ubuntu1
libice6:
-   arch: amd64
    category: libs
    name: libice6
    origin: Ubuntu
    source: apt
    version: 2:1.0.9-1
libicu-dev:
-   arch: amd64
    category: libdevel
    name: libicu-dev
    origin: Ubuntu
    source: apt
    version: 55.1-7ubuntu0.5
libicu55:
-   arch: amd64
    category: libs
    name: libicu55
    origin: Ubuntu
    source: apt
    version: 55.1-7ubuntu0.5
libidn11:
-   arch: amd64
    category: libs
    name: libidn11
    origin: Ubuntu
    source: apt
    version: 1.32-3ubuntu1.2
libiec61883-0:
-   arch: amd64
    category: libs
    name: libiec61883-0
    origin: Ubuntu
    source: apt
    version: 1.2.0-0.2
libieee1284-3:
-   arch: amd64
    category: libs
    name: libieee1284-3
    origin: Ubuntu
    source: apt
    version: 0.2.11-12
libijs-0.35:
-   arch: amd64
    category: libs
    name: libijs-0.35
    origin: Ubuntu
    source: apt
    version: 0.35-12
libilmbase12:
-   arch: amd64
    category: libs
    name: libilmbase12
    origin: Ubuntu
    source: apt
    version: 2.2.0-11ubuntu2
libinput-bin:
-   arch: amd64
    category: libs
    name: libinput-bin
    origin: Ubuntu
    source: apt
    version: 1.6.3-1ubuntu1~16.04.1
libinput10:
-   arch: amd64
    category: libs
    name: libinput10
    origin: Ubuntu
    source: apt
    version: 1.6.3-1ubuntu1~16.04.1
libio-html-perl:
-   arch: all
    category: perl
    name: libio-html-perl
    origin: Ubuntu
    source: apt
    version: 1.001-1
libio-multiplex-perl:
-   arch: all
    category: perl
    name: libio-multiplex-perl
    origin: Ubuntu
    source: apt
    version: 1.16-1
libio-socket-inet6-perl:
-   arch: all
    category: perl
    name: libio-socket-inet6-perl
    origin: Ubuntu
    source: apt
    version: 2.72-2
libio-socket-ssl-perl:
-   arch: all
    category: perl
    name: libio-socket-ssl-perl
    origin: Ubuntu
    source: apt
    version: 2.024-1
libio-string-perl:
-   arch: all
    category: perl
    name: libio-string-perl
    origin: Ubuntu
    source: apt
    version: 1.08-3
libipc-sharelite-perl:
-   arch: amd64
    category: universe/perl
    name: libipc-sharelite-perl
    origin: Ubuntu
    source: apt
    version: 0.17-3build3
libisc-export160:
-   arch: amd64
    category: libs
    name: libisc-export160
    origin: Ubuntu
    source: apt
    version: 1:9.10.3.dfsg.P4-8ubuntu1.16
libisc160:
-   arch: amd64
    category: libs
    name: libisc160
    origin: Ubuntu
    source: apt
    version: 1:9.10.3.dfsg.P4-8ubuntu1.16
libisccc140:
-   arch: amd64
    category: libs
    name: libisccc140
    origin: Ubuntu
    source: apt
    version: 1:9.10.3.dfsg.P4-8ubuntu1.16
libisccfg140:
-   arch: amd64
    category: libs
    name: libisccfg140
    origin: Ubuntu
    source: apt
    version: 1:9.10.3.dfsg.P4-8ubuntu1.16
libisl15:
-   arch: amd64
    category: libs
    name: libisl15
    origin: Ubuntu
    source: apt
    version: 0.16.1-1
libitm1:
-   arch: amd64
    category: libs
    name: libitm1
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libjack-jackd2-0:
-   arch: amd64
    category: libs
    name: libjack-jackd2-0
    origin: Ubuntu
    source: apt
    version: 1.9.10+20150825git1ed50c92~dfsg-1ubuntu1
libjasper1:
-   arch: amd64
    category: libs
    name: libjasper1
    origin: Ubuntu
    source: apt
    version: 1.900.1-debian1-2.4ubuntu1.2
libjbig0:
-   arch: amd64
    category: libs
    name: libjbig0
    origin: Ubuntu
    source: apt
    version: 2.1-3.1
libjbig2dec0:
-   arch: amd64
    category: libs
    name: libjbig2dec0
    origin: Ubuntu
    source: apt
    version: 0.12+20150918-1ubuntu0.1
libjemalloc1:
-   arch: amd64
    category: universe/libs
    name: libjemalloc1
    origin: Ubuntu
    source: apt
    version: 3.6.0-9ubuntu1
libjpeg-turbo8:
-   arch: amd64
    category: libs
    name: libjpeg-turbo8
    origin: Ubuntu
    source: apt
    version: 1.4.2-0ubuntu3.3
libjpeg8:
-   arch: amd64
    category: libs
    name: libjpeg8
    origin: Ubuntu
    source: apt
    version: 8c-2ubuntu8
libjs-jquery:
-   arch: all
    category: web
    name: libjs-jquery
    origin: Ubuntu
    source: apt
    version: 1.11.3+dfsg-4
libjson-c2:
-   arch: amd64
    category: libs
    name: libjson-c2
    origin: Ubuntu
    source: apt
    version: 0.11-4ubuntu2.6
libjson-glib-1.0-0:
-   arch: amd64
    category: libs
    name: libjson-glib-1.0-0
    origin: Ubuntu
    source: apt
    version: 1.1.2-0ubuntu1
libjson-glib-1.0-common:
-   arch: all
    category: libs
    name: libjson-glib-1.0-common
    origin: Ubuntu
    source: apt
    version: 1.1.2-0ubuntu1
libk5crypto3:
-   arch: amd64
    category: libs
    name: libk5crypto3
    origin: Ubuntu
    source: apt
    version: 1.13.2+dfsg-5ubuntu2.1
libkate1:
-   arch: amd64
    category: universe/libs
    name: libkate1
    origin: Ubuntu
    source: apt
    version: 0.4.1-7
libkeyutils1:
-   arch: amd64
    category: misc
    name: libkeyutils1
    origin: Ubuntu
    source: apt
    version: 1.5.9-8ubuntu1
libklibc:
-   arch: amd64
    category: libs
    name: libklibc
    origin: Ubuntu
    source: apt
    version: 2.0.4-8ubuntu1.16.04.4
libkmod2:
-   arch: amd64
    category: libs
    name: libkmod2
    origin: Ubuntu
    source: apt
    version: 22-1ubuntu5.2
libkrb5-26-heimdal:
-   arch: amd64
    category: libs
    name: libkrb5-26-heimdal
    origin: Ubuntu
    source: apt
    version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
libkrb5-3:
-   arch: amd64
    category: libs
    name: libkrb5-3
    origin: Ubuntu
    source: apt
    version: 1.13.2+dfsg-5ubuntu2.1
libkrb5support0:
-   arch: amd64
    category: libs
    name: libkrb5support0
    origin: Ubuntu
    source: apt
    version: 1.13.2+dfsg-5ubuntu2.1
libksba8:
-   arch: amd64
    category: libs
    name: libksba8
    origin: Ubuntu
    source: apt
    version: 1.3.3-1ubuntu0.16.04.1
liblangtag-common:
-   arch: all
    category: libs
    name: liblangtag-common
    origin: Ubuntu
    source: apt
    version: 0.5.7-2ubuntu1
liblangtag1:
-   arch: amd64
    category: libs
    name: liblangtag1
    origin: Ubuntu
    source: apt
    version: 0.5.7-2ubuntu1
liblcms2-2:
-   arch: amd64
    category: libs
    name: liblcms2-2
    origin: Ubuntu
    source: apt
    version: 2.6-3ubuntu2.1
libldap-2.4-2:
-   arch: amd64
    category: libs
    name: libldap-2.4-2
    origin: Ubuntu
    source: apt
    version: 2.4.42+dfsg-2ubuntu3.8
libldb1:
-   arch: amd64
    category: libs
    name: libldb1
    origin: Ubuntu
    source: apt
    version: 2:1.1.24-1ubuntu3.1
libldns1:
-   arch: amd64
    category: libs
    name: libldns1
    origin: Ubuntu
    source: apt
    version: 1.6.17-8ubuntu0.1
liblircclient0:
-   arch: amd64
    category: libs
    name: liblircclient0
    origin: Ubuntu
    source: apt
    version: 0.9.0-0ubuntu6
liblist-moreutils-perl:
-   arch: amd64
    category: perl
    name: liblist-moreutils-perl
    origin: Ubuntu
    source: apt
    version: 0.413-1build1
libllvm6.0:
-   arch: amd64
    category: libs
    name: libllvm6.0
    origin: Ubuntu
    source: apt
    version: 1:6.0-1ubuntu2~16.04.1
liblocale-gettext-perl:
-   arch: amd64
    category: perl
    name: liblocale-gettext-perl
    origin: Ubuntu
    source: apt
    version: 1.07-1build1
liblqr-1-0:
-   arch: amd64
    category: libs
    name: liblqr-1-0
    origin: Ubuntu
    source: apt
    version: 0.4.2-2
liblsan0:
-   arch: amd64
    category: libs
    name: liblsan0
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libltdl7:
-   arch: amd64
    category: libs
    name: libltdl7
    origin: Ubuntu
    source: apt
    version: 2.4.6-0.1
liblua5.1-0:
-   arch: amd64
    category: libs
    name: liblua5.1-0
    origin: Ubuntu
    source: apt
    version: 5.1.5-8ubuntu1
liblua5.2-0:
-   arch: amd64
    category: libs
    name: liblua5.2-0
    origin: Ubuntu
    source: apt
    version: 5.2.4-1ubuntu1
libluajit-5.1-2:
-   arch: amd64
    category: universe/interpreters
    name: libluajit-5.1-2
    origin: Ubuntu
    source: apt
    version: 2.0.4+dfsg-1
libluajit-5.1-common:
-   arch: all
    category: universe/interpreters
    name: libluajit-5.1-common
    origin: Ubuntu
    source: apt
    version: 2.0.4+dfsg-1
liblvm2app2.2:
-   arch: amd64
    category: libs
    name: liblvm2app2.2
    origin: Ubuntu
    source: apt
    version: 2.02.133-1ubuntu10
liblvm2cmd2.02:
-   arch: amd64
    category: libs
    name: liblvm2cmd2.02
    origin: Ubuntu
    source: apt
    version: 2.02.133-1ubuntu10
liblwp-mediatypes-perl:
-   arch: all
    category: perl
    name: liblwp-mediatypes-perl
    origin: Ubuntu
    source: apt
    version: 6.02-1
liblwp-protocol-https-perl:
-   arch: all
    category: perl
    name: liblwp-protocol-https-perl
    origin: Ubuntu
    source: apt
    version: 6.06-2
liblwres141:
-   arch: amd64
    category: libs
    name: liblwres141
    origin: Ubuntu
    source: apt
    version: 1:9.10.3.dfsg.P4-8ubuntu1.16
liblz4-1:
-   arch: amd64
    category: libs
    name: liblz4-1
    origin: Ubuntu
    source: apt
    version: 0.0~r131-2ubuntu2
liblzma5:
-   arch: amd64
    category: libs
    name: liblzma5
    origin: Ubuntu
    source: apt
    version: 5.1.1alpha+20120614-2ubuntu2
liblzo2-2:
-   arch: amd64
    category: libs
    name: liblzo2-2
    origin: Ubuntu
    source: apt
    version: 2.08-1.2
libmad0:
-   arch: amd64
    category: universe/libs
    name: libmad0
    origin: Ubuntu
    source: apt
    version: 0.15.1b-9ubuntu16.04.1
libmagic1:
-   arch: amd64
    category: libs
    name: libmagic1
    origin: Ubuntu
    source: apt
    version: 1:5.25-2ubuntu1.4
libmagickcore-6.q16-2:
-   arch: amd64
    category: libs
    name: libmagickcore-6.q16-2
    origin: Ubuntu
    source: apt
    version: 8:6.8.9.9-7ubuntu5.15
libmagickcore-6.q16-2-extra:
-   arch: amd64
    category: libs
    name: libmagickcore-6.q16-2-extra
    origin: Ubuntu
    source: apt
    version: 8:6.8.9.9-7ubuntu5.15
libmagickwand-6.q16-2:
-   arch: amd64
    category: libs
    name: libmagickwand-6.q16-2
    origin: Ubuntu
    source: apt
    version: 8:6.8.9.9-7ubuntu5.15
libmailtools-perl:
-   arch: all
    category: perl
    name: libmailtools-perl
    origin: Ubuntu
    source: apt
    version: 2.13-1
libmhash2:
-   arch: amd64
    category: libs
    name: libmhash2
    origin: Ubuntu
    source: apt
    version: 0.9.9.9-7
libmimic0:
-   arch: amd64
    category: universe/libs
    name: libmimic0
    origin: Ubuntu
    source: apt
    version: 1.0.4-2.3
libmirclient9:
-   arch: amd64
    category: libs
    name: libmirclient9
    origin: Ubuntu
    source: apt
    version: 0.26.3+16.04.20170605-0ubuntu1.1
libmircommon7:
-   arch: amd64
    category: libs
    name: libmircommon7
    origin: Ubuntu
    source: apt
    version: 0.26.3+16.04.20170605-0ubuntu1.1
libmircore1:
-   arch: amd64
    category: libs
    name: libmircore1
    origin: Ubuntu
    source: apt
    version: 0.26.3+16.04.20170605-0ubuntu1.1
libmirprotobuf3:
-   arch: amd64
    category: libs
    name: libmirprotobuf3
    origin: Ubuntu
    source: apt
    version: 0.26.3+16.04.20170605-0ubuntu1.1
libmjpegutils-2.1-0:
-   arch: amd64
    category: universe/libs
    name: libmjpegutils-2.1-0
    origin: Ubuntu
    source: apt
    version: 1:2.1.0+debian-4
libmms0:
-   arch: amd64
    category: universe/libs
    name: libmms0
    origin: Ubuntu
    source: apt
    version: 0.6.4-1
libmnl0:
-   arch: amd64
    category: libs
    name: libmnl0
    origin: Ubuntu
    source: apt
    version: 1.0.3-5
libmodplug1:
-   arch: amd64
    category: universe/libs
    name: libmodplug1
    origin: Ubuntu
    source: apt
    version: 1:0.8.8.5-2
libmount1:
-   arch: amd64
    category: libs
    name: libmount1
    origin: Ubuntu
    source: apt
    version: 2.27.1-6ubuntu3.10
libmp3lame0:
-   arch: amd64
    category: universe/libs
    name: libmp3lame0
    origin: Ubuntu
    source: apt
    version: 3.99.5+repack1-9build1
libmpc3:
-   arch: amd64
    category: libs
    name: libmpc3
    origin: Ubuntu
    source: apt
    version: 1.0.3-1
libmpdec2:
-   arch: amd64
    category: libs
    name: libmpdec2
    origin: Ubuntu
    source: apt
    version: 2.4.2-1
libmpeg2-4:
-   arch: amd64
    category: universe/libs
    name: libmpeg2-4
    origin: Ubuntu
    source: apt
    version: 0.5.1-7
libmpeg2encpp-2.1-0:
-   arch: amd64
    category: universe/libs
    name: libmpeg2encpp-2.1-0
    origin: Ubuntu
    source: apt
    version: 1:2.1.0+debian-4
libmpfr4:
-   arch: amd64
    category: libs
    name: libmpfr4
    origin: Ubuntu
    source: apt
    version: 3.1.4-1
libmpg123-0:
-   arch: amd64
    category: universe/libs
    name: libmpg123-0
    origin: Ubuntu
    source: apt
    version: 1.22.4-1ubuntu0.1
libmplex2-2.1-0:
-   arch: amd64
    category: universe/libs
    name: libmplex2-2.1-0
    origin: Ubuntu
    source: apt
    version: 1:2.1.0+debian-4
libmpx0:
-   arch: amd64
    category: libs
    name: libmpx0
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libmspack0:
-   arch: amd64
    category: libs
    name: libmspack0
    origin: Ubuntu
    source: apt
    version: 0.5-1ubuntu0.16.04.4
libmspub-0.1-1:
-   arch: amd64
    category: libs
    name: libmspub-0.1-1
    origin: Ubuntu
    source: apt
    version: 0.1.2-2ubuntu1
libmtdev1:
-   arch: amd64
    category: libs
    name: libmtdev1
    origin: Ubuntu
    source: apt
    version: 1.1.5-1ubuntu2
libmwaw-0.3-3:
-   arch: amd64
    category: libs
    name: libmwaw-0.3-3
    origin: Ubuntu
    source: apt
    version: 0.3.7-1ubuntu2.1
libmythes-1.2-0:
-   arch: amd64
    category: libs
    name: libmythes-1.2-0
    origin: Ubuntu
    source: apt
    version: 2:1.2.4-1ubuntu3
libncurses5:
-   arch: amd64
    category: libs
    name: libncurses5
    origin: Ubuntu
    source: apt
    version: 6.0+20160213-1ubuntu1
libncurses5-dev:
-   arch: amd64
    category: libdevel
    name: libncurses5-dev
    origin: Ubuntu
    source: apt
    version: 6.0+20160213-1ubuntu1
libncursesw5:
-   arch: amd64
    category: libs
    name: libncursesw5
    origin: Ubuntu
    source: apt
    version: 6.0+20160213-1ubuntu1
libneon27-gnutls:
-   arch: amd64
    category: libs
    name: libneon27-gnutls
    origin: Ubuntu
    source: apt
    version: 0.30.1-3build1
libnet-cidr-perl:
-   arch: all
    category: perl
    name: libnet-cidr-perl
    origin: Ubuntu
    source: apt
    version: 0.17-1
libnet-http-perl:
-   arch: all
    category: perl
    name: libnet-http-perl
    origin: Ubuntu
    source: apt
    version: 6.09-1
libnet-server-perl:
-   arch: all
    category: perl
    name: libnet-server-perl
    origin: Ubuntu
    source: apt
    version: 2.008-2
libnet-smtp-ssl-perl:
-   arch: all
    category: perl
    name: libnet-smtp-ssl-perl
    origin: Ubuntu
    source: apt
    version: 1.03-1
libnet-snmp-perl:
-   arch: all
    category: perl
    name: libnet-snmp-perl
    origin: Ubuntu
    source: apt
    version: 6.0.1-2
libnet-ssleay-perl:
-   arch: amd64
    category: perl
    name: libnet-ssleay-perl
    origin: Ubuntu
    source: apt
    version: 1.72-1build1
libnetpbm10:
-   arch: amd64
    category: libs
    name: libnetpbm10
    origin: Ubuntu
    source: apt
    version: 2:10.0-15.3
libnettle6:
-   arch: amd64
    category: libs
    name: libnettle6
    origin: Ubuntu
    source: apt
    version: 3.2-1ubuntu0.16.04.1
libnewt0.52:
-   arch: amd64
    category: libs
    name: libnewt0.52
    origin: Ubuntu
    source: apt
    version: 0.52.18-1ubuntu2
libnfnetlink0:
-   arch: amd64
    category: libs
    name: libnfnetlink0
    origin: Ubuntu
    source: apt
    version: 1.0.1-3
libnice10:
-   arch: amd64
    category: libs
    name: libnice10
    origin: BigBlueButton
    source: apt
    version: 0.1.15-1kurento3.16.04
libnih1:
-   arch: amd64
    category: libs
    name: libnih1
    origin: Ubuntu
    source: apt
    version: 1.0.3-4.3ubuntu1
libnl-3-200:
-   arch: amd64
    category: libs
    name: libnl-3-200
    origin: Ubuntu
    source: apt
    version: 3.2.27-1ubuntu0.16.04.1
libnl-genl-3-200:
-   arch: amd64
    category: libs
    name: libnl-genl-3-200
    origin: Ubuntu
    source: apt
    version: 3.2.27-1ubuntu0.16.04.1
libnotify4:
-   arch: amd64
    category: libs
    name: libnotify4
    origin: Ubuntu
    source: apt
    version: 0.7.6-2svn1
libnpth0:
-   arch: amd64
    category: libs
    name: libnpth0
    origin: Ubuntu
    source: apt
    version: 1.2-3
libnspr4:
-   arch: amd64
    category: libs
    name: libnspr4
    origin: Ubuntu
    source: apt
    version: 2:4.13.1-0ubuntu0.16.04.1
libnss3:
-   arch: amd64
    category: libs
    name: libnss3
    origin: Ubuntu
    source: apt
    version: 2:3.28.4-0ubuntu0.16.04.10
libnss3-nssdb:
-   arch: all
    category: admin
    name: libnss3-nssdb
    origin: Ubuntu
    source: apt
    version: 2:3.28.4-0ubuntu0.16.04.10
libnuma1:
-   arch: amd64
    category: libs
    name: libnuma1
    origin: Ubuntu
    source: apt
    version: 2.0.11-1ubuntu1.1
libodfgen-0.1-1:
-   arch: amd64
    category: libs
    name: libodfgen-0.1-1
    origin: Ubuntu
    source: apt
    version: 0.1.6-1ubuntu2
libofa0:
-   arch: amd64
    category: universe/libs
    name: libofa0
    origin: Ubuntu
    source: apt
    version: 0.9.3-10ubuntu1
libogg0:
-   arch: amd64
    category: libs
    name: libogg0
    origin: Ubuntu
    source: apt
    version: 1.3.2-1
libopenal-data:
-   arch: all
    category: universe/libs
    name: libopenal-data
    origin: Ubuntu
    source: apt
    version: 1:1.16.0-3
libopenal1:
-   arch: amd64
    category: universe/libs
    name: libopenal1
    origin: Ubuntu
    source: apt
    version: 1:1.16.0-3
libopencore-amrnb0:
-   arch: amd64
    category: universe/libs
    name: libopencore-amrnb0
    origin: Ubuntu
    source: apt
    version: 0.1.3-2.1
libopencore-amrwb0:
-   arch: amd64
    category: universe/libs
    name: libopencore-amrwb0
    origin: Ubuntu
    source: apt
    version: 0.1.3-2.1
libopencv-core2.4v5:
-   arch: amd64
    category: universe/libs
    name: libopencv-core2.4v5
    origin: Ubuntu
    source: apt
    version: 2.4.9.1+dfsg-1.5ubuntu1.1
libopencv-highgui2.4v5:
-   arch: amd64
    category: universe/libs
    name: libopencv-highgui2.4v5
    origin: Ubuntu
    source: apt
    version: 2.4.9.1+dfsg-1.5ubuntu1.1
libopencv-imgproc2.4v5:
-   arch: amd64
    category: universe/libs
    name: libopencv-imgproc2.4v5
    origin: Ubuntu
    source: apt
    version: 2.4.9.1+dfsg-1.5ubuntu1.1
libopencv-objdetect2.4v5:
-   arch: amd64
    category: universe/libs
    name: libopencv-objdetect2.4v5
    origin: Ubuntu
    source: apt
    version: 2.4.9.1+dfsg-1.5ubuntu1.1
libopenexr22:
-   arch: amd64
    category: libs
    name: libopenexr22
    origin: Ubuntu
    source: apt
    version: 2.2.0-10ubuntu2.2
libopenjpeg5:
-   arch: amd64
    category: universe/libs
    name: libopenjpeg5
    origin: Ubuntu
    source: apt
    version: 1:1.5.2-3.1
libopus0:
-   arch: amd64
    category: libs
    name: libopus0
    origin: Ubuntu
    source: apt
    version: 1.1.2-1ubuntu1
libopusenc0:
-   arch: amd64
    category: sound
    name: libopusenc0
    origin: LP-PPA-bigbluebutton-support
    source: apt
    version: 0.2.1-1bbb1
libopusfile0:
-   arch: amd64
    category: universe/libs
    name: libopusfile0
    origin: Ubuntu
    source: apt
    version: 0.7-1
liborc-0.4-0:
-   arch: amd64
    category: libs
    name: liborc-0.4-0
    origin: Ubuntu
    source: apt
    version: 1:0.4.25-1
liborcus-0.10-0v5:
-   arch: amd64
    category: libs
    name: liborcus-0.10-0v5
    origin: Ubuntu
    source: apt
    version: 0.9.2-4ubuntu2
libp11-kit0:
-   arch: amd64
    category: libs
    name: libp11-kit0
    origin: Ubuntu
    source: apt
    version: 0.23.2-5~ubuntu16.04.1
libpagemaker-0.0-0:
-   arch: amd64
    category: libs
    name: libpagemaker-0.0-0
    origin: Ubuntu
    source: apt
    version: 0.0.3-1ubuntu1
libpam-modules:
-   arch: amd64
    category: admin
    name: libpam-modules
    origin: Ubuntu
    source: apt
    version: 1.1.8-3.2ubuntu2.1
libpam-modules-bin:
-   arch: amd64
    category: admin
    name: libpam-modules-bin
    origin: Ubuntu
    source: apt
    version: 1.1.8-3.2ubuntu2.1
libpam-runtime:
-   arch: all
    category: admin
    name: libpam-runtime
    origin: Ubuntu
    source: apt
    version: 1.1.8-3.2ubuntu2.1
libpam-systemd:
-   arch: amd64
    category: admin
    name: libpam-systemd
    origin: Ubuntu
    source: apt
    version: 229-4ubuntu21.28
libpam0g:
-   arch: amd64
    category: libs
    name: libpam0g
    origin: Ubuntu
    source: apt
    version: 1.1.8-3.2ubuntu2.1
libpango-1.0-0:
-   arch: amd64
    category: libs
    name: libpango-1.0-0
    origin: Ubuntu
    source: apt
    version: 1.38.1-1
libpangocairo-1.0-0:
-   arch: amd64
    category: libs
    name: libpangocairo-1.0-0
    origin: Ubuntu
    source: apt
    version: 1.38.1-1
libpangoft2-1.0-0:
-   arch: amd64
    category: libs
    name: libpangoft2-1.0-0
    origin: Ubuntu
    source: apt
    version: 1.38.1-1
libpangox-1.0-0:
-   arch: amd64
    category: oldlibs
    name: libpangox-1.0-0
    origin: Ubuntu
    source: apt
    version: 0.0.2-5
libpaper-utils:
-   arch: amd64
    category: utils
    name: libpaper-utils
    origin: Ubuntu
    source: apt
    version: 1.1.24+nmu4ubuntu1
libpaper1:
-   arch: amd64
    category: libs
    name: libpaper1
    origin: Ubuntu
    source: apt
    version: 1.1.24+nmu4ubuntu1
libparse-debianchangelog-perl:
-   arch: all
    category: perl
    name: libparse-debianchangelog-perl
    origin: Ubuntu
    source: apt
    version: 1.2.0-8
libpci3:
-   arch: amd64
    category: libs
    name: libpci3
    origin: Ubuntu
    source: apt
    version: 1:3.3.1-1.1ubuntu1.3
libpciaccess0:
-   arch: amd64
    category: libs
    name: libpciaccess0
    origin: Ubuntu
    source: apt
    version: 0.13.4-1
libpcre16-3:
-   arch: amd64
    category: libs
    name: libpcre16-3
    origin: Ubuntu
    source: apt
    version: 2:8.38-3.1
libpcre3:
-   arch: amd64
    category: libs
    name: libpcre3
    origin: Ubuntu
    source: apt
    version: 2:8.38-3.1
libpcsclite1:
-   arch: amd64
    category: libs
    name: libpcsclite1
    origin: Ubuntu
    source: apt
    version: 1.8.14-1ubuntu1.16.04.1
libperl5.22:
-   arch: amd64
    category: libs
    name: libperl5.22
    origin: Ubuntu
    source: apt
    version: 5.22.1-9ubuntu0.6
libpixman-1-0:
-   arch: amd64
    category: libs
    name: libpixman-1-0
    origin: Ubuntu
    source: apt
    version: 0.33.6-1
libplymouth4:
-   arch: amd64
    category: libs
    name: libplymouth4
    origin: Ubuntu
    source: apt
    version: 0.9.2-3ubuntu13.5
libpng12-0:
-   arch: amd64
    category: libs
    name: libpng12-0
    origin: Ubuntu
    source: apt
    version: 1.2.54-1ubuntu1.1
libpolkit-agent-1-0:
-   arch: amd64
    category: libs
    name: libpolkit-agent-1-0
    origin: Ubuntu
    source: apt
    version: 0.105-14.1ubuntu0.5
libpolkit-backend-1-0:
-   arch: amd64
    category: libs
    name: libpolkit-backend-1-0
    origin: Ubuntu
    source: apt
    version: 0.105-14.1ubuntu0.5
libpolkit-gobject-1-0:
-   arch: amd64
    category: libs
    name: libpolkit-gobject-1-0
    origin: Ubuntu
    source: apt
    version: 0.105-14.1ubuntu0.5
libpoppler58:
-   arch: amd64
    category: libs
    name: libpoppler58
    origin: Ubuntu
    source: apt
    version: 0.41.0-0ubuntu1.14
libpopt0:
-   arch: amd64
    category: libs
    name: libpopt0
    origin: Ubuntu
    source: apt
    version: 1.16-10
libpostproc-ffmpeg53:
-   arch: amd64
    category: universe/libs
    name: libpostproc-ffmpeg53
    origin: Ubuntu
    source: apt
    version: 7:2.8.15-0ubuntu0.16.04.1
libpostproc55:
-   arch: amd64
    category: libs
    name: libpostproc55
    origin: LP-PPA-bigbluebutton-support
    source: apt
    version: 7:4.2.2-1bbb1~ubuntu16.04
libprocps4:
-   arch: amd64
    category: libs
    name: libprocps4
    origin: Ubuntu
    source: apt
    version: 2:3.3.10-4ubuntu2.5
libprotobuf-lite9v5:
-   arch: amd64
    category: libs
    name: libprotobuf-lite9v5
    origin: Ubuntu
    source: apt
    version: 2.6.1-1.3
libproxy1v5:
-   arch: amd64
    category: libs
    name: libproxy1v5
    origin: Ubuntu
    source: apt
    version: 0.4.11-5ubuntu1
libpulse0:
-   arch: amd64
    category: libs
    name: libpulse0
    origin: Ubuntu
    source: apt
    version: 1:8.0-0ubuntu3.12
libpython-stdlib:
-   arch: amd64
    category: python
    name: libpython-stdlib
    origin: Ubuntu
    source: apt
    version: 2.7.12-1~16.04
libpython2.7:
-   arch: amd64
    category: libs
    name: libpython2.7
    origin: Ubuntu
    source: apt
    version: 2.7.12-1ubuntu0~16.04.11
libpython2.7-minimal:
-   arch: amd64
    category: python
    name: libpython2.7-minimal
    origin: Ubuntu
    source: apt
    version: 2.7.12-1ubuntu0~16.04.11
libpython2.7-stdlib:
-   arch: amd64
    category: python
    name: libpython2.7-stdlib
    origin: Ubuntu
    source: apt
    version: 2.7.12-1ubuntu0~16.04.11
libpython3-stdlib:
-   arch: amd64
    category: python
    name: libpython3-stdlib
    origin: Ubuntu
    source: apt
    version: 3.5.1-3
libpython3.5:
-   arch: amd64
    category: libs
    name: libpython3.5
    origin: Ubuntu
    source: apt
    version: 3.5.2-2ubuntu0~16.04.10
libpython3.5-minimal:
-   arch: amd64
    category: python
    name: libpython3.5-minimal
    origin: Ubuntu
    source: apt
    version: 3.5.2-2ubuntu0~16.04.10
libpython3.5-stdlib:
-   arch: amd64
    category: python
    name: libpython3.5-stdlib
    origin: Ubuntu
    source: apt
    version: 3.5.2-2ubuntu0~16.04.10
libqt5core5a:
-   arch: amd64
    category: libs
    name: libqt5core5a
    origin: Ubuntu
    source: apt
    version: 5.5.1+dfsg-16ubuntu7.7
libqt5dbus5:
-   arch: amd64
    category: libs
    name: libqt5dbus5
    origin: Ubuntu
    source: apt
    version: 5.5.1+dfsg-16ubuntu7.7
libqt5gui5:
-   arch: amd64
    category: libs
    name: libqt5gui5
    origin: Ubuntu
    source: apt
    version: 5.5.1+dfsg-16ubuntu7.7
libqt5network5:
-   arch: amd64
    category: libs
    name: libqt5network5
    origin: Ubuntu
    source: apt
    version: 5.5.1+dfsg-16ubuntu7.7
libqt5printsupport5:
-   arch: amd64
    category: libs
    name: libqt5printsupport5
    origin: Ubuntu
    source: apt
    version: 5.5.1+dfsg-16ubuntu7.7
libqt5svg5:
-   arch: amd64
    category: libs
    name: libqt5svg5
    origin: Ubuntu
    source: apt
    version: 5.5.1-2build1
libqt5widgets5:
-   arch: amd64
    category: libs
    name: libqt5widgets5
    origin: Ubuntu
    source: apt
    version: 5.5.1+dfsg-16ubuntu7.7
libquadmath0:
-   arch: amd64
    category: libs
    name: libquadmath0
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libraptor2-0:
-   arch: amd64
    category: libs
    name: libraptor2-0
    origin: Ubuntu
    source: apt
    version: 2.0.14-1
librasqal3:
-   arch: amd64
    category: libs
    name: librasqal3
    origin: Ubuntu
    source: apt
    version: 0.9.32-1
libraw1394-11:
-   arch: amd64
    category: libs
    name: libraw1394-11
    origin: Ubuntu
    source: apt
    version: 2.1.1-2
librdf0:
-   arch: amd64
    category: libs
    name: librdf0
    origin: Ubuntu
    source: apt
    version: 1.0.17-1build1
libreadline5:
-   arch: amd64
    category: libs
    name: libreadline5
    origin: Ubuntu
    source: apt
    version: 5.2+dfsg-3build1
libreadline6:
-   arch: amd64
    category: libs
    name: libreadline6
    origin: Ubuntu
    source: apt
    version: 6.3-8ubuntu2
libreoffice:
-   arch: amd64
    category: universe/editors
    name: libreoffice
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-avmedia-backend-gstreamer:
-   arch: amd64
    category: misc
    name: libreoffice-avmedia-backend-gstreamer
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-base:
-   arch: amd64
    category: database
    name: libreoffice-base
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-base-core:
-   arch: amd64
    category: editors
    name: libreoffice-base-core
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-base-drivers:
-   arch: amd64
    category: database
    name: libreoffice-base-drivers
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-calc:
-   arch: amd64
    category: editors
    name: libreoffice-calc
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-common:
-   arch: all
    category: editors
    name: libreoffice-common
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-core:
-   arch: amd64
    category: editors
    name: libreoffice-core
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-draw:
-   arch: amd64
    category: editors
    name: libreoffice-draw
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-gnome:
-   arch: amd64
    category: gnome
    name: libreoffice-gnome
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-gtk:
-   arch: amd64
    category: gnome
    name: libreoffice-gtk
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-impress:
-   arch: amd64
    category: editors
    name: libreoffice-impress
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-java-common:
-   arch: all
    category: editors
    name: libreoffice-java-common
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-math:
-   arch: amd64
    category: editors
    name: libreoffice-math
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-pdfimport:
-   arch: amd64
    category: misc
    name: libreoffice-pdfimport
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-report-builder-bin:
-   arch: amd64
    category: universe/misc
    name: libreoffice-report-builder-bin
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-sdbc-firebird:
-   arch: amd64
    category: database
    name: libreoffice-sdbc-firebird
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-sdbc-hsqldb:
-   arch: amd64
    category: database
    name: libreoffice-sdbc-hsqldb
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-style-elementary:
-   arch: all
    category: universe/x11
    name: libreoffice-style-elementary
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-style-galaxy:
-   arch: all
    category: editors
    name: libreoffice-style-galaxy
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
libreoffice-writer:
-   arch: amd64
    category: editors
    name: libreoffice-writer
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
librest-0.7-0:
-   arch: amd64
    category: libs
    name: librest-0.7-0
    origin: Ubuntu
    source: apt
    version: 0.7.93-1
librevenge-0.0-0:
-   arch: amd64
    category: libs
    name: librevenge-0.0-0
    origin: Ubuntu
    source: apt
    version: 0.0.4-4ubuntu1
libroken18-heimdal:
-   arch: amd64
    category: libs
    name: libroken18-heimdal
    origin: Ubuntu
    source: apt
    version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
librsvg2-2:
-   arch: amd64
    category: libs
    name: librsvg2-2
    origin: Ubuntu
    source: apt
    version: 2.40.13-3
librsvg2-common:
-   arch: amd64
    category: libs
    name: librsvg2-common
    origin: Ubuntu
    source: apt
    version: 2.40.13-3
librtmp1:
-   arch: amd64
    category: libs
    name: librtmp1
    origin: Ubuntu
    source: apt
    version: 2.4+20151223.gitfa8646d-1ubuntu0.1
librubberband2v5:
-   arch: amd64
    category: universe/libs
    name: librubberband2v5
    origin: Ubuntu
    source: apt
    version: 1.8.1-6ubuntu2
libruby2.3:
-   arch: amd64
    category: libs
    name: libruby2.3
    origin: Ubuntu
    source: apt
    version: 2.3.1-2~ubuntu16.04.14
libsamplerate0:
-   arch: amd64
    category: libs
    name: libsamplerate0
    origin: Ubuntu
    source: apt
    version: 0.1.8-8
libsane:
-   arch: amd64
    category: libs
    name: libsane
    origin: Ubuntu
    source: apt
    version: 1.0.25+git20150528-1ubuntu2.16.04.1
libsane-common:
-   arch: all
    category: libs
    name: libsane-common
    origin: Ubuntu
    source: apt
    version: 1.0.25+git20150528-1ubuntu2.16.04.1
libsasl2-2:
-   arch: amd64
    category: libs
    name: libsasl2-2
    origin: Ubuntu
    source: apt
    version: 2.1.26.dfsg1-14ubuntu0.2
libsasl2-modules:
-   arch: amd64
    category: devel
    name: libsasl2-modules
    origin: Ubuntu
    source: apt
    version: 2.1.26.dfsg1-14ubuntu0.2
libsasl2-modules-db:
-   arch: amd64
    category: libs
    name: libsasl2-modules-db
    origin: Ubuntu
    source: apt
    version: 2.1.26.dfsg1-14ubuntu0.2
libsbc1:
-   arch: amd64
    category: libs
    name: libsbc1
    origin: Ubuntu
    source: apt
    version: 1.3-1
libschroedinger-1.0-0:
-   arch: amd64
    category: universe/libs
    name: libschroedinger-1.0-0
    origin: Ubuntu
    source: apt
    version: 1.0.11-2.1build1
libsdl1.2debian:
-   arch: amd64
    category: libs
    name: libsdl1.2debian
    origin: Ubuntu
    source: apt
    version: 1.2.15+dfsg1-3ubuntu0.1
libsdl2-2.0-0:
-   arch: amd64
    category: universe/libs
    name: libsdl2-2.0-0
    origin: Ubuntu
    source: apt
    version: 2.0.4+dfsg1-2ubuntu2.16.04.2
libseccomp2:
-   arch: amd64
    category: libs
    name: libseccomp2
    origin: Ubuntu
    source: apt
    version: 2.4.1-0ubuntu0.16.04.2
libselinux1:
-   arch: amd64
    category: libs
    name: libselinux1
    origin: Ubuntu
    source: apt
    version: 2.4-3build2
libsemanage-common:
-   arch: all
    category: libs
    name: libsemanage-common
    origin: Ubuntu
    source: apt
    version: 2.3-1build3
libsemanage1:
-   arch: amd64
    category: libs
    name: libsemanage1
    origin: Ubuntu
    source: apt
    version: 2.3-1build3
libsensors4:
-   arch: amd64
    category: libs
    name: libsensors4
    origin: Ubuntu
    source: apt
    version: 1:3.4.0-2
libsepol1:
-   arch: amd64
    category: libs
    name: libsepol1
    origin: Ubuntu
    source: apt
    version: 2.4-2
libservlet3.1-java:
-   arch: all
    category: java
    name: libservlet3.1-java
    origin: Ubuntu
    source: apt
    version: 8.0.32-1ubuntu1.11
libshine3:
-   arch: amd64
    category: universe/libs
    name: libshine3
    origin: Ubuntu
    source: apt
    version: 3.1.0-4
libshout3:
-   arch: amd64
    category: libs
    name: libshout3
    origin: Ubuntu
    source: apt
    version: 2.3.1-3
libsidplay1v5:
-   arch: amd64
    category: universe/libs
    name: libsidplay1v5
    origin: Ubuntu
    source: apt
    version: 1.36.59-8
libsigc++-2.0-0v5:
-   arch: amd64
    category: libs
    name: libsigc++-2.0-0v5
    origin: Ubuntu
    source: apt
    version: 2.6.2-1
libsigsegv2:
-   arch: amd64
    category: libs
    name: libsigsegv2
    origin: Ubuntu
    source: apt
    version: 2.10-4
libslang2:
-   arch: amd64
    category: libs
    name: libslang2
    origin: Ubuntu
    source: apt
    version: 2.3.0-2ubuntu1.1
libsm6:
-   arch: amd64
    category: libs
    name: libsm6
    origin: Ubuntu
    source: apt
    version: 2:1.2.2-1
libsmartcols1:
-   arch: amd64
    category: libs
    name: libsmartcols1
    origin: Ubuntu
    source: apt
    version: 2.27.1-6ubuntu3.10
libsmbclient:
-   arch: amd64
    category: libs
    name: libsmbclient
    origin: Ubuntu
    source: apt
    version: 2:4.3.11+dfsg-0ubuntu0.16.04.27
libsnappy1v5:
-   arch: amd64
    category: libs
    name: libsnappy1v5
    origin: Ubuntu
    source: apt
    version: 1.1.3-2
libsndfile1:
-   arch: amd64
    category: libs
    name: libsndfile1
    origin: Ubuntu
    source: apt
    version: 1.0.25-10ubuntu0.16.04.2
libsndio6.1:
-   arch: amd64
    category: universe/libs
    name: libsndio6.1
    origin: Ubuntu
    source: apt
    version: 1.1.0-2
libsocket6-perl:
-   arch: amd64
    category: perl
    name: libsocket6-perl
    origin: Ubuntu
    source: apt
    version: 0.25-1build2
libsoundtouch1:
-   arch: amd64
    category: universe/libs
    name: libsoundtouch1
    origin: Ubuntu
    source: apt
    version: 1.9.2-2+deb9u1build0.16.04.1
libsoup-gnome2.4-1:
-   arch: amd64
    category: libs
    name: libsoup-gnome2.4-1
    origin: Ubuntu
    source: apt
    version: 2.52.2-1ubuntu0.3
libsoup2.4-1:
-   arch: amd64
    category: libs
    name: libsoup2.4-1
    origin: Ubuntu
    source: apt
    version: 2.52.2-1ubuntu0.3
libsox-fmt-alsa:
-   arch: amd64
    category: universe/sound
    name: libsox-fmt-alsa
    origin: Ubuntu
    source: apt
    version: 14.4.1-5+deb8u4ubuntu0.1
libsox-fmt-base:
-   arch: amd64
    category: universe/sound
    name: libsox-fmt-base
    origin: Ubuntu
    source: apt
    version: 14.4.1-5+deb8u4ubuntu0.1
libsox2:
-   arch: amd64
    category: universe/sound
    name: libsox2
    origin: Ubuntu
    source: apt
    version: 14.4.1-5+deb8u4ubuntu0.1
libsoxr0:
-   arch: amd64
    category: universe/libs
    name: libsoxr0
    origin: Ubuntu
    source: apt
    version: 0.1.2-1
libspandsp2:
-   arch: amd64
    category: universe/libs
    name: libspandsp2
    origin: Ubuntu
    source: apt
    version: 0.0.6-2.1
libspeex1:
-   arch: amd64
    category: libs
    name: libspeex1
    origin: Ubuntu
    source: apt
    version: 1.2~rc1.2-1ubuntu1
libspeexdsp1:
-   arch: amd64
    category: libs
    name: libspeexdsp1
    origin: Ubuntu
    source: apt
    version: 1.2~rc1.2-1ubuntu1
libsqlite3-0:
-   arch: amd64
    category: libs
    name: libsqlite3-0
    origin: Ubuntu
    source: apt
    version: 3.11.0-1ubuntu1.4
libsrtp0:
-   arch: amd64
    category: libs
    name: libsrtp0
    origin: BigBlueButton
    source: apt
    version: 1.6.0-0kurento1.16.04
libss2:
-   arch: amd64
    category: libs
    name: libss2
    origin: Ubuntu
    source: apt
    version: 1.42.13-1ubuntu1.2
libssh-gcrypt-4:
-   arch: amd64
    category: libs
    name: libssh-gcrypt-4
    origin: Ubuntu
    source: apt
    version: 0.6.3-4.3ubuntu0.5
libssl1.0.0:
-   arch: amd64
    category: libs
    name: libssl1.0.0
    origin: Ubuntu
    source: apt
    version: 1.0.2g-1ubuntu4.16
libstdc++-5-dev:
-   arch: amd64
    category: libdevel
    name: libstdc++-5-dev
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libstdc++6:
-   arch: amd64
    category: libs
    name: libstdc++6
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libstring-crc32-perl:
-   arch: amd64
    category: universe/perl
    name: libstring-crc32-perl
    origin: Ubuntu
    source: apt
    version: 1.5-1build2
libsub-name-perl:
-   arch: amd64
    category: perl
    name: libsub-name-perl
    origin: Ubuntu
    source: apt
    version: 0.14-1build1
libsuitesparseconfig4.4.6:
-   arch: amd64
    category: libs
    name: libsuitesparseconfig4.4.6
    origin: Ubuntu
    source: apt
    version: 1:4.4.6-1
libswresample-ffmpeg1:
-   arch: amd64
    category: universe/libs
    name: libswresample-ffmpeg1
    origin: Ubuntu
    source: apt
    version: 7:2.8.15-0ubuntu0.16.04.1
libswresample3:
-   arch: amd64
    category: libs
    name: libswresample3
    origin: LP-PPA-bigbluebutton-support
    source: apt
    version: 7:4.2.2-1bbb1~ubuntu16.04
libswscale-ffmpeg3:
-   arch: amd64
    category: universe/libs
    name: libswscale-ffmpeg3
    origin: Ubuntu
    source: apt
    version: 7:2.8.15-0ubuntu0.16.04.1
libswscale5:
-   arch: amd64
    category: libs
    name: libswscale5
    origin: LP-PPA-bigbluebutton-support
    source: apt
    version: 7:4.2.2-1bbb1~ubuntu16.04
libsystemd-dev:
-   arch: amd64
    category: libdevel
    name: libsystemd-dev
    origin: Ubuntu
    source: apt
    version: 229-4ubuntu21.28
libsystemd0:
-   arch: amd64
    category: libs
    name: libsystemd0
    origin: Ubuntu
    source: apt
    version: 229-4ubuntu21.28
libtag1v5:
-   arch: amd64
    category: libs
    name: libtag1v5
    origin: Ubuntu
    source: apt
    version: 1.9.1-2.4ubuntu1
libtag1v5-vanilla:
-   arch: amd64
    category: libs
    name: libtag1v5-vanilla
    origin: Ubuntu
    source: apt
    version: 1.9.1-2.4ubuntu1
libtalloc2:
-   arch: amd64
    category: libs
    name: libtalloc2
    origin: Ubuntu
    source: apt
    version: 2.1.5-2
libtasn1-6:
-   arch: amd64
    category: libs
    name: libtasn1-6
    origin: Ubuntu
    source: apt
    version: 4.7-3ubuntu0.16.04.3
libtbb2:
-   arch: amd64
    category: universe/libs
    name: libtbb2
    origin: Ubuntu
    source: apt
    version: 4.4~20151115-0ubuntu3
libtdb1:
-   arch: amd64
    category: libs
    name: libtdb1
    origin: Ubuntu
    source: apt
    version: 1.3.8-2
libtevent0:
-   arch: amd64
    category: libs
    name: libtevent0
    origin: Ubuntu
    source: apt
    version: 0.9.28-0ubuntu0.16.04.1
libtext-charwidth-perl:
-   arch: amd64
    category: perl
    name: libtext-charwidth-perl
    origin: Ubuntu
    source: apt
    version: 0.04-7build5
libtext-iconv-perl:
-   arch: amd64
    category: perl
    name: libtext-iconv-perl
    origin: Ubuntu
    source: apt
    version: 1.7-5build4
libtext-wrapi18n-perl:
-   arch: all
    category: perl
    name: libtext-wrapi18n-perl
    origin: Ubuntu
    source: apt
    version: 0.06-7.1
libthai-data:
-   arch: all
    category: libs
    name: libthai-data
    origin: Ubuntu
    source: apt
    version: 0.1.24-2
libthai0:
-   arch: amd64
    category: libs
    name: libthai0
    origin: Ubuntu
    source: apt
    version: 0.1.24-2
libtheora0:
-   arch: amd64
    category: libs
    name: libtheora0
    origin: Ubuntu
    source: apt
    version: 1.1.1+dfsg.1-8
libtidy-0.99-0:
-   arch: amd64
    category: libs
    name: libtidy-0.99-0
    origin: Ubuntu
    source: apt
    version: 20091223cvs-1.5
libtiff5:
-   arch: amd64
    category: libs
    name: libtiff5
    origin: Ubuntu
    source: apt
    version: 4.0.6-1ubuntu0.7
libtimedate-perl:
-   arch: all
    category: perl
    name: libtimedate-perl
    origin: Ubuntu
    source: apt
    version: 2.3000-2
libtinfo-dev:
-   arch: amd64
    category: libdevel
    name: libtinfo-dev
    origin: Ubuntu
    source: apt
    version: 6.0+20160213-1ubuntu1
libtinfo5:
-   arch: amd64
    category: libs
    name: libtinfo5
    origin: Ubuntu
    source: apt
    version: 6.0+20160213-1ubuntu1
libtokyocabinet9:
-   arch: amd64
    category: libs
    name: libtokyocabinet9
    origin: Ubuntu
    source: apt
    version: 1.4.48-10
libtsan0:
-   arch: amd64
    category: libs
    name: libtsan0
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libtwolame0:
-   arch: amd64
    category: universe/libs
    name: libtwolame0
    origin: Ubuntu
    source: apt
    version: 0.3.13-1.2
libtxc-dxtn-s2tc0:
-   arch: amd64
    category: libs
    name: libtxc-dxtn-s2tc0
    origin: Ubuntu
    source: apt
    version: 0~git20131104-1.1
libubsan0:
-   arch: amd64
    category: libs
    name: libubsan0
    origin: Ubuntu
    source: apt
    version: 5.4.0-6ubuntu1~16.04.12
libudev1:
-   arch: amd64
    category: libs
    name: libudev1
    origin: Ubuntu
    source: apt
    version: 229-4ubuntu21.28
liburi-perl:
-   arch: all
    category: perl
    name: liburi-perl
    origin: Ubuntu
    source: apt
    version: 1.71-1
libusb-0.1-4:
-   arch: amd64
    category: libs
    name: libusb-0.1-4
    origin: Ubuntu
    source: apt
    version: 2:0.1.12-28
libusb-1.0-0:
-   arch: amd64
    category: libs
    name: libusb-1.0-0
    origin: Ubuntu
    source: apt
    version: 2:1.0.20-1
libusrsctp:
-   arch: amd64
    category: utils
    name: libusrsctp
    origin: BigBlueButton
    source: apt
    version: 0.9.2-1kurento1.16.04
libustr-1.0-1:
-   arch: amd64
    category: libs
    name: libustr-1.0-1
    origin: Ubuntu
    source: apt
    version: 1.0.4-5
libutempter0:
-   arch: amd64
    category: libs
    name: libutempter0
    origin: Ubuntu
    source: apt
    version: 1.1.6-3
libuuid1:
-   arch: amd64
    category: libs
    name: libuuid1
    origin: Ubuntu
    source: apt
    version: 2.27.1-6ubuntu3.10
libv4l-0:
-   arch: amd64
    category: libs
    name: libv4l-0
    origin: Ubuntu
    source: apt
    version: 1.10.0-1
libv4lconvert0:
-   arch: amd64
    category: libs
    name: libv4lconvert0
    origin: Ubuntu
    source: apt
    version: 1.10.0-1
libva-drm1:
-   arch: amd64
    category: universe/libs
    name: libva-drm1
    origin: Ubuntu
    source: apt
    version: 1.7.0-1ubuntu0.1
libva-x11-1:
-   arch: amd64
    category: universe/libs
    name: libva-x11-1
    origin: Ubuntu
    source: apt
    version: 1.7.0-1ubuntu0.1
libva1:
-   arch: amd64
    category: universe/libs
    name: libva1
    origin: Ubuntu
    source: apt
    version: 1.7.0-1ubuntu0.1
libvdpau1:
-   arch: amd64
    category: libs
    name: libvdpau1
    origin: Ubuntu
    source: apt
    version: 1.1.1-3ubuntu1
libvisio-0.1-1:
-   arch: amd64
    category: libs
    name: libvisio-0.1-1
    origin: Ubuntu
    source: apt
    version: 0.1.5-1ubuntu1
libvisual-0.4-0:
-   arch: amd64
    category: libs
    name: libvisual-0.4-0
    origin: Ubuntu
    source: apt
    version: 0.4.0-8
libvo-aacenc0:
-   arch: amd64
    category: universe/libs
    name: libvo-aacenc0
    origin: Ubuntu
    source: apt
    version: 0.1.3-1
libvo-amrwbenc0:
-   arch: amd64
    category: universe/libs
    name: libvo-amrwbenc0
    origin: Ubuntu
    source: apt
    version: 0.1.3-1
libvorbis0a:
-   arch: amd64
    category: libs
    name: libvorbis0a
    origin: Ubuntu
    source: apt
    version: 1.3.5-3ubuntu0.2
libvorbisenc2:
-   arch: amd64
    category: libs
    name: libvorbisenc2
    origin: Ubuntu
    source: apt
    version: 1.3.5-3ubuntu0.2
libvorbisfile3:
-   arch: amd64
    category: libs
    name: libvorbisfile3
    origin: Ubuntu
    source: apt
    version: 1.3.5-3ubuntu0.2
libvorbisidec1:
-   arch: amd64
    category: universe/libs
    name: libvorbisidec1
    origin: Ubuntu
    source: apt
    version: 1.0.2+svn18153-0.2+deb7u1build0.16.04.1
libvpx3:
-   arch: amd64
    category: libs
    name: libvpx3
    origin: Ubuntu
    source: apt
    version: 1.5.0-2ubuntu1.1
libwacom-bin:
-   arch: amd64
    category: libs
    name: libwacom-bin
    origin: Ubuntu
    source: apt
    version: 0.22-1~ubuntu16.04.1
libwacom-common:
-   arch: all
    category: libs
    name: libwacom-common
    origin: Ubuntu
    source: apt
    version: 0.22-1~ubuntu16.04.1
libwacom2:
-   arch: amd64
    category: libs
    name: libwacom2
    origin: Ubuntu
    source: apt
    version: 0.22-1~ubuntu16.04.1
libwavpack1:
-   arch: amd64
    category: libs
    name: libwavpack1
    origin: Ubuntu
    source: apt
    version: 4.75.2-2ubuntu0.2
libwayland-client0:
-   arch: amd64
    category: libs
    name: libwayland-client0
    origin: Ubuntu
    source: apt
    version: 1.12.0-1~ubuntu16.04.3
libwayland-cursor0:
-   arch: amd64
    category: libs
    name: libwayland-cursor0
    origin: Ubuntu
    source: apt
    version: 1.12.0-1~ubuntu16.04.3
libwayland-egl1-mesa:
-   arch: amd64
    category: libs
    name: libwayland-egl1-mesa
    origin: Ubuntu
    source: apt
    version: 18.0.5-0ubuntu0~16.04.1
libwayland-server0:
-   arch: amd64
    category: libs
    name: libwayland-server0
    origin: Ubuntu
    source: apt
    version: 1.12.0-1~ubuntu16.04.3
libwbclient0:
-   arch: amd64
    category: libs
    name: libwbclient0
    origin: Ubuntu
    source: apt
    version: 2:4.3.11+dfsg-0ubuntu0.16.04.27
libwebp5:
-   arch: amd64
    category: libs
    name: libwebp5
    origin: Ubuntu
    source: apt
    version: 0.4.4-1
libwildmidi-config:
-   arch: all
    category: universe/misc
    name: libwildmidi-config
    origin: Ubuntu
    source: apt
    version: 0.3.8-2
libwildmidi1:
-   arch: amd64
    category: universe/libs
    name: libwildmidi1
    origin: Ubuntu
    source: apt
    version: 0.3.8-2
libwind0-heimdal:
-   arch: amd64
    category: libs
    name: libwind0-heimdal
    origin: Ubuntu
    source: apt
    version: 1.7~git20150920+dfsg-4ubuntu1.16.04.1
libwmf0.2-7:
-   arch: amd64
    category: libs
    name: libwmf0.2-7
    origin: Ubuntu
    source: apt
    version: 0.2.8.4-10.5ubuntu1
libwpd-0.10-10:
-   arch: amd64
    category: libs
    name: libwpd-0.10-10
    origin: Ubuntu
    source: apt
    version: 0.10.1-1ubuntu1
libwpg-0.3-3:
-   arch: amd64
    category: libs
    name: libwpg-0.3-3
    origin: Ubuntu
    source: apt
    version: 0.3.1-1ubuntu1
libwps-0.4-4:
-   arch: amd64
    category: libs
    name: libwps-0.4-4
    origin: Ubuntu
    source: apt
    version: 0.4.3-1ubuntu1
libwrap0:
-   arch: amd64
    category: libs
    name: libwrap0
    origin: Ubuntu
    source: apt
    version: 7.6.q-25
libwww-perl:
-   arch: all
    category: perl
    name: libwww-perl
    origin: Ubuntu
    source: apt
    version: 6.15-1
libwww-robotrules-perl:
-   arch: all
    category: perl
    name: libwww-robotrules-perl
    origin: Ubuntu
    source: apt
    version: 6.01-1
libwxbase3.0-0v5:
-   arch: amd64
    category: universe/libs
    name: libwxbase3.0-0v5
    origin: Ubuntu
    source: apt
    version: 3.0.2+dfsg-1.3ubuntu0.1
libwxgtk3.0-0v5:
-   arch: amd64
    category: universe/libs
    name: libwxgtk3.0-0v5
    origin: Ubuntu
    source: apt
    version: 3.0.2+dfsg-1.3ubuntu0.1
libx11-6:
-   arch: amd64
    category: libs
    name: libx11-6
    origin: Ubuntu
    source: apt
    version: 2:1.6.3-1ubuntu2.1
libx11-data:
-   arch: all
    category: x11
    name: libx11-data
    origin: Ubuntu
    source: apt
    version: 2:1.6.3-1ubuntu2.1
libx11-xcb1:
-   arch: amd64
    category: libs
    name: libx11-xcb1
    origin: Ubuntu
    source: apt
    version: 2:1.6.3-1ubuntu2.1
libx264-148:
-   arch: amd64
    category: universe/libs
    name: libx264-148
    origin: Ubuntu
    source: apt
    version: 2:0.148.2643+git5c65704-1
libx265-79:
-   arch: amd64
    category: universe/libs
    name: libx265-79
    origin: Ubuntu
    source: apt
    version: 1.9-3
libxapian22v5:
-   arch: amd64
    category: libs
    name: libxapian22v5
    origin: Ubuntu
    source: apt
    version: 1.2.22-2
libxau6:
-   arch: amd64
    category: libs
    name: libxau6
    origin: Ubuntu
    source: apt
    version: 1:1.0.8-1
libxcb-dri2-0:
-   arch: amd64
    category: libs
    name: libxcb-dri2-0
    origin: Ubuntu
    source: apt
    version: 1.11.1-1ubuntu1
libxcb-dri3-0:
-   arch: amd64
    category: libs
    name: libxcb-dri3-0
    origin: Ubuntu
    source: apt
    version: 1.11.1-1ubuntu1
libxcb-glx0:
-   arch: amd64
    category: libs
    name: libxcb-glx0
    origin: Ubuntu
    source: apt
    version: 1.11.1-1ubuntu1
libxcb-icccm4:
-   arch: amd64
    category: libs
    name: libxcb-icccm4
    origin: Ubuntu
    source: apt
    version: 0.4.1-1ubuntu1
libxcb-image0:
-   arch: amd64
    category: libdevel
    name: libxcb-image0
    origin: Ubuntu
    source: apt
    version: 0.4.0-1build1
libxcb-keysyms1:
-   arch: amd64
    category: libs
    name: libxcb-keysyms1
    origin: Ubuntu
    source: apt
    version: 0.4.0-1
libxcb-present0:
-   arch: amd64
    category: libs
    name: libxcb-present0
    origin: Ubuntu
    source: apt
    version: 1.11.1-1ubuntu1
libxcb-randr0:
-   arch: amd64
    category: libs
    name: libxcb-randr0
    origin: Ubuntu
    source: apt
    version: 1.11.1-1ubuntu1
libxcb-render-util0:
-   arch: amd64
    category: libs
    name: libxcb-render-util0
    origin: Ubuntu
    source: apt
    version: 0.3.9-1
libxcb-render0:
-   arch: amd64
    category: libs
    name: libxcb-render0
    origin: Ubuntu
    source: apt
    version: 1.11.1-1ubuntu1
libxcb-shape0:
-   arch: amd64
    category: libs
    name: libxcb-shape0
    origin: Ubuntu
    source: apt
    version: 1.11.1-1ubuntu1
libxcb-shm0:
-   arch: amd64
    category: libs
    name: libxcb-shm0
    origin: Ubuntu
    source: apt
    version: 1.11.1-1ubuntu1
libxcb-sync1:
-   arch: amd64
    category: libs
    name: libxcb-sync1
    origin: Ubuntu
    source: apt
    version: 1.11.1-1ubuntu1
libxcb-util1:
-   arch: amd64
    category: libs
    name: libxcb-util1
    origin: Ubuntu
    source: apt
    version: 0.4.0-0ubuntu3
libxcb-xfixes0:
-   arch: amd64
    category: libs
    name: libxcb-xfixes0
    origin: Ubuntu
    source: apt
    version: 1.11.1-1ubuntu1
libxcb-xkb1:
-   arch: amd64
    category: libs
    name: libxcb-xkb1
    origin: Ubuntu
    source: apt
    version: 1.11.1-1ubuntu1
libxcb1:
-   arch: amd64
    category: libs
    name: libxcb1
    origin: Ubuntu
    source: apt
    version: 1.11.1-1ubuntu1
libxcomposite1:
-   arch: amd64
    category: libs
    name: libxcomposite1
    origin: Ubuntu
    source: apt
    version: 1:0.4.4-1
libxcursor1:
-   arch: amd64
    category: libs
    name: libxcursor1
    origin: Ubuntu
    source: apt
    version: 1:1.1.14-1ubuntu0.16.04.2
libxdamage1:
-   arch: amd64
    category: libs
    name: libxdamage1
    origin: Ubuntu
    source: apt
    version: 1:1.1.4-2
libxdmcp6:
-   arch: amd64
    category: libs
    name: libxdmcp6
    origin: Ubuntu
    source: apt
    version: 1:1.1.2-1.1
libxext6:
-   arch: amd64
    category: libs
    name: libxext6
    origin: Ubuntu
    source: apt
    version: 2:1.3.3-1
libxfixes3:
-   arch: amd64
    category: libs
    name: libxfixes3
    origin: Ubuntu
    source: apt
    version: 1:5.0.1-2
libxfont1:
-   arch: amd64
    category: libs
    name: libxfont1
    origin: Ubuntu
    source: apt
    version: 1:1.5.1-1ubuntu0.16.04.4
libxi6:
-   arch: amd64
    category: libs
    name: libxi6
    origin: Ubuntu
    source: apt
    version: 2:1.7.6-1
libxinerama1:
-   arch: amd64
    category: libs
    name: libxinerama1
    origin: Ubuntu
    source: apt
    version: 2:1.1.3-1
libxkbcommon-x11-0:
-   arch: amd64
    category: libs
    name: libxkbcommon-x11-0
    origin: Ubuntu
    source: apt
    version: 0.5.0-1ubuntu2.1
libxkbcommon0:
-   arch: amd64
    category: libs
    name: libxkbcommon0
    origin: Ubuntu
    source: apt
    version: 0.5.0-1ubuntu2.1
libxml2:
-   arch: amd64
    category: libs
    name: libxml2
    origin: Ubuntu
    source: apt
    version: 2.9.3+dfsg1-1ubuntu0.7
libxml2-dev:
-   arch: amd64
    category: libdevel
    name: libxml2-dev
    origin: Ubuntu
    source: apt
    version: 2.9.3+dfsg1-1ubuntu0.7
libxmu6:
-   arch: amd64
    category: libs
    name: libxmu6
    origin: Ubuntu
    source: apt
    version: 2:1.1.2-2
libxpm4:
-   arch: amd64
    category: libs
    name: libxpm4
    origin: Ubuntu
    source: apt
    version: 1:3.5.11-1ubuntu0.16.04.1
libxrandr2:
-   arch: amd64
    category: libs
    name: libxrandr2
    origin: Ubuntu
    source: apt
    version: 2:1.5.0-1
libxrender1:
-   arch: amd64
    category: libs
    name: libxrender1
    origin: Ubuntu
    source: apt
    version: 1:0.9.9-0ubuntu1
libxshmfence1:
-   arch: amd64
    category: libs
    name: libxshmfence1
    origin: Ubuntu
    source: apt
    version: 1.2-1
libxslt1-dev:
-   arch: amd64
    category: libdevel
    name: libxslt1-dev
    origin: Ubuntu
    source: apt
    version: 1.1.28-2.1ubuntu0.3
libxslt1.1:
-   arch: amd64
    category: libs
    name: libxslt1.1
    origin: Ubuntu
    source: apt
    version: 1.1.28-2.1ubuntu0.3
libxss1:
-   arch: amd64
    category: libs
    name: libxss1
    origin: Ubuntu
    source: apt
    version: 1:1.2.2-1
libxt6:
-   arch: amd64
    category: libs
    name: libxt6
    origin: Ubuntu
    source: apt
    version: 1:1.1.5-0ubuntu1
libxtables11:
-   arch: amd64
    category: net
    name: libxtables11
    origin: Ubuntu
    source: apt
    version: 1.6.0-2ubuntu3
libxtst6:
-   arch: amd64
    category: libs
    name: libxtst6
    origin: Ubuntu
    source: apt
    version: 2:1.2.2-1
libxv1:
-   arch: amd64
    category: libs
    name: libxv1
    origin: Ubuntu
    source: apt
    version: 2:1.0.10-1
libxvidcore4:
-   arch: amd64
    category: universe/libs
    name: libxvidcore4
    origin: Ubuntu
    source: apt
    version: 2:1.3.4-1
libxvmc1:
-   arch: amd64
    category: libs
    name: libxvmc1
    origin: Ubuntu
    source: apt
    version: 2:1.0.9-1ubuntu1
libxxf86dga1:
-   arch: amd64
    category: libs
    name: libxxf86dga1
    origin: Ubuntu
    source: apt
    version: 2:1.1.4-1
libxxf86vm1:
-   arch: amd64
    category: libs
    name: libxxf86vm1
    origin: Ubuntu
    source: apt
    version: 1:1.1.4-1
libyajl2:
-   arch: amd64
    category: libs
    name: libyajl2
    origin: Ubuntu
    source: apt
    version: 2.1.0-2
libyaml-0-2:
-   arch: amd64
    category: libs
    name: libyaml-0-2
    origin: Ubuntu
    source: apt
    version: 0.1.6-3
libzbar0:
-   arch: amd64
    category: universe/libs
    name: libzbar0
    origin: Ubuntu
    source: apt
    version: 0.10+doc-10ubuntu1
libzvbi-common:
-   arch: all
    category: universe/devel
    name: libzvbi-common
    origin: Ubuntu
    source: apt
    version: 0.2.35-10
libzvbi0:
-   arch: amd64
    category: universe/libs
    name: libzvbi0
    origin: Ubuntu
    source: apt
    version: 0.2.35-10
linux-base:
-   arch: all
    category: kernel
    name: linux-base
    origin: Ubuntu
    source: apt
    version: 4.5ubuntu1.1~16.04.1
linux-firmware:
-   arch: all
    category: misc
    name: linux-firmware
    origin: Ubuntu
    source: apt
    version: 1.157.23
linux-image-4.4.0-179-generic:
-   arch: amd64
    category: kernel
    name: linux-image-4.4.0-179-generic
    origin: Ubuntu
    source: apt
    version: 4.4.0-179.209
linux-image-generic:
-   arch: amd64
    category: kernel
    name: linux-image-generic
    origin: Ubuntu
    source: apt
    version: 4.4.0.179.187
linux-libc-dev:
-   arch: amd64
    category: devel
    name: linux-libc-dev
    origin: Ubuntu
    source: apt
    version: 4.4.0-179.209
linux-modules-4.4.0-179-generic:
-   arch: amd64
    category: kernel
    name: linux-modules-4.4.0-179-generic
    origin: Ubuntu
    source: apt
    version: 4.4.0-179.209
linux-modules-extra-4.4.0-179-generic:
-   arch: amd64
    category: kernel
    name: linux-modules-extra-4.4.0-179-generic
    origin: Ubuntu
    source: apt
    version: 4.4.0-179.209
linuxlogo:
-   arch: amd64
    category: universe/misc
    name: linuxlogo
    origin: Ubuntu
    source: apt
    version: 5.11-8
locales:
-   arch: all
    category: libs
    name: locales
    origin: Ubuntu
    source: apt
    version: 2.23-0ubuntu11
login:
-   arch: amd64
    category: admin
    name: login
    origin: Ubuntu
    source: apt
    version: 1:4.2-3.1ubuntu5.4
logrotate:
-   arch: amd64
    category: admin
    name: logrotate
    origin: Ubuntu
    source: apt
    version: 3.8.7-2ubuntu2.16.04.2
lp-solve:
-   arch: amd64
    category: math
    name: lp-solve
    origin: Ubuntu
    source: apt
    version: 5.5.0.13-7build2
lsb-base:
-   arch: all
    category: misc
    name: lsb-base
    origin: Ubuntu
    source: apt
    version: 9.20160110ubuntu0.2
lsb-release:
-   arch: all
    category: misc
    name: lsb-release
    origin: Ubuntu
    source: apt
    version: 9.20160110ubuntu0.2
lvm2:
-   arch: amd64
    category: admin
    name: lvm2
    origin: Ubuntu
    source: apt
    version: 2.02.133-1ubuntu10
make:
-   arch: amd64
    category: devel
    name: make
    origin: Ubuntu
    source: apt
    version: 4.1-6
makedev:
-   arch: all
    category: admin
    name: makedev
    origin: Ubuntu
    source: apt
    version: 2.3.1-93ubuntu2~ubuntu16.04.1
manpages:
-   arch: all
    category: doc
    name: manpages
    origin: Ubuntu
    source: apt
    version: 4.04-2
manpages-dev:
-   arch: all
    category: doc
    name: manpages-dev
    origin: Ubuntu
    source: apt
    version: 4.04-2
mawk:
-   arch: amd64
    category: utils
    name: mawk
    origin: Ubuntu
    source: apt
    version: 1.3.3-17ubuntu2
mdadm:
-   arch: amd64
    category: admin
    name: mdadm
    origin: Ubuntu
    source: apt
    version: 3.3-2ubuntu7.6
memtest86+:
-   arch: amd64
    category: misc
    name: memtest86+
    origin: Ubuntu
    source: apt
    version: 5.01-3ubuntu2
mencoder:
-   arch: amd64
    category: universe/video
    name: mencoder
    origin: Ubuntu
    source: apt
    version: 2:1.2.1-1ubuntu1.1
mesa-va-drivers:
-   arch: amd64
    category: universe/libs
    name: mesa-va-drivers
    origin: Ubuntu
    source: apt
    version: 18.0.5-0ubuntu0~16.04.1
mesa-vdpau-drivers:
-   arch: amd64
    category: libs
    name: mesa-vdpau-drivers
    origin: Ubuntu
    source: apt
    version: 18.0.5-0ubuntu0~16.04.1
mime-support:
-   arch: all
    category: net
    name: mime-support
    origin: Ubuntu
    source: apt
    version: 3.59ubuntu1
mongodb-org:
-   arch: amd64
    category: database
    name: mongodb-org
    origin: mongodb
    source: apt
    version: 3.4.24
mongodb-org-mongos:
-   arch: amd64
    category: database
    name: mongodb-org-mongos
    origin: mongodb
    source: apt
    version: 3.4.24
mongodb-org-server:
-   arch: amd64
    category: database
    name: mongodb-org-server
    origin: mongodb
    source: apt
    version: 3.4.24
mongodb-org-shell:
-   arch: amd64
    category: database
    name: mongodb-org-shell
    origin: mongodb
    source: apt
    version: 3.4.24
mongodb-org-tools:
-   arch: amd64
    category: database
    name: mongodb-org-tools
    origin: mongodb
    source: apt
    version: 3.4.24
mount:
-   arch: amd64
    category: admin
    name: mount
    origin: Ubuntu
    source: apt
    version: 2.27.1-6ubuntu3.10
mplayer:
-   arch: amd64
    category: universe/video
    name: mplayer
    origin: Ubuntu
    source: apt
    version: 2:1.2.1-1ubuntu1.1
multiarch-support:
-   arch: amd64
    category: libs
    name: multiarch-support
    origin: Ubuntu
    source: apt
    version: 2.23-0ubuntu11
munin-common:
-   arch: all
    category: universe/net
    name: munin-common
    origin: Ubuntu
    source: apt
    version: 2.0.25-2ubuntu0.16.04.3
munin-node:
-   arch: all
    category: universe/net
    name: munin-node
    origin: Ubuntu
    source: apt
    version: 2.0.25-2ubuntu0.16.04.3
munin-plugins-c:
-   arch: amd64
    category: universe/net
    name: munin-plugins-c
    origin: Ubuntu
    source: apt
    version: 0.0.9-1
munin-plugins-core:
-   arch: all
    category: universe/net
    name: munin-plugins-core
    origin: Ubuntu
    source: apt
    version: 2.0.25-2ubuntu0.16.04.3
munin-plugins-extra:
-   arch: all
    category: universe/net
    name: munin-plugins-extra
    origin: Ubuntu
    source: apt
    version: 2.0.25-2ubuntu0.16.04.3
mutt:
-   arch: amd64
    category: mail
    name: mutt
    origin: Ubuntu
    source: apt
    version: 1.5.24-1ubuntu0.2
ncurses-base:
-   arch: all
    category: utils
    name: ncurses-base
    origin: Ubuntu
    source: apt
    version: 6.0+20160213-1ubuntu1
ncurses-bin:
-   arch: amd64
    category: utils
    name: ncurses-bin
    origin: Ubuntu
    source: apt
    version: 6.0+20160213-1ubuntu1
net-tools:
-   arch: amd64
    category: net
    name: net-tools
    origin: Ubuntu
    source: apt
    version: 1.60-26ubuntu1
netbase:
-   arch: all
    category: admin
    name: netbase
    origin: Ubuntu
    source: apt
    version: '5.3'
netcat-openbsd:
-   arch: amd64
    category: net
    name: netcat-openbsd
    origin: Ubuntu
    source: apt
    version: 1.105-7ubuntu1
netpbm:
-   arch: amd64
    category: graphics
    name: netpbm
    origin: Ubuntu
    source: apt
    version: 2:10.0-15.3
nginx-common:
-   arch: all
    category: httpd
    name: nginx-common
    origin: Ubuntu
    source: apt
    version: 1.10.3-0ubuntu0.16.04.5
nginx-extras:
-   arch: amd64
    category: universe/httpd
    name: nginx-extras
    origin: Ubuntu
    source: apt
    version: 1.10.3-0ubuntu0.16.04.5
nmon:
-   arch: amd64
    category: universe/utils
    name: nmon
    origin: Ubuntu
    source: apt
    version: 14g+debian-1build1
nodejs:
-   arch: amd64
    category: web
    name: nodejs
    origin: Node Source
    source: apt
    version: 8.17.0-1nodesource1
notification-daemon:
-   arch: amd64
    category: x11
    name: notification-daemon
    origin: Ubuntu
    source: apt
    version: 3.18.2-1
opencv-data:
-   arch: all
    category: universe/libdevel
    name: opencv-data
    origin: Ubuntu
    source: apt
    version: 2.4.9.1+dfsg-1.5ubuntu1.1
openh264:
-   arch: amd64
    category: utils
    name: openh264
    origin: BigBlueButton
    source: apt
    version: 1.4.0-1kurento1.16.04
openh264-gst-plugins-bad-1.5:
-   arch: amd64
    category: libs
    name: openh264-gst-plugins-bad-1.5
    origin: BigBlueButton
    source: apt
    version: 1.8.1-1kurento4.16.04
openjdk-8-jre:
-   arch: amd64
    category: java
    name: openjdk-8-jre
    origin: Ubuntu
    source: apt
    version: 8u252-b09-1~16.04
openjdk-8-jre-headless:
-   arch: amd64
    category: java
    name: openjdk-8-jre-headless
    origin: Ubuntu
    source: apt
    version: 8u252-b09-1~16.04
openssh-client:
-   arch: amd64
    category: net
    name: openssh-client
    origin: Ubuntu
    source: apt
    version: 1:7.2p2-4ubuntu2.8
openssh-server:
-   arch: amd64
    category: net
    name: openssh-server
    origin: Ubuntu
    source: apt
    version: 1:7.2p2-4ubuntu2.8
openssh-sftp-server:
-   arch: amd64
    category: net
    name: openssh-sftp-server
    origin: Ubuntu
    source: apt
    version: 1:7.2p2-4ubuntu2.8
openssl:
-   arch: amd64
    category: utils
    name: openssl
    origin: Ubuntu
    source: apt
    version: 1.0.2g-1ubuntu4.16
openwebrtc-gst-plugins:
-   arch: amd64
    category: utils
    name: openwebrtc-gst-plugins
    origin: BigBlueButton
    source: apt
    version: 0.10.0-1kurento1.16.04
os-prober:
-   arch: amd64
    category: utils
    name: os-prober
    origin: Ubuntu
    source: apt
    version: 1.70ubuntu3.3
passwd:
-   arch: amd64
    category: admin
    name: passwd
    origin: Ubuntu
    source: apt
    version: 1:4.2-3.1ubuntu5.4
patch:
-   arch: amd64
    category: utils
    name: patch
    origin: Ubuntu
    source: apt
    version: 2.7.5-1ubuntu0.16.04.2
pciutils:
-   arch: amd64
    category: admin
    name: pciutils
    origin: Ubuntu
    source: apt
    version: 1:3.3.1-1.1ubuntu1.3
perl:
-   arch: amd64
    category: perl
    name: perl
    origin: Ubuntu
    source: apt
    version: 5.22.1-9ubuntu0.6
perl-base:
-   arch: amd64
    category: perl
    name: perl-base
    origin: Ubuntu
    source: apt
    version: 5.22.1-9ubuntu0.6
perl-modules-5.22:
-   arch: all
    category: perl
    name: perl-modules-5.22
    origin: Ubuntu
    source: apt
    version: 5.22.1-9ubuntu0.6
pigz:
-   arch: amd64
    category: universe/utils
    name: pigz
    origin: Ubuntu
    source: apt
    version: 2.3.1-2
pinentry-curses:
-   arch: amd64
    category: utils
    name: pinentry-curses
    origin: Ubuntu
    source: apt
    version: 0.9.7-3
plymouth:
-   arch: amd64
    category: x11
    name: plymouth
    origin: Ubuntu
    source: apt
    version: 0.9.2-3ubuntu13.5
policykit-1:
-   arch: amd64
    category: admin
    name: policykit-1
    origin: Ubuntu
    source: apt
    version: 0.105-14.1ubuntu0.5
poppler-data:
-   arch: all
    category: misc
    name: poppler-data
    origin: Ubuntu
    source: apt
    version: 0.4.7-7
poppler-utils:
-   arch: amd64
    category: utils
    name: poppler-utils
    origin: Ubuntu
    source: apt
    version: 0.41.0-0ubuntu1.14
powermgmt-base:
-   arch: all
    category: utils
    name: powermgmt-base
    origin: Ubuntu
    source: apt
    version: 1.31+nmu1
procinfo:
-   arch: amd64
    category: universe/utils
    name: procinfo
    origin: Ubuntu
    source: apt
    version: 1:2.0.304-1ubuntu2
procps:
-   arch: amd64
    category: admin
    name: procps
    origin: Ubuntu
    source: apt
    version: 2:3.3.10-4ubuntu2.5
psmisc:
-   arch: amd64
    category: admin
    name: psmisc
    origin: Ubuntu
    source: apt
    version: 22.21-2.1ubuntu0.1
pwgen:
-   arch: amd64
    category: universe/admin
    name: pwgen
    origin: Ubuntu
    source: apt
    version: 2.07-1.1ubuntu1
python:
-   arch: amd64
    category: python
    name: python
    origin: Ubuntu
    source: apt
    version: 2.7.12-1~16.04
python-apt:
-   arch: amd64
    category: python
    name: python-apt
    origin: Ubuntu
    source: apt
    version: 1.1.0~beta1ubuntu0.16.04.9
python-apt-common:
-   arch: all
    category: python
    name: python-apt-common
    origin: Ubuntu
    source: apt
    version: 1.1.0~beta1ubuntu0.16.04.9
python-backports.ssl-match-hostname:
-   arch: all
    category: universe/python
    name: python-backports.ssl-match-hostname
    origin: Ubuntu
    source: apt
    version: 3.4.0.2-1
python-cffi-backend:
-   arch: amd64
    category: python
    name: python-cffi-backend
    origin: Ubuntu
    source: apt
    version: 1.5.2-1ubuntu1
python-chardet:
-   arch: all
    category: python
    name: python-chardet
    origin: Ubuntu
    source: apt
    version: 2.3.0-2
python-cryptography:
-   arch: amd64
    category: python
    name: python-cryptography
    origin: Ubuntu
    source: apt
    version: 1.2.3-1ubuntu0.2
python-dns:
-   arch: all
    category: universe/python
    name: python-dns
    origin: Ubuntu
    source: apt
    version: 2.3.6-3
python-dnspython:
-   arch: all
    category: python
    name: python-dnspython
    origin: Ubuntu
    source: apt
    version: 1.12.0-1
python-docker:
-   arch: all
    category: universe/python
    name: python-docker
    origin: Ubuntu
    source: apt
    version: 1.9.0-1~16.04.1
python-enum34:
-   arch: all
    category: python
    name: python-enum34
    origin: Ubuntu
    source: apt
    version: 1.1.2-1
python-idna:
-   arch: all
    category: python
    name: python-idna
    origin: Ubuntu
    source: apt
    version: 2.0-3
python-ipaddress:
-   arch: all
    category: python
    name: python-ipaddress
    origin: Ubuntu
    source: apt
    version: 1.0.16-1
python-minimal:
-   arch: amd64
    category: python
    name: python-minimal
    origin: Ubuntu
    source: apt
    version: 2.7.12-1~16.04
python-ndg-httpsclient:
-   arch: all
    category: python
    name: python-ndg-httpsclient
    origin: Ubuntu
    source: apt
    version: 0.4.0-3
python-openssl:
-   arch: all
    category: python
    name: python-openssl
    origin: Ubuntu
    source: apt
    version: 0.15.1-2ubuntu0.2
python-passlib:
-   arch: all
    category: python
    name: python-passlib
    origin: Ubuntu
    source: apt
    version: 1.6.5-4
python-pkg-resources:
-   arch: all
    category: python
    name: python-pkg-resources
    origin: Ubuntu
    source: apt
    version: 20.7.0-1
python-pyasn1:
-   arch: all
    category: python
    name: python-pyasn1
    origin: Ubuntu
    source: apt
    version: 0.1.9-1
python-requests:
-   arch: all
    category: python
    name: python-requests
    origin: Ubuntu
    source: apt
    version: 2.9.1-3ubuntu0.1
python-six:
-   arch: all
    category: python
    name: python-six
    origin: Ubuntu
    source: apt
    version: 1.10.0-3
python-talloc:
-   arch: amd64
    category: python
    name: python-talloc
    origin: Ubuntu
    source: apt
    version: 2.1.5-2
python-urllib3:
-   arch: all
    category: python
    name: python-urllib3
    origin: Ubuntu
    source: apt
    version: 1.13.1-2ubuntu0.16.04.3
python-websocket:
-   arch: all
    category: universe/python
    name: python-websocket
    origin: Ubuntu
    source: apt
    version: 0.18.0-2
python2.7:
-   arch: amd64
    category: python
    name: python2.7
    origin: Ubuntu
    source: apt
    version: 2.7.12-1ubuntu0~16.04.11
python2.7-minimal:
-   arch: amd64
    category: python
    name: python2.7-minimal
    origin: Ubuntu
    source: apt
    version: 2.7.12-1ubuntu0~16.04.11
python3:
-   arch: amd64
    category: python
    name: python3
    origin: Ubuntu
    source: apt
    version: 3.5.1-3
python3-apt:
-   arch: amd64
    category: python
    name: python3-apt
    origin: Ubuntu
    source: apt
    version: 1.1.0~beta1ubuntu0.16.04.9
python3-bs4:
-   arch: all
    category: python
    name: python3-bs4
    origin: Ubuntu
    source: apt
    version: 4.4.1-1
python3-chardet:
-   arch: all
    category: python
    name: python3-chardet
    origin: Ubuntu
    source: apt
    version: 2.3.0-2
python3-dbus:
-   arch: amd64
    category: python
    name: python3-dbus
    origin: Ubuntu
    source: apt
    version: 1.2.0-3
python3-debian:
-   arch: all
    category: python
    name: python3-debian
    origin: Ubuntu
    source: apt
    version: 0.1.27ubuntu2
python3-distupgrade:
-   arch: all
    category: python
    name: python3-distupgrade
    origin: Ubuntu
    source: apt
    version: 1:16.04.30
python3-dnspython:
-   arch: all
    category: python
    name: python3-dnspython
    origin: Ubuntu
    source: apt
    version: 1.12.0-0ubuntu3
python3-docker:
-   arch: all
    category: universe/python
    name: python3-docker
    origin: Ubuntu
    source: apt
    version: 1.9.0-1~16.04.1
python3-gi:
-   arch: amd64
    category: python
    name: python3-gi
    origin: Ubuntu
    source: apt
    version: 3.20.0-0ubuntu1
python3-html5lib:
-   arch: all
    category: python
    name: python3-html5lib
    origin: Ubuntu
    source: apt
    version: 0.999-4
python3-icu:
-   arch: amd64
    category: python
    name: python3-icu
    origin: Ubuntu
    source: apt
    version: 1.9.2-2build1
python3-lxml:
-   arch: amd64
    category: python
    name: python3-lxml
    origin: Ubuntu
    source: apt
    version: 3.5.0-1ubuntu0.1
python3-minimal:
-   arch: amd64
    category: python
    name: python3-minimal
    origin: Ubuntu
    source: apt
    version: 3.5.1-3
python3-pkg-resources:
-   arch: all
    category: python
    name: python3-pkg-resources
    origin: Ubuntu
    source: apt
    version: 20.7.0-1
python3-pycurl:
-   arch: amd64
    category: python
    name: python3-pycurl
    origin: Ubuntu
    source: apt
    version: 7.43.0-1ubuntu1
python3-requests:
-   arch: all
    category: python
    name: python3-requests
    origin: Ubuntu
    source: apt
    version: 2.9.1-3ubuntu0.1
python3-six:
-   arch: all
    category: python
    name: python3-six
    origin: Ubuntu
    source: apt
    version: 1.10.0-3
python3-software-properties:
-   arch: all
    category: python
    name: python3-software-properties
    origin: Ubuntu
    source: apt
    version: 0.96.20.9
python3-uno:
-   arch: amd64
    category: python
    name: python3-uno
    origin: Ubuntu
    source: apt
    version: 1:5.1.6~rc2-0ubuntu1~xenial10
python3-update-manager:
-   arch: all
    category: python
    name: python3-update-manager
    origin: Ubuntu
    source: apt
    version: 1:16.04.17
python3-urllib3:
-   arch: all
    category: python
    name: python3-urllib3
    origin: Ubuntu
    source: apt
    version: 1.13.1-2ubuntu0.16.04.3
python3-websocket:
-   arch: all
    category: universe/python
    name: python3-websocket
    origin: Ubuntu
    source: apt
    version: 0.18.0-2
python3.5:
-   arch: amd64
    category: python
    name: python3.5
    origin: Ubuntu
    source: apt
    version: 3.5.2-2ubuntu0~16.04.10
python3.5-minimal:
-   arch: amd64
    category: python
    name: python3.5-minimal
    origin: Ubuntu
    source: apt
    version: 3.5.2-2ubuntu0~16.04.10
qttranslations5-l10n:
-   arch: all
    category: localization
    name: qttranslations5-l10n
    origin: Ubuntu
    source: apt
    version: 5.5.1-2build1
rake:
-   arch: all
    category: devel
    name: rake
    origin: Ubuntu
    source: apt
    version: 10.5.0-2ubuntu0.1
readline-common:
-   arch: all
    category: utils
    name: readline-common
    origin: Ubuntu
    source: apt
    version: 6.3-8ubuntu2
redis-server:
-   arch: amd64
    category: universe/misc
    name: redis-server
    origin: Ubuntu
    source: apt
    version: 2:3.0.6-1ubuntu0.4
redis-tools:
-   arch: amd64
    category: universe/database
    name: redis-tools
    origin: Ubuntu
    source: apt
    version: 2:3.0.6-1ubuntu0.4
rename:
-   arch: all
    category: perl
    name: rename
    origin: Ubuntu
    source: apt
    version: 0.20-4
resolvconf:
-   arch: all
    category: net
    name: resolvconf
    origin: Ubuntu
    source: apt
    version: 1.78ubuntu7
rsync:
-   arch: amd64
    category: net
    name: rsync
    origin: Ubuntu
    source: apt
    version: 3.1.1-3ubuntu1.3
rsyslog:
-   arch: amd64
    category: admin
    name: rsyslog
    origin: Ubuntu
    source: apt
    version: 8.16.0-1ubuntu3.1
ruby:
-   arch: all
    category: interpreters
    name: ruby
    origin: Ubuntu
    source: apt
    version: 1:2.3.0+1
ruby-dev:
-   arch: amd64
    category: devel
    name: ruby-dev
    origin: Ubuntu
    source: apt
    version: 1:2.3.0+1
ruby-did-you-mean:
-   arch: all
    category: ruby
    name: ruby-did-you-mean
    origin: Ubuntu
    source: apt
    version: 1.0.0-2
ruby-minitest:
-   arch: all
    category: ruby
    name: ruby-minitest
    origin: Ubuntu
    source: apt
    version: 5.8.4-2
ruby-net-telnet:
-   arch: all
    category: ruby
    name: ruby-net-telnet
    origin: Ubuntu
    source: apt
    version: 0.1.1-2
ruby-power-assert:
-   arch: all
    category: ruby
    name: ruby-power-assert
    origin: Ubuntu
    source: apt
    version: 0.2.7-1
ruby-test-unit:
-   arch: all
    category: ruby
    name: ruby-test-unit
    origin: Ubuntu
    source: apt
    version: 3.1.7-2
ruby2.3:
-   arch: amd64
    category: ruby
    name: ruby2.3
    origin: Ubuntu
    source: apt
    version: 2.3.1-2~ubuntu16.04.14
ruby2.3-dev:
-   arch: amd64
    category: ruby
    name: ruby2.3-dev
    origin: Ubuntu
    source: apt
    version: 2.3.1-2~ubuntu16.04.14
rubygems-integration:
-   arch: all
    category: ruby
    name: rubygems-integration
    origin: Ubuntu
    source: apt
    version: '1.10'
s-nail:
-   arch: amd64
    category: universe/mail
    name: s-nail
    origin: Ubuntu
    source: apt
    version: 14.8.6-1
samba-libs:
-   arch: amd64
    category: libs
    name: samba-libs
    origin: Ubuntu
    source: apt
    version: 2:4.3.11+dfsg-0ubuntu0.16.04.27
sed:
-   arch: amd64
    category: utils
    name: sed
    origin: Ubuntu
    source: apt
    version: 4.2.2-7
sensible-utils:
-   arch: all
    category: utils
    name: sensible-utils
    origin: Ubuntu
    source: apt
    version: 0.0.9ubuntu0.16.04.1
sgml-base:
-   arch: all
    category: text
    name: sgml-base
    origin: Ubuntu
    source: apt
    version: 1.26+nmu4ubuntu1
shared-mime-info:
-   arch: amd64
    category: misc
    name: shared-mime-info
    origin: Ubuntu
    source: apt
    version: 1.5-2ubuntu0.2
slay:
-   arch: all
    category: universe/admin
    name: slay
    origin: Ubuntu
    source: apt
    version: 2.7.0
software-properties-common:
-   arch: all
    category: admin
    name: software-properties-common
    origin: Ubuntu
    source: apt
    version: 0.96.20.9
sox:
-   arch: amd64
    category: universe/sound
    name: sox
    origin: Ubuntu
    source: apt
    version: 14.4.1-5+deb8u4ubuntu0.1
ssh-import-id:
-   arch: all
    category: misc
    name: ssh-import-id
    origin: Ubuntu
    source: apt
    version: 5.5-0ubuntu1
ssl-cert:
-   arch: all
    category: utils
    name: ssl-cert
    origin: Ubuntu
    source: apt
    version: 1.0.37
ssl-cert-check:
-   arch: all
    category: universe/net
    name: ssl-cert-check
    origin: Ubuntu
    source: apt
    version: 3.27-2
sudo:
-   arch: amd64
    category: admin
    name: sudo
    origin: Ubuntu
    source: apt
    version: 1.8.16-0ubuntu1.9
systemd:
-   arch: amd64
    category: admin
    name: systemd
    origin: Ubuntu
    source: apt
    version: 229-4ubuntu21.28
systemd-sysv:
-   arch: amd64
    category: admin
    name: systemd-sysv
    origin: Ubuntu
    source: apt
    version: 229-4ubuntu21.28
sysv-rc:
-   arch: all
    category: admin
    name: sysv-rc
    origin: Ubuntu
    source: apt
    version: 2.88dsf-59.3ubuntu2
sysvinit-utils:
-   arch: amd64
    category: admin
    name: sysvinit-utils
    origin: Ubuntu
    source: apt
    version: 2.88dsf-59.3ubuntu2
tar:
-   arch: amd64
    category: utils
    name: tar
    origin: Ubuntu
    source: apt
    version: 1.28-2.1ubuntu0.1
thermald:
-   arch: amd64
    category: admin
    name: thermald
    origin: Ubuntu
    source: apt
    version: 1.5-2ubuntu4
tidy:
-   arch: amd64
    category: universe/web
    name: tidy
    origin: Ubuntu
    source: apt
    version: 20091223cvs-1.5
time:
-   arch: amd64
    category: utils
    name: time
    origin: Ubuntu
    source: apt
    version: 1.7-25.1
tmux:
-   arch: amd64
    category: admin
    name: tmux
    origin: Ubuntu
    source: apt
    version: 2.1-3build1
ttf-mscorefonts-installer:
-   arch: all
    category: multiverse/x11
    name: ttf-mscorefonts-installer
    origin: Ubuntu
    source: apt
    version: 3.4+nmu1ubuntu2
tzdata:
-   arch: all
    category: libs
    name: tzdata
    origin: Ubuntu
    source: apt
    version: 2020a-0ubuntu0.16.04
ubuntu-advantage-tools:
-   arch: all
    category: misc
    name: ubuntu-advantage-tools
    origin: Ubuntu
    source: apt
    version: 10ubuntu0.16.04.1
ubuntu-keyring:
-   arch: all
    category: misc
    name: ubuntu-keyring
    origin: Ubuntu
    source: apt
    version: 2012.05.19
ubuntu-minimal:
-   arch: amd64
    category: metapackages
    name: ubuntu-minimal
    origin: Ubuntu
    source: apt
    version: 1.361.4
ubuntu-mono:
-   arch: all
    category: gnome
    name: ubuntu-mono
    origin: Ubuntu
    source: apt
    version: 14.04+16.04.20180326-0ubuntu1
ubuntu-release-upgrader-core:
-   arch: all
    category: admin
    name: ubuntu-release-upgrader-core
    origin: Ubuntu
    source: apt
    version: 1:16.04.30
ucf:
-   arch: all
    category: utils
    name: ucf
    origin: Ubuntu
    source: apt
    version: '3.0036'
udev:
-   arch: amd64
    category: admin
    name: udev
    origin: Ubuntu
    source: apt
    version: 229-4ubuntu21.28
ufw:
-   arch: all
    category: admin
    name: ufw
    origin: Ubuntu
    source: apt
    version: 0.35-0ubuntu2
unattended-upgrades:
-   arch: all
    category: admin
    name: unattended-upgrades
    origin: Ubuntu
    source: apt
    version: 1.1ubuntu1.18.04.7~16.04.6
uno-libs3:
-   arch: amd64
    category: libs
    name: uno-libs3
    origin: Ubuntu
    source: apt
    version: 5.1.6~rc2-0ubuntu1~xenial10
unzip:
-   arch: amd64
    category: utils
    name: unzip
    origin: Ubuntu
    source: apt
    version: 6.0-20ubuntu1
update-manager-core:
-   arch: all
    category: admin
    name: update-manager-core
    origin: Ubuntu
    source: apt
    version: 1:16.04.17
update-notifier-common:
-   arch: all
    category: gnome
    name: update-notifier-common
    origin: Ubuntu
    source: apt
    version: 3.168.10
ure:
-   arch: amd64
    category: libs
    name: ure
    origin: Ubuntu
    source: apt
    version: 5.1.6~rc2-0ubuntu1~xenial10
ureadahead:
-   arch: amd64
    category: admin
    name: ureadahead
    origin: Ubuntu
    source: apt
    version: 0.100.0-19.1
usbutils:
-   arch: amd64
    category: utils
    name: usbutils
    origin: Ubuntu
    source: apt
    version: 1:007-4
util-linux:
-   arch: amd64
    category: utils
    name: util-linux
    origin: Ubuntu
    source: apt
    version: 2.27.1-6ubuntu3.10
va-driver-all:
-   arch: amd64
    category: universe/video
    name: va-driver-all
    origin: Ubuntu
    source: apt
    version: 1.7.0-1ubuntu0.1
vdpau-driver-all:
-   arch: amd64
    category: video
    name: vdpau-driver-all
    origin: Ubuntu
    source: apt
    version: 1.1.1-3ubuntu1
vim:
-   arch: amd64
    category: editors
    name: vim
    origin: Ubuntu
    source: apt
    version: 2:7.4.1689-3ubuntu1.4
vim-common:
-   arch: amd64
    category: editors
    name: vim-common
    origin: Ubuntu
    source: apt
    version: 2:7.4.1689-3ubuntu1.4
vim-runtime:
-   arch: all
    category: editors
    name: vim-runtime
    origin: Ubuntu
    source: apt
    version: 2:7.4.1689-3ubuntu1.4
vim-tiny:
-   arch: amd64
    category: editors
    name: vim-tiny
    origin: Ubuntu
    source: apt
    version: 2:7.4.1689-3ubuntu1.4
vorbis-tools:
-   arch: amd64
    category: universe/sound
    name: vorbis-tools
    origin: Ubuntu
    source: apt
    version: 1.4.0-7ubuntu1
wget:
-   arch: amd64
    category: web
    name: wget
    origin: Ubuntu
    source: apt
    version: 1.17.1-1ubuntu1.5
whiptail:
-   arch: amd64
    category: utils
    name: whiptail
    origin: Ubuntu
    source: apt
    version: 0.52.18-1ubuntu2
whois:
-   arch: amd64
    category: net
    name: whois
    origin: Ubuntu
    source: apt
    version: 5.2.11
wireless-regdb:
-   arch: all
    category: net
    name: wireless-regdb
    origin: Ubuntu
    source: apt
    version: 2018.05.09-0ubuntu1~16.04.1
x11-common:
-   arch: all
    category: x11
    name: x11-common
    origin: Ubuntu
    source: apt
    version: 1:7.7+13ubuntu3.1
xdg-user-dirs:
-   arch: amd64
    category: utils
    name: xdg-user-dirs
    origin: Ubuntu
    source: apt
    version: 0.15-2ubuntu6.16.04.1
xfonts-encodings:
-   arch: all
    category: x11
    name: xfonts-encodings
    origin: Ubuntu
    source: apt
    version: 1:1.0.4-2
xfonts-utils:
-   arch: amd64
    category: x11
    name: xfonts-utils
    origin: Ubuntu
    source: apt
    version: 1:7.7+3ubuntu0.16.04.2
xkb-data:
-   arch: all
    category: x11
    name: xkb-data
    origin: Ubuntu
    source: apt
    version: 2.16-1ubuntu1
xml-core:
-   arch: all
    category: text
    name: xml-core
    origin: Ubuntu
    source: apt
    version: 0.13+nmu2
xmlstarlet:
-   arch: amd64
    category: universe/text
    name: xmlstarlet
    origin: Ubuntu
    source: apt
    version: 1.6.1-1ubuntu1
xz-utils:
-   arch: amd64
    category: utils
    name: xz-utils
    origin: Ubuntu
    source: apt
    version: 5.1.1alpha+20120614-2ubuntu2
yq:
-   arch: amd64
    category: devel
    name: yq
    origin: LP-PPA-rmescandon-yq
    source: apt
    version: 3.1-2
zip:
-   arch: amd64
    category: utils
    name: zip
    origin: Ubuntu
    source: apt
    version: 3.0-11
zlib1g:
-   arch: amd64
    category: libs
    name: zlib1g
    origin: Ubuntu
    source: apt
    version: 1:1.2.8.dfsg-2ubuntu4.3
zlib1g-dev:
-   arch: amd64
    category: libdevel
    name: zlib1g-dev
    origin: Ubuntu
    source: apt
    version: 1:1.2.8.dfsg-2ubuntu4.3


# vim: set ft=yaml: 
...
```

## lshw

```
meet-coop0
    description: System
    product: Super Server (To be filled by O.E.M.)
    vendor: Supermicro
    version: 0123456789
    serial: 0123456789
    width: 64 bits
    capabilities: smbios-3.2 dmi-3.2 vsyscall32
    configuration: boot=normal chassis=server family=To be filled by O.E.M. sku=To be filled by O.E.M. uuid=004476E7-E0EB-E911-8000-3CECEF40552A
  *-core
       description: Motherboard
       product: X11DPL-i
       vendor: Supermicro
       physical id: 0
       version: 1.10
       serial: WM19AS004675
       slot: To be filled by O.E.M.
     *-firmware
          description: BIOS
          vendor: American Megatrends Inc.
          physical id: 0
          version: 3.2
          date: 12/02/2019
          size: 64KiB
          capacity: 15MiB
          capabilities: pci upgrade shadowing cdboot bootselect socketedrom edd int13floppy1200 int13floppy720 int13floppy2880 int5printscreen int14serial int17printer acpi usb biosbootspecification uefi
     *-memory:0
          description: System Memory
          physical id: 22
          slot: System board or motherboard
        *-bank:0
             description: DIMM Synchronous 2666 MHz (0.4 ns)
             product: HMA82GR7CJR8N-VK
             vendor: SK Hynix
             physical id: 0
             serial: 93A4502A
             slot: P1-DIMMA1
             size: 16GiB
             width: 64 bits
             clock: 2666MHz (0.4ns)
        *-bank:1
             description: DIMM Synchronous 2666 MHz (0.4 ns)
             product: HMA82GR7CJR8N-VK
             vendor: SK Hynix
             physical id: 1
             serial: 93A45045
             slot: P1-DIMMB1
             size: 16GiB
             width: 64 bits
             clock: 2666MHz (0.4ns)
     *-memory:1
          description: System Memory
          physical id: 2a
          slot: System board or motherboard
        *-bank:0
             description: DIMM Synchronous 2666 MHz (0.4 ns)
             product: HMA82GR7CJR8N-VK
             vendor: SK Hynix
             physical id: 0
             serial: 93A4507F
             slot: P1-DIMMD1
             size: 16GiB
             width: 64 bits
             clock: 2666MHz (0.4ns)
        *-bank:1
             description: DIMM [empty]
             product: NO DIMM
             vendor: NO DIMM
             physical id: 1
             serial: NO DIMM
             slot: P1-DIMME1
     *-memory:2
          description: System Memory
          physical id: 32
          slot: System board or motherboard
        *-bank:0
             description: DIMM Synchronous 2666 MHz (0.4 ns)
             product: HMA82GR7CJR8N-VK
             vendor: SK Hynix
             physical id: 0
             serial: 93A45070
             slot: P2-DIMMA1
             size: 16GiB
             width: 64 bits
             clock: 2666MHz (0.4ns)
        *-bank:1
             description: DIMM Synchronous 2666 MHz (0.4 ns)
             product: HMA82GR7CJR8N-VK
             vendor: SK Hynix
             physical id: 1
             serial: 93A45006
             slot: P2-DIMMB1
             size: 16GiB
             width: 64 bits
             clock: 2666MHz (0.4ns)
     *-memory:3
          description: System Memory
          physical id: 3a
          slot: System board or motherboard
        *-bank:0
             description: DIMM Synchronous 2666 MHz (0.4 ns)
             product: HMA82GR7CJR8N-VK
             vendor: SK Hynix
             physical id: 0
             serial: 93A4501D
             slot: P2-DIMMD1
             size: 16GiB
             width: 64 bits
             clock: 2666MHz (0.4ns)
        *-bank:1
             description: DIMM [empty]
             product: NO DIMM
             vendor: NO DIMM
             physical id: 1
             serial: NO DIMM
             slot: P2-DIMME1
     *-cache:0
          description: L1 cache
          physical id: 4d
          slot: L1 Cache
          size: 768KiB
          capacity: 768KiB
          capabilities: synchronous internal write-back instruction
          configuration: level=1
     *-cache:1
          description: L2 cache
          physical id: 4e
          slot: L2 Cache
          size: 12MiB
          capacity: 12MiB
          capabilities: synchronous internal varies unified
          configuration: level=2
     *-cache:2
          description: L3 cache
          physical id: 4f
          slot: L3 Cache
          size: 16MiB
          capacity: 16MiB
          capabilities: synchronous internal varies unified
          configuration: level=3
     *-cpu:0
          description: CPU
          product: Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
          vendor: Intel Corp.
          physical id: 50
          bus info: cpu@0
          version: Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
          slot: CPU1
          size: 2200MHz
          width: 64 bits
          clock: 100MHz
          capabilities: x86-64 fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid dca sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch epb invpcid_single intel_pt ssbd ibrs ibpb stibp ibrs_enhanced tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid cqm mpx avx512f rdseed adx smap clflushopt clwb avx512cd xsaveopt xsavec xgetbv1 cqm_llc cqm_occup_llc cqm_mbm_total cqm_mbm_local dtherm arat pln pts pku md_clear flush_l1d arch_capabilities
          configuration: cores=12 enabledcores=12 threads=24
     *-cache:3
          description: L1 cache
          physical id: 51
          slot: L1 Cache
          size: 768KiB
          capacity: 768KiB
          capabilities: synchronous internal write-back instruction
          configuration: level=1
     *-cache:4
          description: L2 cache
          physical id: 52
          slot: L2 Cache
          size: 12MiB
          capacity: 12MiB
          capabilities: synchronous internal varies unified
          configuration: level=2
     *-cache:5
          description: L3 cache
          physical id: 53
          slot: L3 Cache
          size: 16MiB
          capacity: 16MiB
          capabilities: synchronous internal varies unified
          configuration: level=3
     *-cpu:1
          description: CPU
          product: Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
          vendor: Intel Corp.
          physical id: 54
          bus info: cpu@1
          version: Intel(R) Xeon(R) Silver 4214 CPU @ 2.20GHz
          slot: CPU2
          size: 2200MHz
          width: 64 bits
          clock: 100MHz
          capabilities: x86-64 fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid dca sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch epb invpcid_single intel_pt ssbd ibrs ibpb stibp ibrs_enhanced tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid cqm mpx avx512f rdseed adx smap clflushopt clwb avx512cd xsaveopt xsavec xgetbv1 cqm_llc cqm_occup_llc cqm_mbm_total cqm_mbm_local dtherm arat pln pts pku md_clear flush_l1d arch_capabilities
          configuration: cores=12 enabledcores=12 threads=24
     *-memory:4 UNCLAIMED
          physical id: 1
     *-memory:5 UNCLAIMED
          physical id: 2
     *-pci:0
          description: Host bridge
          product: Sky Lake-E DMI3 Registers
          vendor: Intel Corporation
          physical id: 100
          bus info: pci@0000:00:00.0
          version: 07
          width: 32 bits
          clock: 33MHz
        *-generic:0
             description: System peripheral
             product: Sky Lake-E CBDMA Registers
             vendor: Intel Corporation
             physical id: 4
             bus info: pci@0000:00:04.0
             version: 07
             width: 64 bits
             clock: 33MHz
             capabilities: msix pciexpress pm bus_master cap_list
             configuration: driver=ioatdma latency=0
             resources: iomemory:383f0-383ef irq:157 memory:383ffff1c000-383ffff1ffff
        *-generic:1
             description: System peripheral
             product: Sky Lake-E CBDMA Registers
             vendor: Intel Corporation
             physical id: 4.1
             bus info: pci@0000:00:04.1
             version: 07
             width: 64 bits
             clock: 33MHz
             capabilities: msix pciexpress pm bus_master cap_list
             configuration: driver=ioatdma latency=0
             resources: iomemory:383f0-383ef irq:160 memory:383ffff18000-383ffff1bfff
        *-generic:2
             description: System peripheral
             product: Sky Lake-E CBDMA Registers
             vendor: Intel Corporation
             physical id: 4.2
             bus info: pci@0000:00:04.2
             version: 07
             width: 64 bits
             clock: 33MHz
             capabilities: msix pciexpress pm bus_master cap_list
             configuration: driver=ioatdma latency=0
             resources: iomemory:383f0-383ef irq:157 memory:383ffff14000-383ffff17fff
        *-generic:3
             description: System peripheral
             product: Sky Lake-E CBDMA Registers
             vendor: Intel Corporation
             physical id: 4.3
             bus info: pci@0000:00:04.3
             version: 07
             width: 64 bits
             clock: 33MHz
             capabilities: msix pciexpress pm bus_master cap_list
             configuration: driver=ioatdma latency=0
             resources: iomemory:383f0-383ef irq:160 memory:383ffff10000-383ffff13fff
        *-generic:4
             description: System peripheral
             product: Sky Lake-E CBDMA Registers
             vendor: Intel Corporation
             physical id: 4.4
             bus info: pci@0000:00:04.4
             version: 07
             width: 64 bits
             clock: 33MHz
             capabilities: msix pciexpress pm bus_master cap_list
             configuration: driver=ioatdma latency=0
             resources: iomemory:383f0-383ef irq:157 memory:383ffff0c000-383ffff0ffff
        *-generic:5
             description: System peripheral
             product: Sky Lake-E CBDMA Registers
             vendor: Intel Corporation
             physical id: 4.5
             bus info: pci@0000:00:04.5
             version: 07
             width: 64 bits
             clock: 33MHz
             capabilities: msix pciexpress pm bus_master cap_list
             configuration: driver=ioatdma latency=0
             resources: iomemory:383f0-383ef irq:160 memory:383ffff08000-383ffff0bfff
        *-generic:6
             description: System peripheral
             product: Sky Lake-E CBDMA Registers
             vendor: Intel Corporation
             physical id: 4.6
             bus info: pci@0000:00:04.6
             version: 07
             width: 64 bits
             clock: 33MHz
             capabilities: msix pciexpress pm bus_master cap_list
             configuration: driver=ioatdma latency=0
             resources: iomemory:383f0-383ef irq:157 memory:383ffff04000-383ffff07fff
        *-generic:7
             description: System peripheral
             product: Sky Lake-E CBDMA Registers
             vendor: Intel Corporation
             physical id: 4.7
             bus info: pci@0000:00:04.7
             version: 07
             width: 64 bits
             clock: 33MHz
             capabilities: msix pciexpress pm bus_master cap_list
             configuration: driver=ioatdma latency=0
             resources: iomemory:383f0-383ef irq:160 memory:383ffff00000-383ffff03fff
        *-generic:8 UNCLAIMED
             description: System peripheral
             product: Sky Lake-E MM/Vt-d Configuration Registers
             vendor: Intel Corporation
             physical id: 5
             bus info: pci@0000:00:05.0
             version: 07
             width: 32 bits
             clock: 33MHz
             capabilities: pciexpress cap_list
             configuration: latency=0
        *-generic:9 UNCLAIMED
             description: System peripheral
             product: Intel Corporation
             vendor: Intel Corporation
             physical id: 5.2
             bus info: pci@0000:00:05.2
             version: 07
             width: 32 bits
             clock: 33MHz
             capabilities: pciexpress cap_list
             configuration: latency=0
        *-generic:10 UNCLAIMED
             description: PIC
             product: Intel Corporation
             vendor: Intel Corporation
             physical id: 5.4
             bus info: pci@0000:00:05.4
             version: 07
             width: 32 bits
             clock: 33MHz
             capabilities: pciexpress pm io_x_-apic bus_master cap_list
             configuration: latency=0
             resources: memory:9d21a000-9d21afff
        *-generic:11 UNCLAIMED
             description: System peripheral
             product: Sky Lake-E Ubox Registers
             vendor: Intel Corporation
             physical id: 8
             bus info: pci@0000:00:08.0
             version: 07
             width: 32 bits
             clock: 33MHz
             capabilities: pciexpress cap_list
             configuration: latency=0
        *-generic:12 UNCLAIMED
             description: Performance counters
             product: Sky Lake-E Ubox Registers
             vendor: Intel Corporation
             physical id: 8.1
             bus info: pci@0000:00:08.1
             version: 07
             width: 32 bits
             clock: 33MHz
             configuration: latency=0
        *-generic:13 UNCLAIMED
             description: System peripheral
             product: Sky Lake-E Ubox Registers
             vendor: Intel Corporation
             physical id: 8.2
             bus info: pci@0000:00:08.2
             version: 07
             width: 32 bits
             clock: 33MHz
             capabilities: pciexpress cap_list
             configuration: latency=0
        *-generic:14 UNCLAIMED
             description: Unassigned class
             product: C620 Series Chipset Family MROM 0
             vendor: Intel Corporation
             physical id: 11
             bus info: pci@0000:00:11.0
             version: 09
             width: 32 bits
             clock: 33MHz
             capabilities: pm bus_master cap_list
             configuration: latency=0
        *-generic:15 UNCLAIMED
             description: Unassigned class
             product: C620 Series Chipset Family MROM 1
             vendor: Intel Corporation
             physical id: 11.1
             bus info: pci@0000:00:11.1
             version: 09
             width: 32 bits
             clock: 33MHz
             capabilities: pm bus_master cap_list
             configuration: latency=0
        *-storage:0
             description: SATA controller
             product: C620 Series Chipset Family SSATA Controller [AHCI mode]
             vendor: Intel Corporation
             physical id: 11.5
             bus info: pci@0000:00:11.5
             version: 09
             width: 32 bits
             clock: 66MHz
             capabilities: storage msi pm ahci_1.0 bus_master cap_list
             configuration: driver=ahci latency=0
             resources: irq:30 memory:9d216000-9d217fff memory:9d219000-9d2190ff ioport:800(size=8) ioport:810(size=4) ioport:820(size=32) memory:9d180000-9d1fffff
        *-usb
             description: USB controller
             product: C620 Series Chipset Family USB 3.0 xHCI Controller
             vendor: Intel Corporation
             physical id: 14
             bus info: pci@0000:00:14.0
             version: 09
             width: 64 bits
             clock: 33MHz
             capabilities: pm msi xhci bus_master cap_list
             configuration: driver=xhci_hcd latency=0
             resources: irq:29 memory:9d200000-9d20ffff
           *-usbhost:0
                product: xHCI Host Controller
                vendor: Linux 4.4.0-179-generic xhci-hcd
                physical id: 0
                bus info: usb@2
                logical name: usb2
                version: 4.04
                capabilities: usb-3.00
                configuration: driver=hub slots=10 speed=5000Mbit/s
           *-usbhost:1
                product: xHCI Host Controller
                vendor: Linux 4.4.0-179-generic xhci-hcd
                physical id: 1
                bus info: usb@1
                logical name: usb1
                version: 4.04
                capabilities: usb-2.00
                configuration: driver=hub slots=16 speed=480Mbit/s
              *-usb
                   description: USB hub
                   product: Hub
                   vendor: ATEN International Co., Ltd
                   physical id: 7
                   bus info: usb@1:7
                   version: 0.00
                   capabilities: usb-2.00
                   configuration: driver=hub maxpower=100mA slots=4 speed=480Mbit/s
                 *-usb
                      description: Keyboard
                      vendor: ATEN International Co., Ltd
                      physical id: 1
                      bus info: usb@1:7.1
                      version: 1.00
                      capabilities: usb-1.10
                      configuration: driver=usbhid maxpower=160mA speed=2Mbit/s
        *-generic:16 UNCLAIMED
             description: Signal processing controller
             product: C620 Series Chipset Family Thermal Subsystem
             vendor: Intel Corporation
             physical id: 14.2
             bus info: pci@0000:00:14.2
             version: 09
             width: 64 bits
             clock: 33MHz
             capabilities: pm msi bus_master cap_list
             configuration: latency=0
             resources: iomemory:383f0-383ef memory:383ffff23000-383ffff23fff
        *-communication:0
             description: Communication controller
             product: C620 Series Chipset Family MEI Controller #1
             vendor: Intel Corporation
             physical id: 16
             bus info: pci@0000:00:16.0
             version: 09
             width: 64 bits
             clock: 33MHz
             capabilities: pm msi bus_master cap_list
             configuration: driver=mei_me latency=0
             resources: iomemory:383f0-383ef irq:159 memory:383ffff22000-383ffff22fff
        *-communication:1 UNCLAIMED
             description: Communication controller
             product: C620 Series Chipset Family MEI Controller #2
             vendor: Intel Corporation
             physical id: 16.1
             bus info: pci@0000:00:16.1
             version: 09
             width: 64 bits
             clock: 33MHz
             capabilities: pm msi bus_master cap_list
             configuration: latency=0
             resources: iomemory:383f0-383ef memory:383ffff21000-383ffff21fff
        *-communication:2 UNCLAIMED
             description: Communication controller
             product: C620 Series Chipset Family MEI Controller #3
             vendor: Intel Corporation
             physical id: 16.4
             bus info: pci@0000:00:16.4
             version: 09
             width: 64 bits
             clock: 33MHz
             capabilities: pm msi bus_master cap_list
             configuration: latency=0
             resources: iomemory:383f0-383ef memory:383ffff20000-383ffff20fff
        *-storage:1
             description: SATA controller
             product: C620 Series Chipset Family SATA Controller [AHCI mode]
             vendor: Intel Corporation
             physical id: 17
             bus info: pci@0000:00:17.0
             version: 09
             width: 32 bits
             clock: 66MHz
             capabilities: storage msi pm ahci_1.0 bus_master cap_list
             configuration: driver=ahci latency=0
             resources: irq:31 memory:9d214000-9d215fff memory:9d218000-9d2180ff ioport:840(size=8) ioport:850(size=4) ioport:860(size=32) memory:9d100000-9d17ffff
        *-pci:0
             description: PCI bridge
             product: C620 Series Chipset Family PCI Express Root Port #1
             vendor: Intel Corporation
             physical id: 1c
             bus info: pci@0000:00:1c.0
             version: f9
             width: 32 bits
             clock: 33MHz
             capabilities: pci pciexpress msi pm normal_decode bus_master cap_list
             configuration: driver=pcieport
             resources: irq:24
        *-pci:1
             description: PCI bridge
             product: C620 Series Chipset Family PCI Express Root Port #6
             vendor: Intel Corporation
             physical id: 1c.5
             bus info: pci@0000:00:1c.5
             version: f9
             width: 32 bits
             clock: 33MHz
             capabilities: pci pciexpress msi pm normal_decode bus_master cap_list
             configuration: driver=pcieport
             resources: irq:25 ioport:3000(size=4096) memory:9c000000-9d0fffff
           *-pci
                description: PCI bridge
                product: AST1150 PCI-to-PCI Bridge
                vendor: ASPEED Technology, Inc.
                physical id: 0
                bus info: pci@0000:02:00.0
                version: 04
                width: 32 bits
                clock: 33MHz
                capabilities: pci msi pm pciexpress normal_decode bus_master cap_list
                resources: ioport:3000(size=4096) memory:9c000000-9d0fffff
              *-display
                   description: VGA compatible controller
                   product: ASPEED Graphics Family
                   vendor: ASPEED Technology, Inc.
                   physical id: 0
                   bus info: pci@0000:03:00.0
                   version: 41
                   width: 32 bits
                   clock: 33MHz
                   capabilities: pm msi vga_controller cap_list rom
                   configuration: driver=ast latency=0
                   resources: irq:17 memory:9c000000-9cffffff memory:9d000000-9d01ffff ioport:3000(size=128)
        *-isa
             description: ISA bridge
             product: C621 Series Chipset LPC/eSPI Controller
             vendor: Intel Corporation
             physical id: 1f
             bus info: pci@0000:00:1f.0
             version: 09
             width: 32 bits
             clock: 33MHz
             capabilities: isa bus_master
             configuration: latency=0
        *-memory UNCLAIMED
             description: Memory controller
             product: C620 Series Chipset Family Power Management Controller
             vendor: Intel Corporation
             physical id: 1f.2
             bus info: pci@0000:00:1f.2
             version: 09
             width: 32 bits
             clock: 33MHz (30.3ns)
             capabilities: bus_master
             configuration: latency=0
             resources: memory:9d210000-9d213fff
        *-serial:0 UNCLAIMED
             description: SMBus
             product: C620 Series Chipset Family SMBus
             vendor: Intel Corporation
             physical id: 1f.4
             bus info: pci@0000:00:1f.4
             version: 09
             width: 64 bits
             clock: 33MHz
             configuration: latency=0
             resources: iomemory:38000-37fff memory:380000000000-3800000000ff ioport:780(size=32)
        *-serial:1 UNCLAIMED
             description: Serial bus controller
             product: C620 Series Chipset Family SPI Controller
             vendor: Intel Corporation
             physical id: 1f.5
             bus info: pci@0000:00:1f.5
             version: 09
             width: 32 bits
             clock: 33MHz
             configuration: latency=0
             resources: memory:fe010000-fe010fff
     *-generic:0 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 3
          bus info: pci@0000:17:05.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:1 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E RAS Configuration Registers
          vendor: Intel Corporation
          physical id: 6
          bus info: pci@0000:17:05.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:2 UNCLAIMED
          description: PIC
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 7
          bus info: pci@0000:17:05.4
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress pm io_x_-apic bus_master cap_list
          configuration: latency=0
          resources: memory:aaf00000-aaf00fff
     *-generic:3 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 9
          bus info: pci@0000:17:08.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:4 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: a
          bus info: pci@0000:17:08.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:5 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: b
          bus info: pci@0000:17:08.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:6 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: c
          bus info: pci@0000:17:08.3
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:7 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: d
          bus info: pci@0000:17:08.4
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:8 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: e
          bus info: pci@0000:17:08.5
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:9 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: f
          bus info: pci@0000:17:08.6
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:10 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 10
          bus info: pci@0000:17:08.7
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:11 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 11
          bus info: pci@0000:17:09.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:12 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 12
          bus info: pci@0000:17:09.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:13 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 13
          bus info: pci@0000:17:09.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:14 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 14
          bus info: pci@0000:17:09.3
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:15 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 15
          bus info: pci@0000:17:09.4
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:16 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 16
          bus info: pci@0000:17:09.5
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:17 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 17
          bus info: pci@0000:17:09.6
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:18 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 18
          bus info: pci@0000:17:09.7
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:19 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 19
          bus info: pci@0000:17:0a.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:20 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 1a
          bus info: pci@0000:17:0a.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:21 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 1b
          bus info: pci@0000:17:0e.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:22 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 1c
          bus info: pci@0000:17:0e.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:23 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 1d
          bus info: pci@0000:17:0e.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:24 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 1e
          bus info: pci@0000:17:0e.3
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:25 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 1f
          bus info: pci@0000:17:0e.4
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:26 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 20
          bus info: pci@0000:17:0e.5
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:27 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 21
          bus info: pci@0000:17:0e.6
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:28 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 23
          bus info: pci@0000:17:0e.7
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:29 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 24
          bus info: pci@0000:17:0f.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:30 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 25
          bus info: pci@0000:17:0f.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:31 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 26
          bus info: pci@0000:17:0f.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:32 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 27
          bus info: pci@0000:17:0f.3
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:33 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 28
          bus info: pci@0000:17:0f.4
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:34 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 29
          bus info: pci@0000:17:0f.5
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:35 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 2b
          bus info: pci@0000:17:0f.6
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:36 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 2c
          bus info: pci@0000:17:0f.7
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:37 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 2d
          bus info: pci@0000:17:10.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:38 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 2e
          bus info: pci@0000:17:10.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:39 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 2f
          bus info: pci@0000:17:1d.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:40 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 30
          bus info: pci@0000:17:1d.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:41 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 31
          bus info: pci@0000:17:1d.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:42 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 33
          bus info: pci@0000:17:1d.3
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:43 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: 34
          bus info: pci@0000:17:1e.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:44 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: 35
          bus info: pci@0000:17:1e.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:45 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: 36
          bus info: pci@0000:17:1e.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:46 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: 37
          bus info: pci@0000:17:1e.3
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:47 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: 38
          bus info: pci@0000:17:1e.4
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:48 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: 39
          bus info: pci@0000:17:1e.5
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:49 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: 3b
          bus info: pci@0000:17:1e.6
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:50 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 3c
          bus info: pci@0000:3a:05.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:51 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E RAS Configuration Registers
          vendor: Intel Corporation
          physical id: 3d
          bus info: pci@0000:3a:05.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:52 UNCLAIMED
          description: PIC
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 3e
          bus info: pci@0000:3a:05.4
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress pm io_x_-apic bus_master cap_list
          configuration: latency=0
          resources: memory:b8700000-b8700fff
     *-generic:53 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 3f
          bus info: pci@0000:3a:08.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:54 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 40
          bus info: pci@0000:3a:09.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:55 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 41
          bus info: pci@0000:3a:0a.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:56 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 42
          bus info: pci@0000:3a:0a.1
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:57 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 43
          bus info: pci@0000:3a:0a.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:58 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 44
          bus info: pci@0000:3a:0a.3
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:59 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 45
          bus info: pci@0000:3a:0a.4
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:60 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 46
          bus info: pci@0000:3a:0a.5
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:61 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 47
          bus info: pci@0000:3a:0a.6
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:62 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 48
          bus info: pci@0000:3a:0a.7
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:63 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 49
          bus info: pci@0000:3a:0b.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:64 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 4a
          bus info: pci@0000:3a:0b.1
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:65 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 4b
          bus info: pci@0000:3a:0b.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:66 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 4c
          bus info: pci@0000:3a:0b.3
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:67 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 55
          bus info: pci@0000:3a:0c.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:68 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 56
          bus info: pci@0000:3a:0c.1
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:69 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 57
          bus info: pci@0000:3a:0c.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:70 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 58
          bus info: pci@0000:3a:0c.3
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:71 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 59
          bus info: pci@0000:3a:0c.4
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:72 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 5a
          bus info: pci@0000:3a:0c.5
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:73 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 5b
          bus info: pci@0000:3a:0c.6
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:74 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 5c
          bus info: pci@0000:3a:0c.7
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:75 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 5d
          bus info: pci@0000:3a:0d.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:76 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 5e
          bus info: pci@0000:3a:0d.1
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:77 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 5f
          bus info: pci@0000:3a:0d.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:78 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 60
          bus info: pci@0000:3a:0d.3
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-pci:1
          description: PCI bridge
          product: Sky Lake-E PCI Express Root Port A
          vendor: Intel Corporation
          physical id: 101
          bus info: pci@0000:5d:00.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pci msi pciexpress pm normal_decode bus_master cap_list
          configuration: driver=pcieport
          resources: irq:27 memory:c5c00000-c5efffff ioport:38c000000000(size=67108864)
        *-pci
             description: PCI bridge
             product: Intel Corporation
             vendor: Intel Corporation
             physical id: 0
             bus info: pci@0000:5e:00.0
             version: 09
             width: 64 bits
             clock: 33MHz
             capabilities: pci pciexpress pm normal_decode bus_master cap_list rom
             configuration: driver=pcieport
             resources: irq:28 memory:c5e00000-c5e1ffff memory:c5c00000-c5cfffff memory:c5d00000-c5dfffff ioport:38c000000000(size=67108864)
           *-pci
                description: PCI bridge
                product: Intel Corporation
                vendor: Intel Corporation
                physical id: 3
                bus info: pci@0000:5f:03.0
                version: 09
                width: 32 bits
                clock: 33MHz
                capabilities: pci pciexpress pm normal_decode bus_master cap_list
                configuration: driver=pcieport
                resources: irq:28 memory:c5d00000-c5dfffff ioport:38c000000000(size=67108864)
              *-network:0
                   description: Ethernet interface
                   product: Ethernet Connection X722 for 1GbE
                   vendor: Intel Corporation
                   physical id: 0
                   bus info: pci@0000:60:00.0
                   logical name: eno1
                   version: 09
                   serial: 3c:ec:ef:40:55:2a
                   size: 1Gbit/s
                   capacity: 1Gbit/s
                   width: 64 bits
                   clock: 33MHz
                   capabilities: pm msi msix pciexpress vpd bus_master cap_list rom ethernet physical tp 1000bt-fd autonegotiation
                   configuration: autonegotiation=on broadcast=yes driver=i40e driverversion=1.4.25-k duplex=full firmware=3.31 0x80000cd9 1.1747.0 ip=199.58.82.68 latency=0 link=yes multicast=yes port=twisted pair speed=1Gbit/s
                   resources: iomemory:38c00-38bff iomemory:38c00-38bff irq:32 memory:38c000000000-38c000ffffff memory:38c002800000-38c002807fff memory:c5d00000-c5d7ffff memory:38c002000000-38c0023fffff memory:38c002810000-38c00288ffff
              *-network:1 DISABLED
                   description: Ethernet interface
                   product: Ethernet Connection X722 for 1GbE
                   vendor: Intel Corporation
                   physical id: 0.1
                   bus info: pci@0000:60:00.1
                   logical name: eno2
                   version: 09
                   serial: 3c:ec:ef:40:55:2b
                   size: 1Gbit/s
                   capacity: 1Gbit/s
                   width: 64 bits
                   clock: 33MHz
                   capabilities: pm msi msix pciexpress vpd bus_master cap_list rom ethernet physical tp 1000bt-fd autonegotiation
                   configuration: autonegotiation=on broadcast=yes driver=i40e driverversion=1.4.25-k duplex=full firmware=3.31 0x80000cd9 1.1747.0 latency=0 link=no multicast=yes port=twisted pair speed=1Gbit/s
                   resources: iomemory:38c00-38bff iomemory:38c00-38bff irq:32 memory:38c001000000-38c001ffffff memory:38c002808000-38c00280ffff memory:c5d80000-c5dfffff memory:38c002400000-38c0027fffff memory:38c002890000-38c00290ffff
     *-generic:79 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 61
          bus info: pci@0000:5d:05.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:80 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E RAS Configuration Registers
          vendor: Intel Corporation
          physical id: 62
          bus info: pci@0000:5d:05.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:81 UNCLAIMED
          description: PIC
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 63
          bus info: pci@0000:5d:05.4
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress pm io_x_-apic bus_master cap_list
          configuration: latency=0
          resources: memory:c5f00000-c5f00fff
     *-generic:82 UNCLAIMED
          description: Performance counters
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 64
          bus info: pci@0000:5d:0e.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:83 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 65
          bus info: pci@0000:5d:0e.1
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:84 UNCLAIMED
          description: Performance counters
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 66
          bus info: pci@0000:5d:0f.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:85 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 67
          bus info: pci@0000:5d:0f.1
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:86 UNCLAIMED
          description: Performance counters
          product: Sky Lake-E M3KTI Registers
          vendor: Intel Corporation
          physical id: 68
          bus info: pci@0000:5d:12.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:87 UNCLAIMED
          description: Performance counters
          product: Sky Lake-E M3KTI Registers
          vendor: Intel Corporation
          physical id: 69
          bus info: pci@0000:5d:12.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:88 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E M3KTI Registers
          vendor: Intel Corporation
          physical id: 6a
          bus info: pci@0000:5d:12.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:89 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E M2PCI Registers
          vendor: Intel Corporation
          physical id: 6b
          bus info: pci@0000:5d:15.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:90 UNCLAIMED
          description: Performance counters
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 6c
          bus info: pci@0000:5d:15.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:91 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E M2PCI Registers
          vendor: Intel Corporation
          physical id: 6d
          bus info: pci@0000:5d:16.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:92 UNCLAIMED
          description: Performance counters
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 6e
          bus info: pci@0000:5d:16.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:93 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E M2PCI Registers
          vendor: Intel Corporation
          physical id: 6f
          bus info: pci@0000:5d:16.4
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:94 UNCLAIMED
          description: Performance counters
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 70
          bus info: pci@0000:5d:16.5
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:95
          description: System peripheral
          product: Sky Lake-E CBDMA Registers
          vendor: Intel Corporation
          physical id: 4
          bus info: pci@0000:80:04.0
          version: 07
          width: 64 bits
          clock: 33MHz
          capabilities: msix pciexpress pm bus_master cap_list
          configuration: driver=ioatdma latency=0
          resources: iomemory:393f0-393ef irq:168 memory:393ffff1c000-393ffff1ffff
     *-generic:96
          description: System peripheral
          product: Sky Lake-E CBDMA Registers
          vendor: Intel Corporation
          physical id: 4.1
          bus info: pci@0000:80:04.1
          version: 07
          width: 64 bits
          clock: 33MHz
          capabilities: msix pciexpress pm bus_master cap_list
          configuration: driver=ioatdma latency=0
          resources: iomemory:393f0-393ef irq:170 memory:393ffff18000-393ffff1bfff
     *-generic:97
          description: System peripheral
          product: Sky Lake-E CBDMA Registers
          vendor: Intel Corporation
          physical id: 4.2
          bus info: pci@0000:80:04.2
          version: 07
          width: 64 bits
          clock: 33MHz
          capabilities: msix pciexpress pm bus_master cap_list
          configuration: driver=ioatdma latency=0
          resources: iomemory:393f0-393ef irq:168 memory:393ffff14000-393ffff17fff
     *-generic:98
          description: System peripheral
          product: Sky Lake-E CBDMA Registers
          vendor: Intel Corporation
          physical id: 4.3
          bus info: pci@0000:80:04.3
          version: 07
          width: 64 bits
          clock: 33MHz
          capabilities: msix pciexpress pm bus_master cap_list
          configuration: driver=ioatdma latency=0
          resources: iomemory:393f0-393ef irq:170 memory:393ffff10000-393ffff13fff
     *-generic:99
          description: System peripheral
          product: Sky Lake-E CBDMA Registers
          vendor: Intel Corporation
          physical id: 4.4
          bus info: pci@0000:80:04.4
          version: 07
          width: 64 bits
          clock: 33MHz
          capabilities: msix pciexpress pm bus_master cap_list
          configuration: driver=ioatdma latency=0
          resources: iomemory:393f0-393ef irq:168 memory:393ffff0c000-393ffff0ffff
     *-generic:100
          description: System peripheral
          product: Sky Lake-E CBDMA Registers
          vendor: Intel Corporation
          physical id: 4.5
          bus info: pci@0000:80:04.5
          version: 07
          width: 64 bits
          clock: 33MHz
          capabilities: msix pciexpress pm bus_master cap_list
          configuration: driver=ioatdma latency=0
          resources: iomemory:393f0-393ef irq:170 memory:393ffff08000-393ffff0bfff
     *-generic:101
          description: System peripheral
          product: Sky Lake-E CBDMA Registers
          vendor: Intel Corporation
          physical id: 4.6
          bus info: pci@0000:80:04.6
          version: 07
          width: 64 bits
          clock: 33MHz
          capabilities: msix pciexpress pm bus_master cap_list
          configuration: driver=ioatdma latency=0
          resources: iomemory:393f0-393ef irq:168 memory:393ffff04000-393ffff07fff
     *-generic:102
          description: System peripheral
          product: Sky Lake-E CBDMA Registers
          vendor: Intel Corporation
          physical id: 4.7
          bus info: pci@0000:80:04.7
          version: 07
          width: 64 bits
          clock: 33MHz
          capabilities: msix pciexpress pm bus_master cap_list
          configuration: driver=ioatdma latency=0
          resources: iomemory:393f0-393ef irq:170 memory:393ffff00000-393ffff03fff
     *-generic:103 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E MM/Vt-d Configuration Registers
          vendor: Intel Corporation
          physical id: 71
          bus info: pci@0000:80:05.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:104 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 72
          bus info: pci@0000:80:05.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:105 UNCLAIMED
          description: PIC
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 73
          bus info: pci@0000:80:05.4
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress pm io_x_-apic bus_master cap_list
          configuration: latency=0
          resources: memory:d3700000-d3700fff
     *-generic:106 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E Ubox Registers
          vendor: Intel Corporation
          physical id: 74
          bus info: pci@0000:80:08.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:107 UNCLAIMED
          description: Performance counters
          product: Sky Lake-E Ubox Registers
          vendor: Intel Corporation
          physical id: 75
          bus info: pci@0000:80:08.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:108 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E Ubox Registers
          vendor: Intel Corporation
          physical id: 76
          bus info: pci@0000:80:08.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:109 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 77
          bus info: pci@0000:85:05.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:110 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E RAS Configuration Registers
          vendor: Intel Corporation
          physical id: 78
          bus info: pci@0000:85:05.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:111 UNCLAIMED
          description: PIC
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 79
          bus info: pci@0000:85:05.4
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress pm io_x_-apic bus_master cap_list
          configuration: latency=0
          resources: memory:e0f00000-e0f00fff
     *-generic:112 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 7a
          bus info: pci@0000:85:08.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:113 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 8.1
          bus info: pci@0000:85:08.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:114 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 8.2
          bus info: pci@0000:85:08.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:115 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 7b
          bus info: pci@0000:85:08.3
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:116 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 7c
          bus info: pci@0000:85:08.4
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:117 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 7d
          bus info: pci@0000:85:08.5
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:118 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 7e
          bus info: pci@0000:85:08.6
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:119 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 7f
          bus info: pci@0000:85:08.7
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:120 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 80
          bus info: pci@0000:85:09.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:121 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 81
          bus info: pci@0000:85:09.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:122 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 82
          bus info: pci@0000:85:09.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:123 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 83
          bus info: pci@0000:85:09.3
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:124 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 84
          bus info: pci@0000:85:09.4
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:125 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 85
          bus info: pci@0000:85:09.5
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:126 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 86
          bus info: pci@0000:85:09.6
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:127 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 87
          bus info: pci@0000:85:09.7
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:128 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 88
          bus info: pci@0000:85:0a.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:129 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 89
          bus info: pci@0000:85:0a.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:130 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 8a
          bus info: pci@0000:85:0e.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:131 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 8b
          bus info: pci@0000:85:0e.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:132 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 8c
          bus info: pci@0000:85:0e.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:133 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 8d
          bus info: pci@0000:85:0e.3
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:134 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 8e
          bus info: pci@0000:85:0e.4
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:135 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 8f
          bus info: pci@0000:85:0e.5
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:136 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 90
          bus info: pci@0000:85:0e.6
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:137 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 91
          bus info: pci@0000:85:0e.7
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:138 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 92
          bus info: pci@0000:85:0f.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:139 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 93
          bus info: pci@0000:85:0f.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:140 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 94
          bus info: pci@0000:85:0f.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:141 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 95
          bus info: pci@0000:85:0f.3
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:142 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 96
          bus info: pci@0000:85:0f.4
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:143 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 97
          bus info: pci@0000:85:0f.5
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:144 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 98
          bus info: pci@0000:85:0f.6
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:145 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 99
          bus info: pci@0000:85:0f.7
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:146 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 9a
          bus info: pci@0000:85:10.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:147 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 9b
          bus info: pci@0000:85:10.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:148 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 9c
          bus info: pci@0000:85:1d.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:149 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 9d
          bus info: pci@0000:85:1d.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:150 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 9e
          bus info: pci@0000:85:1d.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:151 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E CHA Registers
          vendor: Intel Corporation
          physical id: 9f
          bus info: pci@0000:85:1d.3
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:152 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: a0
          bus info: pci@0000:85:1e.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:153 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: a1
          bus info: pci@0000:85:1e.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:154 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: a2
          bus info: pci@0000:85:1e.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:155 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: a3
          bus info: pci@0000:85:1e.3
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:156 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: a4
          bus info: pci@0000:85:1e.4
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:157 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: a5
          bus info: pci@0000:85:1e.5
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:158 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E PCU Registers
          vendor: Intel Corporation
          physical id: a6
          bus info: pci@0000:85:1e.6
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:159 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: a7
          bus info: pci@0000:ae:05.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:160 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E RAS Configuration Registers
          vendor: Intel Corporation
          physical id: a8
          bus info: pci@0000:ae:05.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:161 UNCLAIMED
          description: PIC
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: a9
          bus info: pci@0000:ae:05.4
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress pm io_x_-apic bus_master cap_list
          configuration: latency=0
          resources: memory:ee700000-ee700fff
     *-generic:162 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 8
          bus info: pci@0000:ae:08.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:163 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: aa
          bus info: pci@0000:ae:09.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:164 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: ab
          bus info: pci@0000:ae:0a.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:165 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: ac
          bus info: pci@0000:ae:0a.1
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:166 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: ad
          bus info: pci@0000:ae:0a.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:167 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: ae
          bus info: pci@0000:ae:0a.3
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:168 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: af
          bus info: pci@0000:ae:0a.4
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:169 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: b0
          bus info: pci@0000:ae:0a.5
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:170 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: b1
          bus info: pci@0000:ae:0a.6
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:171 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: b2
          bus info: pci@0000:ae:0a.7
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:172 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: b3
          bus info: pci@0000:ae:0b.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:173 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: b4
          bus info: pci@0000:ae:0b.1
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:174 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: b5
          bus info: pci@0000:ae:0b.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:175 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: b6
          bus info: pci@0000:ae:0b.3
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:176 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: b7
          bus info: pci@0000:ae:0c.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:177 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: b8
          bus info: pci@0000:ae:0c.1
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:178 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: b9
          bus info: pci@0000:ae:0c.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:179 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: ba
          bus info: pci@0000:ae:0c.3
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:180 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: bb
          bus info: pci@0000:ae:0c.4
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:181 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: bc
          bus info: pci@0000:ae:0c.5
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:182 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: bd
          bus info: pci@0000:ae:0c.6
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:183 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: be
          bus info: pci@0000:ae:0c.7
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:184 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: bf
          bus info: pci@0000:ae:0d.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:185 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: c0
          bus info: pci@0000:ae:0d.1
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:186 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: c1
          bus info: pci@0000:ae:0d.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:187 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: c2
          bus info: pci@0000:ae:0d.3
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:188 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 5
          bus info: pci@0000:d7:05.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:189 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E RAS Configuration Registers
          vendor: Intel Corporation
          physical id: 5.2
          bus info: pci@0000:d7:05.2
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:190 UNCLAIMED
          description: PIC
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: 5.4
          bus info: pci@0000:d7:05.4
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress pm io_x_-apic bus_master cap_list
          configuration: latency=0
          resources: memory:fbf00000-fbf00fff
     *-generic:191 UNCLAIMED
          description: Performance counters
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: c3
          bus info: pci@0000:d7:0e.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:192 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: c4
          bus info: pci@0000:d7:0e.1
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:193 UNCLAIMED
          description: Performance counters
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: c5
          bus info: pci@0000:d7:0f.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:194 UNCLAIMED
          description: System peripheral
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: c6
          bus info: pci@0000:d7:0f.1
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:195 UNCLAIMED
          description: Performance counters
          product: Sky Lake-E M3KTI Registers
          vendor: Intel Corporation
          physical id: c7
          bus info: pci@0000:d7:12.0
          version: 07
          width: 32 bits
          clock: 33MHz
          capabilities: pciexpress cap_list
          configuration: latency=0
     *-generic:196 UNCLAIMED
          description: Performance counters
          product: Sky Lake-E M3KTI Registers
          vendor: Intel Corporation
          physical id: c8
          bus info: pci@0000:d7:12.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:197 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E M3KTI Registers
          vendor: Intel Corporation
          physical id: c9
          bus info: pci@0000:d7:12.2
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:198 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E M2PCI Registers
          vendor: Intel Corporation
          physical id: ca
          bus info: pci@0000:d7:15.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:199 UNCLAIMED
          description: Performance counters
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: cb
          bus info: pci@0000:d7:15.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:200 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E M2PCI Registers
          vendor: Intel Corporation
          physical id: cc
          bus info: pci@0000:d7:16.0
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:201 UNCLAIMED
          description: Performance counters
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: cd
          bus info: pci@0000:d7:16.1
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:202 UNCLAIMED
          description: System peripheral
          product: Sky Lake-E M2PCI Registers
          vendor: Intel Corporation
          physical id: ce
          bus info: pci@0000:d7:16.4
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-generic:203 UNCLAIMED
          description: Performance counters
          product: Intel Corporation
          vendor: Intel Corporation
          physical id: cf
          bus info: pci@0000:d7:16.5
          version: 07
          width: 32 bits
          clock: 33MHz
          configuration: latency=0
     *-scsi:0
          physical id: d0
          logical name: scsi2
          capabilities: emulated
        *-disk
             description: ATA Disk
             product: Samsung SSD 860
             physical id: 0.0.0
             bus info: scsi@2:0.0.0
             logical name: /dev/sda
             version: 4B6Q
             serial: S596NE0N102381Z
             size: 3726GiB (4TB)
             capabilities: gpt-1.00 partitioned partitioned:gpt
             configuration: ansiversion=5 guid=277ee38f-2fd7-4d07-860f-d0e17806e4bb logicalsectorsize=512 sectorsize=512
           *-volume:0
                description: RAID partition
                vendor: Linux
                physical id: 1
                bus info: scsi@2:0.0.0,1
                logical name: /dev/sda1
                serial: 37b59641-71f9-432b-9f62-7bd744e90604
                capacity: 2047MiB
                capabilities: multi
                configuration: name=primary
           *-volume:1
                description: RAID partition
                vendor: Linux
                physical id: 2
                bus info: scsi@2:0.0.0,2
                logical name: /dev/sda2
                serial: 8b959c0a-4238-40b4-8b35-0f0f6a4412d8
                capacity: 3724GiB
                capabilities: multi
                configuration: name=primary
           *-volume:2
                description: BIOS Boot partition
                vendor: EFI
                physical id: 3
                bus info: scsi@2:0.0.0,3
                logical name: /dev/sda3
                serial: 15c96be2-68c9-451a-abdb-7105e6a119d7
                capacity: 1023KiB
                capabilities: nofs
                configuration: name=primary
     *-scsi:1
          physical id: d1
          logical name: scsi3
          capabilities: emulated
        *-disk
             description: ATA Disk
             product: Samsung SSD 860
             physical id: 0.0.0
             bus info: scsi@3:0.0.0
             logical name: /dev/sdb
             version: 4B6Q
             serial: S596NE0N102388A
             size: 3726GiB (4TB)
             capabilities: gpt-1.00 partitioned partitioned:gpt
             configuration: ansiversion=5 guid=1ff924cd-9c02-4e7a-a9cc-60d17db4dabb logicalsectorsize=512 sectorsize=512
           *-volume:0
                description: RAID partition
                vendor: Linux
                physical id: 1
                bus info: scsi@3:0.0.0,1
                logical name: /dev/sdb1
                serial: 5e47d90d-091b-41f6-9ca2-fbe6893c3ed3
                capacity: 2047MiB
                capabilities: multi
                configuration: name=primary
           *-volume:1
                description: RAID partition
                vendor: Linux
                physical id: 2
                bus info: scsi@3:0.0.0,2
                logical name: /dev/sdb2
                serial: d58493e8-3792-45b3-8e04-03548ba401b4
                capacity: 3724GiB
                capabilities: multi
                configuration: name=primary
           *-volume:2
                description: BIOS Boot partition
                vendor: EFI
                physical id: 3
                bus info: scsi@3:0.0.0,3
                logical name: /dev/sdb3
                serial: 22513870-f494-4241-b485-b8cdad696b3e
                capacity: 1023KiB
                capabilities: nofs
                configuration: name=primary
     *-scsi:2
          physical id: d2
          logical name: scsi4
          capabilities: emulated
        *-disk
             description: ATA Disk
             product: Samsung SSD 860
             physical id: 0.0.0
             bus info: scsi@4:0.0.0
             logical name: /dev/sdc
             version: 1B6Q
             serial: S5G8NE0MB03193J
             size: 953GiB (1024GB)
             capabilities: gpt-1.00 partitioned partitioned:gpt
             configuration: ansiversion=5 guid=1093ef1c-9763-48cb-a8cd-0ddebba9dc8d logicalsectorsize=512 sectorsize=512
           *-volume:0
                description: RAID partition
                vendor: Linux
                physical id: 1
                bus info: scsi@4:0.0.0,1
                logical name: /dev/sdc1
                serial: 2a3df743-c0cb-4075-99a6-83bf501101da
                capacity: 1023MiB
                capabilities: multi
                configuration: name=primary
           *-volume:1
                description: RAID partition
                vendor: Linux
                physical id: 2
                bus info: scsi@4:0.0.0,2
                logical name: /dev/sdc2
                serial: 19e9719b-c3ed-439f-9ae7-4110d2b5f25f
                capacity: 34GiB
                capabilities: multi
                configuration: name=primary
           *-volume:2
                description: RAID partition
                vendor: Linux
                physical id: 3
                bus info: scsi@4:0.0.0,3
                logical name: /dev/sdc3
                serial: 49b79566-7958-41c0-8af1-c88b6efab830
                capacity: 917GiB
                capabilities: multi
                configuration: name=primary
           *-volume:3
                description: BIOS Boot partition
                vendor: EFI
                physical id: 4
                bus info: scsi@4:0.0.0,4
                logical name: /dev/sdc4
                serial: 855d2f6f-28f0-4189-a922-08acb348238d
                capacity: 1023KiB
                capabilities: nofs
                configuration: name=primary
     *-scsi:3
          physical id: d3
          logical name: scsi5
          capabilities: emulated
        *-disk
             description: ATA Disk
             product: Samsung SSD 860
             physical id: 0.0.0
             bus info: scsi@5:0.0.0
             logical name: /dev/sdd
             version: 1B6Q
             serial: S5G8NE0MB03187V
             size: 953GiB (1024GB)
             capabilities: gpt-1.00 partitioned partitioned:gpt
             configuration: ansiversion=5 guid=d7616e12-7df2-4548-a4b8-a64359850daf logicalsectorsize=512 sectorsize=512
           *-volume:0
                description: RAID partition
                vendor: Linux
                physical id: 1
                bus info: scsi@5:0.0.0,1
                logical name: /dev/sdd1
                serial: 3c935497-934c-4ab9-8be2-f364871ff18f
                capacity: 1023MiB
                capabilities: multi
                configuration: name=primary
           *-volume:1
                description: RAID partition
                vendor: Linux
                physical id: 2
                bus info: scsi@5:0.0.0,2
                logical name: /dev/sdd2
                serial: c37e0546-170a-4a82-8439-6fa6772cca1c
                capacity: 34GiB
                capabilities: multi
                configuration: name=primary
           *-volume:2
                description: RAID partition
                vendor: Linux
                physical id: 3
                bus info: scsi@5:0.0.0,3
                logical name: /dev/sdd3
                serial: 322b25fe-fa75-4519-a248-a28aa9556dea
                capacity: 917GiB
                capabilities: multi
                configuration: name=primary
           *-volume:3
                description: BIOS Boot partition
                vendor: EFI
                physical id: 4
                bus info: scsi@5:0.0.0,4
                logical name: /dev/sdd4
                serial: f765c4d7-8572-4770-b224-63bc70a3f9f5
                capacity: 1023KiB
                capabilities: nofs
                configuration: name=primary
  *-power:0 UNCLAIMED
       description: PWS-605P-1H
       product: PWS-605P-1H
       vendor: SUPERMICRO
       physical id: 1
       version: 1.1
       serial: P605A0H19C65466
       capacity: 600mWh
  *-power:1 UNCLAIMED
       description: To Be Filled By O.E.M.
       product: To Be Filled By O.E.M.
       vendor: To Be Filled By O.E.M.
       physical id: 2
       version: To Be Filled By O.E.M.
       serial: To Be Filled By O.E.M.
       capacity: 32768mWh
  *-network:0
       description: Ethernet interface
       physical id: 3
       logical name: veth0e18f13
       serial: e6:9c:ed:1b:7e:ba
       size: 10Gbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=veth driverversion=1.0 duplex=full link=yes multicast=yes port=twisted pair speed=10Gbit/s
  *-network:1
       description: Ethernet interface
       physical id: 4
       logical name: vethc060a5f
       serial: 3a:77:d1:e9:ae:33
       size: 10Gbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=veth driverversion=1.0 duplex=full link=yes multicast=yes port=twisted pair speed=10Gbit/s
```
